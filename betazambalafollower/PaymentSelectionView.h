//
//  PaymentSelectionView.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/20/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentSelectionView : UIViewController





- (IBAction)UPIBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *cardBtn;

@property (weak, nonatomic) IBOutlet UIButton *netBankingBtn;


@property (weak, nonatomic) IBOutlet UIButton *upiBtn;


@property (weak, nonatomic) IBOutlet UIButton *makePaymentBtn;

@property (weak, nonatomic) IBOutlet UIButton *redeemSelectionBtn;


@property (weak, nonatomic) IBOutlet UILabel *totalPoints;
@property (weak, nonatomic) IBOutlet UILabel *redeemablePoints;
@property (weak, nonatomic) IBOutlet UILabel *availablePoints;
@property (weak, nonatomic) IBOutlet UILabel *grandTotal;

@property (weak, nonatomic) IBOutlet UILabel *amountPayable;

@property (weak, nonatomic) IBOutlet UIView *redeemView;

@property (weak, nonatomic) IBOutlet UIView *amountView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectionViewHgt;

@property (weak, nonatomic) IBOutlet UITextField *upiInputTxt;
@property (weak, nonatomic) IBOutlet UILabel *brokerUpi;

@property (weak, nonatomic) IBOutlet UILabel *targetVPALbl;
@property (weak, nonatomic) IBOutlet UILabel *enterVPALbl;

@end
