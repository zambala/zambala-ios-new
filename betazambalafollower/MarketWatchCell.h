//
//  MarketWatchCell.h
//  Statistics
//
//  Created by zenwise technologies on 06/01/17.
//  Copyright © 2017 zenwise technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketWatchCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *LTPLabel;

@property (strong, nonatomic) IBOutlet UILabel *chgLbl;

@property (strong, nonatomic) IBOutlet UILabel *chgPerLbl;


@property (strong, nonatomic) IBOutlet UILabel *companyName;

@property (strong, nonatomic) IBOutlet UIButton *AdvicNavgt;


@end
