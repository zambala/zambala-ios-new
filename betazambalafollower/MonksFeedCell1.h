//
//  MonksFeedCell1.h
//  zambala leader
//
//  Created by zenwise mac 2 on 2/20/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonksFeedCell1 : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLbl1, *timeLbl1, *desLbl1;
@property (strong, nonatomic) IBOutlet UIImageView *imgVideo1;

@end
