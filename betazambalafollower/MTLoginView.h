//
//  MTLoginView.h
//  
//
//  Created by zenwise technologies on 25/05/17.
//
//

#import <UIKit/UIKit.h>

@interface MTLoginView : UIViewController<NSStreamDelegate>
{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    NSInputStream   *inputStream;
    NSOutputStream  *outputStream;
    
    NSMutableArray  *messages;
}

@property (strong, nonatomic) IBOutlet UITextField *userName;

@property (strong, nonatomic) IBOutlet UITextField *passwordTxt;

- (IBAction)onLoginTap:(id)sender;

@end

















