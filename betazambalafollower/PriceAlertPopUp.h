//
//  PriceAlertPopUp.h
//  PriceAlert
//
//  Created by zenwise mac 2 on 12/28/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]


@interface PriceAlertPopUp : UIViewController<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

{
    UIView *expiryDateViewContainer;
    UIDatePicker *expiryDatePicker;
    NSDate *expiryDate;
    
    UIView *strikeViewContainer;
    UIPickerView *strikePicker;
    
    NSArray *priceArray;
    
    NSString *priceStr;

}

@property (weak, nonatomic) IBOutlet UISegmentedControl *segControl;

@property (strong, nonatomic) IBOutlet UIButton *cancelBtn, *equityRadioBtn, *deriRadioBtn, *CurrRadioBtn, *commRadioBtn;

@property (strong, nonatomic) IBOutlet UIButton *expiryDateBtn, *strikePriceBtn;

@property (strong, nonatomic) IBOutlet UITextField *symbolTxtField;

@property (strong, nonatomic) IBOutlet UIButton *futureRadioBtn, *optionRadioBtn, *CERadinBtn, *PERadioBtn;





@end
