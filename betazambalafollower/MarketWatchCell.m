//
//  MarketWatchCell.m
//  Statistics
//
//  Created by zenwise technologies on 06/01/17.
//  Copyright © 2017 zenwise technologies. All rights reserved.
//

#import "MarketWatchCell.h"

@implementation MarketWatchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
