//
//  MTFunds.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 03/01/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "MTFunds.h"

@implementation MTFunds
- (id)init {
    if (self = [super init]) {
        self.SurveillanceReportRequest=189;
        self.SurveillanceReport=188;
        self.stClientID=(short)5512;
        self.stSegmentName=(short)55310;
        self.dbBalance=(short)40023;
        self.dbAdhocBalance=(short)40063;
        self.dbHaircutBalance=(short)40065;
        self.dbCarryBalance=(short)40066;
        self.dbIntraDayBalance=(short)40067;
        self.dbCCEBalance=(short)40068;
        self.dbMMEBalance=(short)40070;
        self.dbCCEUtilize=(short)40069;
        self.dbMMEUtilize=(short)40071;
        self.dbSpanMargin=(short)40056;
        self.dbExposureMargin=(short)40057;
        self.dbPremium=(short)40019;
        self.SurvellianceReportResponse=190;
    }
    return self;
}

+ (id)Mt9 {
    static MTFunds *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

@end
