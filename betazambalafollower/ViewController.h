//
//  ViewController.h
//  Payment
//
//  Created by zenwise mac 2 on 7/17/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIWebViewDelegate>

- (IBAction)backAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property NSString * myString;
@property NSString * payInCheck;
@property NSString * accountIFSC;
@property NSString * accountNo;
@property NSString * segment;
@property NSString * txnAmount;
@property NSString * upiAlias;
@property NSString * bankAccount;
@property NSString * bankName;

@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewLayoutConstraint;

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property NSString * redirectString;
@property NSMutableDictionary * balanceUpdateResponse;
@property NSString * upStoxRedirectURL;
@property NSString * redirectURL;
@property NSMutableDictionary * paymentResponseDictionary;







@end

