//
//  PriceAlertPopUp.m
//  PriceAlert
//
//  Created by zenwise mac 2 on 12/28/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "PriceAlertPopUp.h"
#import "UIViewController+MJPopupViewController.h"
#import "MJPopupBackgroundView.h"
#import "AddAlertPopUp.h"

@interface PriceAlertPopUp ()

@end

@implementation PriceAlertPopUp


@synthesize segControl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    expiryDateViewContainer = [[UIView alloc]init];
    expiryDatePicker = [[UIDatePicker alloc]init];
    
    strikeViewContainer = [[UIView alloc]init];
    strikePicker = [[UIPickerView alloc]init];
    
    strikePicker.delegate = self;
    strikePicker.dataSource = self;
    
    [[segControl.subviews objectAtIndex:0] setTintColor:[UIColor lightGrayColor]];
    [[segControl.subviews objectAtIndex:1] setTintColor:[UIColor lightGrayColor]];
    [[segControl.subviews objectAtIndex:2] setTintColor:[UIColor lightGrayColor]];
    
    _cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(12, 12, 12, 12)];
    
      [_cancelBtn addTarget:self action:@selector(dismissPopupViewControllerWithanimation:) forControlEvents:UIControlEventTouchUpInside];
    
    
    priceArray = [[NSArray alloc]initWithObjects:@"140.50",@"120.00", nil];
    
    
    [self SetRadioBtn];
    
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.symbolTxtField resignFirstResponder];
}

-(void)SetRadioBtn
{
    
    [_equityRadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [_equityRadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    _equityRadioBtn.selected = NO;
    
    [_deriRadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [_deriRadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    _deriRadioBtn.selected = NO;
    
    [_commRadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [_commRadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    _commRadioBtn.selected = NO;
    
    [_CurrRadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [_CurrRadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    _CurrRadioBtn.selected = NO;
    
    [self.futureRadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.futureRadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.futureRadioBtn.selected = NO;
    
    [self.optionRadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.optionRadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.optionRadioBtn.selected = NO;
    
    [self.CERadinBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.CERadinBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.CERadinBtn.selected = NO;
    
    [self.PERadioBtn setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    [self.PERadioBtn setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateSelected];
    self.PERadioBtn.selected = NO;
    

    
    
    [self addBorderToButton:self.expiryDateBtn];
    [self addBorderToButton:self.strikePriceBtn];
    
    [self outFocusTextField];

}

-(void)inFocusTextField
{
    self.symbolTxtField.leftViewMode = UITextFieldViewModeAlways;
    self.symbolTxtField.font=[UIFont fontWithName:@"Helvetica Neue" size:12];
    self.symbolTxtField.textColor = [UIColor blackColor];
    self.symbolTxtField.backgroundColor = [UIColor clearColor];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.symbolTxtField.frame.size.height - 1, self.symbolTxtField.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.symbolTxtField.layer addSublayer:bottomBorder];
}

-(void)outFocusTextField
{
    self.symbolTxtField.leftViewMode = UITextFieldViewModeAlways;
    self.symbolTxtField.font=[UIFont fontWithName:@"Helvetica Neue" size:12];
    self.symbolTxtField.textColor = [UIColor blackColor];
    self.symbolTxtField.backgroundColor = [UIColor clearColor];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.symbolTxtField.frame.size.height - 1, self.symbolTxtField.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [self.symbolTxtField.layer addSublayer:bottomBorder];
}



-(IBAction)equityAction:(id)sender
{
    _equityRadioBtn.selected = YES;
    _deriRadioBtn.selected = NO;
    _commRadioBtn.selected = NO;
    _CurrRadioBtn.selected = NO;
    
}

-(IBAction)deriAction:(id)sender
{
    _equityRadioBtn.selected = NO;
    _deriRadioBtn.selected = YES;
    _commRadioBtn.selected = NO;
    _CurrRadioBtn.selected = NO;
    
}
-(IBAction)currAction:(id)sender
{
    _equityRadioBtn.selected = NO;
    _deriRadioBtn.selected = NO;
    _commRadioBtn.selected = NO;
    _CurrRadioBtn.selected = YES;
    
}
-(IBAction)commAction:(id)sender
{
    _equityRadioBtn.selected = NO;
    _deriRadioBtn.selected = NO;
    _commRadioBtn.selected = YES;
    _CurrRadioBtn.selected = NO;
    
}

-(IBAction)futureAction:(id)sender
{
    _futureRadioBtn.selected = YES;
    _optionRadioBtn.selected = NO;
}

-(IBAction)optionAction:(id)sender
{
    _optionRadioBtn.selected = YES;
    _futureRadioBtn.selected = NO;
    
}
-(IBAction)CEAction:(id)sender
{
    _CERadinBtn.selected = YES;
    _PERadioBtn.selected = NO;
    
}
-(IBAction)PEAction:(id)sender
{
    _CERadinBtn.selected = NO;
    _PERadioBtn.selected = YES;
    

    
}

- (void)addBorderToButton:(UIButton *)button
{
    button.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12];
    [button setTitleColor:RGB(0,0,0) forState:UIControlStateNormal];
    button.backgroundColor = RGB(255,255,255);
    
    CALayer *border = [CALayer layer];
    border.backgroundColor = RGB(192,192,192).CGColor;
    button.backgroundColor = [UIColor clearColor];
    border.frame = CGRectMake(0.0f, button.frame.size.height - 1, button.frame.size.width, 1.0f);
    [button.layer addSublayer:border];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segColorBtnTapped:(id)sender {
    
    if(segControl.selectedSegmentIndex==0)
    {
    }
    else if(segControl.selectedSegmentIndex==1)
    {
    }
    else
    {
    }
}

-(IBAction)dismissPopupViewControllerWithanimation:(id)sender
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField

{
    [self inFocusTextField];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self outFocusTextField];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField;
{
    [self.symbolTxtField becomeFirstResponder];
    
}


-(IBAction)expiryDateAction:(id)sender
{
    [self.view endEditing:YES];
    
    expiryDateViewContainer.frame = CGRectMake(self.expiryDateBtn.frame.origin.x, (self.view.bounds.size.height)-200, self.expiryDateBtn.frame.size.width+50, 150);
    
    
    expiryDatePicker.frame= CGRectMake(0, 44, expiryDateViewContainer.frame.size.width, 150);
    expiryDatePicker.backgroundColor= [UIColor whiteColor];
    expiryDatePicker.datePickerMode = UIDatePickerModeDate;
    expiryDatePicker.hidden=NO;
    
    UIToolbar *controlToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, expiryDateViewContainer.bounds.size.width, 44)];
    [controlToolBar setBarStyle:UIBarStyleDefault];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(expiryDateDoneBtn)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [controlToolBar setItems:[NSArray arrayWithObjects:spacer,cancelButton,nil]animated:NO];
    
    [expiryDateViewContainer addSubview:controlToolBar];
    [expiryDateViewContainer addSubview:expiryDatePicker];
    [self.view addSubview:expiryDateViewContainer];
}

- (void)expiryDateDoneBtn
{
    NSArray *listofViews=[expiryDateViewContainer subviews];
    
    for(UIView *subView in listofViews)
    {
        if([subView isKindOfClass:[UIDatePicker class]])
        {
            expiryDate =[(UIDatePicker *)subView date];
        }
    }
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *_fromTime =[formatter stringFromDate:expiryDate];
    [_expiryDateBtn setTitle:[NSString stringWithFormat:@"%@",_fromTime] forState:UIControlStateNormal];
    [expiryDateViewContainer removeFromSuperview];
    
}

-(IBAction)strikeAction:(id)sender
{
    
    strikeViewContainer.frame = CGRectMake(self.strikePriceBtn.frame.origin.x, (self.view.bounds.size.height)-140, self.strikePriceBtn.frame.size.width, 100);
    
    strikePicker.frame = CGRectMake(0, 44, strikeViewContainer.frame.size.width, 100);
    strikePicker.hidden = NO;
    strikePicker.showsSelectionIndicator = YES;
    strikePicker.layer.borderColor = [UIColor lightGrayColor].CGColor;
    strikePicker.backgroundColor = [UIColor whiteColor];
    strikePicker.layer.borderWidth = 1.0f;
    strikeViewContainer.backgroundColor = [UIColor whiteColor];
    
    UIToolbar *controlToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, strikeViewContainer.bounds.size.width, 44)];
    [controlToolBar setBarStyle:UIBarStyleDefault];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(strikeDoneBtn)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [controlToolBar setItems:[NSArray arrayWithObjects:spacer,cancelButton,nil]animated:NO];
    
    [strikeViewContainer addSubview:controlToolBar];
    [strikeViewContainer addSubview:strikePicker];
    [self.view addSubview:strikeViewContainer];

    
}

-(void)strikeDoneBtn
{
    
    priceStr = [NSString stringWithFormat:@"%@",[priceArray objectAtIndex:[strikePicker selectedRowInComponent:0]]];
    
    [self.strikePriceBtn setTitle:[NSString stringWithFormat:@"%@",priceStr] forState:UIControlStateNormal];
    
    [strikeViewContainer removeFromSuperview];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [priceArray count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [priceArray objectAtIndex:row];
}

-(IBAction)addAlert:(id)sender
{
    AddAlertPopUp *detailViewController = [[AddAlertPopUp alloc] initWithNibName:@"AddAlertPopUp" bundle:nil];
    [self presentPopupViewController:detailViewController animationType: MJPopupViewAnimationSlideTopBottom];
}






@end
