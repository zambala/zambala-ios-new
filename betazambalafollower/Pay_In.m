//
//  Pay_In.m
//  PayPage
//
//  Created by zenwise mac 2 on 12/22/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "Pay_In.h"
#import "FundTransferView.h"


@interface Pay_In ()

@end

@implementation Pay_In

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    bankDataArray=[[NSArray alloc]initWithObjects:@"HDFC Bank", @"Axis Bank", @"SBI",nil];
    accountDataArray=[[NSArray alloc]initWithObjects:@"10024887872323",@"14535262526362",@"27383878273212",nil];
    segmentDataArray = [[NSArray alloc]initWithObjects:@"Equity",@"Derivatives",@"Currency", @"Commodity", nil];
    

    
    bankpickerView=[[UIPickerView alloc]init];
    bankViewContainer=[[UIView alloc]init];
    
    bankpickerView.dataSource=self;
    bankpickerView.delegate=self;
    
    accountPickerView=[[UIPickerView alloc]init];
    accountViewContainer=[[UIView alloc]init];
    
    accountPickerView.dataSource=self;
    accountPickerView.delegate=self;
    
    segmentpickerView=[[UIPickerView alloc]init];
    segmentViewContainer=[[UIView alloc]init];
    
    segmentpickerView.dataSource=self;
    segmentpickerView.delegate=self;
    
    [self addBorderToButton:self.bankBtn];
    [self addBorderToButton:self.accountBtn];
    [self addBorderToButton:self.segmentBtn];
    
    self.payInView1.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.payInView1.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.payInView1.layer.shadowOpacity = 1.0f;
    self.payInView1.layer.shadowRadius = 1.0f;
    self.payInView1.layer.cornerRadius=2.1f;
    self.payInView1.layer.masksToBounds = YES;
    
    self.payInSubmit.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.payInSubmit.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.payInSubmit.layer.shadowOpacity = 1.0f;
    self.payInSubmit.layer.shadowRadius = 1.0f;
    self.payInSubmit.layer.cornerRadius=2.1f;
    self.payInSubmit.layer.masksToBounds = NO;

    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;
    
    
    [self outFocusTextField];
    
}
-(void)hideKeyboard
{
    [self.amountTxtField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addBorderToButton:(UIButton *)button
{
    button.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12];
    [button setTitleColor:RGB(0,0,0) forState:UIControlStateNormal];
    button.backgroundColor = RGB(255,255,255);
    
    CALayer *border = [CALayer layer];
    border.backgroundColor = RGB(192,192,192).CGColor;
    button.backgroundColor = [UIColor clearColor];
    border.frame = CGRectMake(0.0f, button.frame.size.height - 1, button.frame.size.width, 1.0f);
    [button.layer addSublayer:border];
}

-(void)inFocusTextField
{
    self.amountTxtField.leftViewMode = UITextFieldViewModeAlways;
    self.amountTxtField.font=[UIFont fontWithName:@"OpenSans" size:13];
    self.amountTxtField.textColor = [UIColor blackColor];
    self.amountTxtField.backgroundColor = [UIColor clearColor];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.amountTxtField.frame.size.height - 1, self.amountTxtField.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor darkGrayColor].CGColor;
    [self.amountTxtField.layer addSublayer:bottomBorder];
}



-(void)outFocusTextField
{
    self.amountTxtField.leftViewMode = UITextFieldViewModeAlways;
    self.amountTxtField.font=[UIFont fontWithName:@"OpenSans" size:13];
    self.amountTxtField.textColor = [UIColor blackColor];
    self.amountTxtField.backgroundColor = [UIColor clearColor];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.amountTxtField.frame.size.height - 1, self.amountTxtField.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [self.amountTxtField.layer addSublayer:bottomBorder];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField

{
    
    
    [self inFocusTextField];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self outFocusTextField];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField;
{
    [self.amountTxtField becomeFirstResponder];
}


-(IBAction)bankDrpDwnBtn:(id)sender
{
    //    ViewContainer.frame = CGRectMake(self.brokerNameBtn.frame.origin.x, (self.view.bounds.size.height)-420, self.brokerNameBtn.frame.size.width, 100);
    //    pickerView.frame = CGRectMake(0, 44, ViewContainer.frame.size.width, 100);
    
    bankViewContainer.frame=CGRectMake(0, (self.view.bounds.size.height)-260, [[UIScreen mainScreen] bounds].size.width, 260);
    bankpickerView.frame=CGRectMake(0, 44, [[UIScreen mainScreen] bounds].size.width, 216);
    bankpickerView.hidden = NO;
    bankpickerView.showsSelectionIndicator = YES;
    bankpickerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    bankpickerView.layer.borderWidth = 1.0f;
    bankViewContainer.backgroundColor = [UIColor whiteColor];// clearColor for transparent
    
    UIToolbar *controlToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, bankViewContainer.bounds.size.width, 44)];
    [controlToolBar setBarStyle:UIBarStyleDefault];
    
    controlToolBar.backgroundColor = RGB(255, 255, 0);
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *setButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(bankSetBtn)];
    [setButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(bankCancelBtn)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [controlToolBar setItems:[NSArray arrayWithObjects:spacer,setButton,cancelButton,nil]animated:NO];
    
    [bankViewContainer addSubview:controlToolBar];
    [bankViewContainer addSubview:bankpickerView];
    [self.view addSubview:bankViewContainer];
    
    
}
-(void)bankSetBtn
{
    bankDataStr = [NSString stringWithFormat:@"%@",[bankDataArray objectAtIndex:[bankpickerView selectedRowInComponent:0]]];
    
    [self.bankBtn setTitle:[NSString stringWithFormat:@"%@",bankDataStr] forState:UIControlStateNormal];
    
    [bankViewContainer removeFromSuperview];
}
-(void)bankCancelBtn
{
    [bankViewContainer removeFromSuperview];
    
}

-(IBAction)accDrpDwnBtn:(id)sender
{
    //    ViewContainer.frame = CGRectMake(self.brokerNameBtn.frame.origin.x, (self.view.bounds.size.height)-420, self.brokerNameBtn.frame.size.width, 100);
    //    pickerView.frame = CGRectMake(0, 44, ViewContainer.frame.size.width, 100);
    
    accountViewContainer.frame=CGRectMake(0, (self.view.bounds.size.height)-260, [[UIScreen mainScreen] bounds].size.width, 260);
    accountPickerView.frame=CGRectMake(0, 44, [[UIScreen mainScreen] bounds].size.width, 216);
    accountPickerView.hidden = NO;
    accountPickerView.showsSelectionIndicator = YES;
    accountPickerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    accountPickerView.layer.borderWidth = 1.0f;
    accountViewContainer.backgroundColor = [UIColor whiteColor];// clearColor for transparent
    
    UIToolbar *controlToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, accountViewContainer.bounds.size.width, 44)];
    [controlToolBar setBarStyle:UIBarStyleDefault];
    
    controlToolBar.backgroundColor = RGB(255, 255, 0);
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *setButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(accSetBtn)];
    [setButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(accCancelBtn)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [controlToolBar setItems:[NSArray arrayWithObjects:spacer,setButton,cancelButton,nil]animated:NO];
    
    [accountViewContainer addSubview:controlToolBar];
    [accountViewContainer addSubview:accountPickerView];
    [self.view addSubview:accountViewContainer];
    
    
}
-(void)accSetBtn
{
    accountDataStr = [NSString stringWithFormat:@"%@",[accountDataArray objectAtIndex:[accountPickerView selectedRowInComponent:0]]];
    
    [self.accountBtn setTitle:[NSString stringWithFormat:@"%@",accountDataStr] forState:UIControlStateNormal];
    
    [accountViewContainer removeFromSuperview];
}
-(void)accCancelBtn
{
    [accountViewContainer removeFromSuperview];
    
}

-(IBAction)segDrpDwnBtn:(id)sender
{
    //    ViewContainer.frame = CGRectMake(self.brokerNameBtn.frame.origin.x, (self.view.bounds.size.height)-420, self.brokerNameBtn.frame.size.width, 100);
    //    pickerView.frame = CGRectMake(0, 44, ViewContainer.frame.size.width, 100);
    
    segmentViewContainer.frame=CGRectMake(0, (self.view.bounds.size.height)-260, [[UIScreen mainScreen] bounds].size.width, 260);
    segmentpickerView.frame=CGRectMake(0, 44, [[UIScreen mainScreen] bounds].size.width, 216);
    segmentpickerView.hidden = NO;
    segmentpickerView.showsSelectionIndicator = YES;
    segmentpickerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    segmentpickerView.layer.borderWidth = 1.0f;
    segmentViewContainer.backgroundColor = [UIColor whiteColor];// clearColor for transparent
    
    UIToolbar *controlToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, segmentViewContainer.bounds.size.width, 44)];
    [controlToolBar setBarStyle:UIBarStyleDefault];
    
    controlToolBar.backgroundColor = RGB(255, 255, 0);
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *setButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(segSetBtn)];
    [setButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(segCancelBtn)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [controlToolBar setItems:[NSArray arrayWithObjects:spacer,setButton,cancelButton,nil]animated:NO];
    
    [segmentViewContainer addSubview:controlToolBar];
    [segmentViewContainer addSubview:segmentpickerView];
    [self.view addSubview:segmentViewContainer];
    
    
}

-(void)segSetBtn
{
    segmentDataStr = [NSString stringWithFormat:@"%@",[segmentDataArray objectAtIndex:[segmentpickerView selectedRowInComponent:0]]];
    
    [self.segmentBtn setTitle:[NSString stringWithFormat:@"%@",segmentDataStr] forState:UIControlStateNormal];
    
    [segmentViewContainer removeFromSuperview];
}
-(void)segCancelBtn
{
    [segmentViewContainer removeFromSuperview];
    
}



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == bankpickerView)
    {
        return [bankDataArray count];
    }
    else if (pickerView == accountPickerView)
    {
        return [accountDataArray count];
    }
    else if (pickerView == segmentpickerView)
    {
        return [segmentDataArray count];
    }
    
    return false;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == bankpickerView)
    {
        return [bankDataArray objectAtIndex:row];
        
    }
    else if (pickerView == accountPickerView)
    {
        return [accountDataArray objectAtIndex:row];
    }
    else if (pickerView == segmentpickerView)
    {
        return [segmentDataArray objectAtIndex:row];
    }
    
    return false;
}

-(IBAction)proceedBtn:(id)sender
{
    
}





- (IBAction)backBtn:(id)sender {
    
    FundTransferView * fundView=[self.storyboard instantiateViewControllerWithIdentifier:@"fund"];
    
    [self presentViewController:fundView animated:YES completion:nil];

}
@end
