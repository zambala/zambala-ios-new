//
//  TabBarViewController.h
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCFloatingActionButton.h"

@interface TabBarViewController : UIViewController

@property (strong, nonatomic) VCFloatingActionButton *addButton;

@end
