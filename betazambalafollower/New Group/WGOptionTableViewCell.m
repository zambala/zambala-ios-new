//
//  WGOptionTableViewCell.m
//  ZambalaUSA
//
//  Created by guna on 25/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "WGOptionTableViewCell.h"

@implementation WGOptionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
