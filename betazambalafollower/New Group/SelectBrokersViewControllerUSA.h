//
//  SelectBrokersViewControllerUSA.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 06/10/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectBrokersViewControllerUSA : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *stepOneImageView;
@property (weak, nonatomic) IBOutlet UITableView *listofBrokersTableView;

@end
