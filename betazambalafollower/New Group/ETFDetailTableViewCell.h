//
//  ETFDetailTableViewCell.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 26/09/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ETFDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *holdingsTickerLabel;
@property (weak, nonatomic) IBOutlet UILabel *holdingsPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;

@end
