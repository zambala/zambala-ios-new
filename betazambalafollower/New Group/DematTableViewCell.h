//
//  DematTableViewCell.h
//  
//
//  Created by Zenwise Technologies on 28/09/17.
//
//

#import <UIKit/UIKit.h>

@interface DematTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *tickerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *realisedPandL;
@property (weak, nonatomic) IBOutlet UILabel *unRealisedPandL;
@property (weak, nonatomic) IBOutlet UIButton *tradeButton;

@end
