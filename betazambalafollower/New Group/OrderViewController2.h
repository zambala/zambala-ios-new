//
//  OrderViewController2.h
//  ZambalaUSA
//
//  Created by guna on 21/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANStepperView.h"

@interface OrderViewController2 : UIViewController
@property (weak, nonatomic) IBOutlet UIView *orderTypeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marketQTYHeight;
@property (weak, nonatomic) IBOutlet UILabel *marketQTYLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marketStepperHeight;
@property (weak, nonatomic) IBOutlet ANStepperView *marketStepperView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marketPrizeHeight;
@property (weak, nonatomic) IBOutlet UILabel *marketPrizeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marketPrizeButtonHeight;
@property (weak, nonatomic) IBOutlet UIButton *marketPrizeButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *limitQTYHeight;
@property (weak, nonatomic) IBOutlet UILabel *LimitQTYLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *limitStepperHeight;
@property (weak, nonatomic) IBOutlet ANStepperView *limitStepperView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sumbitOrderHeight;
@property (weak, nonatomic) IBOutlet UIButton *marketButtonOutlet;
- (IBAction)marketButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *limitButtonOutlet;
- (IBAction)limitButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *stopButtonOutlet;
@property (weak, nonatomic) IBOutlet UIView *orderMainView;
@property (weak, nonatomic) IBOutlet UIButton *submitButtonOrder;

- (IBAction)stopButtonAction:(id)sender;
@end
