//
//  TreadingStocksTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 26/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TreadingStocksTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *tickerNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *consensusLabel;
@property (strong, nonatomic) IBOutlet UILabel *averagePriceTarget;
@property (strong, nonatomic) IBOutlet UILabel *buyLabel;
@property (strong, nonatomic) IBOutlet UILabel *holdLabel;
@property (strong, nonatomic) IBOutlet UILabel *sellLabel;
@property (strong, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *LTPLabel;
@property (weak, nonatomic) IBOutlet UILabel *changePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *upSidePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *upSideLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
