//
//  EquitiesSellTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 19/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquitiesSellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *sellView;

@end
