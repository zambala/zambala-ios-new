//
//  FeedsViewController.m
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "FeedsViewController.h"
#import "Reachability.h"
//#import <TwitterKit/TwitterKit.h>
@import TwitterKit;


@interface FeedsViewController ()

@end

@implementation FeedsViewController
{
    TWTRTimelineFilter * filter;
    TWTRSearchTimelineDataSource * search;
    TWTRTweetView * tweetView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self twitterTest];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)twitterTest
{
    TWTRAPIClient *APIClient = [[TWTRAPIClient alloc] init];
    self.dataSource = [[TWTRListTimelineDataSource alloc] initWithListSlug:@"ZambalaUSA" listOwnerScreenName:@"myzambala" APIClient:APIClient];
    
    
    
    
//    TWTRAPIClient *client = [[TWTRAPIClient alloc] init];
//
//    NSMutableArray * array1 = [[NSMutableArray alloc]init];
//    array1 = [[TWTRUserTimelineDataSource alloc] initWithScreenName:@"CNBCTV18Live" APIClient:APIClient];
//    NSMutableArray * array2 = [[NSMutableArray alloc]init];
//    array2 = [[TWTRUserTimelineDataSource alloc] initWithScreenName:@"CNBCTV18Live" APIClient:APIClient];
    
   // self.dataSource = [[TWTRUserTimelineDataSource alloc] initWithScreenName:@"ZambalaUSA" APIClient:APIClient];
    
    
    
  //  NSMutableArray * testArray = [[NSMutableArray alloc]initWithObjects:@"CNBCTV18News",@"CNBCTV18Live", nil];
   // NSString * testString = [testArray componentsJoinedByString:@","];
    
   // self.dataSource = [[TWTRUserTimelineDataSource alloc] initWithScreenName:testString APIClient:APIClient];
//
//    search = [[TWTRSearchTimelineDataSource alloc]initWithSearchQuery:@"twitter" APIClient:client];
    // Set the theme directly
    tweetView = [[TWTRTweetView alloc]init];
    tweetView.theme = TWTRTweetViewThemeDark;
    // Use custom colors
    tweetView.primaryTextColor = [UIColor yellowColor];
    tweetView.backgroundColor = [UIColor blueColor];
//    filter = [[TWTRTimelineFilter alloc]init];
//    NSSet * set = [[NSSet alloc]initWithArray:@[@"ETNOWlive", @"BBC" ]];
//    NSSet * hashSet = [[NSSet alloc]initWithArray:@[@"#MarketOpen",@"#OnTheMarket"]];
//    filter.keywords = set;
//    filter.hashtags = hashSet;
//    self.dataSource.timelineFilter = filter;
   // [self.init dataSource];
    
}

#pragma mark - Check network
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
            
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
