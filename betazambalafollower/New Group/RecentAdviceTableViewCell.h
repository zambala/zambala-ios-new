//
//  RecentAdviceTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 18/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentAdviceTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *tickerLabel;
@property (strong, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *adviceType;
@property (strong, nonatomic) IBOutlet UILabel *priceTargetLabel;
@property (strong, nonatomic) IBOutlet UIButton *adviceTypeButton;
@property (weak, nonatomic) IBOutlet UILabel *upsideLabel;
@property (weak, nonatomic) IBOutlet UILabel *ltpLabel;
@property (weak, nonatomic) IBOutlet UILabel *actedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *sharesSoldLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
