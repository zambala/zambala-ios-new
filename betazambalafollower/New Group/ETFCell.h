//
//  ETFCell.h
//  ZambalaUSA
//
//  Created by zenwise mac 2 on 8/24/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ETFCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *tradeButton;
@property (weak, nonatomic) IBOutlet UIImageView *etfImageView;
@property (weak, nonatomic) IBOutlet UILabel *etfCompanyName;
@property (weak, nonatomic) IBOutlet UILabel *etfCompanyDescription;

@end
