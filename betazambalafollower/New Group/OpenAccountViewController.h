//
//  OpenAccountViewController.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 13/11/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenAccountViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *brokerInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *virtualAccountButton;
@property (weak, nonatomic) IBOutlet UILabel *enterEmailLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *enterEmailHeight;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailTFHeight;
@property (weak, nonatomic) IBOutlet UIButton *openAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *alreadyHaveButton;

@end
