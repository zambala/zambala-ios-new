//
//  stockDetailTableViewCell.h
//  
//
//  Created by Zenwise Technologies on 18/09/17.
//
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface stockDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel2;
@property (weak, nonatomic) IBOutlet UILabel *sectorLabel1;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UIButton *followPlusButton;
@property (weak, nonatomic) IBOutlet UILabel *buySellLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceTargetLabel;
@property (weak, nonatomic) IBOutlet UILabel *actionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *actedBylabel;
@property (weak, nonatomic) IBOutlet UILabel *sharesSoldLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;

@end
