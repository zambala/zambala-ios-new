//
//  EquitiesBuyTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 19/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"


@interface EquitiesBuyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *analystName;


@property (weak, nonatomic) IBOutlet UIImageView *analystImg;
@property (weak, nonatomic) IBOutlet UILabel *sectorLbl;

@property (weak, nonatomic) IBOutlet UILabel *tradingSymbol;

@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *transactionTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceTrgetLbl;
@property (weak, nonatomic) IBOutlet UILabel *LtpLbl;

@property (weak, nonatomic) IBOutlet UILabel *percentageLbl;


@property (weak, nonatomic) IBOutlet UILabel *changePercentageLbl;
@property (weak, nonatomic) IBOutlet UILabel *adviceDate;
@property (weak, nonatomic) IBOutlet UILabel *successRateLbl;

@property (weak, nonatomic) IBOutlet UILabel *averageLbl;

@property (weak, nonatomic) IBOutlet UIButton *buySellBtn;

@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;


@end
