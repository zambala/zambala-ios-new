//
//  TradesTableViewCell.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 04/12/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TradesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateAndTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *symbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *buySellLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *pendingQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *filledQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *mtomPandLLabel;

@end
