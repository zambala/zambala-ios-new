//
//  LoginViewController.m
//  ZambalaUSA
//
//  Created by zenwise mac 2 on 9/5/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "TabMenuView.h"
#import "SelectLoginViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "Mixpanel/Mixpanel.h"



@interface LoginViewController ()

@end

@implementation LoginViewController
{
    AppDelegate * delegate;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.activityIndicator.hidden=YES;
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Login"
//                                                                                  message: @"Input username and password"
//                                                                           preferredStyle:UIAlertControllerStyleAlert];
//        UIButton * guestLoginButton = [[UIButton alloc]initWithFrame:CGRectMake(20, 10, 50, 30)];
//        guestLoginButton.titleLabel.text = @"Guest Login";
//        
//        UIButton * clientLoginButton = [[UIButton alloc]initWithFrame:CGRectMake(60, 10, 50, 30)];
//        clientLoginButton.titleLabel.text = @"Client Login";
//        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//            textField.placeholder = @"name";
//            textField.textColor = [UIColor blueColor];
//            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//            textField.borderStyle = UITextBorderStyleNone;
//        }];
//        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//            textField.placeholder = @"password";
//            textField.textColor = [UIColor blueColor];
//            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//            textField.borderStyle = UITextBorderStyleNone;
//            textField.secureTextEntry = YES;
//        }];
//        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            NSArray * textfields = alertController.textFields;
//            UITextField * namefield = textfields[0];
//            UITextField * passwordfiled = textfields[1];
//            NSLog(@"%@:%@",namefield.text,passwordfiled.text);
//            
//        }]];
//        [alertController.view addSubview:guestLoginButton];
//        [alertController.view addSubview:clientLoginButton];
//        [self presentViewController:alertController animated:YES completion:nil];
//        
//       
//         
//    });
//
    
//    SelectLoginViewController *selectLogin = [[SelectLoginViewController alloc] initWithNibName:@"SelectLoginViewController" bundle:nil];
//    [self presentPopupViewController:selectLogin animationType: MJPopupViewAnimationFade];
//
//
//
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
//

    
    // Do any additional setup after loading the view.
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(uslogin:)
                                                 name:@"uslogin" object:nil];
    // To be called when required.
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"uslogin" object:nil];
    
    
    
}


-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dismissKeyboard
{
    [self.clientIDTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
}
-(void)uslogin:(NSNotification *)note
{
    
    [self loginCheckServer];
}

-(void)loginCheckServer
{
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"%@\",\"password\":\"%@\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}",self.clientIDTF.text,self.passwordTF.text];
   // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSString * url = [NSString stringWithFormat:@"%@",delegate.realAccountURL];
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
//    NSDictionary *cookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
//                                      @"domain.com", NSHTTPCookieDomain,
//                                      @"\\", NSHTTPCookiePath,
//                                      @"myCookie", NSHTTPCookieName,
//                                      @"1234", NSHTTPCookieValue,
//                                      nil];
//    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
//    NSArray* cookieArray = [NSArray arrayWithObject:cookie];
//    NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:cookieArray];
//    [request setAllHTTPHeaderFields:headers];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                        int successCheck = [[json objectForKey:@"success"] intValue];
                                                        
                                                        if(successCheck==1)
                                                        {
                                                            delegate.accountCheck=@"real";
                                                            delegate.USAuserID = self.clientIDTF.text;
                                                            
                                                        delegate.sessionID = [json objectForKey:@"session_id"];
                                                            NSLog(@"Session ID:%@",delegate.sessionID);
                                                            Mixpanel *mixpanel = [Mixpanel sharedInstance];
                                                            
                                                            [mixpanel identify:delegate.USAuserID];
                                                                NSString * name = [NSString stringWithFormat:@"%@",[[json objectForKey:@"accounts"]objectForKey:@"name"]];
                                                                [mixpanel.people set:@{@"first_name":name}];
                                                            
                                                            [mixpanel.people set:@{@"brokername":@"ChoiceTrade"}];
                                                            
                                                            
                                                            [self getAccountNumberServer];
                                                        }else if ([[json objectForKey:@"message"]isEqualToString:@"Error: Firm is Invalid."])
                                                        {
                                                            
                                                            [self loginCheckServer];
                                                        }else if (successCheck==0)
                                                        {
                                                            NSString * message = [NSString stringWithFormat:@"%@",[json objectForKey:@"message"]];
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                    UIAlertController * alert = [UIAlertController
                                                                                                 alertControllerWithTitle:@"Error"
                                                                                                 message:message
                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    //Add Buttons
                                                                    
                                                                    UIAlertAction* okButton = [UIAlertAction
                                                                                               actionWithTitle:@"Ok"
                                                                                               style:UIAlertActionStyleDefault
                                                                                               handler:^(UIAlertAction * action) {
                                                                                                   
                                                                                                   self.activityIndicator.hidden=YES;
                                                                                                   [self.activityIndicator stopAnimating];
                                                                                                   //Handle your yes please button action here
                                                                                                   
                                                                                               }];
                                                                    //Add your buttons to alert controller
                                                                    
                                                                    [alert addAction:okButton];
                                                                    
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:nil];
                                                        
                                                            });
                                                        }
                                                    }
                                                }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getAccountNumberServer
{
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetAccountList\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"byCust\":\"0\",\"userID\":\"%@\",\"accountMode\":\"A\"}",delegate.sessionID,delegate.USAuserID];
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                        int successCheck = [[json objectForKey:@"success"] intValue];
                                                        
                                                        if(successCheck==1)
                                                        {
                                                            delegate.accountNumber = [[json objectForKey:@"accounts"] objectForKey:@"accountNo"];
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                if(delegate.accountCheck.length>0)
                                                                {
                                                                  //  if([self.reLoginCheck isEqualToString:@"relogin"])
                                                                  //  {
                                                                        self.activityIndicator.hidden=YES;
                                                                        [self.activityIndicator stopAnimating];
                                                                        TabMenuView * tabbar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
                                                                        [self presentViewController:tabbar animated:YES completion:nil];
                                                                    
                                                                  //  }else
                                                                        
                                                                //    {
                                                                    
                                                                    if(delegate.marketWatchLocalStoreDict.count>0)
                                                                    {
                                                                        [delegate.marketWatchLocalStoreDict removeAllObjects];
                                                                    }
                                                                        self.accountName = [NSString stringWithFormat:@"%@",[[json objectForKey:@"accounts"]objectForKey:@"name"]];
                                                                        delegate.usaId = self.clientIDTF.text;
                                                                        [self createClient];
                                                                TabMenuView * view = [self.storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
                                                                [self presentViewController:view animated:YES completion:nil];
                                                              //      }
                                                                }else
                                                                {
                                                                    UIAlertController * alert = [UIAlertController
                                                                                                 alertControllerWithTitle:@"Error"
                                                                                                 message:@"Error while retriving Account number. Login Again."
                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    //Add Buttons
                                                                    
                                                                    UIAlertAction* okButton = [UIAlertAction
                                                                                               actionWithTitle:@"Ok"
                                                                                               style:UIAlertActionStyleDefault
                                                                                               handler:^(UIAlertAction * action) {
                                                                                                   //Handle your yes please button action here
                                                                                                   
                                                                                               }];
                                                                    //Add your buttons to alert controller
                                                                    
                                                                    [alert addAction:okButton];
                                                                    
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:nil];
                                                                }
                                                            });
                                                        }
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            self.activityIndicator.hidden=YES;
                                                            [self.activityIndicator stopAnimating];
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

-(void)createClient
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * firstName;
    
    if(self.accountName.length>0)
    {
        firstName = self.accountName;
    }else
    {
        firstName = @"";
    }

    NSString * userID = [NSString stringWithFormat:@"%@",self.clientIDTF.text];
    
    NSString * brokerID = [NSString stringWithFormat:@"%@",[delegate.brokerInfoDict objectForKey:@"brokerid"]];

        @try {
            NSDictionary *headers = @{ @"content-type": @"application/json",
                                       @"cache-control": @"no-cache",
                                       };
            NSDictionary *parameters = @{ @"clientmemberid":userID,
                                          @"referralcode":delegate.referralCode,
                                          @"brokerid":brokerID,
                                          @"email":@"",
                                          @"firstname":firstName,
                                          @"lastname":@"",
                                          @"notificationinfo": @{ @"deviceid": delegate.currentDeviceId, @"devicetoken":[prefs objectForKey:@"devicetoken"],
                                                                  @"devicetype":@1
                                                                  }
                                          };

            NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

            NSString * requestStr=[NSString stringWithFormat:@"%@follower/client",delegate.baseUrl];

            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestStr]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"POST"];
            [request setAllHTTPHeaderFields:headers];
            [request setHTTPBody:postData];

            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                NSLog(@"%@", httpResponse);

                                                                self.clientResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                NSLog(@"Client info:%@",self.clientResponseDictionary);
                                                            }

                                                            dispatch_async(dispatch_get_main_queue(), ^{

                                                                NSString * meesage=[NSString stringWithFormat:@"%@",[self.clientResponseDictionary objectForKey:@"message"]];
                                                                
                                                                NSLog(@"Message:%@",meesage);
                                                               
                                                                delegate.usaId=self.clientIDTF.text;

                                                               

                                                             

                                                            });

                                                        }];
            [dataTask resume];

        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onContinueButtonTap:(id)sender {
    if(self.clientIDTF.text.length==0||self.passwordTF.text.length==0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Warning"
                                         message:@"Missing Fields!"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                           
                                       }];
            //Add your buttons to alert controller
            
            [alert addAction:okButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
        });
        
    }else
    {
        [self loginCheckServer];
    }
    
}
@end
