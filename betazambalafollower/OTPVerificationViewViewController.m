//
//  OTPVerificationViewViewController.m
//  testing
//
//  Created by zenwise technologies on 23/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "OTPVerificationViewViewController.h"
#import "TradingAccountView.h"

@interface OTPVerificationViewViewController ()

@end

@implementation OTPVerificationViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard
{
    [self.OTPText resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)verifyAction:(id)sender {
    
    if(self.OTPText.text.length>0)
    {
        TradingAccountView * trading=[self.storyboard instantiateViewControllerWithIdentifier:@"TradingAccountView"];
        [self.navigationController pushViewController:trading animated:YES];
    }
    else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter OTP" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        

    }
}

- (IBAction)resendAction:(id)sender {
}
@end
