//
//  MonksFeedCell2.h
//  zambala leader
//
//  Created by zenwise mac 2 on 2/20/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonksFeedCell2 : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *nameLbl2, *timeLbl2, *desLbl2;
@property (strong, nonatomic) IBOutlet UIImageView *imgView2;


@end
