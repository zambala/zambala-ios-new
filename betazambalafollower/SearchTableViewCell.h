//
//  SearchTableViewCell.h
//  testing
//
//  Created by zenwise technologies on 27/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *tradingSymbol;


@end
