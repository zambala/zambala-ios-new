//
//  OffersCollectionCell.h
//  testing
//
//  Created by zenwise mac 2 on 1/24/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersCollectionCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgView;


@end
