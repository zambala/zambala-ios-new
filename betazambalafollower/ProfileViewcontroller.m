//
//  ProfileViewcontroller.m
//  testing
//
//  Created by zenwise technologies on 20/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "ProfileViewcontroller.h"
#import "AdviceCell.h"

@interface ProfileViewcontroller ()

@end

@implementation ProfileViewcontroller

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.headerImageViewHeight.constant = 250;
    [self adjustContentViewHeight];
    self.contentViewHeight.constant = [UIScreen mainScreen].bounds.size.height;
    self.adviceTableView.delegate=self;
    self.adviceTableView.dataSource=self;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
        //        self.brokerMesssagesTableView.hidden=YES;
        //        self.notoficationstableView.hidden=NO;
    
    NSString * reuse=@"que";
    AdviceCell * cell1=[tableView dequeueReusableCellWithIdentifier:reuse];
    NSArray *cellArray=[[NSBundle mainBundle]loadNibNamed:@"AdviceCell" owner:self options:nil];
    cell1=[cellArray objectAtIndex:0];
    
        return cell1;
   
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 128;
}


@end
