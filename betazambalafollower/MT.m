//
//  MT.m
//  
//
//  Created by zenwise technologies on 27/05/17.
//
//

#import "MT.h"

@implementation MT




- (id)init {
    if (self = [super init]) {
        
        self.shPacketSize = 4096;
         self.shHeaderSize = 10;
        
        
         self.shHash = '#';
        
        // Data Length
         self.shCharSize = 1;
         self.shBoolSize = 1;
        self.shShortSize=2;

        self.shIntSize = 4;
         self.shLongSize = 8;
         self.shDoubleSize = 8;
         self.shFloatSize = 4;
         self.shDecimalSize = 16;
        
        // Compress Type
         self.shCompressNone = 0;
         self.shCompressZLib = 1;
         self.shCompressCrypt = 2;
        
        //2FA
        
        self.stSingleSecurityAnswer = (short)55314;
        self.SecurityQueAndAns = 202;
        self.bl2FA = 5045;
        self.blEnable = 5004;
        self.blLock = 5019;
        self.inSecurityQuestionNo = 20102;
        self.inVersion = 20005;
        
        
        
        
        
        // Tag Code 1 - 5000 - Byte
         self.btDataType = 1;
         self.btWalletType = 2;
        
         self.btSide = 101;
         self.btOrderType = 102;
        
         self.btTimeinForce = 104;
        
         self.btTerminal = 106;
         self.btOrderSituation = 107;
         self.btOrderStatus = 108;
        
        // Tag Code 5001 - 10000 - Bool
         self.blLoginCkeck = 5001;
         self.blSuspend = 5002;
         self.blCancelOnLogout = 5011;
        //         blValidate = 5003;
        
        // Tag Code 10001 - 20000 - 
         self.shDataSize = 10001;
         self.shMsgCode = 10002;
         self.shPos = 10004;
         self.shPosCurrent = 10005;
         self.shNoofRows = 10006;
         self.shMasterType = 10007;
         self.shErrorCode = 11111;
         self.shRecordNo = 20027;
        
        // Tag Code 20001 - 30000 - Int
         self.inMasterID = 20007;
         self.inATINTradeNo = 20008;
         self.inATINOrderNo = 20009;
         self.inExchangeClientOrderNo = 20010;
         self.inOrderNo = 20011;
         self.inOrderQty = 20012;
         self.inProductType  = 20084;
         self.inPendingQty  = 20013;
         self.inExecuteQty = 20014;
         self.inDiscloseQry = 20015;
         self.inID = 20027;
         self.inBuyQty = 20028;
         self.inSellQty = 20029;
         self.inNetQty = 20030;
         self.inLTQ = 20078;
        self.inRefID = 20052;
        self.inBroadCastID = 20092;
        
        
        // Tag Code 30001 - 35000 - Long
         self.lnWalletID = 30001;
         self.lnPayeeID = 30002;
         self.lnTransactionID = 30003;
         self.lnAmount = 30004;
         self.lnBalance = 30005;
         self.lnExpense = 30006;
         self.lnWalletBufferKey = 30007;
        
        
        // Tag Code 50001 - 55000 - DateTime
         self.dtSystemTime = (short) 50001;
         self.dtTransactionTime = (short) 50002;
         self.dtExpireTime = (short) 50003;
         self.dtExpiryDate = (short) 50004;
         self.dtModifiedDate = (short) 50005;
         self.dtDateTime = (short) 50006;
         self.dtLicenseEndDate = (short) 50007;
         self.dtLicenseStartDate = (short) 50008;
        
        // Tag Code 40001 - 45000 - Double
        
         self.dbTickSize = (short) 40001;
         self.dbPrice = (short) 40002;
         self.dbTriggerPrice = (short) 40003;
         self.dbStrikePrice = (short) 40004;
         self.dbUpperband = (short) 40005;
         self.dbLowerband = (short) 40006;
         self.dbDivider = (short) 40007;
         self.dbMultiplier = (short) 40008;
         self.dbBuyAmount = (short) 40011;
         self.dbSellAmount = (short) 40012;
         self.dbBuyAvg = (short) 40013;
         self.dbSellAvg = (short) 40014;
         self.dbNetAmount = (short) 40015;
         self.dbNetAvg = (short) 40016;
         self.dbMTM = (short) 40017;
         self.dbLTP = (short) 40018;
         self.dbPrevClose = (short) 40036;
        
        
        // Tag Code 55001 - 60000 - String
         self.stUserID = (short) 55001;
         self.stPassword = (short) 55002;
         self.stNewPassword = (short) 55003;
         self.stHostAddress = (short) 55004;
         self.stSenderCompID = (short) 55005;
         self.stSenderSubID = (short) 55006;
         self.stTargetComID = (short) 55007;
         self.stTargetSubID = (short) 55008;
         self.stDataServerHost = (short) 55009;
         self.stExchange = (short) 55010;
         self.stDealerID = (short) 55011;
         self.stCtclID = (short) 55012;
         self.stManagerID = (short) 55018;
         self.stTradeNo = (short) 55101;
         self.stExchangeOrderNo = (short) 55100;
         self.stClientID = (short) 55102;
         self.stText = (short) 55110;
         self.stSymbol = (short) 55103;
         self.stSecurityID = (short) 55104;
         self.stSecurityType = (short) 55105;
         self.stOptionType = (short) 55106;
         self.stSecurityDesc = (short) 55107;
        self. stRefText = (short) 55109;
        
         self.stErrorText = (short) 55111;
         self.stFileDescription = (short) 55112;
         self.stOrderTime = (short) 55116;
         self.stExchangeOrderTime = (short) 55117;
         self.stTradeTime = (short) 55118;
         self.stTerminalInfo = (short) 55124;
        
         self.stCurrency = (short) 55128;
        self. stMemberID = (short) 55131;
         self.stSettleCurrency = (short) 55133;
         self.stTraderID = (short) 55134;
        
         self.stExpiryDate = (short) 55143;
         self.stParameter = (short) 55145;
        self. stApplicationName = (short) 55113;
        self. stSeries = (short) 55154;
        self. stDigit = (short) 55169;
        
        // Tag Code 60001 - 65000 - Binary & Other
        self. obBinaryData = (short) 60001;
        
         self.nlTagFooter = (short) 65535;
        
        
        
        
        
         self.PreLogin = 100;
        self. Login = 101;
        self. Logout = 102;
        
        self. Gateway = 103;
         self.GatewayRequest = 104;
         self.GatewayResponse = 105;
        
        self. GatewayStart = 106;
        self. GatewayStop = 107;
        self. GatewayStatus = 108;
        
        self. GatewayParameter = 109;
        
        self. UserMapRequest = 110;
        self. UserMap = 111;
        self. UserMapResponse = 112;
        
         self.UserMaster = 113;
        self. UserMasterRequest = 114;
        self. UserMasterResponse = 115;
        
         self.UserOrderID = 116;
        
         self.ServerMasterRequest = 123;
        self. ServerMaster = 124;
        self. ServerMasterResponse = 125;
        
        // File Details Message Code //
        
         self.Application_Update_Request = 2000;
        self. Application_Update_Response = 2001;
        self. Application_Delete_Request = 2002;
        
        // BroadCast Code //
        
        self. Broadcast = 5000;
        self. Watch = 5001;
         self.BroadCastPrice = 5002;
         self.IndexWatch = 5003;
         self.SecurityRequest = 5004;
         self.SecurityResponse = 5005;
        self.broadCastId=5001;
        
        // Application //
        
        self. Application_List = 6000;
         self.Application_Desc_Start = 6001;
         self.Application_Desc = 6002;
         self.Application_Desc_End = 6003;
         self.Application_Desc_Request = 6004;
         self.Application_Data_Start = 6005;
         self.Application_Data = 6006;
         self.Application_Data_End = 6007;
         self.Application_Error = 6008;
        
        // Master //
        
         self.MasterRequest = 7000;
         self.Master = 7001;
        self. MasterResponse = 7002;
        
        //Order //
        self. OrderDownloadRequest = 8000;
        self. OrderDownload = 8001;
        self. OrderDownloadResponce = 8002;
        self. TradeDownloadRequest = 8110;
        self. TradeDownloadResponce = 8112;
         self.TradeDownload = 8111;
         self.Order = 9000;
        self.cancelOrder = 9003;
         self.Trade = 9101;
        self. NetPositionHistry = 9108;
        self. NetPositionHistryRequest = 9109;
        self. NetPositionHistryResponce = 9110;
        
        
        self. SocketNotify = 501;
        self. modifyOrder = 9002;
        
        
        //Holdings
        
        self.holdingRequest=186;
        self.blDelete=5005;
        self.inRecordNo=20004;
        self.stClientIDHolding=(short)55102;
        self.stSymbolHolding=(short)55103;
        self.stSeriesHolding=(short)55154;
        self.inNetQtyHolding=20030;
        self.blIsCollateral=5047;
        self.InEvoluationMethod=20105;
        self.inCollateralQty=20106;
        self.InHairCut=20107;
        self.inCollateralUpdateQty=20108;
        self.InHoldingUpdateQty=20109;
        self.stProduct=(short)55190;
        self.stSecurityIDHolding=(short)55104;
        self.dbClosingPrice=(short)40064;
        self.inWithheldHoldingQty=20110;
        self.intWithheldCollateralQty=20111;
        self.holdingsResponse = 185;
        self.holdingsDownloadResponse = 187;

        
    }
    return self;
}

    
        
  



+ (id)Mt1 {
    static MT *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}





@end
