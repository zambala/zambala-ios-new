 //
//  NewLoginViewController.m
//  
//
//  Created by zenwise mac 2 on 8/28/17.
//
//

#import "NewLoginViewController.h"
#import "AppDelegate.h"
#import "MT.h"
#import "TagDecode.h"
#import "TagEncode.h"
#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <Endian.h>
#import "TabBar.h"
#import "MTORDER.h"
#import "StockOrderView.h"
#import "MTTrade.h"
#import "DepthWatch.h"
#import "BroadTagDecode.h"
#import "MTNetPosition.h"
#import "SixQuestionsViewController.h"
#import "TwoAnsViewController.h"
#import "TabBar.h"
#import "IndexWatch.h"
#import "MTFunds.h"
#import "BrokerViewNavigation.h"
#import "BrokerView.h"
@interface NewLoginViewController ()<UITextFieldDelegate>
{
    AppDelegate * delegate1;
    NSString * ipAdderess;
    int portNo;
    NSThread * internalThread;
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    CFReadStreamRef readStream1;
    CFWriteStreamRef writeStream1;
    
//    NSInputStream   *inputStream;
//    NSOutputStream  *outputStream;
    
    NSMutableArray  *messages;
    NSMutableData * data1;
    NSMutableData * data2;
    BOOL exchangeTokenCheck;
    BOOL placedOrderBool;
    
    MT *sharedManager;
    BOOL loginTest;
    
    MTORDER * sharedManagerOrder;
   
//    MTORDER *sharedManagerOrder;
//    MTMaster *sharedManagerMaster;
    
    
    NSMutableArray * btPendingBuffer;
    
    NSMutableArray * broadbtPendingBuffer;
    
    uint8_t * buffer1;
    TagDecode * tagDecode;
    
    BOOL blWrite_;
    BOOL blWrite1_;
    NSMutableArray * dataBuffer;
    NSMutableArray * broadDataBuffer;
    NSInteger len;
    BOOL check;
    NSNumber *bytesRead;
    TagEncode * tag1;
    BroadTagDecode * broadTag;
    IndexWatch * sharedManagerIndexWatch;
    MTFunds * sharedManagerFunds;
    short shTag1;
    short broadshTag1;

    short shDataType;
    short shDataSize;
    
    short broadshDataType;
    short broadshDataSize;
    NSMutableArray * testBtOutBuffer;
    
    BOOL blDataType;
    BOOL blDataSize;
    
    BOOL broadblDataType;
    BOOL broadblDataSize;
    
    short shPendingBufferSize1;
    int broadshPendingBufferSize1;
    
    NSInteger len1;
    NSData *data123;
    short shStart1;
    int broadshStart1;
    
    NSUInteger count;
    
    NSString * swithStr;
    
    NSString * broadswithStr;
    
    BOOL byteCheck;
    
    
    NSUInteger byteIndex;
    
    
    NSMutableData *data;
    NSMutableData *data3;
    int modifyRefferanceOrderNo;
    int broadcastPort;
    
    BOOL broadStreamCheck;
    
    BOOL broadMessageCheck;
    
    ///new//
    
    NSDictionary * loginReplyDict;
    
    

}

@end

@implementation NewLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.txtFld.text=@"";
    
     UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
     [self.view addGestureRecognizer:tap];
    
    self.clientIDTF.delegate=self;
    self.passwordTF.delegate=self;
     self.txtFld.delegate=self;
    self.passwordLbl.hidden=YES;
     self.txtFld.hidden=YES;
    self.view1.hidden=YES;
    
//   [self.view addGestureRecognizer:tap];
    
    if([[[delegate1.brokerInfoDict objectForKey:@"connectioninfo"] objectForKey:@"data"]count]>0)
    {
        
        ipAdderess=[[[[delegate1.brokerInfoDict objectForKey:@"connectioninfo"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"ip"];
        
        broadcastPort=[[[[[delegate1.brokerInfoDict objectForKey:@"connectioninfo"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"port"] intValue];
        
        portNo=[[delegate1.brokerInfoDict objectForKey:@"port"] intValue];
        
    }
    
    else
    {
        ipAdderess=@"103.69.91.237";
                portNo=15007;
                broadcastPort=15008;
    }
    
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.title =delegate1.brokerNameStr;
    self.navigationItem.leftBarButtonItem.title=@" ";
    
    
    delegate1.messageCheck=true;
    
    byteCheck=true;
    count=0;
    data1 = [[NSMutableData alloc]init];
    
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    data1=[[NSMutableData alloc]init];
    
    tagDecode = [[TagDecode alloc]init];
    broadTag = [[BroadTagDecode alloc]init];
    
    
    sharedManager = [MT Mt1];
    sharedManagerOrder = [MTORDER Mt2];
    sharedManagerIndexWatch = [IndexWatch Mt6];
    sharedManagerFunds = [MTFunds Mt9];
//    sharedManagerMaster = [MTMaster Mt3];
    tag1 = [[TagEncode alloc]init];
    
    
    
    bytesRead=0;
    
    check= true;
    
    if([delegate1.loginActivityStr isEqualToString:@"OTP1"])
    {
        ipAdderess=@"103.69.91.237";
        portNo=15007;
        broadcastPort=15008;
        
//        [self broadCastMethod];
        
//         [self performSelector:@selector(broadCastMethod) withObject:self afterDelay:0.0f];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
        dispatch_async(queue, ^{
            [self broadCastMethod];
        });
        
        
    }
    
   
    
    
   

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onContinueButtonTap:(id)sender {
    
    if(self.clientIDTF.text>0&&self.passwordTF.text>0)
    {
        loginTest=true;
        [self serverMethod];
       // [self newMtLogin];
        
    }
    
    
    
}

-(void)newMtLogin
{
    TagEncode * enocde=[[TagEncode alloc]init];
    loginReplyDict=[[NSDictionary alloc]init];
    enocde.inputRequestString=@"prelogin";
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    
    [inputArray addObject:self.clientIDTF.text];
    [inputArray addObject:self.passwordTF.text];
    [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/prelogin",delegate1.baseUrl]];
    
    [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
       
        loginReplyDict=dict;
        
        NSString * msg=[[[loginReplyDict objectForKey:@"data"] objectForKey:@"questions"]objectForKey:@"msg"];
        delegate1.questionArray=[[NSMutableArray alloc]init];
        NSArray * array=[[loginReplyDict objectForKey:@"data"] objectForKey:@"questions"];
        if(array.count>0)
        {
        for(int i=0;i<array.count;i++)
        {
            int j=(int)[array objectAtIndex:i];
        [delegate1.questionArray addObject:[NSNumber numberWithInt:j]];
        }
        
        }
        
        if([msg containsString:@"Login Successful"])
        {
            if(delegate1.questionArray.count==2)
            {
                TwoAnsViewController * two=[self.storyboard instantiateViewControllerWithIdentifier:@"TwoAnsViewController"];
                
                [self.navigationController pushViewController:two animated:YES];
                
            
                
            }
            
           else if(delegate1.questionArray.count==6)
            {
                SixQuestionsViewController * six=[self.storyboard instantiateViewControllerWithIdentifier:@"SixQuestionsViewController"];
                
                [self.navigationController pushViewController:six animated:YES];
                
            }
            
            else
                
            {
                TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
                
                [self presentViewController:tabPage animated:YES completion:nil];
                
            }
        }
    
    }];
    
    
    
    
   
}




-(void)viewWillAppear:(BOOL)animated
{
    if([delegate1.loginActivityStr isEqualToString:@"OTP1"])
    {
   
    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
    
    [self presentViewController:tabPage animated:YES completion:nil];
    
    }
}

-(void)serverMethod
{
    delegate1.messageCheck=true;
    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (__bridge CFStringRef)ipAdderess,portNo,&readStream, &writeStream);
    
    
    
    delegate1.inputStream = (__bridge NSInputStream *)readStream;
    delegate1.outputStream = (__bridge NSOutputStream *)writeStream;
    
    [delegate1.outputStream setDelegate:self];
    [delegate1.inputStream setDelegate:self];
    
   
    NSMethodSignature *sgn = [self methodSignatureForSelector:@selector(runLoopMethod:)];
    NSInvocation *inv = [NSInvocation invocationWithMethodSignature:sgn];
    [inv setTarget: self];
    [inv setSelector:@selector(runLoopMethod:)];
    
    NSTimer *t = [NSTimer timerWithTimeInterval: 1.0
                                     invocation:inv
                                        repeats:YES];

    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer: t forMode: NSDefaultRunLoopMode];
   
    
    [delegate1.inputStream scheduleInRunLoop:runner forMode:NSDefaultRunLoopMode];
    [delegate1.outputStream scheduleInRunLoop:runner forMode:NSDefaultRunLoopMode];
    [delegate1.inputStream open];
    [delegate1.outputStream open];
    
    [self open];

}

-(void)runLoopMethod:(NSTimer *)timer
{
    
}

-(void)broadCastMethod
{
    
    
    broadMessageCheck=true;
    
    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (__bridge CFStringRef)ipAdderess,broadcastPort,&readStream, &writeStream);
    
    [self runLoops];
   
    
}

-(void)runLoops
{
    
   
   
    
    delegate1.broadCastInputStream = (__bridge NSInputStream *)readStream;
    delegate1.broadCastOutputstream = (__bridge NSOutputStream *)writeStream;
    
    
    [delegate1.broadCastInputStream setDelegate:self];
    
//    if ([delegate1.broadCastInputStream  streamStatus] == NSStreamStatusOpen) {
//    
//    [delegate1.broadCastInputStream close];
//    [delegate1.broadCastInputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
//    
//    delegate1.broadCastInputStream = nil;
//        
//    }
//
//    [delegate1.broadCastInputStream close];
//    [delegate1.broadCastOutputstream close];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(queue, ^ {
        [delegate1.broadCastInputStream scheduleInRunLoop:[NSRunLoop currentRunLoop]
                                                  forMode:NSDefaultRunLoopMode];
        [delegate1.broadCastInputStream open];

        // here: start the loop
        @try {
            [[NSRunLoop currentRunLoop] run];

        } @catch (NSException *exception) {

        } @finally {

        }

        // note: all code below this line won't be executed, because the above method NEVER returns.
    });


//
   // [delegate1.broadCastOutputstream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
//    NSMethodSignature *sgn = [self methodSignatureForSelector:@selector(runLoopMethod:)];
//    NSInvocation *inv = [NSInvocation invocationWithMethodSignature:sgn];
//    [inv setTarget: self];
//    [inv setSelector:@selector(runLoopMethod:)];
//
//    NSTimer *t = [NSTimer timerWithTimeInterval: 1.0
//                                     invocation:inv
//                                        repeats:YES];
//
//
//    NSRunLoop *runner = [NSRunLoop currentRunLoop];
//    [runner addTimer: t forMode: NSDefaultRunLoopMode];
//
    
   
    [delegate1.broadCastOutputstream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    [delegate1.broadCastOutputstream open];
//
    
//    NSMethodSignature *sgn = [self methodSignatureForSelector:@selector(runLoopMethod:)];
//    NSInvocation *inv = [NSInvocation invocationWithMethodSignature:sgn];
//    [inv setTarget: self];
//    [inv setSelector:@selector(runLoopMethod:)];
//
//    NSTimer *t = [NSTimer timerWithTimeInterval: 1.0
//                                     invocation:inv
//                                        repeats:YES];
//
//
//    NSRunLoop *runner = [NSRunLoop currentRunLoop];
//    [runner addTimer: t forMode: NSDefaultRunLoopMode];
//
//
////    [delegate1.broadCastInputStream scheduleInRunLoop:runner forMode:NSDefaultRunLoopMode];
//    [delegate1.broadCastOutputstream scheduleInRunLoop:runner forMode:NSDefaultRunLoopMode];
////    [delegate1.broadCastInputStream open];
//    [delegate1.broadCastOutputstream open];
    
    
    
    delegate1.marketWatch1=[[NSMutableArray alloc]init];
    
    
    
    
  
    
    
//    if(broadStreamCheck==true)
//    {
//        broadStreamCheck=false;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"ordertowatch" object:nil];
//    }
    

}

- (void)open {
    
//    [self broadCastMethod];
//     [self performSelector:@selector(broadCastMethod) withObject:self afterDelay:0.0f];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
                                dispatch_async(queue, ^{
                                    [self broadCastMethod];
                                });
    
    NSLog(@"Opening streams.");
    
    NSLog(@"connected");
    
    delegate1.mtCheck=true;
    
    
    delegate1.mtClientId=self.clientIDTF.text;
    delegate1.mtPassword=self.passwordTF.text;
    
    //PreLogin
    
    [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.PreLogin];
    [tag1 TagData:sharedManager.stUserID stringMethod:self.clientIDTF.text];
    [tag1 TagData:sharedManager.stPassword stringMethod:self.passwordTF.text];
    [tag1 TagData:sharedManager.stNewPassword stringMethod:self.txtFld.text];
    // [tag1 TagData:sharedManager.btTerminal byteMethod:m];
    [tag1 TagData:sharedManager.inVersion intMethod:2];
    [tag1 TagData:sharedManager.btTerminal byteMethod:3];
    
    [tag1 GetBuffer];
    [self newMessage];
    
}

-(void)newMessage
{
//    delegate1.outputStream = (__bridge NSOutputStream *)writeStream;
//    
//    [delegate1.outputStream setDelegate:self];
//   
//    [delegate1.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
//    
//    
//    [delegate1.outputStream open];
//    
    
      Byte UUID[delegate1.btOutBuffer.count];
    
    for (int i = 0; i < delegate1.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate1.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp;
        [scanner scanHexInt:&temp];
        NSLog(@"%d",temp);
        
        UUID[i]=temp;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate1.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate1.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    [delegate1.outputStream write:bytes maxLength:delegate1.btOutBuffer.count];
    
}
-(void)newMessage1
{
    //    delegate1.outputStream = (__bridge NSOutputStream *)writeStream;
    //
    //    [delegate1.outputStream setDelegate:self];
    //
    //    [delegate1.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    //
    //
    //    [delegate1.outputStream open];
    //
    
    Byte UUID[delegate1.btOutBuffer.count];
    
    for (int i = 0; i < delegate1.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate1.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp;
        [scanner scanHexInt:&temp];
        NSLog(@"%d",temp);
        
        UUID[i]=temp;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate1.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate1.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    [delegate1.broadCastOutputstream write:bytes maxLength:delegate1.btOutBuffer.count];
    
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    NSLog(@"stream event %lu", streamEvent);
    
    @try
    {
    
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            NSLog(@"Stream opened");
            NSLog(@"connected");
            
            
            
            break;
        case NSStreamEventHasBytesAvailable:
            
            
            if (theStream == delegate1.broadCastInputStream)
            {
                
                uint8_t buffer[sharedManager.shPacketSize];
                
                 len1=0;
                
                
                
                NSLog(@"%s",buffer);
                while ([delegate1.broadCastInputStream hasBytesAvailable])
                {
                    
                    len1=[(NSInputStream *)delegate1.broadCastInputStream read:buffer maxLength:sharedManager.shPacketSize];
                    
                    NSLog(@"%ld",len1);
                    
                    if(len1) {
                        @try {
                            
                            broadDataBuffer =[[NSMutableArray alloc]initWithCapacity:sharedManager.shPacketSize];
                            
                            for(int i=0;i<sharedManager.shPacketSize;i++)
                            {
                                
                                [broadDataBuffer insertObject:@0 atIndex:i];
                            }
                            
                           
                            
                            data3=[[NSMutableData alloc]init];
                            
                            
                            [data3 appendBytes:(const void *)buffer length:len1];
                    
                            
                            NSLog(@"%@",data3);
                            
                            //
                            
                            for(int i=0;i<data3.length;i++)
                            {
                                id first = [data3 subdataWithRange:NSMakeRange(i,1)];
                                
                                NSLog(@"%@",first);
                                
                                int16_t firstInt = CFSwapInt16LittleToHost(*(int16_t*)([first bytes]));
                                [broadDataBuffer replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:firstInt]];
                                
                            }
                            NSLog(@"%lu",(unsigned long)count);
                           
//                            [self performSelector:@selector(BufferProcess1) withObject:self afterDelay:1.0f];
                            
//                            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
//                            dispatch_async(queue, ^{
//                                [self BufferProcess1];
//                            });
                            [self BufferProcess1:true];
                            
                           
                            
                            
                            
                        } @catch (NSException *exception) {
                            NSLog(@"Caught Exception");
                        } @finally {
                            NSLog(@"Finally");
                        }
                    } else {
                        NSLog(@"no buffer!");
                    }
                    
                    
                }
            }
            
            else if(theStream == delegate1.inputStream)
            {
                uint8_t buffer[sharedManager.shPacketSize];
                
                
                len=0;
                
                NSLog(@"%s",buffer);
                while ([delegate1.inputStream hasBytesAvailable])
                {
                    
                    len=[(NSInputStream *)delegate1.inputStream read:buffer maxLength:sharedManager.shPacketSize];
                    
                    NSLog(@"%ld",len);
                    
                    if(len) {
                        @try {
                            
                            dataBuffer =[[NSMutableArray alloc]initWithCapacity:sharedManager.shPacketSize];
                            
                            for(int i=0;i<sharedManager.shPacketSize;i++)
                            {
                                
                                [dataBuffer insertObject:@0 atIndex:i];
                            }
                            
                            
                            
                            data1=[[NSMutableData alloc]init];
                            
                            
                            [data1 appendBytes:(const void *)buffer length:len];
                            
                            
                            NSLog(@"%@",data1);
                            
                            //
                            
                            for(int i=0;i<data1.length;i++)
                            {
                                id first = [data1 subdataWithRange:NSMakeRange(i,1)];
                                
                                NSLog(@"%@",first);
                                
                                int16_t firstInt = CFSwapInt16LittleToHost(*(int16_t*)([first bytes]));
                                [dataBuffer replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:firstInt]];
                                
                            }
                            NSLog(@"%lu",(unsigned long)count);
                            
                            
                            
                            [self BufferProcess:true];
                            
                            
                            
                            
                            
                        } @catch (NSException *exception) {
                            NSLog(@"Caught Exception");
                        } @finally {
                            NSLog(@"Finally");
                        }
                    } else {
                        NSLog(@"no buffer!");
                    }
                    
                    
                }

                
            }
            break;
            
        case NSStreamEventHasSpaceAvailable:
            NSLog(@"Stream has space available now");
            
            if(delegate1.messageCheck==true)
                
            {
                delegate1.messageCheck=false;
                [self newMessage];
                //                [self broadCastRequest];
                
                
                //    uint8_t *readBytes = (uint8_t *)[newData bytes];
                
               
                
                
            }
            
            if(broadMessageCheck==true)
            {
                broadMessageCheck=false;
//                [self newMessage1];
                
            }
            
           
            break;
            
        case NSStreamEventErrorOccurred:
            NSLog(@"%@",[theStream streamError].localizedDescription);
            
            if (theStream == delegate1.broadCastInputStream)
            {
//                broadStreamCheck=true;
//                [self broadCastMethod];
                [delegate1.broadCastInputStream close];
                [delegate1.broadCastOutputstream close];
            }else  if (theStream == delegate1.broadCastOutputstream)
            {
                //                broadStreamCheck=true;
                //                [self broadCastMethod];
                [delegate1.broadCastInputStream close];
                [delegate1.broadCastOutputstream close];
            }
           else  if (theStream == delegate1.inputStream)
            {
                [delegate1.inputStream close];
                [delegate1.outputStream close];
                [delegate1.broadCastInputStream close];
                [delegate1.broadCastOutputstream close];
                delegate1.expiryBool = true;
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
               
                
                
                
            }
            
           else  if (theStream == delegate1.outputStream)
           {
               [delegate1.inputStream close];
               [delegate1.outputStream close];
               [delegate1.broadCastInputStream close];
               [delegate1.broadCastOutputstream close];
               delegate1.expiryBool = true;
               [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
               
           }
            
            break;
            
        case NSStreamEventEndEncountered:
            
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            NSLog(@"Disconnected");
            NSLog(@"close stream");
//            if (theStream == delegate1.broadCastInputStream)
//            {
//                broadStreamCheck=true;
//                [self broadCastMethod];
//            }
            
             if (theStream == delegate1.inputStream)
            {
               
                [delegate1.inputStream close];
                [delegate1.outputStream close];
                [delegate1.broadCastInputStream close];
                [delegate1.broadCastOutputstream close];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
            }
            
            
           else if (theStream == delegate1.outputStream)
            {
                
                [delegate1.inputStream close];
                [delegate1.outputStream close];
                [delegate1.broadCastInputStream close];
                [delegate1.broadCastOutputstream close];
                
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
            }
            
            break;
        default:
            NSLog(@"Unknown event");
    }
        
    }
    
@catch (NSException *exception) {
    NSLog(@"Caught Exception");
} @finally {
    NSLog(@"Finally");
}
    
}


-(void)BufferProcess:(BOOL)blwrite
{
    @try
    {
    NSLog(@"ShortMaxValue:%d",SHRT_MAX);
    btPendingBuffer = [[NSMutableArray alloc]initWithCapacity:SHRT_MAX];
    for(int i=0;i<SHRT_MAX;i++)
    {
        [btPendingBuffer insertObject:@0 atIndex:i];
    }
    blwrite = blWrite_;
    
    NSLog(@"Data Buffer1:%@",dataBuffer);
    NSLog(@"Len:%lu",len);
    [self Process:dataBuffer intMethod:len];
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
    
}

-(void)BufferProcess1:(BOOL)blwrite1
{
   
    @try
    {
    NSLog(@"ShortMaxValue:%d",SHRT_MAX);
    broadbtPendingBuffer = [[NSMutableArray alloc]initWithCapacity:32767];
    for(int i=0;i<32767;i++)
    {
        [broadbtPendingBuffer insertObject:@0 atIndex:i];
    }
    blWrite1_=blwrite1;
    
    if(broadDataBuffer.count>0)
    {
    [self Process1:broadDataBuffer intMethod:len1];
    }
    
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}

-(void)Process:(NSMutableArray *)btBufferData intMethod:(NSInteger)shLen
{
    @try
    {
    // NSLog(@"Pending Buffer:%@",btPendingBuffer);
    shPendingBufferSize1 = 0;
    
    for (int i=0; i<shLen; i++) {
        [btPendingBuffer replaceObjectAtIndex:shPendingBufferSize1 withObject:[btBufferData objectAtIndex:i]];
        
        //        [btPendingBuffer insertObject:[btBufferData objectAtIndex:i] atIndex:shPendingBufferSize1];
        shPendingBufferSize1 = shPendingBufferSize1 +1;
        
    }
    
    shStart1 = 0;
    
    [self firstWhilemethod];
    
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
    
}

-(void)Process1:(NSMutableArray *)btBufferData intMethod:(NSInteger)shLen
{
    @try
    
    {
    // NSLog(@"Pending Buffer:%@",btPendingBuffer);
    broadshPendingBufferSize1 = 0;
    
    for (int i=0; i<shLen; i++) {
        [broadbtPendingBuffer replaceObjectAtIndex:broadshPendingBufferSize1 withObject:[btBufferData objectAtIndex:i]];
        
        //        [btPendingBuffer insertObject:[btBufferData objectAtIndex:i] atIndex:shPendingBufferSize1];
        broadshPendingBufferSize1 = broadshPendingBufferSize1 +1;
        
    }
    
    broadshStart1 = 0;
    
    [self firstWhilemethod1];
    
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
    
}

-(void)updateUIThread:(NSMutableArray *)btBufferDeCode boolMethod:(BOOL)blWrite
{
    @try
    
    {
    //NSMutableArray * btBufferDeCode_ = [[NSMutableArray alloc]initWithObjects:btBufferDeCode, nil];
    
    NSMutableArray * btBufferDeCode_ = [[NSMutableArray alloc]initWithArray:btBufferDeCode];
    blWrite_ = blWrite;
    
    [tagDecode TagDecode:btBufferDeCode_];
    
    NSLog(@"%@",tagDecode);
    
    
    
    short shTag, shMsgCode = 0;
    shTag = [tagDecode NextTag];
    if(shTag == sharedManager.shMsgCode && [tagDecode CheckCapacity:sharedManager.shShortSize])
    {
        shMsgCode = [tagDecode TagShort];
    }
    
    NSLog(@"First Socket shMsgCode:%hd",shMsgCode);
    if (shMsgCode != 0)
    {
        NSString * check1 = @"check";
        
        
        if(shMsgCode==sharedManager.Login)
        {
            NSLog(@"Socket message code value:%hd",sharedManager.Login);
            NSLog(@"tagdecode:%@",tagDecode);
            [self LoginReply];
        }
        
        else
        {
            check1 = @"YES";
        }
        if(shMsgCode == sharedManagerFunds.SurveillanceReport)
        {
            [self fundsDecode];
        }
        
        else
        {
            check1 = @"YES";
        }
        if(shMsgCode == sharedManagerFunds.SurvellianceReportResponse)
        {
            
        }
        else
        {
            check1 = @"YES";
        }
        
        if(shMsgCode==sharedManager.Logout)
        {
//            NSLog(@"Socket message code value:%hd",sharedManager.Login);
//            NSLog(@"tagdecode:%@",tagDecode);
//            [self LoginReply];

            [delegate1.outputStream close];
            [delegate1.broadCastOutputstream close];


            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];



        }

        else
        {
            check1 = @"YES";
            
            
        }
        
        if(shMsgCode==sharedManager.holdingsDownloadResponse)
        {
           
            if(delegate1.mtHoldingArray.count>0)
            {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"holdingsuccess" object:nil];
                
                
            }
        }
        
        else
        {
            check1 = @"YES";
            
            
        }
//
        if(shMsgCode == sharedManager.PreLogin)
        {
            NSLog(@"Prelogin Response!!");
            [self PreLoginReply];
        }
        else
        {
            check1 = @"YES";
        }
        
        if(shMsgCode==sharedManager.TradeDownload)
        {
            NSLog(@"Socket message code value:%hd",sharedManager.Login);
            NSLog(@"tagdecode:%@",tagDecode);
            
        }
        
        else
        {
            check1 = @"YES";
        }
        
        if(shMsgCode == sharedManager.holdingsResponse)
        {
            
            [self holdingsDecode];
            
        }
        
        else
        {
            check1 = @"YES";
        }
        
        
        if(shMsgCode==sharedManager.NetPositionHistry)
        {
            NSLog(@"Socket message code value:%hd",sharedManager.Login);
            NSLog(@"tagdecode:%@",tagDecode);
            [self netPositonsDecode];
        }
        
        else
        {
            check1 = @"YES";
        }
        
        if(shMsgCode == sharedManager.OrderDownload)
        {
            NSLog(@"Socket OrderDownload value:%hd",sharedManager.OrderDownload);
            //            OrderDecode(mTagDeCode);
            
            [self OrderDecode];
            
        }
        
        else
        {
            check1 = @"YES";
        }
        
        if(shMsgCode == sharedManager.TradeDownload)
        {
            NSLog(@"Socket OrderDownload value:%hd",sharedManager.OrderDownload);
            //            OrderDecode(mTagDeCode);
            
            [self TradeDecode];
            
        }
        
        else
        {
            check1 = @"YES";
        }
        
        if(shMsgCode == sharedManager.OrderDownloadResponce)
        {
//            NSLog(@"Socket OrderDownloadResponse value:%hd",sharedManager.OrderDownloadResponce);
//            [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.TradeDownloadRequest];
            // [self open];
            
            if(delegate1.allOrderHistory.count>0||delegate1.allTradeArray.count>0)
            {
                
                //        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete1" object:nil];
                
                
                
                
                if(delegate1.mainorderCheck==true)
                {
                    
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete" object:nil];
                }
                
                else
                    
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"trade" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"positions" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ordersuccess" object:nil];
                }
                
            }
        }
        
        else
        {
            check1 = @"YES";
        }
        
        if(shMsgCode == sharedManager.Order)
        {
            //OrderDecode(mTagDeCode);
            placedOrderBool=true;
            [self OrderDecode];
            
            
        }
        
        
        
        else
        {
            check1 = @"YES";
        }
        
//        if(shMsgCode == sharedManager.Broadcast)
//        {
//            //OrderDecode(mTagDeCode);
////            [self BroadcastIDDecode];
//            
//            [self broadDepth];
//            
//        }
//        
//        
//        
//        else
//        {
//            check1 = @"YES";
//        }
        if([check1 isEqualToString:@"YES"])
        {
            //            [self Process:dataBuffer intMethod:len];
        }
        
        
        
    }
        
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}


-(void)LoginReply
{
    @try
    {
    NSLog(@"Login Reply Method");
    NSString *stText = @"";
    NSString *stDataServerHost = @"";
    NSString *stApplicationName = @"";
    NSString *stParameter = @"";
    NSString *stDealerID = @"";
    NSString *stExchanges= @"";
    
    BOOL blLoginCkeck = false;
    short shTerminal;
    int orderno=0;
    short shTag;
    while ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
        shTag = [tagDecode NextTag];
        if(shTag == sharedManager.blLoginCkeck)
        {
            if ([tagDecode CheckCapacity:sharedManager.shLongSize]) {
                blLoginCkeck = [tagDecode TagBool];
                if(blLoginCkeck==true)
                {
                    delegate1.userID=[self.clientIDTF.text uppercaseString];
                    delegate1.userName=delegate1.userID;
                    [delegate1.stdealerId addObject:delegate1.userID];
                    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
                    
                    [self presentViewController:tabPage animated:YES completion:nil];
                }
            }
        }
        if(shTag == sharedManager.inOrderNo)
        {
            if([tagDecode CheckCapacity:sharedManager.shLongSize])
            {
                orderno = [tagDecode TagInt];
                NSLog(@"Order No:%d",orderno);
                
                delegate1.referanceOrderNo=orderno;
            }
        }
        
        if(shTag == sharedManager.btTerminal)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                shTerminal = [tagDecode TagShort];
            }
        }
        
        if(shTag == sharedManager.stApplicationName)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                stApplicationName = [tagDecode TagString];
            }
        }
        
        if(shTag == sharedManager.stDataServerHost)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                stDataServerHost = [tagDecode TagString];
            }
        }
        
        if(shTag == sharedManager.stText)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                stText = [tagDecode TagString];
                
                if([stText containsString:@"your account is locked"])
                {
                    
                    
                }
                
                
            }
        }
        
        if(shTag == sharedManager.stExchange)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                stExchanges = [tagDecode TagString];
            }
        }
        
        if(shTag == sharedManager.stParameter)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                stParameter = [tagDecode TagString];
            }
        }
        
        if(shTag == sharedManager.stDealerID)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                stDealerID = [tagDecode TagString];
                
                
                
               delegate1.stdealerId=[[stDealerID componentsSeparatedByString:@","] mutableCopy];
                
            }
        }
        
        
        
    }
    
    if(stExchanges.length>0)
    {
       
    }
    
    else
    {
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:stText preferredStyle:UIAlertControllerStyleAlert];
//
//        [self presentViewController:alert animated:YES completion:^{
//
//        }];
//
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
//
//        [alert addAction:okAction];
//
        
        
    }
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
   
}

-(void)whileMethod

{
    @try
    {
    while ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
        shTag1 = [tagDecode NextTag];
        
        
        
        
        
        
        if(shTag1)
        {
            if(shTag1==sharedManager.btDataType)
            {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    shDataType = [tagDecode TagShort];
                    blDataType = true;
                    blDataSize = false;
                    
                    
                }
                
                swithStr=@"YES";
                
                
            }
            if(shTag1==sharedManager.shDataSize)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    shDataSize = [tagDecode TagShort];
                    blDataSize = true;
                    
                    
                }
                
                swithStr=@"YES";
                
            }
        }
        
        if([swithStr isEqualToString:@"YES"])
        {
            
            swithStr=@"";
            if (blDataType && blDataSize)
            {
                [self nextMethod];
                
            }
            
            else
            {
                [self whileMethod];
                
            }
            
        }
        
    }
    
    
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}


-(void)nextMethod
{
    @try
    {
    if (!(blDataType && blDataSize)) {
        shStart1 += [tagDecode GetIndex];
        
        [self firstWhilemethod];
        
    }
    if ((shPendingBufferSize1 - shStart1 - sharedManager.shHeaderSize) < shDataSize) {
        
        //        [self firstWhilemethod];
        [self shstartMethod];
        
        
    }
    NSMutableArray * btProcess = [[NSMutableArray alloc]initWithCapacity:shDataSize];
    
    for(int i=0;i<shDataSize;i++)
    {
        [btProcess insertObject:@0 atIndex:i];
    }
    shStart1 +=[tagDecode GetIndex];
    
    short sampleShort=shStart1;
    
    
    
    for(int i=0;i<shDataSize;i++)
    {
        //        [btProcess insertObject:[btPendingBuffer objectAtIndex:sampleShort] atIndex:i];
        
        [btProcess replaceObjectAtIndex:i withObject:[btPendingBuffer objectAtIndex:sampleShort]];
        sampleShort = sampleShort +1;
    }
    
    
    
    if (blWrite_) {
//        dispatch_async(dispatch_get_main_queue(), ^{
        
          [self updateUIThread:btProcess boolMethod:blWrite_];
            
            
            
            
//        });
        
    } else {
        
        [self updateUIThread:btProcess boolMethod:blWrite_];
        
        
    }
    
    shStart1 =shStart1+shDataSize;
        
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}


-(void)firstWhilemethod
{
    
    @try
    {
    while (shPendingBufferSize1 - shStart1 >= sharedManager.shHeaderSize) {
        
        
        NSMutableArray * btTagBuffer = [[NSMutableArray alloc]initWithCapacity:(shPendingBufferSize1-shStart1)];
        
        for(int i=0;i<(shPendingBufferSize1-shStart1);i++)
        {
            [btTagBuffer insertObject:@0 atIndex:i];
        }
        
        short sampleShort=shStart1;
        NSUInteger countStore = btTagBuffer.count;
        for(int i=0;i<countStore;i++)
        {
            //  [btTagBuffer insertObject:[btPendingBuffer objectAtIndex:shStart] atIndex:i];
            
            [btTagBuffer replaceObjectAtIndex:i withObject:[btPendingBuffer objectAtIndex:sampleShort]];
            sampleShort = sampleShort +1;
        }
        //        shStart1 = 0;
        [tagDecode TagDecode:btTagBuffer];
        
        shDataType = 0;
        shDataSize = 0;
        
        blDataType = false;
        blDataSize = false;
        
        
        
        
        
        while ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
            shTag1 = [tagDecode NextTag];
            
            
            
            
            
            
            if(shTag1)
            {
                if(shTag1==sharedManager.btDataType)
                {
                    if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                        shDataType = [tagDecode TagShort];
                        blDataType = true;
                        blDataSize = false;
                        
                        
                    }
                    
                    swithStr=@"YES";
                    
                    
                }
                else if(shTag1==sharedManager.shDataSize)
                {
                    if([tagDecode CheckCapacity:sharedManager.shShortSize])
                    {
                        shDataSize = [tagDecode TagShort];
                        blDataSize = true;
                        
                        
                    }
                    
                    swithStr=@"YES";
                    
                }
            }
            
            if([swithStr isEqualToString:@"YES"])
            {
                
                swithStr=@"";
                if (blDataType && blDataSize)
                {
                    
                    [self nextMethod];
                }
                
                else
                {
                    
                    
                    
                    [self whileMethod];
                }
                
            }
            
        }
        
        
        
    }
    
    [self shstartMethod];
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}

-(void)shstartMethod
{
    @try
    {
    if (shStart1 != 0) {
        shPendingBufferSize1 -= shStart1;
        if (shPendingBufferSize1 != 0) {
            //system.arraycopy(btPendingBuffer, shStart, btPendingBuffer, 0, shPendingBufferSize);
            
            short sampleShort=shStart1;
            
            for(int i=0;i<shPendingBufferSize1;i++)
            {
                [btPendingBuffer insertObject:[btPendingBuffer objectAtIndex:sampleShort] atIndex:i];
                sampleShort = sampleShort+1;
            }
        }
        
        else
        {
            
            //            [self BufferProcess:true];
        }
    }
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}


-(void)OrderDecode
{
    @try
    {
    NSLog(@"Order Decode Method");
    short shTag;
    MTORDER * order = [[MTORDER alloc]init];
    int inMasterID = 0;
    
    
    NSMutableDictionary * orderDict;

    
    
    orderDict=[[NSMutableDictionary alloc]init];
    
    
    while ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
        
        
        
       
        shTag = [tagDecode NextTag];
        if(shTag == sharedManager.inATINOrderNo)
        {
            if([tagDecode CheckCapacity:sharedManager.shLongSize])
            {
                order.inATINOrderID = [tagDecode TagInt];
                NSLog(@"inATINOrderID Tag value:%d",order.inATINOrderID);
                NSString * local=[NSString stringWithFormat:@"%d",order.inATINOrderID];
                
                [orderDict setObject:local forKey:@"ATINOrderId"];
            }
        }
        
        if(shTag == sharedManager.stUserID)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stUserID = [tagDecode TagString];
                NSLog(@"stUserID Tag value:%@",order.stUserID);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stUserID];
                
                [orderDict setObject:local forKey:@"stUserID"];
            }
        }
        
        if(shTag == sharedManager.inMasterID)
        {
            if([tagDecode CheckCapacity:sharedManager.shIntSize])
            {
                inMasterID = [tagDecode TagInt];
                NSLog(@"inMasterID Tag value:%d",inMasterID);
                
                NSString * local=[NSString stringWithFormat:@"%d",inMasterID];
                
                [orderDict setObject:local forKey:@"inMasterID"];
            }
        }
        
        if(shTag == sharedManager.inExchangeClientOrderNo)
        {
            if([tagDecode CheckCapacity:sharedManager.shIntSize])
            {
                order.inExchangeClientOrderID = [tagDecode TagInt];
                NSLog(@"Order.inExchangeClientOrderID Tag value:%d",order.inExchangeClientOrderID);
                NSString * local=[NSString stringWithFormat:@"%d",order.inExchangeClientOrderID];
                
                [orderDict setObject:local forKey:@"inExchangeClientOrderID"];
                
            }
        }
        
        if(shTag == sharedManager.stExchangeOrderNo)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stExchangeOrderID = [tagDecode TagString];
                NSLog(@"Order.stExchangeOrderID Tag value:%@",order.stExchangeOrderID);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stExchangeOrderID];
                
                [orderDict setObject:local forKey:@"stExchangeOrderID"];

            }
        }
        
        if(shTag == sharedManager.stOrderTime)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stOrderTime = [tagDecode TagString];
                NSLog(@"Order.stOrderTime Tag value:%@",order.stOrderTime);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stOrderTime];
                
                [orderDict setObject:local forKey:@"stOrderTime"];
            }
        }
        
        if(shTag == sharedManager.stExchangeOrderTime)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stExchangeOrderTime = [tagDecode TagString];
                NSLog(@" Order.stExchangeOrderTime Tag value:%@ ",order.stExchangeOrderTime);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stExchangeOrderTime];
                
                [orderDict setObject:local forKey:@"stExchangeOrderTime"];

            }
        }
        
        if(shTag == sharedManager.stExchange)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stExchange = [tagDecode TagString];
                NSLog(@" Order.stExchange Tag value:%@",order.stExchange);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stExchange];
                
                [orderDict setObject:local forKey:@"stExchange"];
            }
        }
        
        
        if(shTag == sharedManager.stSecurityID)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stSecurityID = [tagDecode TagString];
                NSLog(@"Order.stSecurityID Tag value:%@",order.stSecurityID);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stSecurityID];
                
                [orderDict setObject:local forKey:@"stSecurityID"];
            }
        }
        
        if(shTag == sharedManager.stSymbol)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stSymbol = [tagDecode TagString];
                NSLog(@"Order.stSymbol Tag value:%@ ",order.stSymbol);
                
                NSString * local=[NSString stringWithFormat:@"Empty"];
                
                [orderDict setObject:local forKey:@"stSymbol"];

            }
        }
        
        if(shTag == sharedManager.stExpiryDate)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stExpiryDate = [tagDecode TagString];
                NSLog(@"Order.stExpiryDate Tag value:%@",order.stExpiryDate);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stExpiryDate];
                
                [orderDict setObject:local forKey:@"stExpiryDate"];
            }
        }
        
        if(shTag == sharedManager.stOptionType)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stOptionType = [tagDecode TagString];
                NSLog(@"Order.stOptionType Tag value:%@",order.stOptionType);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stOptionType];
                
                [orderDict setObject:local forKey:@"stOptionType"];
            }
        }
        
        if(shTag == sharedManager.stSecurityType)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stSecurityType = [tagDecode TagString];
                NSLog(@"Order.stSecurityType Tag value:%@",order.stSecurityType);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stSecurityType];
                
                [orderDict setObject:local forKey:@"stSecurityType"];
            }
        }
        
        if(shTag == sharedManager.btTimeinForce)
        {
            if([tagDecode CheckCapacity:sharedManager.shCharSize])
            {
                Byte btTimeinForce = [tagDecode TagByte];
                NSLog(@"Order.btTimeinForce Tag value:%hhu",btTimeinForce);
                
                NSString * local=[NSString stringWithFormat:@"%hhu",btTimeinForce];
                
                [orderDict setObject:local forKey:@"btTimeinForce"];
            }
        }
        
        if(shTag == sharedManager.stTerminalInfo)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stTerminalInfo = [tagDecode TagString];
                NSLog(@"Order.stTerminalInfo Tag value:%@",order.stTerminalInfo);
                
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stTerminalInfo];
                
                [orderDict setObject:local forKey:@"stTerminalInfo"];
            }
        }
        
        if(shTag == sharedManager.dtExpiryDate)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stOrderExpiryDateTime = [tagDecode TagString];
                NSLog(@"Order.stOrderExpiryDateTime Tag value:%@",order.stOrderExpiryDateTime);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stOrderExpiryDateTime];
                
                [orderDict setObject:local forKey:@"stOrderExpiryDateTime"];
            }
        }
        
        if(shTag == sharedManager.btSide)
        {
            if([tagDecode CheckCapacity:sharedManager.shCharSize])
            {
                Byte btSide = [tagDecode TagByte];
                NSLog(@"Order.btSide Tag value:%hhu",btSide);
                
                NSString * local=[NSString stringWithFormat:@"%hhu",btSide];
                
                [orderDict setObject:local forKey:@"btSide"];
            }
        }
        
        if(shTag == sharedManager.inOrderQty)
        {
            if([tagDecode CheckCapacity:sharedManager.shIntSize])
            {
                order.inQty = [tagDecode TagInt];
                NSLog(@"Order.inQty Tag value:%d",order.inQty);
                
                NSString * local=[NSString stringWithFormat:@"%d",order.inQty];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"inQty"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"inQty"];
                }
                
                
            }
        }
        
        if(shTag == sharedManager.inPendingQty)
        {
            if([tagDecode CheckCapacity:sharedManager.shIntSize])
            {
                order.inPendingQty = [tagDecode TagInt];
                NSLog(@"Order.inPendingQty Tag value:%d",order.inPendingQty);
                
                NSString * local=[NSString stringWithFormat:@"%d",order.inPendingQty];
                
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"inPendingQty"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"inPendingQty"];
                }
                
               

                

            }
        }
        
        if(shTag == sharedManager.inExecuteQty)
        {
            if([tagDecode CheckCapacity:sharedManager.shIntSize])
            {
                order.inExeQty = [tagDecode TagInt];
                NSLog(@"Order.inExeQty Tag value:%d",order.inExeQty);
                
                
                NSString * local=[NSString stringWithFormat:@"%d",order.inExeQty];
                
                
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"inExeQty"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"inExeQty"];
                }
                
                
                
            }
        }
        
        if(shTag == sharedManager.inDiscloseQry)
        {
            if([tagDecode CheckCapacity:sharedManager.shIntSize])
            {
                order.inDiscloseQty = [tagDecode TagInt];
                NSLog(@"Order.inDiscloseQty Tag value:%d",order.inDiscloseQty);
                
//                NSString * local=[NSString stringWithFormat:@"%d",order.inDiscloseQty];
//                
//                [orderDict setObject:local forKey:@"inDiscloseQty"];
            }
        }
        
        if(shTag == sharedManager.dbPrice)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                order.dbPrice = [tagDecode TagDouble];
                NSLog(@"Order.dbPrice Tag value:%f",order.dbPrice);
                
                NSString * local=[NSString stringWithFormat:@"%.2f",order.dbPrice];
                
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"dbPrice"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"dbPrice"];
                }
                
               
            }
        }
        
        if(shTag == sharedManager.dbTriggerPrice)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                order.dbTriggerPrice = [tagDecode TagDouble];
                NSLog(@"Order.dbTriggerPrice Tag value:%f",order.dbTriggerPrice);
                
                
                
                NSString * local=[NSString stringWithFormat:@"%f",order.dbTriggerPrice];
                
                if([local isEqualToString:@""])
                {
                
                [orderDict setObject:@"0" forKey:@"dbTriggerPrice"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"dbTriggerPrice"];
                }
            }
        }
        
        if(shTag == sharedManager.dbStrikePrice)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                order.dbStrikePrice = [tagDecode TagDouble];
                NSLog(@"Order.dbStrikePrice Tag value:%f",order.dbStrikePrice);
                
                NSString * local=[NSString stringWithFormat:@"%f",order.dbStrikePrice];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"dbStrikePrice"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"dbStrikePrice"];
                }
            }
        }
        
//        if(shTag == sharedManager.dbStrikePrice)
//        {
//            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
//            {
//                order.dbStrikePrice = [tagDecode TagDouble];
//                NSLog(@"Order.dbStrikePrice Tag value:%f",order.dbStrikePrice);
//            }
//        }
        
        if(shTag == sharedManager.stClientID)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stClientID = [tagDecode TagString];
                NSLog(@"Order.stClientID Tag value:%@",order.stClientID);
                
                NSString * local=[NSString stringWithFormat:@"%f",order.dbTriggerPrice];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"dbTriggerPrice"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"dbTriggerPrice"];
                }
            }
        }
        
        if(shTag == sharedManager.btOrderType)
        {
            if([tagDecode CheckCapacity:sharedManager.shCharSize])
            {
                Byte btOrderType = [tagDecode TagByte];
                NSLog(@"Order.btOrderType Tag value:%hhu",btOrderType);
                
                NSString * local=[NSString stringWithFormat:@"%hhu",btOrderType];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"btOrderType"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"btOrderType"];
                }
            }
        }
        
        if(shTag == sharedManager.btOrderSituation)
        {
            if([tagDecode CheckCapacity:sharedManager.shCharSize])
            {
                Byte btOrderSituation = [tagDecode TagByte];
                NSLog(@"Order.btOrderSituation Tag value:%hhu",btOrderSituation);
                NSString * local=[NSString stringWithFormat:@"%hhu",btOrderSituation];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"btOrderSituation"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"btOrderSituation"];
                }
                
            }
        }
        
        if(shTag == sharedManager.btOrderStatus)
        {
            if([tagDecode CheckCapacity:sharedManager.shCharSize])
            {
                Byte btOrderStatus = [tagDecode TagByte];
                NSLog(@"Order.btOrderStatus Tag value:%hhu",btOrderStatus);
                
                delegate1.orderStatus=btOrderStatus;
//                StockOrderView * stock;
//                [stock orderStatusAlertMethod];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete" object:nil];
                
//                if (self.delegate && [self.delegate respondsToSelector:@selector(orderStatusAlertMethod)]) {
//                    [self.delegate orderStatusAlertMethod];
//                }
                
                
                NSString * local=[NSString stringWithFormat:@"%hhu",btOrderStatus];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"btOrderStatus"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"btOrderStatus"];
                }
            }
        }
        
        if(shTag == sharedManager.stCtclID)
        {
            if([tagDecode CheckCapacity:sharedManager.shCharSize])
            {
                order.stCTCLID = [tagDecode TagString];
                NSLog(@"Order.stCTCLID Tag value:%@",order.stCTCLID);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stCTCLID];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"stCTCLID"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"stCTCLID"];
                }
            }
        }
        
        if(shTag == sharedManager.stTraderID)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stTraderID = [tagDecode TagString];
                NSLog(@"Order.stTraderID Tag value:%@",order.stTraderID);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stTraderID];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"stTraderID"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"stTraderID"];
                }
            }
        }
        
        if(shTag == sharedManager.stMemberID)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stMemberID = [tagDecode TagString];
                NSLog(@"Order.stMemberID Tag value:%@",order.stMemberID);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stMemberID];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"stMemberID"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"stMemberID"];
                }
            }
        }
        
        if(shTag == sharedManager.stRefText)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stRefText = [tagDecode TagString];
                NSLog(@"Order.stRefText Tag value:%@",order.stRefText);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stRefText];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"stRefText"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"stRefText"];
                }
            }
        }
        
        if(shTag == sharedManager.stErrorText)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.stErrorText = [tagDecode TagString];
                NSLog(@"Order.stErrorText Tag value:%@",order.stErrorText);
                
                NSString * local=[NSString stringWithFormat:@"%@",order.stErrorText];
                
                if([local isEqualToString:@""])
                {
                    
                    [orderDict setObject:@"0" forKey:@"stErrorText"];
                }
                
                else
                {
                    
                    [orderDict setObject:local forKey:@"stErrorText"];
                }
            }
        }
        
        if(shTag == sharedManager.shErrorCode)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                order.shErrorCode = [tagDecode TagShort];
                NSLog(@"Order.shErrorCode Tag value:%hd",order.shErrorCode);
            }
        }
        
        
    }
    
   [delegate1.allOrderHistory addObject:orderDict];
    [delegate1.allTradeArray addObject:orderDict];
        
        if(placedOrderBool==true)
        {
            placedOrderBool=false;
        if(delegate1.allOrderHistory.count>0||delegate1.allTradeArray.count>0)
        {
            
            //        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete1" object:nil];
            
            
            
            
            if(delegate1.mainorderCheck==true)
            {
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete" object:nil];
            }
            
            else
                
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"trade" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"positions" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ordersuccess" object:nil];
            }
            
        }
            
            
        }
   
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}

-(void)TradeDecode

    
    {
        @try
        {
        short shTag;
        MTTrade * trade = [[MTTrade alloc]init];
        
        NSMutableDictionary * orderDict;
        
        
        
        orderDict=[[NSMutableDictionary alloc]init];
        
        
        while ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
            shTag = [tagDecode NextTag];
            if(shTag==sharedManager.inATINOrderNo)
            {
                if([tagDecode CheckCapacity:sharedManager.shLongSize])
                {
                    trade.inATINOrderID = [tagDecode TagShort];
                    NSLog(@"trade.inATINOrderID:%d",trade.inATINOrderID);
                    
                    NSString * local=[NSString stringWithFormat:@"%d",trade.inATINOrderID];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"ATINOrderId"];

                        
                    }
                    
                    else
                    {
                    
                    [orderDict setObject:local forKey:@"ATINOrderId"];
                        
                    }
                }
            }
            if(shTag==sharedManager.stUserID)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stUserID = [tagDecode TagString];
                    NSLog(@"trade.stUserID:%@",trade.stUserID);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stUserID];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stUserID"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stUserID"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.stExchange)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stExchange = [tagDecode TagString];
                    NSLog(@"trade.stExchange:%@",trade.stExchange);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stExchange];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stExchange"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stExchange"];
                        
                    }

                }
            }
            
            if(shTag==sharedManager.stSecurityID)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stSecurityID = [tagDecode TagString];
                    NSLog(@"trade.stSecurityID:%@",trade.stSecurityID);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stSecurityID];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stSecurityID"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stSecurityID"];
                        
                    }

                }
            }
            
            if(shTag==sharedManager.stManagerID)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stManagerID = [tagDecode TagString];
                    NSLog(@"trade.stManagerID:%@",trade.stManagerID);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stManagerID];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stManagerID"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stManagerID"];
                        
                    }

                    
                    
                }
            }
            
            if(shTag==sharedManager.inATINTradeNo)
            {
                if([tagDecode CheckCapacity:sharedManager.shIntSize])
                {
                    trade.inATINTradeID = [tagDecode TagInt];
                    NSLog(@"trade.inATINTradeID:%d",trade.inATINTradeID);
                    NSString * local=[NSString stringWithFormat:@"%d",trade.inATINTradeID];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"inATINTradeID"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inATINTradeID"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.stExchangeOrderNo)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stExchangeOrderID = [tagDecode TagString];
                    NSLog(@"trade.stExchangeOrderID:%@",trade.stExchangeOrderID);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stExchangeOrderID];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stExchangeOrderID"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stExchangeOrderID"];
                        
                    }
                    
                }
            }
            
            if(shTag==sharedManager.stOrderTime)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stOrderTime = [tagDecode TagString];
                    NSLog(@"trade.stOrderTime:%@",trade.stOrderTime);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stOrderTime];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stOrderTime"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stOrderTime"];
                        
                    }
                    
                }
            }
            
            if(shTag==sharedManager.stTradeTime)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stTradeTime = [tagDecode TagString];
                    NSLog(@"trade.stTradeTime:%@",trade.stTradeTime);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stTradeTime];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stTradeTime"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stTradeTime"];
                        
                    }

                }
            }
            
            if(shTag==sharedManager.stExchangeOrderTime)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stExchangeOrderTime = [tagDecode TagString];
                    NSLog(@"trade.stExchangeOrderTime:%@",trade.stExchangeOrderTime);
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stExchangeOrderTime];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stExchangeOrderTime"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stExchangeOrderTime"];
                        
                    }
                    
                }
            }
            
            
            if(shTag==sharedManager.inOrderNo)
            {
                if([tagDecode CheckCapacity:sharedManager.shIntSize])
                {
                    trade.inOrderID = [tagDecode TagInt];
                    NSLog(@"trade.inOrderID:%d",trade.inOrderID);
                    
                    NSString * local=[NSString stringWithFormat:@"%d",trade.inOrderID];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"inOrderID"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inOrderID"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.stSymbol)
            {
                if([tagDecode CheckCapacity:sharedManager.shIntSize])
                {
                    trade.stSymbol = [tagDecode TagString];
                    NSLog(@"trade.stSymbol:%@",trade.stSymbol);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stSymbol];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stSymbol"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stSymbol"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.stExpiryDate)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stExpiryDate = [tagDecode TagString];
                    NSLog(@"trade.stExpiryDate:%@",trade.stExpiryDate);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stExpiryDate];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stExpiryDate"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stExpiryDate"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.stOptionType)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stOptionType = [tagDecode TagString];
                    NSLog(@"trade.stOptionType:%@",trade.stOptionType);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stOptionType];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stOptionType"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stOptionType"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.stSecurityType)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stSecurityType = [tagDecode TagString];
                    NSLog(@"trade.stSecurityType:%@",trade.stSecurityType);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stSecurityType];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stSecurityType"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stSecurityType"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.btSide)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stSide = [tagDecode TagString];
                    NSLog(@"trade.stSide:%@",trade.stSide);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stSide];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stSide"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stSide"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.stSecurityType)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stSecurityType = [tagDecode TagString];
                    NSLog(@"trade.stSecurityType:%@",trade.stSecurityType);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stSecurityType];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stSecurityType"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stSecurityType"];
                        
                    }

                }
            }
            
            if(shTag==sharedManager.inOrderQty)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.inQty = [tagDecode TagInt];
                    NSLog(@"trade.inQty:%d",trade.inQty);
                    
                    NSString * local=[NSString stringWithFormat:@"%d",trade.inQty];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"inQty"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inQty"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.inPendingQty)
            {
                if([tagDecode CheckCapacity:sharedManager.shIntSize])
                {
                    trade.inPendingQty = [tagDecode TagInt];
                    NSLog(@"trade.inPendingQty:%d",trade.inPendingQty);
                    
                    NSString * local=[NSString stringWithFormat:@"%d",trade.inPendingQty];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"inPendingQty"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inPendingQty"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.dbPrice)
            {
                if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
                {
                    trade.dbPrice = [tagDecode TagDouble];
                    NSLog(@"trade.dbPrice:%f",trade.dbPrice);
                    NSString * local=[NSString stringWithFormat:@"%f",trade.dbPrice];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"dbPrice"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"dbPrice"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.dbStrikePrice)
            {
                if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
                {
                    trade.dbStrikePrice = [tagDecode TagDouble];
                    NSLog(@"trade.dbStrikePrice:%f",trade.dbStrikePrice);
                    
                    NSString * local=[NSString stringWithFormat:@"%f",trade.dbStrikePrice];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"dbStrikePrice"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"dbStrikePrice"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.stClientID)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stClientID = [tagDecode TagString];
                    NSLog(@"trade.stClient:%@",trade.stClientID);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stClientID];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stClientID"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stClientID"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.btOrderType)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stOrderType = [tagDecode TagString];
                    NSLog(@"trade.stOrderType:%@",trade.stOrderType);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stOrderType];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stOrderType"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stOrderType"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.stTradeNo)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stTradeID = [tagDecode TagString];
                    NSLog(@"trade.stTradeID:%@",trade.stTradeID);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stTradeID];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stTradeID"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stTradeID"];
                        
                    }
                }
            }
            
            if(shTag==sharedManager.stRefText)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    trade.stRefText = [tagDecode TagString];
                    NSLog(@"trade.stRefText:%@",trade.stRefText);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",trade.stRefText];
                    
                    if([local isEqualToString:@""])
                    {
                        [orderDict setObject:@"0" forKey:@"stRefText"];
                        
                        
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stRefText"];
                        
                    }
                }
            }
            
            
        }
        
        [delegate1.allTradeOrderArray addObject:orderDict];
            
            
            
            
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete1" object:nil];
            
            
        } @catch (NSException *exception) {
            NSLog(@"Caught Exception");
        } @finally {
            NSLog(@"Finally");
        }
        
    }
    
    
    
    


-(void)dismissKeyboard
{
    [self.clientIDTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
     [self.txtFld resignFirstResponder];
//    [self.scrollView setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
//
//
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
//    [self.scrollView setFrame:CGRectMake(0,-100,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

-(void)keyboardDidHide:(NSNotification *)notification
{
//    [self.scrollView setFrame:CGRectMake(0,65,self.view.frame.size.width,self.view.frame.size.height)];
}

-(void)firstWhilemethod1
{
    @try
    {
    
    while (broadshPendingBufferSize1 - broadshStart1 >= sharedManager.shHeaderSize) {
        
        
        NSMutableArray * btTagBuffer = [[NSMutableArray alloc]initWithCapacity:(broadshPendingBufferSize1-broadshStart1)];
        
        for(int i=0;i<(broadshPendingBufferSize1-broadshStart1);i++)
        {
            [btTagBuffer insertObject:@0 atIndex:i];
        }
        
        short sampleShort=broadshStart1;
        NSUInteger countStore = btTagBuffer.count;
        for(int i=0;i<countStore;i++)
        {
            //  [btTagBuffer insertObject:[btPendingBuffer objectAtIndex:shStart] atIndex:i];
            
            [btTagBuffer replaceObjectAtIndex:i withObject:[broadbtPendingBuffer objectAtIndex:sampleShort]];
            sampleShort = sampleShort +1;
        }
        //        shStart1 = 0;
        [broadTag TagDecode:btTagBuffer];
        
        broadshDataType = 0;
        broadshDataSize = 0;
        
        broadblDataType = false;
        broadblDataSize = false;
        
        
        
        
        
        while ([broadTag CheckCapacity:sharedManager.shShortSize]) {
            broadshTag1 = [broadTag NextTag];
            
            
            
            
            
            
            if(broadshTag1)
            {
                if(broadshTag1==sharedManager.btDataType)
                {
                    if ([broadTag CheckCapacity:sharedManager.shShortSize]) {
                        broadshDataType = [broadTag TagShort];
                        broadblDataType = true;
                        broadblDataSize = false;
                        
                        
                    }
                    
                    broadswithStr=@"YES";
                    
                    
                }
                else if(broadshTag1==sharedManager.shDataSize)
                {
                    if([broadTag CheckCapacity:sharedManager.shShortSize])
                    {
                        broadshDataSize = [broadTag TagShort];
                        broadblDataSize = true;
                        
                        
                    }
                    
                    broadswithStr=@"YES";
                    
                }
            }
            
            
            
            if([broadswithStr isEqualToString:@"YES"])
            {
                
                broadswithStr=@"";
                if (broadblDataType && broadblDataSize)
                {
                    
                    [self nextMethod1];
                    
                }
                
                else
                {
                    
                    
                    
                    [self whileMethod1];
                }
                
            }
            
            
            
        }
        
        
        
    }
    
    [self shstartMethod1];
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
    
}

-(void)shstartMethod1
{
    @try
    {
    if (broadshStart1 != 0) {
        broadshPendingBufferSize1 -= broadshStart1;
        if (broadshPendingBufferSize1 != 0) {
            //system.arraycopy(btPendingBuffer, shStart, btPendingBuffer, 0, shPendingBufferSize);
            
            short sampleShort=broadshStart1;
            
            for(int i=0;i<broadshPendingBufferSize1;i++)
            {
                [broadbtPendingBuffer insertObject:[broadbtPendingBuffer objectAtIndex:sampleShort] atIndex:i];
                sampleShort = sampleShort+1;
            }
        }
        
        
        
        else
        {
            
            broadshPendingBufferSize1=broadshStart1;
        }
    }
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}
-(void)nextMethod1
{
    @try
    {
    if (!(broadblDataType && broadblDataSize)) {
        broadshStart1 += [broadTag GetIndex];
        
        [self firstWhilemethod1];
        
    }
    if ((broadshPendingBufferSize1 - broadshStart1 - sharedManager.shHeaderSize) < broadshDataSize) {
        
        //        [self firstWhilemethod];
        [self shstartMethod1];
        
        
    }
    NSMutableArray * btProcess = [[NSMutableArray alloc]initWithCapacity:broadshDataSize];
    
    for(int i=0;i<broadshDataSize;i++)
    {
        [btProcess insertObject:@0 atIndex:i];
    }
    broadshStart1 +=[broadTag GetIndex];
    
    short sampleShort=broadshStart1;
    
    
    
    for(int i=0;i<broadshDataSize;i++)
    {
        //        [btProcess insertObject:[btPendingBuffer objectAtIndex:sampleShort] atIndex:i];
        
        [btProcess replaceObjectAtIndex:i withObject:[broadbtPendingBuffer objectAtIndex:sampleShort]];
        sampleShort = sampleShort +1;
    }
    
    
    
    
    
        if (blWrite1_) {
           
                
                [self updateUIThread1:btProcess boolMethod:blWrite1_];
                
        
        } else {
           
                [self updateUIThread1:btProcess boolMethod:blWrite1_];
        }

   
    
    broadshStart1 =broadshStart1+broadshDataSize;
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}

-(void)whileMethod1

{
    @try
    {
    while ([broadTag CheckCapacity:sharedManager.shShortSize]) {
        broadshTag1 = [broadTag NextTag];
        
        
        
        
        
        
        if(broadshTag1)
        {
            if(broadshTag1==sharedManager.btDataType)
            {
                if ([broadTag CheckCapacity:sharedManager.shShortSize]) {
                    broadshDataType = [broadTag TagShort];
                    broadblDataType = true;
                    broadblDataSize = false;
                    
                    
                }
                
                broadswithStr=@"YES";
                
                
            }
            if(broadshTag1==sharedManager.shDataSize)
            {
                if([broadTag CheckCapacity:sharedManager.shShortSize])
                {
                    broadshDataSize = [broadTag TagShort];
                    broadblDataSize = true;
                    
                    
                }
                
                broadswithStr=@"YES";
                
            }
        }
        
        if([broadswithStr isEqualToString:@"YES"])
        {
            
            broadswithStr=@"";
            if (broadblDataType && broadblDataSize)
            {
                [self nextMethod1];
                
            }
            
            else
            {
                [self whileMethod1];
                
            }
            
        }
        
    }
    
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
    
}

-(void)updateUIThread1:(NSMutableArray *)btBufferDeCode boolMethod:(BOOL)blWrite
{
    @try
    {
    //NSMutableArray * btBufferDeCode_ = [[NSMutableArray alloc]initWithObjects:btBufferDeCode, nil];
    
    NSMutableArray * btBufferDeCode_ = [[NSMutableArray alloc]initWithArray:btBufferDeCode];
    blWrite1_ = blWrite;
    if(btBufferDeCode_.count>0)
    {
    [broadTag TagDecode:btBufferDeCode_];
    
    NSLog(@"%@",broadTag);
    
    }
    
    short shTag, shMsgCode = 0;
    shTag = [broadTag NextTag];
    if(shTag == sharedManager.shMsgCode && [broadTag CheckCapacity:sharedManager.shShortSize])
    {
        shMsgCode = [broadTag TagShort];
    }
    
    NSLog(@"First Socket shMsgCode:%hd",shMsgCode);
    if (shMsgCode != 0)
    {
        NSString * check1 = @"check";
    
        if(shMsgCode == sharedManager.IndexWatch)
        {
            //OrderDecode(mTagDeCode);
              //          [self BroadcastIDDecode];
            
           // [self broadDepth];
            if(delegate1.tickerCheck==true)
            {
            [self indexWatch];
            }
        }
        else
        {
            check1 = @"YES";
        }
        
        
        if([check1 isEqualToString:@"YES"])
        {
            //            [self Process:dataBuffer intMethod:len];
        }
        
        if(shMsgCode == sharedManager.Broadcast)
        {
            //OrderDecode(mTagDeCode);
            //            [self BroadcastIDDecode];
            
            [self broadDepth];
            
        }
        
        
        
        else
        {
            check1 = @"YES";
        }
        if([check1 isEqualToString:@"YES"])
        {
            //            [self Process:dataBuffer intMethod:len];
        }
        
        
        
    }
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}


-(void)BroadcastIDDecode
{
    @try
    {
    NSLog(@"BroadCast ID Decode");
    short shTag;
    NSString *stExchnge =@"";
    NSString *stSecurityID =@"";
    double buyPrice=0;
    double  sellPrice=0;
    int buyQty=0;
    double LTP=0;
    int  inBroadcastID = 0;
    while ([broadTag CheckCapacity:sharedManager.shShortSize]) {
        
        shTag = [broadTag NextTag];
        
        if(shTag == sharedManager.stExchange)
        {
            if([broadTag CheckCapacity:sharedManager.shShortSize])
            {
                stExchnge = [broadTag TagString];
            }
            
            NSLog(@"stExchange Value:%@",stExchnge);
        }
        
        if(shTag == sharedManager.stSecurityID)
        {
            if([broadTag CheckCapacity:sharedManager.shShortSize])
            {
                stSecurityID = [broadTag TagString];
            }
            NSLog(@"stSecurityID Value:%@",stSecurityID);
        }
        
        if(shTag == sharedManager.inBroadCastID)
        {
            if([broadTag CheckCapacity:sharedManager.shIntSize])
            {
                inBroadcastID = [broadTag TagInt];
            }
            NSLog(@"inBroadcastID Value:%d",inBroadcastID);
        }
        
        
        
        if(shTag == sharedManager.dbBuyAmount)
        {
            if([broadTag CheckCapacity:sharedManager.shDoubleSize])
            {
                buyPrice = [broadTag TagDouble];
            }
            NSLog(@"dbBuyAmount Value:%f",buyPrice);
        }
        
        if(shTag == sharedManager.dbSellAmount)
        {
            if([broadTag CheckCapacity:sharedManager.shDoubleSize])
            {
                sellPrice = [broadTag TagDouble];
            }
            NSLog(@"dbSellAmount Value:%f",sellPrice);
        }
        
        if(shTag == sharedManager.dbLTP)
        {
            if([broadTag CheckCapacity:sharedManager.shDoubleSize])
            {
                LTP = [broadTag TagDouble];
            }
            NSLog(@"dbLTP Value:%f",LTP);
        }
        
    }
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }

}

-(void)broadDepth
{
    @try
    {
    short shTag;
    short shIndex_=0;
    
    NSMutableDictionary * orderDict;
    
    
    
    orderDict=[[NSMutableDictionary alloc]init];
    
    
    while ([broadTag CheckCapacity:sharedManager.shShortSize]) {
        shTag = [broadTag NextTag];
        
        if(shTag == sharedManager.obBinaryData)
        {
            DepthWatch * depth = [[DepthWatch alloc]init];
            NSMutableArray * btDepthWatchBuffer = [[NSMutableArray alloc]initWithArray:[broadTag TagBinary]];
            if(btDepthWatchBuffer.count>0)
            {
                
                [broadTag TagDecode:btDepthWatchBuffer];
                // TagDecode* tagDecode =[[TagDecode alloc]init];
                depth.usAT = [broadTag TagShort];
                NSLog(@"Depth.usAT:%hd",depth.usAT);
                depth.btDataType = [broadTag TagByte];
                NSLog(@"Depth.btDataType:%d",depth.btDataType);
                depth.usSize = [broadTag TagShort];
                NSLog(@"Depth.usSize:%hd",depth.usSize);
                depth.usMsgCode = [broadTag TagShort];
                NSLog(@"Depth.usMsgCode:%hd",depth.usMsgCode);
                depth.inSeqID = [broadTag TagInt];
                NSLog(@"Depth.inSeqID:%d",depth.inSeqID);
                depth.stExchange = [broadTag TagStringBuffer:(short)48];
                NSLog(@"Depth.stExchange:%@",depth.stExchange);
                
                NSString * local12=[NSString stringWithFormat:@"%@",depth.stExchange];
                
                if([local12 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"stExchange"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:local12 forKey:@"stExchange"];
                    
                }
                
                depth.btNull1 = [broadTag TagByte];
                NSLog(@"Depth.btNull1:%d",depth.btNull1);
                depth.stSecurityID = [broadTag TagStringBuffer:(short)48];
                NSLog(@"Depth.stSecurityID:%@",depth.stSecurityID);
                
                NSString * local=[NSString stringWithFormat:@"%@",depth.stSecurityID];
                
                NSString *newString = [[local componentsSeparatedByCharactersInSet:
                                        [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                       componentsJoinedByString:@""];
                
                if([newString isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"stSecurityID"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:newString forKey:@"stSecurityID"];
                    
                }
                
                
               
                
                
                depth.btNull2 = [broadTag TagByte];
                NSLog(@"Depth.btNull2:%d",depth.btNull2);
                depth.inBroadCastTime = [broadTag TagInt];
                NSLog(@"Depth.inBroadCastTime:%d",depth.inBroadCastTime);
                depth.btSessionIndex = [broadTag TagByte];
                NSLog(@"Depth.btSessionIndex:%d",depth.btSessionIndex);
                
                
                
                depth.dbBuy_Price_0 = [broadTag TagDouble];
                
                NSString * buyPrice=[NSString stringWithFormat:@"%.2f",depth.dbBuy_Price_0];
                
                if([buyPrice isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"buy1"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:buyPrice forKey:@"buy1"];
                    
                }
                
                
                
                depth.inBuy_Qty_0 = [broadTag TagInt];
                NSString * buyQty=[NSString stringWithFormat:@"%d",depth.inBuy_Qty_0];
                
                if([buyQty isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"buyQty"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:buyQty forKey:@"buyQty"];
                    
                }

                depth.inBuy_Order_0 = [broadTag TagInt];
                
                
                
                depth.dbBuy_Price_1 = [broadTag TagDouble];
                
                NSString * buyPrice1=[NSString stringWithFormat:@"%.2f",depth.dbBuy_Price_1];
                
                if([buyPrice1 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"buyPrice1"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:buyPrice1 forKey:@"buyPrice1"];
                    
                }

                depth.inBuy_Qty_1 = [broadTag TagInt];
                
                NSString * buyQty1=[NSString stringWithFormat:@"%d",depth.inBuy_Qty_1];
                
                if([buyQty1 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"buyQty1"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:buyQty1 forKey:@"buyQty1"];
                    
                }

                depth.inBuy_Order_1 = [broadTag TagInt];
                
                
                depth.dbBuy_Price_2 = [broadTag TagDouble];
                NSString * buyPrice2=[NSString stringWithFormat:@"%.2f",depth.dbBuy_Price_2];
                
                if([buyPrice2 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"buyPrice2"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:buyPrice2 forKey:@"buyPrice2"];
                    
                }
                depth.inBuy_Qty_2 = [broadTag TagInt];
                
                NSString * buyQty2=[NSString stringWithFormat:@"%d",depth.inBuy_Qty_2];
                
                if([buyQty2 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"buyQty2"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:buyQty2 forKey:@"buyQty2"];
                    
                }
                
                depth.inBuy_Order_2 = [broadTag TagInt];
                
                
                
                depth.dbBuy_Price_3 = [broadTag TagDouble];
                NSString * buyPrice3=[NSString stringWithFormat:@"%.2f",depth.dbBuy_Price_3];
                
                if([buyPrice3 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"buyPrice3"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:buyPrice3 forKey:@"buyPrice3"];
                    
                }
                depth.inBuy_Qty_3 = [broadTag TagInt];
                NSString * buyQty3=[NSString stringWithFormat:@"%d",depth.inBuy_Qty_3];
                
                if([buyQty3 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"buyQty3"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:buyQty3 forKey:@"buyQty3"];
                    
                }
                depth.inBuy_Order_3 = [broadTag TagInt];
                
                
                
                
                depth.dbBuy_Price_4 = [broadTag TagDouble];
                NSString * buyPrice4=[NSString stringWithFormat:@"%.2f",depth.dbBuy_Price_4];
                
                if([buyPrice4 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"buyPrice4"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:buyPrice4 forKey:@"buyPrice4"];
                    
                }
                depth.inBuy_Qty_4 = [broadTag TagInt];
                NSString * buyQty4=[NSString stringWithFormat:@"%d",depth.inBuy_Qty_4];
                
                if([buyQty4 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"buyQty4"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:buyQty4 forKey:@"buyQty4"];
                    
                }
                depth.inBuy_Order_4 = [broadTag TagInt];
                
                
                
                depth.dbSell_Price_0 = [broadTag TagDouble];
                NSString * sellPrice=[NSString stringWithFormat:@"%.2f",depth.dbSell_Price_0];
                
                if([sellPrice isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"sellPrice"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:sellPrice forKey:@"sellPrice"];
                    
                }
                depth.inSell_Qty_0 = [broadTag TagInt];
                NSString * sellQty=[NSString stringWithFormat:@"%d",depth.inSell_Qty_0];
                
                if([sellQty isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"sellQty"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:sellQty forKey:@"sellQty"];
                    
                }
                depth.inSell_Order_0 = [broadTag TagInt];
                depth.dbSell_Price_1 = [broadTag TagDouble];
                NSString * sellPrice1=[NSString stringWithFormat:@"%.2f",depth.dbSell_Price_1];
                
                if([sellPrice1 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"sellPrice1"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:sellPrice1 forKey:@"sellPrice1"];
                    
                }
                depth.inSell_Qty_1 = [broadTag TagInt];
                NSString * sellQty1=[NSString stringWithFormat:@"%d",depth.inSell_Qty_1];
                
                if([sellQty1 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"sellQty1"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:sellQty1 forKey:@"sellQty1"];
                    
                }
                depth.inSell_Order_1 = [broadTag TagInt];
                
                
                depth.dbSell_Price_2 = [broadTag TagDouble];
                NSString * sellPrice2=[NSString stringWithFormat:@"%.2f",depth.dbSell_Price_2];
                
                if([sellPrice2 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"sellPrice2"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:sellPrice2 forKey:@"sellPrice2"];
                    
                }
                depth.inSell_Qty_2 = [broadTag TagInt];
                NSString * sellQty2=[NSString stringWithFormat:@"%d",depth.inSell_Qty_2];
                
                if([sellQty2 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"sellQty2"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:sellQty2 forKey:@"sellQty2"];
                    
                }
                depth.inSell_Order_2 = [broadTag TagInt];
                
                
                depth.dbSell_Price_3 = [broadTag TagDouble];
                NSString * sellPrice3=[NSString stringWithFormat:@"%.2f",depth.dbSell_Price_3];
                
                if([sellPrice3 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"sellPrice3"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:sellPrice3 forKey:@"sellPrice3"];
                    
                }
                depth.inSell_Qty_3 = [broadTag TagInt];
                NSString * sellQty3=[NSString stringWithFormat:@"%d",depth.inSell_Qty_3];
                
                if([sellQty3 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"sellQty3"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:sellQty3 forKey:@"sellQty3"];
                    
                }
                depth.inSell_Order_3 = [broadTag TagInt];
                
                
                depth.dbSell_Price_4 = [broadTag TagDouble];
                NSString * sellPrice4=[NSString stringWithFormat:@"%.2f",depth.dbSell_Price_4];
                
                if([sellPrice4 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"sellPrice4"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:sellPrice4 forKey:@"sellPrice4"];
                    
                }
                depth.inSell_Qty_4 = [broadTag TagInt];
                NSString * sellQty4=[NSString stringWithFormat:@"%d",depth.inSell_Qty_4];
                
                if([sellQty4 isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"sellQty4"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:sellQty4 forKey:@"sellQty4"];
                    
                }
                depth.inSell_Order_4 = [broadTag TagInt];
                
                depth.inTotalQtyTraded = [broadTag TagInt];
                depth.dbLTP = [broadTag TagDouble];
                NSLog(@"%.2f",depth.dbLTP);
                NSString * local2=[NSString stringWithFormat:@"%.2f",depth.dbLTP];
                
                if([local isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"dbLTP"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:local2 forKey:@"dbLTP"];
                    
                }
                
               
                
                
                depth.inLTQ = [broadTag TagInt];
                depth.inTradeTime = [broadTag TagInt];
                depth.dbVWap = [broadTag TagDouble];
                depth.inTotalBuyQty = [broadTag TagInt];
                NSString * totalBuy=[NSString stringWithFormat:@"%d",depth.inTotalBuyQty];
                
                if([totalBuy isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"totalBuy"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:totalBuy forKey:@"totalBuy"];
                    
                }
                depth.inTotalSellQty = [broadTag TagInt];
                
                NSString * totalSell=[NSString stringWithFormat:@"%d",depth.inTotalSellQty];
                
                if([totalSell isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"totalSell"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:totalSell forKey:@"totalSell"];
                    
                }
                depth.dbOpen = [broadTag TagDouble];
                NSString * open=[NSString stringWithFormat:@"%.2f",depth.dbOpen];
                
                if([open isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"dbOpen"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:open forKey:@"dbOpen"];
                    
                }
                depth.dbHigh = [broadTag TagDouble];
                NSString * dbHigh=[NSString stringWithFormat:@"%.2f",depth.dbHigh];
                
                if([dbHigh isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"dbHigh"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:dbHigh forKey:@"dbHigh"];
                    
                }
                depth.dbLow = [broadTag TagDouble];
                NSString * dbLow=[NSString stringWithFormat:@"%.2f",depth.dbLow];
                
                if([dbLow isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"dbLow"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:dbLow forKey:@"dbLow"];
                    
                }
                depth.dbClose = [broadTag TagDouble];
                NSLog(@"%.2f",depth.dbClose);
                
                NSString * local3=[NSString stringWithFormat:@"%.2f",depth.dbClose];
                
                if([local isEqualToString:@""])
                {
                    [orderDict setObject:@"0" forKey:@"dbClose"];
                    
                    
                }
                
                else
                {
                    
                    [orderDict setObject:local3 forKey:@"dbClose"];
                    
                }
                
               
                depth.inOpenInterest = [broadTag TagInt];
                depth.dbLifeLow = [broadTag TagDouble];
                depth.dbLifeHigh = [broadTag TagDouble];
                depth.dbTotalValue = [broadTag TagDouble];
                
                NSMutableArray * localArray=[[NSMutableArray alloc]init];
                
                if(delegate1.marketWatch1.count>0)
                {
                    for(int i=0;i<delegate1.marketWatch1.count;i++)
                    {
                        NSString * localStr=[NSString stringWithFormat:@"%@",[[delegate1.marketWatch1 objectAtIndex:i] objectForKey:@"stSecurityID"]];
                        
                        if([localStr containsString:newString]||[newString containsString:localStr])
                        {
                            exchangeTokenCheck=true;
                            [delegate1.marketWatch1 replaceObjectAtIndex:i withObject:orderDict];
                        }
                        
                        
                        
                    }
                    
                    if(exchangeTokenCheck==true)
                    {
                        
                        exchangeTokenCheck=false;
                    }
                    
                    else
                    {
                        [delegate1.marketWatch1 addObject:orderDict];
                    }
                    
                    
                  
                    
                    
                }
                
                else
                {
                    [delegate1.marketWatch1 addObject:orderDict];
                }
                
                
//                if(delegate1.depthMt==true)
//                {
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"depth" object:nil];
//                }
                
//                else
//                {
                
                NSLog(@"Market Watch:%@",delegate1.marketWatch1);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                
                @try {
                    if(delegate1.marketWatch1.count>0)
                    {
                        if(delegate1.depthMt==true)
                        {
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"depth" object:nil];
                        }
                        
                        if(delegate1.homeMt==true)
                        {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"marketwatch" object:nil];
                        }
                        
                       if(delegate1.orderMt==true)
                        {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"orderwatch" object:nil];
                        }
                        if(delegate1.holdingsMt==true)
                        {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"holdingwatch" object:nil];
                        }
                        
                    }
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                

                    
                    
               });
                

                
//              }
                
                
               
            }
        }
        
        
    }
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
    
    

}


-(void)indexWatch
{
    NSMutableDictionary * indexWatchDicitonary = [[NSMutableDictionary alloc]init];
   if(delegate1.mtIndexWatchArray.count>0)
   {
       [delegate1.mtIndexWatchArray removeAllObjects];
   }
   
    short shTag;
    while ([broadTag CheckCapacity:sharedManager.shShortSize]) {
        shTag = [broadTag NextTag];
        if(shTag == sharedManager.obBinaryData)
        {
    IndexWatch * indexWatch = [[IndexWatch alloc]init];
    NSMutableArray * btIndexWatchBuffer = [[NSMutableArray alloc]initWithArray:[broadTag TagBinary]];
    if(btIndexWatchBuffer.count>0)
    {
        
        [broadTag TagDecode:btIndexWatchBuffer];
        indexWatch.usAT = [broadTag TagShort];
        indexWatch.btDataType = [broadTag TagByte];
        indexWatch.usSize = [broadTag TagShort];
        indexWatch.usMsgCode = [broadTag TagShort];
        indexWatch.inSeqID = [broadTag TagInt];
        indexWatch.stExchange = [broadTag TagStringBuffer:((short)48)];
        indexWatch.btNull1 = [broadTag TagByte];
        indexWatch.stSymbol1 = [broadTag TagStringBuffer:((short)48)];
        NSString * symbol = [NSString stringWithFormat:@"%@",indexWatch.stSymbol1];
        if(symbol.length>0)
        {
            [indexWatchDicitonary setObject:symbol forKey:@"symbol"];
        }else
        {
            [indexWatchDicitonary setObject:@"0.0" forKey:@"symbol"];
        }
        indexWatch.btNull2 = [broadTag TagByte];
        indexWatch.inBroadcastTime = [broadTag TagInt];
        indexWatch.stSymbol2 = [broadTag TagStringBuffer:((short)32)];
        indexWatch.btNull3 = [broadTag TagByte];
        indexWatch.dbIndexPrice = [broadTag TagDouble];
        NSString * ltp = [NSString stringWithFormat:@"%f",indexWatch.dbIndexPrice];
        if(ltp.length>0)
        {
            [indexWatchDicitonary setObject:ltp forKey:@"LTP"];
        }else
        {
            [indexWatchDicitonary setObject:@"0.0" forKey:@"LTP"];
        }
        indexWatch.dbPrevIndexClose = [broadTag TagDouble];
        NSString * prevClose = [NSString stringWithFormat:@"%f",indexWatch.dbPrevIndexClose];
        if(prevClose.length>0)
        {
            [indexWatchDicitonary setObject:prevClose forKey:@"PrevClose"];
        }else
        {
            [indexWatchDicitonary setObject:@"0.0" forKey:@"PrevClose"];
        }
        
        indexWatch.dbOpen = [broadTag TagDouble];
        indexWatch.dbHigh = [broadTag TagDouble];
        indexWatch.dbLow = [broadTag TagDouble];
        indexWatch.dbLifeTimeHigh = [broadTag TagDouble];
        indexWatch.dbLifeTimeLow = [broadTag TagDouble];
        
        [delegate1.mtIndexWatchArray addObject:indexWatchDicitonary];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"indexWatch" object:nil];
    }
                }
        }
}

-(void)viewDidAppear:(BOOL)animated
{
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(broad:)
                                                 name:@"broad" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(order:)
                                                 name:@"order" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(login:)
                                                 name:@"login" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mtloginCheck:)
                                                 name:@"mtloginCheck" object:nil];
    
    
    
    
    
    loginTest=true;
    
    
   
}







-(void)broad:(NSNotification *)note
{
//      [self performSelector:@selector(broadCastMethod) withObject:self afterDelay:0.0f];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        [self broadCastMethod];
    });
    
//    [self broadCastMethod];
}

-(void)login:(NSNotification *)note
{
    
    [self serverMethod];
}


-(void)netPositonsDecode
{
    @try
    {
        short shTag;
        MTNetPosition *netPosition = [[MTNetPosition alloc]init];
        
        NSMutableDictionary * orderDict;
        
        
        
        orderDict=[[NSMutableDictionary alloc]init];

        
        
        while ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
            shTag = [tagDecode NextTag];
            
            if(shTag == sharedManager.inID)
            {
                if([tagDecode CheckCapacity:sharedManager.shIntSize])
                {
                    netPosition.inRecordNo = [tagDecode TagInt];
                }
            }
            
            if(shTag == sharedManager.stClientID)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    netPosition.stClientID = [tagDecode TagString];
                }
            }
            
            if(shTag == sharedManager.stExchange)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    netPosition.stExchange = [tagDecode TagString];
                    
                    NSString * local=[NSString stringWithFormat:@"%@",netPosition.stExchange];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"stExchange"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stExchange"];
                    }
                }
            }
            
            if(shTag == sharedManager.stSymbol)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    netPosition.stSymbol = [tagDecode TagString];
                    NSString * local=[NSString stringWithFormat:@"%@",netPosition.stSymbol];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"stSymbol"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stSymbol"];
                    }
                }
            }
            
            if(shTag == sharedManager.stExpiryDate)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    netPosition.stExpiryDate = [tagDecode TagString];
                    
                }
            }
            
            if(shTag == sharedManager.stUserID)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    netPosition.stUserID = [tagDecode TagString];
                }
            }
            
            if(shTag == sharedManager.inBuyQty)
            {
                if([tagDecode CheckCapacity:sharedManager.shIntSize])
                {
                    netPosition.inBuyQty = [tagDecode TagInt];
                    
                    NSString * local=[NSString stringWithFormat:@"%d",netPosition.inBuyQty];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"inBuyQty"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inBuyQty"];
                    }
                    
                }
            }
            
            if(shTag == sharedManager.dbBuyAmount)
            {
                if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
                {
                    netPosition.dbBuyAmount = [tagDecode TagDouble];
                    
                    NSString * local=[NSString stringWithFormat:@"%.2f",netPosition.dbBuyAmount];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"dbBuyAmount"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"dbBuyAmount"];
                    }
                }
            }
            
            if(shTag == sharedManager.dbBuyAvg)
            {
                if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
                {
                    netPosition.dbBuyAvg = [tagDecode TagDouble];
                }
            }
            
            if(shTag == sharedManager.inSellQty)
            {
                if([tagDecode CheckCapacity:sharedManager.shIntSize])
                {
                    netPosition.inSellQty = [tagDecode TagInt];
                    
                    NSString * local=[NSString stringWithFormat:@"%d",netPosition.inSellQty];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"inSellQty"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inSellQty"];
                    }
                }
            }
            
            if(shTag == sharedManager.dbSellAmount)
            {
                if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
                {
                    netPosition.dbSellAmount = [tagDecode TagDouble];
                    
                    NSString * local=[NSString stringWithFormat:@"%.2f",netPosition.dbSellAmount];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"dbSellAmount"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"dbSellAmount"];
                    }
                }
            }
            
            if(shTag == sharedManager.dbSellAvg)
            {
                if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
                {
                    netPosition.dbSellAvg = [tagDecode TagDouble];
                }
            }
            
            if(shTag == sharedManager.inNetQty)
            {
                if([tagDecode CheckCapacity:sharedManager.shIntSize])
                {
                    netPosition.inNetQty = [tagDecode TagInt];
                }
            }
            
            if(shTag == sharedManager.dbNetAmount)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    netPosition.dbNetAmount = [tagDecode TagDouble];
                }
            }
            
            if(shTag == sharedManager.dbNetAvg)
            {
                if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
                {
                    netPosition.dbNetAvg = [tagDecode TagDouble];
                }
            }
            
            if(shTag == sharedManager.dbLTP)
            {
                if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
                {
                    netPosition.dbLTP = [tagDecode TagDouble];
                    NSString * local=[NSString stringWithFormat:@"%.2f",netPosition.dbLTP];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"dbLTP"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"dbLTP"];
                    }
                }
            }
            
            if(shTag == sharedManager.dbMTM)
            {
                if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
                {
                    netPosition.dbMTM = [tagDecode TagDouble];
                }
            }
            
            if(shTag == sharedManager.dbMultiplier)
            {
                if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
                {
                    netPosition.dbMultiplier = [tagDecode TagDouble];
                }
            }
        }
        
    
    
    [delegate1.mtpositionsArray addObject:orderDict];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"positions" object:nil];
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}

-(void)PreLoginReply
{
    @try
    {
    BOOL isEnable = false,blLoginCheck = false,isBl2fa = false,isLock = false;
    short shTag;
    NSString * stText;
    //NSMutableArray * qandaArray = [[NSMutableArray alloc]initWithObjects:@"ans1",@"ans2",@"ans3",@"ans4",@"ans5",@"ans6", nil];
     delegate1.questionArray = [[NSMutableArray alloc]init];
    
    while ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
        shTag = [tagDecode NextTag];
        if(shTag == sharedManager.blLoginCkeck)
        {
            if([tagDecode CheckCapacity:sharedManager.shLongSize])
            {
                blLoginCheck = [tagDecode TagBool];
            }
        }
        
      else  if(shTag == sharedManager.stText)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                stText = [tagDecode TagString];
            }
        }
        
     else   if(shTag == sharedManager.bl2FA)
        {
            if([tagDecode CheckCapacity:sharedManager.shLongSize])
            {
                isBl2fa = [tagDecode TagBool];
                
                
            }
        }
        
     else   if(shTag == sharedManager.blEnable)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                isEnable = [tagDecode TagBool1];
                
           
                
            }
        }
        
  
        
     else   if(shTag == sharedManager.blLock)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                isLock = [tagDecode TagBool];
            }
        }
        
       else if(shTag == sharedManager.inSecurityQuestionNo)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                [delegate1.questionArray addObject:[NSNumber numberWithInt:[tagDecode TagInt]]];
                
                
    
                
            
            }
        }
        
        else
        {
            [tagDecode ReadTagDefault:shTag];
        }
        
        
  
       

    }
    
    
    
    if(stText.length!=0)
    {
        if([stText containsString:@"Successful"]&&loginTest==true)
        {
            loginTest=false;
            if(!isBl2fa)
            {
                
                [self login];
            }
            
            else
            {
                if(isEnable)
                {
                    TwoAnsViewController * two=[self.storyboard instantiateViewControllerWithIdentifier:@"TwoAnsViewController"];
                    
                    [self.navigationController pushViewController:two animated:YES];
                    
                    
                    
                    //         [self SecurityQuestions];
                    
                }
                
                else
                {
                    SixQuestionsViewController * six=[self.storyboard instantiateViewControllerWithIdentifier:@"SixQuestionsViewController"];
                    
                    [self.navigationController pushViewController:six animated:YES];
                    
                    //         [self SecurityQuestions];
                    
                }
                
            }
        }else if(loginTest==true)
        {
            loginTest=false;
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:stText preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    if([stText containsString:@"Password expire, Please change the password"])
                    {
                        self.passwordLbl.hidden=NO;
                        self.txtFld.hidden=NO;
                        self.view1.hidden=NO;
                    }
                    
                    
                }];
                
                [alert addAction:okAction];
           
        }
    }
    
    
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }

    
}

-(void)login
{
    //LoginRequest
    
    delegate1.mtCheck=true;
    TagEncode * tag1=[[TagEncode alloc]init];
    [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Login];
    [tag1 TagData:sharedManager.stUserID stringMethod:self.clientIDTF.text];
    if(self.txtFld.text.length>0)
    {
    [tag1 TagData:sharedManager.stPassword stringMethod:self.txtFld.text];
          [tag1 TagData:sharedManager.stNewPassword stringMethod:@""];
    }
    else
    {
        [tag1 TagData:sharedManager.stPassword stringMethod:self.passwordTF.text];
        [tag1 TagData:sharedManager.stNewPassword stringMethod:self.txtFld.text];
    }
  
    // [tag1 TagData:sharedManager.btTerminal byteMethod:m];
    [tag1 TagData:sharedManager.btTerminal byteMethod:3];
    [tag1 GetBuffer];
    [self newMessage];
}



//holdings//

-(void)holdingsDecode
    {
        @try
        {
        NSLog(@"Holdings!!!");
        short shTag;
        NSMutableDictionary * orderDict;
    
        orderDict=[[NSMutableDictionary alloc]init];
        while ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
            shTag = [tagDecode NextTag];
            
            
            if(shTag==sharedManager.blDelete)
            {
                
                if([tagDecode CheckCapacity:sharedManager.shLongSize])
                {
                    BOOL blDelete = [tagDecode TagBool];
                    NSLog(@"Bool");
                }
            }
            if(shTag == sharedManager.inRecordNo)
            {
                if([tagDecode CheckCapacity:sharedManager.shShortSize])
                {
                    int inRecordNo = [tagDecode TagInt];
                    NSString * local=[NSString stringWithFormat:@"%i",inRecordNo];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"inRecordNo"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inRecordNo"];
                    }
                    NSLog(@"inRecordNo:%d",inRecordNo);
                }
            }
            if (shTag== sharedManager.stClientIDHolding) {
                if ([tagDecode CheckCapacity:sharedManager.shIntSize]) {
                    NSString * stClientID = [tagDecode TagString];
                    NSLog(@"stClientID:%@",stClientID);
                }
            }
            
            if (shTag== sharedManager.stSymbolHolding) {
                if ([tagDecode CheckCapacity:sharedManager.shIntSize]) {
                    NSString * stSymbolHolding = [tagDecode TagString];
                    NSString * local=[NSString stringWithFormat:@"%@",stSymbolHolding];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"stSymbolHolding"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stSymbolHolding"];
                    }
                    NSLog(@"stSymbolHolding:%@",stSymbolHolding);
                }
            }
            
            if (shTag== sharedManager.stSeriesHolding) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    NSString * stSeriesHolding = [tagDecode TagString];
                    
                    NSString * local=[NSString stringWithFormat:@"%@",stSeriesHolding];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"stSeriesHolding"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stSeriesHolding"];
                    }
                    
                    NSLog(@"stSeriesHolding:%@",stSeriesHolding);
                }
            }
            
            if (shTag== sharedManager.inNetQtyHolding) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    int  inNetQtyHolding = [tagDecode TagInt];
                    
                    NSString * local=[NSString stringWithFormat:@"%i",inNetQtyHolding];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"inNetQtyHolding"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inNetQtyHolding"];
                    }
                    NSLog(@"inNetQtyHolding:%i",inNetQtyHolding);
                }
            }
            
            if (shTag== sharedManager.blIsCollateral) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    BOOL blIsCollateral = [tagDecode TagBool];
                    NSLog(@"blIsCollateral");
                }
            }
            
            if (shTag== sharedManager.InEvoluationMethod) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    int InEvoluationMethod = [tagDecode TagInt];
                    
                    NSString * local=[NSString stringWithFormat:@"%i",InEvoluationMethod];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"InEvoluationMethod"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"InEvoluationMethod"];
                    }
                    NSLog(@"InEvoluationMethod:%d",InEvoluationMethod);
                }
            }
            
            
            
            
            if (shTag== sharedManager.inCollateralQty) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    int inCollateralQty = [tagDecode TagInt];
                    NSString * local=[NSString stringWithFormat:@"%i",inCollateralQty];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"inCollateralQty"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inCollateralQty"];
                    }
                    NSLog(@"inCollateralQty:%d",inCollateralQty);
                }
            }
            
            if (shTag== sharedManager.InHairCut) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    int InHairCut = [tagDecode TagInt];
                    
                    
                    NSString * local=[NSString stringWithFormat:@"%i",InHairCut];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"InHairCut"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"InHairCut"];
                    }
                    
                   
                    NSLog(@"InHairCut:%d",InHairCut);
                }
            }
            
            if (shTag== sharedManager.inCollateralUpdateQty) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    int inCollateralUpdateQty = [tagDecode TagInt];
                    NSLog(@"inCollateralUpdateQty:%d",inCollateralUpdateQty);
                    NSString * local=[NSString stringWithFormat:@"%i",inCollateralUpdateQty];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"inCollateralUpdateQty"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inCollateralUpdateQty"];
                    }
                }
            }
            
            if (shTag== sharedManager.InHoldingUpdateQty) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    int InHoldingUpdateQty = [tagDecode TagInt];
                    NSLog(@"InHoldingUpdateQty:%d",InHoldingUpdateQty);
                    
                    NSString * local=[NSString stringWithFormat:@"%i",InHoldingUpdateQty];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"InHoldingUpdateQty"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"InHoldingUpdateQty"];
                    }
                }
            }
            
            if (shTag== sharedManager.stProduct) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    NSString* stProduct = [tagDecode TagString];
                    NSLog(@"InHoldingUpdateQty:%@",stProduct);
                    
                    NSString * local=[NSString stringWithFormat:@"%@",stProduct];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"stProduct"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stProduct"];
                    }
                }
            }
            
            if (shTag== sharedManager.stSecurityIDHolding) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    NSString* stSecurityIDHolding = [tagDecode TagString];
                    NSLog(@"stSecurityIDHolding:%@",stSecurityIDHolding);
                    
//                    NSString * security2=[stSecurityIDHolding stringByReplacingOccurrencesOfString:@"Value 20" withString:@""];
//                     NSLog(@"stSecurityIDHolding:%@",security2);
//
//                  NSString * security1=   [security2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//
//                    NSString *newString = [security1 substringToIndex:[security1 length]-1];
//
//                    NSString * local=[NSString stringWithFormat:@"%@",newString];
                    

                    
                    NSArray * seperatedArray=[stSecurityIDHolding componentsSeparatedByCharactersInSet:[NSCharacterSet letterCharacterSet]];
                    
                    
                    NSString * local=[seperatedArray objectAtIndex:0];
                  
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"stSecurity"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"stSecurity"];
                    }
                }
            }
            
            
            if (shTag== sharedManager.dbClosingPrice) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    double dbClosingPrice1 = [tagDecode TagDouble];
                    NSLog(@"dbClosingPrice:%f",dbClosingPrice1);
                    
                    NSString * local=[NSString stringWithFormat:@"%.2f",dbClosingPrice1];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"dbClosingPrice"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"dbClosingPrice"];
                    }
                }
            }
            
            
            if (shTag== sharedManager.inWithheldHoldingQty) {
                if ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
                    int inWithheldHoldingQty = [tagDecode TagInt];
                    NSLog(@"inWithheldHoldingQty:%d",inWithheldHoldingQty);
                    
                    NSString * local=[NSString stringWithFormat:@"%i",inWithheldHoldingQty];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"inWithheldHoldingQty"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"inWithheldHoldingQty"];
                    }
                }
            }
            
            if (shTag== sharedManager.intWithheldCollateralQty) {
                if ([tagDecode CheckCapacity:sharedManager.shCharSize]) {
                    int intWithheldCollateralQty = [tagDecode TagInt];
                    NSLog(@"intWithheldCollateralQty:%d",intWithheldCollateralQty);
                    
                    NSString * local=[NSString stringWithFormat:@"%i",intWithheldCollateralQty];
                    
                    if([local isEqualToString:@""])
                    {
                        
                        [orderDict setObject:@"0" forKey:@"intWithheldCollateralQty"];
                    }
                    
                    else
                    {
                        
                        [orderDict setObject:local forKey:@"intWithheldCollateralQty"];
                    }
                }
            }
            
           
        }
          [delegate1.mtHoldingArray addObject:orderDict];
        
       
        
        } @catch (NSException *exception) {
            NSLog(@"Caught Exception");
        } @finally {
            NSLog(@"Finally");
        }
        
//        NSLog(@"%@",orderDict);
        
       
    }

-(void)fundsDecode
{
    NSString * stClientID,*stSegmentName,*stUserID;
    double dbBalance,dbAdhocBalance,dbHaircutbalance,dbCarryBalance,dbIntraDayBalance,dbCCEBalance,dbMMEBalance,dbCCEUtilize,dbMMEUtilize,dbSpanMargin,dbExposureMargin,dbPremium;
    NSMutableDictionary * fundsDictionary = [[NSMutableDictionary alloc]init];
    short shTag;
    while ([tagDecode CheckCapacity:sharedManager.shShortSize]) {
        shTag = [tagDecode NextTag];
        if(shTag == sharedManager.stUserID)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                stUserID = [tagDecode TagString];
                NSString * client = [NSString stringWithFormat:@"%@",stUserID];
                if([client isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"N/A" forKey:@"stUserID"];
                }else
                {
                    [fundsDictionary setObject:client forKey:@"stUserID"];
                }
            }
        }
        if(shTag == sharedManagerFunds.stClientID)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                stClientID = [tagDecode TagString];
                NSString * client = [NSString stringWithFormat:@"%@",stClientID];
                if([client isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"N/A" forKey:@"stClientID"];
                }else
                {
                    [fundsDictionary setObject:client forKey:@"stClientID"];
                }
            }
        }
        if(shTag == sharedManagerFunds.stSegmentName)
        {
            if([tagDecode CheckCapacity:sharedManager.shShortSize])
            {
                stSegmentName = [tagDecode TagString];
                NSString * local = [NSString stringWithFormat:@"%@",stSegmentName];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"N/A" forKey:@"stSegmentName"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"stSegmentName"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbBalance)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbBalance = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbBalance];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbBalance"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbBalance"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbAdhocBalance)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbAdhocBalance = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbAdhocBalance];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbAdhocBalance"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbAdhocBalance"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbHaircutBalance)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbHaircutbalance = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbHaircutbalance];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbHaircutbalance"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbHaircutbalance"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbCarryBalance)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbCarryBalance = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbCarryBalance];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbCarryBalance"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbCarryBalance"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbIntraDayBalance)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbIntraDayBalance = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbIntraDayBalance];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbIntraDayBalance"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbIntraDayBalance"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbCCEBalance)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbCCEBalance = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbCCEBalance];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbCCEBalance"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbCCEBalance"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbMMEBalance)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbMMEBalance = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbMMEBalance];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbMMEBalance"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbMMEBalance"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbCCEUtilize)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbCCEUtilize = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbCCEUtilize];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbCCEUtilize"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbCCEUtilize"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbMMEUtilize)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbMMEUtilize = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbMMEUtilize];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbMMEUtilize"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbMMEUtilize"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbSpanMargin)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbSpanMargin = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbSpanMargin];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbSpanMargin"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbSpanMargin"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbExposureMargin)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbExposureMargin = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbExposureMargin];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbExposureMargin"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbExposureMargin"];
                }
            }
        }
        if(shTag == sharedManagerFunds.dbPremium)
        {
            if([tagDecode CheckCapacity:sharedManager.shDoubleSize])
            {
                dbPremium = [tagDecode TagDouble];
                NSString * local = [NSString stringWithFormat:@"%f",dbPremium];
                if([local isEqualToString:@""])
                {
                    [fundsDictionary setObject:@"0.0" forKey:@"dbPremium"];
                }else
                {
                    [fundsDictionary setObject:local forKey:@"dbPremium"];
                }
            }
        }
//        }else
//        {
//            [tagDecode ReadTagDefault:shTag];
//        }
    }
    [delegate1.mtFundsArray addObject:fundsDictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"funds" object:nil];
}



//01-04 11:39:01.379 12571-12571/com.zenwise.zambalafollower I/System.out: First Socket shMsgCode 190SurveillanceReportResponse
//01-04 11:39:01.388 12571-12571/com.zenwise.zambalafollower I/System.out: First Socket shMsgCode 188stClientID
//01-04 11:39:01.389 12571-12571/com.zenwise.zambalafollower I/System.out: dbBalance5.067218E-317
//01-04 11:39:01.389 12571-12571/com.zenwise.zambalafollower I/System.out: dbHaircutBalance5.067597E-317
//01-04 11:39:01.389 12571-12571/com.zenwise.zambalafollower I/System.out: dbIntraDayBalance5.06785E-317
//01-04 11:39:01.390 12571-12571/com.zenwise.zambalafollower I/System.out: dbCCEUtilize2.1926956517092288E288
//01-04 11:39:01.390 12571-12571/com.zenwise.zambalafollower I/System.out: dbMMEUtilize5.0663324E-317
//01-04 11:39:01.390 12571-12571/com.zenwise.zambalafollower I/System.out: dbExposureMargin5.0616526E-317



- (IBAction)resetBtn:(id)sender {
    self.passwordLbl.hidden=NO;
    self.txtFld.hidden=NO;
    self.view1.hidden=NO;
}

- (void)order:(NSNotification *)note
{
    [self newMessage];
}
@end
