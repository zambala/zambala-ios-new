//
//  TwitterView.m
//  
//
//  Created by zenwise mac 2 on 11/9/17.
//

#import "TwitterView.h"

@interface TwitterView ()

@end

@implementation TwitterView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    TWTRAPIClient *APIClient = [[TWTRAPIClient alloc] init];
    self.dataSource = [[TWTRListTimelineDataSource alloc] initWithListSlug:@"ZambalaIndia" listOwnerScreenName:@"myzambala" APIClient:APIClient];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
