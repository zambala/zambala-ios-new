//
//  WisdomPortfolioCell.h
//  betazambalafollower
//
//  Created by zenwise technologies on 25/03/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WisdomPortfolioCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (strong, nonatomic) IBOutlet UIView *quantityLabel;
@property (strong, nonatomic) IBOutlet UILabel *ltpLabel;
@property (strong, nonatomic) IBOutlet UILabel *changePercentLabel;

@property (strong, nonatomic) IBOutlet UILabel *quantityLabelNew;

@end
