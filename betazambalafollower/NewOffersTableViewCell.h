//
//  NewOffersTableViewCell.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/19/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewOffersTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *pricelbl;

@property (weak, nonatomic) IBOutlet UILabel *disountLbl;
@property (weak, nonatomic) IBOutlet UILabel *finalPriceLbl;

@property (weak, nonatomic) IBOutlet UIButton *monthSelectionBtn;


@end
