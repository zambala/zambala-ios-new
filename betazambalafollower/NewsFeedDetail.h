//
//  NewsFeedDetail.h
//  ZambalaTest
//
//  Created by zenwise technologies on 14/12/16.
//  Copyright © 2016 zenwise technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFeedDetail : UIViewController

{
}

@property (copy, nonatomic) NSString *nameString;
@property (copy, nonatomic) NSString *descripString;
@property (copy, nonatomic) NSString *timeString;


@property(nonatomic,retain)NSMutableArray *imgArray, *imgArray1;


@property (strong, nonatomic) IBOutlet UIImageView *myImageView;


@property (strong, nonatomic) IBOutlet UILabel *nameLbl, *descriptionLbl, *timeLbl;
@property (strong, nonatomic) IBOutlet  UIImageView *ImgView;


@end
