//
//  NewLoginViewController.h
//  
//
//  Created by zenwise mac 2 on 8/28/17.
//
//

#import <UIKit/UIKit.h>

@protocol MyProtocol <NSObject>

- (void)orderStatusAlertMethod;

@end

@interface NewLoginViewController : UIViewController<NSStreamDelegate>
@property (nonatomic, weak) id<MyProtocol> delegate;
@property (weak, nonatomic) IBOutlet UITextField *clientIDTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
- (IBAction)onContinueButtonTap:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIImageView *brokerImageView;
-(void)newMessage;
-(void)OrderDecode;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UILabel *passwordLbl;
@property (weak, nonatomic) IBOutlet UITextField *txtFld;

- (IBAction)resetBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@end
