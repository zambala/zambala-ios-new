//
//  PortfolioDepthNamesTableViewCell.h PortfolioDepthNamesTableViewCell.h
//  betazambalafollower
//
//  Created by zenwise technologies on 28/06/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioDepthNamesTableViewCell_h_PortfolioDepthNamesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *DepthCompanyLabelName;
@property (strong, nonatomic) IBOutlet UIButton *colorButton;
@property (strong, nonatomic) IBOutlet UILabel *allocationPercentLabel;

@end
