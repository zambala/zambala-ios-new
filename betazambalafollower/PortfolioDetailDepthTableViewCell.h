//
//  PortfolioDetailDepthTableViewCell.h
//  betazambalafollower
//
//  Created by zenwise technologies on 12/06/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioDetailDepthTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *companyNamelabel;
@property (strong, nonatomic) IBOutlet UILabel *percentageLabel;
@property (strong, nonatomic) IBOutlet UILabel *LTPLabel;
@property (strong, nonatomic) IBOutlet UILabel *changePercentageLabel;
@property (strong, nonatomic) IBOutlet UILabel *quantityLabel;

@end
