//
//  ProfileSettings.h
//  testing
//
//  Created by zenwise technologies on 19/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJSecondDetailViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface ProfileSettings : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,MJSecondPopupDelegate>
{
     UIButton * tickerBtn;
     UIButton * notificationBtn;
    
    
    BOOL searchFlag, filterFlag, equityFlag, deriFlag, currencyFlag, commFlag, tradeFlag, shortTermFlag, longTermFlag,dealerFlag;
    
    BOOL starOneFlag, starTwoFlag, starThreeFlag, starFourFlag, starFiveFlag;
    

    
}
@property (strong, nonatomic) IBOutlet UIButton *euityBtn;

- (IBAction)pickingImageButton:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

- (IBAction)showingMainMenu:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *baseView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property (weak, nonatomic) IBOutlet UIButton *leaderAdviceBtn;


@property (weak, nonatomic) IBOutlet UIButton *marketAlertBtn;


@property (weak, nonatomic) IBOutlet UIButton *triggeralertBtn;
@property (weak, nonatomic) IBOutlet UIButton *orderalertBtn;


@property (weak, nonatomic) IBOutlet UIButton *stopAlertBtn;


@property (weak, nonatomic) IBOutlet UIScrollView *tadeScrolView;

@property (strong, nonatomic) IBOutlet UIView *equImgBtn;

@property (strong, nonatomic) IBOutlet UIButton *deriImgBtn;


@property (strong, nonatomic) IBOutlet UIButton *currImgBtn;


@property (strong, nonatomic) IBOutlet UIButton *commImgBtn;











@end
