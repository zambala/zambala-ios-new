//
//  TopPicks.m
//  PayPage
//
//  Created by zenwise mac 2 on 12/22/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "TopPicks.h"
#import "TopPicksCell.h"
#import "AppDelegate.h"
#import "StockView1.h"
#import "StockOrderView.h"
#import "NewProfile.h"
#import "NoTopicksCell.h"

@interface TopPicks ()
{
    AppDelegate * delegate1;
}

@end

@implementation TopPicks
{
    NSString * EPString;
    NSString * TPString;
    NSString * SLString;
    NSString * buysell;
    NSString * messageStr;
    NSArray * segmentsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    
    
    
  
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [self.activityIndicator startAnimating];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
    
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    [self topPicksServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)topPicksServer
{
//    self.activityIndicator.hidden=NO;
//    [self.activityIndicator startAnimating];
    segmentsArray=[[NSArray alloc]init];
    
    segmentsArray=[delegate1.brokerInfoDict objectForKey:@"segment"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateStyle: NSDateFormatterShortStyle];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *date = [dateFormat stringFromDate:[NSDate date]];
    
    @try {
        NSDictionary *headers = @{
                                   @"cache-control": @"no-cache",
                                    @"content-type": @"application/json"
                                   };
        
        NSString * localString=[NSString stringWithFormat:@"%@follower/toppicks",delegate1.baseUrl];
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localString]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:30.0];
        NSDictionary *    parameters;
        parameters = @{
                          @"clientid":delegate1.userID,
                          @"messagetypeid":@1,
                          @"limit":@2000000,
                          @"sortby":@"DESC",
                          @"orderby":@"changepercent",
                          @"closed":@(false),
                          @"segment":segmentsArray,
                          @"active":@(true),
                          };
            
      
     NSData *   postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            self.topPicksResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            NSLog(@"Top picks response array:%@",self.topPicksResponseArray);
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            self.topPicksTbl.delegate=self;
                                                            self.topPicksTbl.dataSource=self;
                                                            [self.topPicksTbl reloadData];
                                                            
                                                            [self.activityIndicator stopAnimating];
                                                            
                                                            self.activityIndicator.hidden=YES;
                                                            
                                                        });
                                                    }];
        [dataTask resume];

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }

   }

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([[self.topPicksResponseArray objectForKey:@"data"] count]>0)
    {
    return [[self.topPicksResponseArray objectForKey:@"data"] count];
    }
    
    else
    {
        
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([[self.topPicksResponseArray objectForKey:@"data"] count]>0)
    {
    
    TopPicksCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    cell1.oLabel.layer.cornerRadius = 12.5f;
    cell1.oLabel.layer.masksToBounds = YES;
    cell1.stLabel.layer.cornerRadius = 12.5f;
    cell1.stLabel.layer.masksToBounds = YES;

    
    
    [cell1.followBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
     [cell1.buySellButton addTarget:self action:@selector(yourButtonClicked1:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSString * public=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"ispublic"]];
        int publicInt=[public intValue];
    
       // NSString * sub=[NSString stringWithFormat:@"%@",[[self.topPicksResponseArray objectAtIndex:indexPath.row] objectForKey:@"subscriptiontypeid"]];
        
        NSString * sub = [NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"subscriptiontypeid"]];
        
        int subInt=[sub intValue];
        
        if(subInt==1)
        {
            cell1.premiumLabel.hidden=YES;
            cell1.premiumImageView.hidden=YES;
        }
        
        else if(subInt==2)
        {
            cell1.premiumLabel.hidden=NO;
            cell1.premiumImageView.hidden=NO;
        }
        
        if(publicInt==0)
        {
            cell1.premiumLabel.hidden=YES;
            cell1.premiumImageView.hidden=YES;
            cell1.premiumImageView.image=[UIImage imageNamed:@"premiumflagFlipped"];
            cell1.premiumLabel.text=@"Premium";
            cell1.premiumLabel.textColor = [UIColor blackColor];
        }else if (publicInt ==1)
        {
            cell1.premiumLabel.hidden=NO;
            cell1.premiumImageView.hidden=NO;
            cell1.premiumImageView.image=[UIImage imageNamed:@"publicflag"];
            cell1.premiumLabel.text=@"Public Advice";
            cell1.premiumLabel.textColor = [UIColor whiteColor];
        }
        NSString * mainString=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"]];
        
    if([mainString containsString:@"@"])
    {
        cell1.valueChangeLabel.text=[mainString uppercaseString];
    }
    
    else
    {
        
        TPString=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"targetprice"]];
        
        if ([mainString isEqual:[NSNull null]]||mainString==nil||[mainString isEqualToString:@"<null>"] ||  [TPString isEqual:[NSNull null]]||TPString==nil||[TPString isEqualToString:@"<null>"]   ) {
            
        }
        
        else{
            
            @try {
                
                NSString * buttonNumber = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"buysell"];
                int buttonTitle = [buttonNumber intValue];
                
                if(buttonTitle==1)
                {
                    buysell=@"BUY";
                    
                    [cell1.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                    
                    [cell1.buySellButton setTitle:buysell forState:UIControlStateNormal];
                    
                }
                
                else if(buttonTitle==2)
                {
                    buysell=@"SELL";
                    
                    [cell1.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                    
                    [cell1.buySellButton setTitle:buysell forState:UIControlStateNormal];
                    
                    
                    
                    
                }
                
                
                
                
                NSLog(@"main String %@",mainString);
                
                
                
                NSArray * mainArray=[mainString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                
                
                NSLog(@"array  %@",[mainArray objectAtIndex:0]);
                
                NSLog(@"array  %@",[mainArray objectAtIndex:1]);
                
               
                
               
                
              
                NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
                
                
                
                NSArray * companyArray=[string componentsSeparatedByString:@";"];
                
                
                
                NSLog(@" array1 %@",companyArray);
                
                
                
                //               for (int i=1; i<[companyArray count];i++) {
                
                
                
                //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                //
                //
                //
                //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
                //
                //                   NSLog(@"%@",array2);
                
                NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
                NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
                NSString * SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
                NSArray * EPArray=[EP1 componentsSeparatedByString:@"="];
                NSArray * TPArray=[TP componentsSeparatedByString:@"="];
                NSArray * SLArray=[SL componentsSeparatedByString:@"="];
                
                EPString=[NSString stringWithFormat:@"%@",[EPArray objectAtIndex:1]];
                TPString=[NSString stringWithFormat:@"%@",[TPArray objectAtIndex:1]];
                SLString=[NSString stringWithFormat:@"%@",[SLArray objectAtIndex:1]];
                
                
                
                NSString * dummyfinalStr=[NSString stringWithFormat:@"%@ %@ @%@%@%@",buysell,companyName,EP1,TP,SL];
                
                NSString * finalStr = [dummyfinalStr stringByReplacingOccurrencesOfString:@"=" withString:@" "];
                
                NSLog(@"%@",finalStr);
                
                NSLog(@"COMPANY %@",companyName);
                
                NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",buysell];
                NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[companyArray objectAtIndex:0]];
                NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
                NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPString];
                NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
                NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPString];
                NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
                NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLString] ;
                
                NSString * string1 = [sell stringByAppendingString:muthootfin];
                NSString *string2 = [string1 stringByAppendingString:ep];
                NSString * string3 = [string2 stringByAppendingString:value1];
                NSString * string4 = [string3 stringByAppendingString:tp];
                NSString * string5 = [string4 stringByAppendingString:value2];
                NSString * string6 = [string5 stringByAppendingString:sl];
                NSString * finalString = [string6 stringByAppendingString:value3];
                
                
                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                
                //UILabel * myLabel = [[UILabel alloc] init];
                // myLabel.attributedText = attrStr;
                cell1.valueChangeLabel.attributedText=attrStr;
                
            }
            
            
            
            @catch (NSException * e) {
                NSLog(@"Exception: %@", e);
            }
            @finally {
                NSLog(@"finally");
                
            }
            
        }
        
        
    }
    
    
                NSString * buttonNumber = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"buysell"];
                int buttonTitle = [buttonNumber intValue];
                
                if(buttonTitle==1)
                {
                    buysell=@"BUY";
                    
                    [cell1.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                    
                    [cell1.buySellButton setTitle:buysell forState:UIControlStateNormal];
                    
                }
                
                else if(buttonTitle==2)
                {
                    buysell=@"SELL";
                    
                    [cell1.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                    
                    [cell1.buySellButton setTitle:buysell forState:UIControlStateNormal];
                    
                    
                    
                    
                }
                
                
    
    
        
            
        
                
                
                //                              if (i==1) {
                //                                  EP=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                //
                //                                  NSLog(@"EP %@",[array2 objectAtIndex:1]);
                //
                //                              }
                
                //                   if (i==2) {
                //                       EPString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                //
                //                       NSLog(@"TP %@",[array2 objectAtIndex:1]);
                //
                //                   }
                //
                //                   if (i==3) {
                //                       SLString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                //
                //                       NSLog(@"SL %@",[array2 objectAtIndex:1]);
                //
                //                   }
                //               }
                //
            
    cell1.profileNameLabel.text=@"nill";
    cell1.dateAndTimeLabel.text=@"nill";
            
            @try
            {
                NSString * profileName = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"];
                NSString * lastName = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lastname"];
                
                if([lastName isEqual:[NSNull null]])
                {
                    lastName=@"";
                }else
                {
                    lastName =lastName;
                }
                NSString * name = [profileName stringByAppendingString:lastName];

                NSString * analystName = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderorgname"];
                if(publicInt==0)
                {
                cell1.profileNameLabel.text = profileName;
                }else if (publicInt ==1)
                {
                    if([analystName containsString:profileName])
                    {
                        cell1.followerCountLabel.text=analystName;
                    }else
                    {
                        cell1.followerCountLabel.text=analystName;
                    }
                }
                
                NSString * dateStr=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"];
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
                
                // change to a readable time format and change to local time zone
                [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                NSString *finalDate = [dateFormatter stringFromDate:date];
                
                
                
                
                
                cell1.dateAndTimeLabel.text=finalDate;
                

            }
            @catch (NSException * e) {
                NSLog(@"Exception: %@", e);
            }
            @finally {
                NSLog(@"finally");
            }
    cell1.stLabel.text=@"nill";
            
    @try {
        
        NSString * duration=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"durationtype"]];
        
        int durationInt=[duration intValue];
        
        if(durationInt==1)
        {
            cell1.stLabel.text=@"DT";
            
            cell1.stLabel.backgroundColor=[UIColor colorWithRed:(255/255.0) green:(192/255.0) blue:(102/255.0) alpha:1];
        }
        
        else if(durationInt==2)
        {
            cell1.stLabel.text=@"ST";
            
            cell1.stLabel.backgroundColor=[UIColor colorWithRed:(165/255.0) green:(225/255.0) blue:(241/255.0) alpha:1];
        }
        else if(durationInt==3)
        {
            
            
            
            cell1.stLabel.text=@"LT";
            
            cell1.stLabel.backgroundColor=[UIColor colorWithRed:(227/255.0) green:(168/255.0) blue:(246/255.0) alpha:1];
            
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
            
            
            // NSString * followerCount = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@""];
            
           
            
            if([[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row ]objectForKey:@"lasttradedprice"] isEqual:[NSNull null]])
            {
                
                cell1.LTPLabel.text=@"0";
                
                
            }else
            {
                
                 NSString * ltp =[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
            
            cell1.LTPLabel.text = ltp;
                
            }
            
            @try {
                NSString * changePercentlabel = [NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
                
                NSString * percent =@"  ";
                NSString * finalChangePercentlabel = [changePercentlabel stringByAppendingString:percent];
                if([changePercentlabel isEqual:[NSNull null]]||changePercentlabel==nil||[changePercentlabel isEqualToString:@"<null>"])
                {
                    
                    
                    
                    NSString * percentage = @" ";
                    NSString * zero = @"0  ";
                    NSString* final = [zero stringByAppendingString:percentage];
                    
                    cell1.changePercentLabel.text=final;
                    cell1.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                else
                {
                    
                    if([finalChangePercentlabel containsString:@"-"])
                    {
                        cell1.changePercentLabel.text=finalChangePercentlabel;
                        cell1.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        
                         cell1.oLabel.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                    }
                    else{
                        cell1.changePercentLabel.text=finalChangePercentlabel;
                        cell1.changePercentLabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                         cell1.oLabel.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                    }
                    
                    
                }

            } @catch (NSException *exception) {
                
            } @finally {
                
            }
             cell1.gainPercent.text=@"0  ";
            
            @try {
                if([[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"] isEqual:[NSNull null]])
                {
                    cell1.gainPercent.text=@"0%";
                    
                    
                }else
                {
                    NSString * gain=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"];
                    
                    float gainFloat=[gain floatValue];
                    
                    NSString * percent=@"%";
                    
                    cell1.gainPercent.text=[NSString stringWithFormat:@"%.2f%@",gainFloat,percent];
                }

            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
//             cell1.followerCountLabel.text=@"0 Followers";
            @try {
                NSString *followersString = [NSString stringWithFormat:@"%@ Followers",[[[self.topPicksResponseArray objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
                
                
                NSString * testString = [[[self.topPicksResponseArray objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"followercount"];
                
                NSString * profileName = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"];
                NSString * lastName = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lastname"];
                if([lastName isEqual:[NSNull null]])
                {
                    lastName=@"";
                }else
                {
                    lastName = lastName;
                }
                
                NSString * name = [profileName stringByAppendingString:lastName];
                NSString * analystName = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderorgname"];
                
                if(publicInt==0)
                {
                
                if([testString isEqual:[NSNull null]])
                {
                    cell1.followerCountLabel.text=@"0 Followers";
                }
                else
                {
                    cell1.followerCountLabel.text=followersString;
                }
                }else if(publicInt==1)
                {
                    if([analystName containsString:profileName])
                    {
                    
                    cell1.profileNameLabel.text =@"";
                    }else
                    {
                        cell1.profileNameLabel.text=name;
                    }
                }
                

            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
        // NSString * subscribedString=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"subscribed"]];
        
        
        int subscribedInt =[[[[self.topPicksResponseArray objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"subscribed"] intValue];
        
        
        //  NSNumber * subscribedNumber = [NSNumber numberWithInt:subscribedInt];
        
        if(subscribedInt==1)
        {
            [cell1.followBtn setTitle:@"Following" forState:UIControlStateNormal];
            
        }else if (subscribedInt==0)
        {
            [cell1.followBtn setTitle:@"Follow +" forState:UIControlStateNormal];
        }
        
        //    if([subscribedString isEqualToString:@"false"])
        //    {
        //        [cell1.followBtn setTitle:@"Follow +" forState:UIControlStateNormal];
        //    }
        //    
        //    else if([subscribedString isEqualToString:@"true"])
        //    {
        //        [cell1.followBtn setTitle:@"Following" forState:UIControlStateNormal];
        //    }

       
       [cell1.topPicksProfileButton addTarget:self action:@selector(topPicksleaderProfileMethod:) forControlEvents:UIControlEventTouchUpInside];
     cell1.actedByLabel.text=@"0";
            
            @try {
                
                NSString * actedBy = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"actedby"];
                
                
                if([actedBy isEqual:[NSNull null]])
                {
                    cell1.actedByLabel.text=@"0";
                }else
                {
                    int actedByInt=[actedBy intValue];
                    cell1.actedByLabel.text = [NSString stringWithFormat:@"%i",actedByInt];
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
    cell1.sharesSoldLabel.text = @"0";
            
            @try {
                
                NSString * sharesSold = [[[self.topPicksResponseArray
                                           objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"sharesold"];
                
                
                
                if([sharesSold isEqual:[NSNull null]])
                {
                    cell1.sharesSoldLabel.text = @"0";
                }else
                {
                    int sharesSoldInt=[sharesSold intValue];
                    cell1.sharesSoldLabel.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
    
     cell1.averageProfitLabel.text = @"0";
            @try {
                
                NSString * averageProfit = [[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"averageprofit"];
                
                
                
                if([averageProfit isEqual:[NSNull null]])
                {
                    cell1.averageProfitLabel.text = @"0";
                }else
                {
                    int sharesSoldInt=[averageProfit intValue];
                    cell1.averageProfitLabel.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
        
       
        
        if(publicInt==0)
        {
            cell1.followBtn.hidden=NO;
            cell1.profileImgView.image=[UIImage imageNamed:@"userimage.png"];
            
            NSString * logo=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"logourl"];
            
            if([logo isEqual:[NSNull null]])
            {
                cell1.profileImgView.image=[UIImage imageNamed:@"userimage.png"];
            }
            
            else
            {
                
                NSURL *url = [NSURL URLWithString:logo];
                
                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (data) {
                        UIImage *image = [UIImage imageWithData:data];
                        if (image) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                               
                                if (cell1)
                                    cell1.profileImgView.image = image;
                            });
                        }
                    }
                }];
                [task resume];
                
            }
        }else if (publicInt==1)
        {
            cell1.followBtn.hidden=YES;
            NSURL *url;
            @try {
                url =  [NSURL URLWithString:@"https://s3-ap-southeast-1.amazonaws.com/elasticbeanstalk-ap-southeast-1-507357262371/Leaders/public_advice_user_logo.png"];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (cell1)
                                cell1.profileImgView.image = image;
                        });
                    }
                }
            }];
            [task resume];
        }
        
        if(publicInt==0)
        {
            cell1.sourceLabel.hidden=YES;
            cell1.sorceDataLabel.hidden=YES;
        }else if (publicInt==1)
        {
            cell1.sourceLabel.hidden=NO;
            cell1.sorceDataLabel.hidden=NO;
            NSString * source = [NSString stringWithFormat:@"%@",[[[[self.topPicksResponseArray objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"otherinfo"]objectForKey:@"source"]];
            cell1.sorceDataLabel.text = source;
        }
        
            


    
    return cell1;
        
    }
    
    else
    {
        
          NoTopicksCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"notoppicks" forIndexPath:indexPath];
        
        return cell2;
    }
            
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if(tableView==self.topPicksTbl)
    {
        if([[self.topPicksResponseArray objectForKey:@"data"] count]>0)
        {
            return 192;
        }
        
        else
        {
        
           return 335;
        }
        
    }
    return 0;

}

-(void)yourButtonClicked:(UIButton*)sender
{
    //    if (sender.tag == 0)
    //    {
    //
    //    }
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.topPicksTbl];
    
    
    
    NSIndexPath *indexPath = [self.topPicksTbl indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"Index path:%ld",(long)indexPath.row);
    TopPicksCell *cell = [self.topPicksTbl cellForRowAtIndexPath:indexPath];
    
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
                
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    
    
    @try {
        NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                                   @"cache-control": @"no-cache",
                                   };
        
        NSString * userId=[NSString stringWithFormat:@"leaderid=%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
        
        NSString * clientId=[NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
        
        
        NSMutableData *postData = [[NSMutableData alloc] initWithData:[userId dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[clientId dataUsingEncoding:NSUTF8StringEncoding]];
        //    if(segmentedControl.selectedSegmentIndex==0)
        //    {
        //         [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
        //    }
        //    else
        //    {
        //    if(self.buttonArray.count>0)
        //    {
        //    if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Follow +"])
        //    {
        //    [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
        //    }else if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Following"])
        //    {
        //        [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
        //
        //    }
        //    }
        //    }
        
        NSArray * subscribeArray=@[@1];
        
        NSString * subscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",subscribeArray];
        NSString * sub = [subscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
        NSString * sub1 = [sub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
        
        NSArray * unSubscribeArray=@[];
        
        NSString * unSubscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",unSubscribeArray];
        NSString * unSub = [unSubscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
        NSString * unSub1 = [unSub stringByReplacingOccurrencesOfString:@")" withString:@"}"];

        
        NSString * btnTitle=[NSString stringWithFormat:@"%@",cell.followBtn.currentTitle];
        
        
        if([btnTitle isEqualToString:@"Following"])
            
        {
            [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[unSub1 dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        else if ([btnTitle isEqualToString:@"Follow +"])
        {
            [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[sub1 dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            NSLog(@"%@",followDict);
                                                            
                                                            messageStr=[followDict objectForKey:@"message"];
                                                            
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                            if([messageStr isEqualToString:@"Subscriber created"])
                                                            {
                                                                //                                                            [cell.followBtn setTitle:[self.buttonArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                                                                [self.topPicksTbl reloadData];
                                                                
                                                                [self topPicksServer];
                                                            }
                                                        });
                                                        
                                                        
                                                        
                                                    }];
        [dataTask resume];
        
        

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    delegate1.navigationCheck=@"TOP";

    
    
   
        if(delegate1.leaderAdviceDetails.count>0)
        {
            [delegate1.leaderAdviceDetails removeAllObjects];
        }
        //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
        //    {
        //
        //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
        //        {
        //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        //
        //            [self.navigationController pushViewController:allocation animated:YES];
        //
        //        }
        
        //        else{
        delegate1.depth=@"WISDOM";
        
        NSLog(@"%@",delegate1.depth);
    
    NSString * public=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"ispublic"]];
    int publicInt=[public intValue];
    if(publicInt==0)
    {
        
        StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
    
    @try {
        delegate1.symbolDepthStr=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
        
        delegate1.exchaneLblStr=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
        
          delegate1.instrumentDepthStr=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"instrumentid"];
        
          delegate1.expirySeriesValue=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"expirydate"];
        
        delegate1.expirySeriesValue=@"";
        
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
        
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
        
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
        
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageid"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"buysell"]];
        NSString * logo=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"logourl"];
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        
        NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        
        [self.navigationController pushViewController:stockDetails animated:YES];

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    }else if (publicInt==1)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"This is a publicly available information from the above source. All users are advised, in their own interests, to seek and obtain advice that is specific or tailored to their own circumstances and that any reliance by any user on any such publicly available advice is entirely at the risk and consequence of such user, for which Zenwise Technologies Private Limited under the brand name Zambala is not responsible or liable." preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            [self presentViewController:alert animated:YES completion:^{
                
                
                
            }];
            
            
            
            UIAlertAction * proceedAction=[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
               
                StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
                
                @try {
                    delegate1.symbolDepthStr=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
                    
                    delegate1.exchaneLblStr=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
                    
                    delegate1.instrumentDepthStr=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"instrumentid"];
                    
                    delegate1.expirySeriesValue=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"expirydate"];
                    
                    delegate1.expirySeriesValue=@"";
                    
                    [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
                    [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
                    [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
                    
                    [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
                    
                    [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"]];
                    [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
                    
                    [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageid"]];
                    [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"buysell"]];
                    NSString * logo=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"logourl"];
                    if([logo isEqual:[NSNull null]])
                    {
                        [delegate1.leaderAdviceDetails addObject:@"no"];
                    }
                    
                    else
                    {
                        [delegate1.leaderAdviceDetails addObject:logo];
                    }
                    
                    NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                    
                    [self.navigationController pushViewController:stockDetails animated:YES];
                    
                    
                }
                @catch (NSException * e) {
                    NSLog(@"Exception: %@", e);
                }
                @finally {
                    NSLog(@"finally");
                }
            }];
            
            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                
            }];
            
            
            
            
            [alert addAction:proceedAction];
            [alert addAction:cancelAction];
            
        });
        
    }
    
    
    
        
}

-(void)topPicksleaderProfileMethod:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.topPicksTbl];
    
    
    
    NSIndexPath *indexPath = [self.topPicksTbl indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"Index path:%ld",(long)indexPath.row);
    
    
    TopPicksCell *cell = [self.topPicksTbl cellForRowAtIndexPath:indexPath];
    NSString * public=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"ispublic"]];
    int publicInt=[public intValue];
    
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
       if(publicInt==1)
       {
           
       }else if (publicInt==0)
       {
        

    
    @try {
        if([[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"portfoliojson"] isEqual:[NSNull null]])
        {
            
            delegate1.leaderIDWhoToFollow=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"leaderid"];
        }
        
        NewProfile * new=[self.storyboard instantiateViewControllerWithIdentifier:@"NewProfile"];
        
        [self.navigationController pushViewController:new animated:YES];

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
        
    }
    }
    
}


-(void)yourButtonClicked1:(UIButton*)sender
{
    delegate1.navigationCheck=@"TOP";
    delegate1.wisdomCheck=true;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.topPicksTbl];
    
    
    
    NSIndexPath *indexPath = [self.topPicksTbl indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"Index path:%ld",(long)indexPath.row);
    TopPicksCell *cell = [self.topPicksTbl cellForRowAtIndexPath:indexPath];
    
    
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
        NSString * public=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"ispublic"]];
        int publicInt=[public intValue];
        
        if(publicInt==0)
        {
    
        
        
        @try {
            delegate1.symbolDepthStr=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"companyname"];
            
              delegate1.tradingSecurityDes=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"symbolname"]];
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
        @try {
            delegate1.orderSegment=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"segmenttype"];
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
        
        delegate1.orderBuySell=cell.buySellButton.currentTitle;
       
        if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
        {
             delegate1.orderinstrument=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"instrumentid"];
        }
        
        else
        {
             delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"exchangetoken"]];
            
          
        }
        
    
    

        if(delegate1.leaderAdviceDetails.count>0)
        {
            [delegate1.leaderAdviceDetails removeAllObjects];
        }
        //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
        //    {
        //
        //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
        //        {
        //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        //
        //            [self.navigationController pushViewController:allocation animated:YES];
        //
        //        }
        
        //        else{
        delegate1.depth=@"WISDOM";
        
        NSLog(@"%@",delegate1.depth);
        
        
        
        //        delegate1.symbolDepthStr=[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"companyname"];
        //
        //        delegate1.exchaneLblStr=[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
        
    @try {
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
        
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
        
        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"]];
        
        NSString * logo=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
        
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        
        
        NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        
        
        NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        
        
        if([cell.followBtn.currentTitle isEqualToString:@"Follow +"])
        {
            
            NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                                       @"cache-control": @"no-cache",
                                       };
            
            NSString * userId=[NSString stringWithFormat:@"leaderid=%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
            
            NSString * clientId=[NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
            
            
            NSMutableData *postData = [[NSMutableData alloc] initWithData:[userId dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[clientId dataUsingEncoding:NSUTF8StringEncoding]];
            //    if(segmentedControl.selectedSegmentIndex==0)
            //    {
            //         [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
            //    }
            //    else
            //    {
            //    if(self.buttonArray.count>0)
            //    {
            //    if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Follow +"])
            //    {
            //    [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
            //    }else if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Following"])
            //    {
            //        [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
            //
            //    }
            //    }
            //    }
            
            NSArray * subscribeArray=@[@1];
            
            NSString * subscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",subscribeArray];
            NSString * sub = [subscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
            NSString * sub1 = [sub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
            
            NSArray * unSubscribeArray=@[];
            
            NSString * unSubscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",unSubscribeArray];
            NSString * unSub = [unSubscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
            NSString * unSub1 = [unSub stringByReplacingOccurrencesOfString:@")" withString:@"}"];

            
            NSString * btnTitle=[NSString stringWithFormat:@"%@",cell.followBtn.currentTitle];
            
            
            if([btnTitle isEqualToString:@"Following"])
                
            {
                [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
                [postData appendData:[unSub1 dataUsingEncoding:NSUTF8StringEncoding]];
               

                
            }
            else if ([btnTitle isEqualToString:@"Follow +"])
            {
                [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
                 [postData appendData:[sub1 dataUsingEncoding:NSUTF8StringEncoding]];
                

                
            }
            
            
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"POST"];
            [request setAllHTTPHeaderFields:headers];
            [request setHTTPBody:postData];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                NSLog(@"%@", httpResponse);
                                                                
                                                                NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                NSLog(@"%@",followDict);
                                                                
                                                                messageStr=[followDict objectForKey:@"message"];
                                                                
                                                                
                                                                
                                                            }
                                                            
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                if([messageStr isEqualToString:@"Subscriber created"])
                                                                {
                                                                    //                                                            [cell.followBtn setTitle:[self.buttonArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//                                                                    [self.topPicksTbl reloadData];
//                                                                    
//                                                                    [self topPicksServer];
                                                                    
                                                                    StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
                                                                    
                                                                    [self.navigationController pushViewController:orderView animated:YES];
                                                                }
                                                            });
                                                            
                                                            
                                                            
                                                        }];
            [dataTask resume];
            
            
            
            
            
    
        

            
        }
        
        else
        {
            
            
            StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            
            [self.navigationController pushViewController:orderView animated:YES];
        }
        
        
        
        
      
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
        
    
    
    

    }else if (publicInt==1)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"This is a publicly available information from the above source. All users are advised, in their own interests, to seek and obtain advice that is specific or tailored to their own circumstances and that any reliance by any user on any such publicly available advice is entirely at the risk and consequence of such user, for which Zenwise Technologies Private Limited under the brand name Zambala is not responsible or liable." preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            [self presentViewController:alert animated:YES completion:^{
                
                
                
            }];
            
            
            
            UIAlertAction * proceedAction=[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                    
                    
                    
                    @try {
                        delegate1.symbolDepthStr=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"companyname"];
                        
                        delegate1.tradingSecurityDes=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"symbolname"]];
                        
                    }
                    @catch (NSException * e) {
                        NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        NSLog(@"finally");
                    }
                    
                    @try {
                        delegate1.orderSegment=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"segmenttype"];
                        
                        
                    }
                    @catch (NSException * e) {
                        NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        NSLog(@"finally");
                    }
                    
                    
                    delegate1.orderBuySell=cell.buySellButton.currentTitle;
                    
                    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
                    {
                        delegate1.orderinstrument=[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"instrumentid"];
                    }
                    
                    else
                    {
                        delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"exchangetoken"]];
                        
                        
                    }
                    
                    
                    
                    
                    if(delegate1.leaderAdviceDetails.count>0)
                    {
                        [delegate1.leaderAdviceDetails removeAllObjects];
                    }
                    //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
                    //    {
                    //
                    //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
                    //        {
                    //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
                    //
                    //            [self.navigationController pushViewController:allocation animated:YES];
                    //
                    //        }
                    
                    //        else{
                    delegate1.depth=@"WISDOM";
                    
                    NSLog(@"%@",delegate1.depth);
                    
                    
                    
                    //        delegate1.symbolDepthStr=[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"companyname"];
                    //
                    //        delegate1.exchaneLblStr=[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
                    
                    @try {
                        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
                        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
                        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
                        
                        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
                        
                        [delegate1.leaderAdviceDetails addObject:[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"]];
                        
                        NSString * logo=[NSString stringWithFormat:@"%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
                        
                        if([logo isEqual:[NSNull null]])
                        {
                            [delegate1.leaderAdviceDetails addObject:@"no"];
                        }
                        
                        else
                        {
                            [delegate1.leaderAdviceDetails addObject:logo];
                        }
                        
                        
                        NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                        
                        
                        NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                        
                        
//                        if([cell.followBtn.currentTitle isEqualToString:@"Follow +"])
//                        {
//
//                            NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
//                                                       @"cache-control": @"no-cache",
//                                                       };
//
//                            NSString * userId=[NSString stringWithFormat:@"leaderid=%@",[[[self.topPicksResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
//
//                            NSString * clientId=[NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
//
//
//                            NSMutableData *postData = [[NSMutableData alloc] initWithData:[userId dataUsingEncoding:NSUTF8StringEncoding]];
//                            [postData appendData:[clientId dataUsingEncoding:NSUTF8StringEncoding]];
//                            //    if(segmentedControl.selectedSegmentIndex==0)
//                            //    {
//                            //         [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
//                            //    }
//                            //    else
//                            //    {
//                            //    if(self.buttonArray.count>0)
//                            //    {
//                            //    if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Follow +"])
//                            //    {
//                            //    [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
//                            //    }else if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Following"])
//                            //    {
//                            //        [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
//                            //
//                            //    }
//                            //    }
//                            //    }
//
//                            NSArray * subscribeArray=@[@1];
//
//                            NSString * subscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",subscribeArray];
//                            NSString * sub = [subscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
//                            NSString * sub1 = [sub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
//
//                            NSArray * unSubscribeArray=@[];
//
//                            NSString * unSubscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",unSubscribeArray];
//                            NSString * unSub = [unSubscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
//                            NSString * unSub1 = [unSub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
//
//
//                            NSString * btnTitle=[NSString stringWithFormat:@"%@",cell.followBtn.currentTitle];
//
//
//                            if([btnTitle isEqualToString:@"Following"])
//
//                            {
//                                [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
//                                [postData appendData:[unSub1 dataUsingEncoding:NSUTF8StringEncoding]];
//
//
//
//                            }
//                            else if ([btnTitle isEqualToString:@"Follow +"])
//                            {
//                                [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
//                                [postData appendData:[sub1 dataUsingEncoding:NSUTF8StringEncoding]];
//
//
//
//                            }
//
//
//
//                            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
//                                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                                               timeoutInterval:10.0];
//                            [request setHTTPMethod:@"POST"];
//                            [request setAllHTTPHeaderFields:headers];
//                            [request setHTTPBody:postData];
//
//                            NSURLSession *session = [NSURLSession sharedSession];
//                            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                                            if (error) {
//                                                                                NSLog(@"%@", error);
//                                                                            } else {
//                                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                                                NSLog(@"%@", httpResponse);
//
//                                                                                NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//                                                                                NSLog(@"%@",followDict);
//
//                                                                                messageStr=[followDict objectForKey:@"message"];
//
//
//
//                                                                            }
//
//
//                                                                            dispatch_async(dispatch_get_main_queue(), ^{
//
//
//                                                                                if([messageStr isEqualToString:@"Subscriber created"])
//                                                                                {
//                                                                                    //                                                            [cell.followBtn setTitle:[self.buttonArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//                                                                                    //                                                                    [self.topPicksTbl reloadData];
//                                                                                    //
//                                                                                    //                                                                    [self topPicksServer];
//
//                                                                                    StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
//
//                                                                                    [self.navigationController pushViewController:orderView animated:YES];
//                                                                                }
//                                                                            });
//
//
//
//                                                                        }];
//                            [dataTask resume];
//
//
//
//
//
//
//
//
//
//                        }
                        
                       // else
                        //{
                            
                            
                            StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
                            
                            [self.navigationController pushViewController:orderView animated:YES];
                       // }
                        
                        
                        
                        
                        
                        
                    }
                    @catch (NSException * e) {
                        NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        NSLog(@"finally");
                    }
                    
                    
                    
                    
                    
                
                
            }];
            
            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                
            }];
            
            
            
            
            [alert addAction:proceedAction];
            [alert addAction:cancelAction];
            
        });
        
    }
    
    }
    
    
}

//REACHABILITY//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}





@end
