//
//  DepthWatch.m
//  betazambalafollower
//
//  Created by zenwise mac 2 on 8/29/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "DepthWatch.h"

@implementation DepthWatch



- (id)init {
    if (self = [super init]) {
        
        
        self.usAT = 0;
        self.btDataType=0;   //Byte
        self.usSize=0;
        self.usMsgCode=0;
        self.inSeqID=0;
        self.stExchange=@"";
        self.btNull1=0;   //Byte
        self.stSecurityID=@"";
        self.btNull2=0;
        self.inBroadCastTime=0;
        self.btSessionIndex=0; //
        
        
        //Buy Depth
        
        self.dbBuy_Price_0=0;
        self.inBuy_Qty_0=0;
        
        
        
        
        //Buy Depth
        
        self.inBuy_Order_0=0;
        self.dbBuy_Price_1=0;
        self.inBuy_Qty_1=0;
        self.inBuy_Order_1=0;
        self.dbBuy_Price_2=0;
        self.inBuy_Qty_2=0;
        self.inBuy_Order_2=0;
        self.dbBuy_Price_3=0;
        self.inBuy_Qty_3=0;
        self.inBuy_Order_3=0;
        
        self.dbBuy_Price_4=0;
        self.inBuy_Qty_4=0;
        self.inBuy_Order_4=0;
        
        //Sell Depth
        
        self.dbSell_Price_0=0;
        self.inSell_Qty_0=0;
        self.inSell_Order_0=0;
        
        self.dbSell_Price_1=0;
        self.inSell_Qty_1=0;
        self.inSell_Order_1=0;
        
        self.dbSell_Price_2=0;
        self.inSell_Qty_2=0;
        self.inSell_Order_2=0;
        
        self.dbSell_Price_3=0;
        self.inSell_Qty_3=0;
        self.inSell_Order_3=0;
        
        self.dbSell_Price_4=0;
        self.inSell_Qty_4=0;
        self.inSell_Order_4=0;
        
        
        self.inTotalQtyTraded=0; //Total Volume
        self.dbLTP=0;
        self.inLTQ=0;
        self.inTradeTime=0;
        self.dbVWap=0; // Volume Wighted Average Price
        self.inTotalBuyQty=0;
        self.inTotalSellQty=0;
        self.dbOpen=0;
        self.dbHigh=0;
        self.dbLow=0;
        self.dbClose=0;
        self.inOpenInterest=0;
        self.dbLifeLow=0; // Life Time Low
        self.dbLifeHigh=0; // Life Time High
        self.dbTotalValue=0;

        
        
    }
    
     return self;
}

+ (id)Mt4 {
    static DepthWatch *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
@end
