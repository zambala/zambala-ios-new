//
//  NewPortOrderCell.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/22/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewPortOrderCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *transactionLbl;

@property (weak, nonatomic) IBOutlet UILabel *companyLbl;

@property (weak, nonatomic) IBOutlet UILabel *segmentLbl;
@property (weak, nonatomic) IBOutlet UILabel *bestBidLbl;

@property (weak, nonatomic) IBOutlet UILabel *bestBidQtyLbl;
@property (weak, nonatomic) IBOutlet UILabel *LTPLbl;

@property (weak, nonatomic) IBOutlet UILabel *chngLbl;

@property (weak, nonatomic) IBOutlet UILabel *bestAskLbl;

@property (weak, nonatomic) IBOutlet UILabel *bestQtyLbl;

@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIButton *sellBtn;

@property (weak, nonatomic) IBOutlet UITextField *qtyTxt;

@property (weak, nonatomic) IBOutlet UILabel *limitLbl;

@property (weak, nonatomic) IBOutlet UITextField *limitTxt;
@property (weak, nonatomic) IBOutlet UIView *limitUnderView;
@property (weak, nonatomic) IBOutlet UIView *orderView;

@end
