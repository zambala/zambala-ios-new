//
//  SubscriptionViewCell.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 12/30/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubscriptionViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIView *cellView;

@property (strong, nonatomic) IBOutlet UIImageView *userImg;
@property (strong, nonatomic) IBOutlet UILabel *nameLbl, *subLbl, *expiryLbl, *dayLbl;
@property (strong, nonatomic) IBOutlet UIView *subscriptionView;

@end
