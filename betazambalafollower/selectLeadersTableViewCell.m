//
//  selectLeadersTableViewCell.m
//  betazambalafollower
//
//  Created by zenwise technologies on 10/07/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "selectLeadersTableViewCell.h"

@implementation selectLeadersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.leaderSelectButton setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    [self.leaderSelectButton setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    self.leaderSelectButton.selected=NO;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onLeaderSelectButtonTap:(id)sender {
    
        if(self.leaderSelectButton.selected==YES)
        {
            self.leaderSelectButton.selected=NO;
        }else if (self.leaderSelectButton.selected==NO)
        {
            self.leaderSelectButton.selected=YES;
        }
        
}
@end
