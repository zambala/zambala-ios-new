//
//  OrderViewController1.h
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallPutTrade : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *orderTableView;
@property (weak, nonatomic) IBOutlet UIView *orderTypeView;
@property (weak, nonatomic) IBOutlet UIButton *submitOrderButton;
@property (strong, nonatomic) IBOutlet UIButton *limitBtn;
@property (strong, nonatomic) IBOutlet UIButton *marketBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewHgt;

@property (strong, nonatomic) IBOutlet UIView *view1;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;

@end
