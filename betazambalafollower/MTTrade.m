//
//  MTTrade.m
//  betazambalafollower
//
//  Created by zenwise mac 2 on 8/31/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "MTTrade.h"

@implementation MTTrade


- (id)init {
    if (self = [super init]) {
        
        self.stUserID =@"";
        self.stManagerID=@"";
        self.inATINTradeID=0;
        self.inATINOrderID=0;
        self.stTradeID=@"";
        
        
        self. stTradeTime=@"";
        self.inOrderID=0;
        self. stOrderTime=@"";
        self. stExchangeOrderID=@"";
        self. stExchangeOrderTime=@"";
        self. stExchange=@"";
        self. stSecurityID=@"";
        self. stSecurityType=@"";
        self. stSymbol=@"";
        self. stExpiryDate=@"";
        self. stOptionType=@"";
        self.dbStrikePrice=0;
        self. stSide=@"";
        self. stOrderType=@"";
        self.inQty=0;
        self.inPendingQty=0;
        self.dbPrice=0;
        self. stClientID=@"";
        self. stRefText=@"";
        self.inTradeSeqNo=0;
        
        
    }
    
    return self;
}



+ (id)Mt5 {
    static MTTrade *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

@end
