//
//  TagDecode.m
//  
//
//  Created by zenwise technologies on 20/07/17.
//
//

#import "TagDecode.h"
#import "AppDelegate.h"
//#import "ByteBuffer.h"
#import "MT.h"



@interface TagDecode ()

{
    short shIndex_;
    NSMutableArray * Buffer;
    short shBufferSize_;
    AppDelegate * delegate;
   // ByteBuffer * buffer;
    MT *sharedManager;
    short newShvalue;
   // NSMutableData * buffer;
}

@end

@implementation TagDecode

- (void)viewDidLoad {
    [super viewDidLoad];
    //Buffer = [[NSMutableArray alloc]init];
    
    
    sharedManager = [MT Mt1];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    // Do any additional setup after loading the view.
}


-(void)TagDecode:(NSMutableArray *)btBuffer
{
    @try
    {
    shIndex_ = 0;
    Buffer = [[NSMutableArray alloc]initWithCapacity:sizeof(btBuffer)];
    
    for(int i=0;i<btBuffer.count;i++)
    {
        [Buffer insertObject:[btBuffer objectAtIndex:i] atIndex:i];
    }
    shBufferSize_ = (short)btBuffer.count;
    
   // buffer = [[NSMutableData alloc]initWithBytes:[NSNumber numberWithInt:btBuffer] length:sizeof(btBuffer)];
    
    
   // buffer = [ByteBuffer initWithOrder:ByteOrderLittleEndian];
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
    
}


//TagDeCode(byte[] btBuffer) {
//    shIndex_ = 0;
//    Buffer = ByteBuffer.allocate(btBuffer.length);
//    Buffer.order(ByteOrder.LITTLE_ENDIAN);
//    Buffer.put(btBuffer, 0, btBuffer.length);
//    shBufferSize_ = (short) btBuffer.length;
//}



-(void)TagDecode:(NSMutableArray *)btBuffer shortMethod:(short)shBufferSize
{@try
    {
    shIndex_ = 0;
    Buffer = [[NSMutableArray alloc]initWithCapacity:sizeof(shBufferSize)];
    
    for(int i=0;i<shBufferSize;i++)
    {
        [Buffer insertObject:[btBuffer objectAtIndex:i] atIndex:i];
    }
    shBufferSize_ = shBufferSize;
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}

//TagDeCode(byte[] btBuffer, short shBufferSize) {
//    shIndex_ = 0;
//    Buffer = ByteBuffer.allocate(shBufferSize);
//    Buffer.order(ByteOrder.LITTLE_ENDIAN);
//    Buffer.put(btBuffer, 0, shBufferSize);
//    shBufferSize_ = shBufferSize;
//}



-(BOOL)CheckCapacity:(int)inLength
{
    return ((shIndex_ + inLength)<=shBufferSize_);
}

//boolean CheckCapacity(int inLength) {
//    return ((shIndex_ + inLength) <= shBufferSize_);
//}


-(BOOL)TagBool
{
    BOOL blValue = [Buffer objectAtIndex:shIndex_]!= 0;
    
    
    shIndex_ += sharedManager.shBoolSize;
    return blValue;
}

-(BOOL)TagBool1
{
    BOOL blValue = false ;
    
    if([[Buffer objectAtIndex:shIndex_] intValue]==0)
    {
        
        blValue=true;
    }
    
    else
    {
         blValue=false;
        
    }
    shIndex_ += sharedManager.shBoolSize;
    return blValue;
}


//boolean TagBool() {
//    boolean blValue = Buffer.get(shIndex_) != (byte) 0;
//    shIndex_ += MT.shBoolSize;
//    return blValue;
//}


-(short)TagShort
{
    @try
    {
     NSString * testString = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+1]];
     NSString * testString1 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_]];
    
    
    NSString *hex1 = [NSString stringWithFormat:@"%02lX",
                     (unsigned long)[testString integerValue]];
    NSLog(@"%@", hex1);
    
    NSString *hex = [NSString stringWithFormat:@"%02lX",
                     (unsigned long)[testString1 integerValue]];
    NSLog(@"%@", hex);
    
    
    NSString * finalString=[hex1 stringByAppendingString:hex];
    
    
    NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                      finalString];
    NSLog(@"%@", finalHex);
    
    
    NSScanner *scanner=[NSScanner scannerWithString:finalHex];
    unsigned int temp;
    [scanner scanHexInt:&temp];
    NSLog(@"%d",temp);

    
    shIndex_ +=sharedManager.shShortSize;

        
        return temp;
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }

   
}

-(double)TagNewDouble
{
    @try
    {
    NSString * testString8 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+7]];
    NSString * testString7 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+6]];
    NSString * testString6 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+5]];
    NSString * testString5 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+4]];
    NSString * testString4 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+3]];
    NSString * testString3 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+2]];
    NSString * testString2 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+1]];
    NSString * testString1 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_]];
    
    
    NSString *hex1 = [NSString stringWithFormat:@"%lX",
                      (unsigned long)[testString1 integerValue]];
    NSLog(@"%@", hex1);
    
    NSString *hex = [NSString stringWithFormat:@"%lX",
                     (unsigned long)[testString2 integerValue]];
    NSLog(@"%@", hex);
    
    NSString *hex2 = [NSString stringWithFormat:@"%lX",
                     (unsigned long)[testString3 integerValue]];
    NSLog(@"%@", hex2);
    
    NSString *hex4 = [NSString stringWithFormat:@"%lX",
                      (unsigned long)[testString4 integerValue]];
    NSLog(@"%@", hex4);
    
    NSString *hex5 = [NSString stringWithFormat:@"%lX",
                      (unsigned long)[testString5 integerValue]];
    NSLog(@"%@", hex5);
    
    NSString *hex6 = [NSString stringWithFormat:@"%lX",
                      (unsigned long)[testString6 integerValue]];
    NSLog(@"%@", hex6);
    
    NSString *hex7 = [NSString stringWithFormat:@"%lX",
                      (unsigned long)[testString7 integerValue]];
    NSLog(@"%@", hex7);
    
    NSString *hex8 = [NSString stringWithFormat:@"%lX",
                      (unsigned long)[testString8 integerValue]];
    NSLog(@"%@", hex8);

    
    NSString * finalString=[hex1 stringByAppendingString:hex];
    
    
     NSString * finalString1=[finalString stringByAppendingString:hex2];
    
     NSString * finalString2=[finalString1 stringByAppendingString:hex4];
    
     NSString * finalString3=[finalString2 stringByAppendingString:hex5];
    
     NSString * finalString4=[finalString3 stringByAppendingString:hex6];
    
     NSString * finalString5=[finalString4 stringByAppendingString:hex7];
    
     NSString * finalString6=[finalString5 stringByAppendingString:hex8];
    
    
    NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                          finalString6];
    NSLog(@"%@", finalHex);
    
    
    NSScanner *scanner=[NSScanner scannerWithString:finalHex];
    unsigned int temp;
    [scanner scanHexInt:&temp];
    NSLog(@"%d",temp);
    
    
    shIndex_ +=sharedManager.shShortSize;
    
    
    return temp;
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
    

}
//short TagShort() {
//    short shValue = Buffer.getShort(shIndex_);
//    shIndex_ += MT.shShortSize;
//    return shValue;
//}

-(Byte)TagByte
{
//    Byte btValue = (Byte)[Buffer objectAtIndex:shIndex_];
    @try
    {
    
    NSString * testString1 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_]];
    
    
    NSString *hex1 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString1 integerValue]];
    NSLog(@"%@", hex1);
    NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                          hex1];
    NSLog(@"%@", finalHex);
    
    
    NSScanner *scanner=[NSScanner scannerWithString:finalHex];
    unsigned int temp;
    [scanner scanHexInt:&temp];
    NSLog(@"%d",temp);


    shIndex_ += sharedManager.shBoolSize;
    
    
    return temp;
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}

//byte TagByte() {
//    byte btValue = Buffer.get(shIndex_);
//    shIndex_ += MT.shBoolSize;
//    return btValue;
//}

-(int)TagInt
{
    @try
    {
    NSString * testString3 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+3]];
    NSString * testString2 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+2]];
    NSString * testString1 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+1]];
    NSString * testString = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_]];
    
    
    
    
    
    
    NSString *hex3 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString3 integerValue]];
    NSLog(@"%@", hex3);
    NSString *hex2 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString2 integerValue]];
    NSLog(@"%@", hex2);
    NSString *hex1 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString1 integerValue]];
    NSLog(@"%@", hex1);
    NSString *hex = [NSString stringWithFormat:@"%02lX",
                     (unsigned long)[testString integerValue]];
    NSLog(@"%@", hex);
    
    
    
    
    NSString * finalString3 = [hex3 stringByAppendingString:hex2];
    NSString * finalString2 = [finalString3 stringByAppendingString:hex1];
    NSString * finalString1 = [finalString2 stringByAppendingString:hex];
    // NSString * finalString = [finalString1 stringByAppendingString:hex];
    
    
    
    NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                          finalString1];
    
    unsigned long long doubleBits;
    int result = 0;
    NSScanner *scanner = [NSScanner scannerWithString:finalHex];
    if ([scanner scanHexLongLong:&doubleBits]) {
        memcpy(&result, &doubleBits, sizeof(result));
    }
    NSLog(@"Result:%d",result);
    
    
    shIndex_ +=sharedManager.shIntSize;
    return result;
    
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}
//int TagInt() {
//    int inValue = Buffer.getInt(shIndex_);
//    shIndex_ += MT.shIntSize;
//    return inValue;
//}

-(long)TagLong
{
    long lnValue = [[Buffer objectAtIndex:shIndex_] longValue];
    shIndex_ +=sharedManager.shLongSize;
    return lnValue;
}

//long TagLong() {
//    long lnValue = Buffer.getLong(shIndex_);
//    shIndex_ += MT.shLongSize;
//    return lnValue;
//}


-(double)TagDouble
{
    @try
    {
    NSString * testString7 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+7]];
    NSString * testString6 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+6]];
    NSString * testString5 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+5]];
    NSString * testString4 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+4]];
    NSString * testString3 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+3]];
    NSString * testString2 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+2]];
    NSString * testString1 = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_+1]];
    NSString * testString = [NSString stringWithFormat:@"%@",[Buffer objectAtIndex:shIndex_]];
    double total;
    
    total = [[Buffer objectAtIndex:shIndex_] doubleValue];
    
    
    NSString *hex7 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString7 integerValue]];
    NSLog(@"%@", hex7);
    NSString *hex6 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString6 integerValue]];
    NSLog(@"%@", hex6);
    NSString *hex5 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString5 integerValue]];
    NSLog(@"%@", hex5);
    NSString *hex4 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString4 integerValue]];
    NSLog(@"%@", hex4);
    NSString *hex3 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString3 integerValue]];
    NSLog(@"%@", hex3);
    NSString *hex2 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString2 integerValue]];
    NSLog(@"%@", hex2);
    NSString *hex1 = [NSString stringWithFormat:@"%02lX",
                      (unsigned long)[testString1 integerValue]];
    NSLog(@"%@", hex1);
    NSString *hex = [NSString stringWithFormat:@"%02lX",
                     (unsigned long)[testString integerValue]];
    NSLog(@"%@", hex);
    
    
    
    NSString * finalString7 = [hex7 stringByAppendingString:hex6];
    NSString * finalString6 = [finalString7 stringByAppendingString:hex5];
    NSString * finalString5 = [finalString6 stringByAppendingString:hex4];
    NSString * finalString4 = [finalString5 stringByAppendingString:hex3];
    NSString * finalString3 = [finalString4 stringByAppendingString:hex2];
    NSString * finalString2 = [finalString3 stringByAppendingString:hex1];
    NSString * finalString1 = [finalString2 stringByAppendingString:hex];
    // NSString * finalString = [finalString1 stringByAppendingString:hex];
    
    
    
    NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                          finalString1];
    
    unsigned long long doubleBits;
    double result = 0;
    NSScanner *scanner = [NSScanner scannerWithString:finalHex];
    if ([scanner scanHexLongLong:&doubleBits]) {
        memcpy(&result, &doubleBits, sizeof(result));
    }
    NSLog(@"Result:%f",result);
    
    
    
    
    
    
    shIndex_ +=sharedManager.shDoubleSize;
    
    
    
    return result;
    
    
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
    
}

//double TagDouble(){
//    double dbValue = Buffer.getDouble(shIndex_);
//    shIndex_ += MT.shDoubleSize;
//    return dbValue;
//    
//}


-(NSString *)TagString
{
    @try
    {
    short shLength = [self TagShort];
    //    NSString * stValue = @"";
    NSMutableString * stValue=[[NSMutableString alloc]init];
    if(shLength !=0 && [self CheckCapacity:shLength])
    {
        NSMutableArray * btStringBuffer = [[NSMutableArray alloc]initWithCapacity:shLength];
        
        
        for(int i=0;i<shLength;i++)
        {
            //            btStringBuffer insertObject:[Buffer objectAtIndex:i] atIndex:
            
            [btStringBuffer insertObject:[Buffer objectAtIndex:shIndex_] atIndex:i];
            
            shIndex_ = shIndex_+1;
            //[btStringBuffer addObject:[Buffer objectAtIndex:i]];
        }
        
        
        for(int i=0;i<btStringBuffer.count;i++)
        {
            
            
            NSString * string=[NSString stringWithFormat:@"%@",[btStringBuffer objectAtIndex:i]];
            
            NSString *hex1 = [NSString stringWithFormat:@"%02lX",
                              (unsigned long)[string integerValue]];
            NSLog(@"%@", hex1);
            
            
            
            
            //        int newInt=[hex1 intValue];
            
            
            NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                                  hex1];
            NSLog(@"%@", finalHex);
            int value = 0;
            
            sscanf([finalHex cStringUsingEncoding:NSASCIIStringEncoding], "%x", &value);
            
            
            [stValue appendFormat:@"%c",(char)value];
            //                i+=2;
            //            }
            NSLog(@"%@", stValue);
            
        }
        
        NSLog(@"%@", stValue);
        
    }
    return stValue;
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}

//String TagString() {
//    short shLength = TagShort();
//    String stValue = "";
//    
//    if (shLength != 0 && CheckCapacity(shLength)) {
//        byte[] btStringBuffer = new byte[shLength];
//        Buffer.position(shIndex_);
//        Buffer.get(btStringBuffer, 0, shLength);
//        ByteArrayOutputStream dataHolder = new ByteArrayOutputStream();
//        dataHolder.write(btStringBuffer, 0, shLength);
//        stValue = dataHolder.toString();
//        shIndex_ += shLength;
//    }
//    
//    return stValue;
//}

-(BOOL)ReadTagDefault:(short)shVal
{
    @try
    {
    int shTag = shVal & 0xFFFF;
    short shSize = 0;
    // Tag Code 65001 - 65535 -  NULL
    if(shTag >= 65001){
        
    }
    // Tag Code 60001 - 65000 - Bianry & Other
    else if(shTag >= 60001 ){
        shSize = [self TagShort];
    }
    // Tag Code 55001 - 60000 - String
    else if( shTag >= 55001){
        shSize = [self TagShort];
    }
    // Tag Code 50001 - 55000 - DateTime
    else if(shTag >= 50001){
        shSize = sharedManager.shLongSize;
    }
    // Tag Code 45001 - 50000 - Decimal
    else if(shTag >= 45001){
        shSize = sharedManager.shDecimalSize;
    }
    // Tag Code 40001 - 45000 - Double
    else if(shTag >= 40001){
        shSize = sharedManager.shDoubleSize;
    }
    // Tag Code 35001 - 40000 - Float
    else if(shTag >= 35001){
        shSize = sharedManager.shFloatSize;
    }
    // Tag Code 30001 - 35000 - Long
    else if(shTag >= 30001){
        shSize = sharedManager.shLongSize;
    }
    // Tag Code 20001 - 30000 - Int
    else if(shTag >= 20001){
        shSize = sharedManager.shIntSize;
    }
    // Tag Code 10001 - 20000 - Short
    else if(shTag >= 10001){
        shSize = sharedManager.shShortSize;
    }
    // Tag Code 5001 - 10000 - Bool
    else if(shTag >= 5001){
        shSize = sharedManager.shBoolSize;
    }
    // Tag Code 1 - 5000 - Byte
    else if(shTag >= 1){
        shSize = sharedManager.shCharSize;
    }
    if( (shSize + shIndex_) <= shBufferSize_){
        shIndex_ += shSize;
        return  true;
    }
    else {
        return false;
    }
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}

// boolean ReadTagDefault(short shVal){

//int shTag = shVal & 0xFFFF;
//short shSize = 0;

//uncomment this
-(short)NextTag
{
    if([self NextHash])
    {
        if([self CheckCapacity:sharedManager.shCharSize])
        {
            shIndex_ ++;
            if([self CheckCapacity:sharedManager.shShortSize])
            {
                return [self TagShort];
                
            }
        }
    }
    
    return 0;
}

//short NextTag() {
//    if (NextHash()) {
//        if (CheckCapacity(MT.shCharSize)) {
//            shIndex_++;
//            
//            if (CheckCapacity(MT.shShortSize)) {
//                return TagShort();
//            }
//        }
//    }
//    
//    return 0;
//}



-(BOOL)NextHash
{
    while (shIndex_<shBufferSize_) {
        
       // NSNumber * check1 = [NSNumber numberWithInt:[Buffer objectAtIndex:shIndex_]];
        
         int check = [[Buffer objectAtIndex:shIndex_] intValue];
        NSLog(@"Int:%d",check);
        
        sharedManager = [MT Mt1];
        NSLog(@"Hash Value:%hd",sharedManager.shHash);
        if(check==sharedManager.shHash)
        {
            return true;
        }else
        {
            shIndex_++;
        }
    }
    return false;
}

//boolean NextHash() {
//    while (shIndex_ < shBufferSize_) {
//        if (Buffer.get(shIndex_) == MT.shHash)
//            return true;
//        else
//            shIndex_++;
//    }
//    
//    return false;
//}

-(int)GetIndex
{
    return shIndex_;
}

//int GetIndex() {
//    return shIndex_;
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSMutableArray*)TagBinary
{
    @try
    {
    short shLength = [self TagShort];
    if(shLength !=0 && [self CheckCapacity:shLength])
    {
        NSMutableArray *btBuffer = [[NSMutableArray alloc]initWithCapacity:shLength];
        for(int i=0;i<shLength;i++)
        {
            [btBuffer insertObject:[Buffer objectAtIndex:shIndex_] atIndex:i];
            shIndex_ = shIndex_ +1;
        }
        
        //shIndex_ +=shLength;
        return btBuffer;
    }else
    {
        return nil;
    }
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}
-(NSString *)TagStringBuffer:(short)shLength
{
    @try
    {
    //    NSString * stValue = @"";
    NSMutableString * stValue=[[NSMutableString alloc]init];
    if(shLength !=0)
    {
        NSMutableArray * btStringBuffer = [[NSMutableArray alloc]initWithCapacity:shLength];
        
        
        
        for(int i=0;i<shLength;i++)
        {
            //            btStringBuffer insertObject:[Buffer objectAtIndex:i] atIndex:
            
            [btStringBuffer insertObject:[Buffer objectAtIndex:shIndex_] atIndex:i];
            
            shIndex_ = shIndex_+1;
            //[btStringBuffer addObject:[Buffer objectAtIndex:i]];
        }
        
        for(int i=0;i<btStringBuffer.count;i++)
        {
            
            
            NSString * string=[NSString stringWithFormat:@"%@",[btStringBuffer objectAtIndex:i]];
            
            NSString *hex1 = [NSString stringWithFormat:@"%02lX",
                              (unsigned long)[string integerValue]];
            NSLog(@"%@", hex1);
            
            
            
            
            //        int newInt=[hex1 intValue];
            
            
            NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                                  hex1];
            NSLog(@"%@", finalHex);
            
            
            
            //            int i = 0;
            //            while (i < [finalHex length]){
            //                NSString * hexChar = [finalHex substringWithRange: NSMakeRange(i, 2)];
            int value = 0;
            
            sscanf([finalHex cStringUsingEncoding:NSASCIIStringEncoding], "%x", &value);
            
            
            [stValue appendFormat:@"%c",(char)value];
            //                i+=2;
            //            }
            NSLog(@"%@", stValue);
            
        }
        
        
        NSLog(@"%@", stValue);
        
        [stValue stringByReplacingOccurrencesOfString:@" " withString:@""];
        
    }
    return stValue;
        
        
    } @catch (NSException *exception) {
        NSLog(@"Caught Exception");
    } @finally {
        NSLog(@"Finally");
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
