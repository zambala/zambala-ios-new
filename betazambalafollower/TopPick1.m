//
//  TopPicks.m
//  PayPage
//
//  Created by zenwise mac 2 on 12/22/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "TopPicks1.h"
#import "TopPicksCell.h"

@interface TopPicks1 ()

@end

@implementation TopPicks1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (TopPicksCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    TopPicksCell *cell = [self.topPicksTbl dequeueReusableCellWithIdentifier:cellIdentifier];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if(tableView==self.topPicksTbl)
    {
        return 192;
        
    }
    return 0;

}

@end
