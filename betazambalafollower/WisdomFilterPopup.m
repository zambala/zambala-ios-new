//
//  WisdomFilterPopup.m
//  testing
//
//  Created by zenwise mac 2 on 12/8/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "WisdomFilterPopup.h"
#import "UIViewController+MJPopupViewController.h"

@interface WisdomFilterPopup ()

@end

@implementation WisdomFilterPopup

@synthesize buyImageBtn, sellImageBtn, tradeImageBtn, shortImgBtn, longImgBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    dataArray=[[NSArray alloc]initWithObjects:@"Zerodha",@"Spinner",@"Broker not available",nil];
    pickerView=[[UIPickerView alloc]init];
    ViewContainer=[[UIView alloc]init];
    
    pickerView.dataSource=self;
    pickerView.delegate=self;
    [pickerView setShowsSelectionIndicator:YES];
    [self outFocusTextField];
    
    
    [buyImageBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [buyImageBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    buyImageBtn.selected = NO;
    
    [sellImageBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [sellImageBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    sellImageBtn.selected = NO;
    
    [tradeImageBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [tradeImageBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    tradeImageBtn.selected = NO;
    
    [shortImgBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [shortImgBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    shortImgBtn.selected = NO;
    
    [longImgBtn setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    [longImgBtn setImage:[UIImage imageNamed:@"checkSel-box.png"] forState:UIControlStateSelected];
    longImgBtn.selected = NO;
    
    fromDateViewContainer = [[UIView alloc]init];
    fromDatePicker = [[UIDatePicker alloc]init];
    
    toDateViewContainer = [[UIView alloc]init];
    toDatePicker = [[UIDatePicker alloc]init];
    
    fromTimeViewContainer = [[UIView alloc]init];
    fromTimePicker = [[UIDatePicker alloc]init];
    
    toTimeViewContainer = [[UIView alloc]init];
    toTimePicker = [[UIDatePicker alloc]init];
    
    pickerView.delegate = self;
    pickerView.dataSource = self;
    

}

-(void)outFocusTextField
{
    self.symbolTxt.leftViewMode = UITextFieldViewModeAlways;
    self.symbolTxt.font=[UIFont fontWithName:@"OpenSans" size:13];
    self.symbolTxt.textColor = [UIColor blackColor];
    self.symbolTxt.backgroundColor = [UIColor clearColor];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.symbolTxt.frame.size.height - 1, self.symbolTxt.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [self.symbolTxt.layer addSublayer:bottomBorder];
    
    self.leaderTxt.leftViewMode = UITextFieldViewModeAlways;
    self.leaderTxt.font=[UIFont fontWithName:@"OpenSans" size:13];
    self.leaderTxt.textColor = [UIColor blackColor];
    self.leaderTxt.backgroundColor = [UIColor clearColor];
    
    CALayer *bottomBorder1 = [CALayer layer];
    bottomBorder1.frame = CGRectMake(0.0f, self.leaderTxt.frame.size.height - 1, self.leaderTxt.frame.size.width, 1.0f);
    bottomBorder1.backgroundColor = [UIColor lightGrayColor].CGColor;
    [self.leaderTxt.layer addSublayer:bottomBorder1];
    
    [self addBorderToButton:self.fromDateBtn];
    [self addBorderToButton:self.toDateBtn];
    [self addBorderToButton:self.fromTimeBtn];
    [self addBorderToButton:self.toTimeBtn];
    
    
  }

- (void)addBorderToButton:(UIButton *)button
{
    button.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12];
    [button setTitleColor:RGB(0,0,0) forState:UIControlStateNormal];
    button.backgroundColor = RGB(255,255,255);
    
    CALayer *border = [CALayer layer];
    border.backgroundColor = RGB(192,192,192).CGColor;
    button.backgroundColor = [UIColor clearColor];
    border.frame = CGRectMake(0.0f, button.frame.size.height - 1, button.frame.size.width, 1.0f);
    [button.layer addSublayer:border];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)SymboldropBtn:(id)sender
{
    ViewContainer.frame = CGRectMake(self.symbolTxt.frame.origin.x, (self.view.bounds.size.height)-420, self.symbolTxt.frame.size.width, 120);
    pickerView.frame = CGRectMake(0, 44, ViewContainer.frame.size.width, 120);
    pickerView.hidden = NO;
    pickerView.showsSelectionIndicator = YES;
    pickerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.layer.borderWidth = 1.0f;
    ViewContainer.backgroundColor = [UIColor whiteColor];// clearColor for transparent
    
    UIToolbar *controlToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, ViewContainer.bounds.size.width, 35)];
    [controlToolBar setBarStyle:UIBarStyleDefault];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *setButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(Set)];
    [setButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(Cancel)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [controlToolBar setItems:[NSArray arrayWithObjects:spacer,setButton,cancelButton,nil]animated:NO];
    
    [ViewContainer addSubview:controlToolBar];
    [ViewContainer addSubview:pickerView];
    [self.view addSubview:ViewContainer];
    
    
}
-(void)Set
{
    dataStr = [NSString stringWithFormat:@"%@",[dataArray objectAtIndex:[pickerView selectedRowInComponent:0]]];
    
    self.symbolTxt.text = dataStr;
    
    [ViewContainer removeFromSuperview];
}
-(void)Cancel
{
    [ViewContainer removeFromSuperview];
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [dataArray count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [dataArray objectAtIndex:row];
}

-(IBAction)buyAction:(id)sender
{
//    if (buyFlag == false)
//    {
        buyImageBtn.selected = YES;
    sellImageBtn.selected = NO;
//        buyFlag = true;
    }
//    else
//    {
//        buyFlag = false;

    //}

-(IBAction)sellAction:(id)sender
{
//    if (sellFlag == false)
//    {
        sellImageBtn.selected = YES;
    buyImageBtn.selected = NO;

//        sellFlag = true;
//    else
//    {
//        sellFlag = false;
//        
//    }
}

-(IBAction)tradeAction:(id)sender
{
    if (tradeFlag == false)
    {
        tradeImageBtn.selected = YES;
        tradeFlag = true;
    }
    else
    {
        tradeImageBtn.selected = NO;
        tradeFlag = false;
        
    }
}

-(IBAction)shortAction:(id)sender
{
    if (shortFlag == false)
    {
        shortImgBtn.selected = YES;
        shortFlag = true;
    }
    else
    {
        shortImgBtn.selected = NO;
        shortFlag = false;
        
    }
}

-(IBAction)longAction:(id)sender
{
    if (longFlag == false)
    {
        longImgBtn.selected = YES;
        longFlag = true;
    }
    else
    {
        longImgBtn.selected = NO;
        longFlag = false;
        
    }
}



-(IBAction)fromDateAction:(id)sender
{
    [self.view endEditing:YES];
    
    fromDateViewContainer.frame = CGRectMake(self.fromDateBtn.frame.origin.x, (self.view.bounds.size.height)-200, self.fromDateBtn.frame.size.width+100, 100);
   
    
    fromDatePicker.frame= CGRectMake(0, 44, fromDateViewContainer.frame.size.width, 100);
    fromDatePicker.backgroundColor= [UIColor whiteColor];
    fromDatePicker.datePickerMode = UIDatePickerModeDate;
    fromDatePicker.hidden=NO;
    
    UIToolbar *controlToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, fromDateViewContainer.bounds.size.width, 44)];
    [controlToolBar setBarStyle:UIBarStyleDefault];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    UIBarButtonItem *setButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(SetBtn)];
//    [setButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(fromDateDoneBtn)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [controlToolBar setItems:[NSArray arrayWithObjects:spacer,cancelButton,nil]animated:NO];
    
    [fromDateViewContainer addSubview:controlToolBar];
    [fromDateViewContainer addSubview:fromDatePicker];
    [self.view addSubview:fromDateViewContainer];
}

- (void)fromDateDoneBtn
{
    NSArray *listofViews=[fromDateViewContainer subviews];
    
    for(UIView *subView in listofViews)
    {
        if([subView isKindOfClass:[UIDatePicker class]])
        {
            fromDate =[(UIDatePicker *)subView date];
        }
    }
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *_fromTime =[formatter stringFromDate:fromDate];
    [_fromDateBtn setTitle:[NSString stringWithFormat:@"%@",_fromTime] forState:UIControlStateNormal];
    [fromDateViewContainer removeFromSuperview];

}

-(IBAction)toDateAction:(id)sender
{
    [self.view endEditing:YES];
    
    toDateViewContainer.frame = CGRectMake(self.toDateBtn.frame.origin.x, (self.view.bounds.size.height)-130, self.toDateBtn.frame.size.width+100, 85);
    
    
    toDatePicker.frame= CGRectMake(0, 44, toDateViewContainer.frame.size.width, 85);
    toDatePicker.backgroundColor= [UIColor whiteColor];
    toDatePicker.datePickerMode = UIDatePickerModeDate;
    toDatePicker.hidden=NO;
    
    UIToolbar *controlToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, toDateViewContainer.bounds.size.width, 44)];
    [controlToolBar setBarStyle:UIBarStyleDefault];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //    UIBarButtonItem *setButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(SetBtn)];
    //    [setButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(toDateDoneBtn)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [controlToolBar setItems:[NSArray arrayWithObjects:spacer,cancelButton,nil]animated:NO];
    
    [toDateViewContainer addSubview:controlToolBar];
    [toDateViewContainer addSubview:toDatePicker];
    [self.view addSubview:toDateViewContainer];
}

- (void)toDateDoneBtn
{
    NSArray *listofViews=[toDateViewContainer subviews];
    
    for(UIView *subView in listofViews)
    {
        if([subView isKindOfClass:[UIDatePicker class]])
        {
            toDate =[(UIDatePicker *)subView date];
        }
    }
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *_fromTime =[formatter stringFromDate:toDate];
    [_toDateBtn setTitle:[NSString stringWithFormat:@"%@",_fromTime] forState:UIControlStateNormal];
    [toDateViewContainer removeFromSuperview];
    
}

-(IBAction)fromTimeAction:(id)sender
{
    [self.view endEditing:YES];
    
    fromTimeViewContainer.frame = CGRectMake(self.fromTimeBtn.frame.origin.x, (self.view.bounds.size.height)-170, self.fromTimeBtn.frame.size.width, 100);
    
    
    fromTimePicker.frame= CGRectMake(0, 44, fromTimeViewContainer.frame.size.width, 100);
    fromTimePicker.backgroundColor= [UIColor whiteColor];
    fromTimePicker.datePickerMode = UIDatePickerModeTime;
    fromTimePicker.hidden=NO;
    
    UIToolbar *controlToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, fromTimeViewContainer.bounds.size.width, 44)];
    [controlToolBar setBarStyle:UIBarStyleDefault];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //    UIBarButtonItem *setButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(SetBtn)];
    //    [setButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(fromTimeDoneBtn)];
    

    
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [controlToolBar setItems:[NSArray arrayWithObjects:spacer,cancelButton,nil]animated:NO];
    
    [fromTimeViewContainer addSubview:controlToolBar];
    [fromTimeViewContainer addSubview:fromTimePicker];
    [self.view addSubview:fromTimeViewContainer];
}

- (void)fromTimeDoneBtn
{
    NSArray *listofViews=[fromTimeViewContainer subviews];
    
    for(UIView *subView in listofViews)
    {
        if([subView isKindOfClass:[UIDatePicker class]])
        {
            fromTime =[(UIDatePicker *)subView date];
        }
    }
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm a"];

    NSString *_fromTime =[dateFormatter stringFromDate:fromTime];
    [_fromTimeBtn setTitle:[NSString stringWithFormat:@"%@",_fromTime] forState:UIControlStateNormal];
    [fromTimeViewContainer removeFromSuperview];
    
}

-(IBAction)toTimeAction:(id)sender
{
    [self.view endEditing:YES];
    
    toTimeViewContainer.frame = CGRectMake(self.toTimeBtn.frame.origin.x, (self.view.bounds.size.height)-130, self.toTimeBtn.frame.size.width, 85);
    
    
    toTimePicker.frame= CGRectMake(0, 44, toTimeViewContainer.frame.size.width, 85);
    toTimePicker.backgroundColor= [UIColor whiteColor];
    toTimePicker.datePickerMode = UIDatePickerModeTime;
    toTimePicker.hidden=NO;
    
    UIToolbar *controlToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, toTimeViewContainer.bounds.size.width, 44)];
    [controlToolBar setBarStyle:UIBarStyleDefault];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //    UIBarButtonItem *setButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(SetBtn)];
    //    [setButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(toTimeDoneBtn)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],  NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    [controlToolBar setItems:[NSArray arrayWithObjects:spacer,cancelButton,nil]animated:NO];
    
    [toTimeViewContainer addSubview:controlToolBar];
    [toTimeViewContainer addSubview:toTimePicker];
    [self.view addSubview:toTimeViewContainer];
}

- (void)toTimeDoneBtn
{
    NSArray *listofViews=[toTimeViewContainer subviews];
    
    for(UIView *subView in listofViews)
    {
        if([subView isKindOfClass:[UIDatePicker class]])
        {
            toTime =[(UIDatePicker *)subView date];
        }
    }
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    NSString *_fromTime =[dateFormatter stringFromDate:toTime];
    [_toTimeBtn setTitle:[NSString stringWithFormat:@"%@",_fromTime] forState:UIControlStateNormal];
    [toTimeViewContainer removeFromSuperview];
    
}



-(IBAction)cancelBtn:(id)sender
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}



@end
