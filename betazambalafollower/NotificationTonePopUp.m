//
//  NotificationTonePopUp.m
//  testing
//
//  Created by zenwise technologies on 22/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "NotificationTonePopUp.h"
#import "RadioButton.h"

@interface NotificationTonePopUp ()

@end

@implementation NotificationTonePopUp

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    RadioButton *rb1 = [[RadioButton alloc] initWithGroupId:@"first group" index:0];
    RadioButton *rb2 = [[RadioButton alloc] initWithGroupId:@"first group" index:1];
    RadioButton *rb3 = [[RadioButton alloc] initWithGroupId:@"first group" index:2];
    RadioButton *rb4 = [[RadioButton alloc] initWithGroupId:@"first group" index:3];
    RadioButton *rb5 = [[RadioButton alloc] initWithGroupId:@"first group" index:4];
    RadioButton *rb6 = [[RadioButton alloc] initWithGroupId:@"first group" index:5];
    RadioButton *rb7 = [[RadioButton alloc] initWithGroupId:@"first group" index:6];

    
    rb1.frame = CGRectMake(23,70,22,22);
    rb2.frame = CGRectMake(23,100,22,22);
    rb3.frame = CGRectMake(23,130,22,22);
    rb4.frame = CGRectMake(23,160,22,22);
    rb5.frame = CGRectMake(23,190,22,22);
    rb6.frame = CGRectMake(23,220,22,22);
    rb7.frame = CGRectMake(23,250,22,22);
    
    [self.view addSubview:rb1];
    [self.view addSubview:rb2];
    [self.view addSubview:rb3];
    [self.view addSubview:rb4];
    [self.view addSubview:rb5];
    [self.view addSubview:rb6];
    [self.view addSubview:rb7];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelBtn:(id)sender {
    
   [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)okBtn:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}
@end
