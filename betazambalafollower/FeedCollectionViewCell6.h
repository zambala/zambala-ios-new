//
//  FeedCollectionViewCell6.h
//  testing
//
//  Created by zenwise mac 2 on 12/26/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCollectionViewCell6 : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *descripLbl6;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl6;


@end
