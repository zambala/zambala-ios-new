//
//  AggrementViewController.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 15/12/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AggrementViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *aggrementWebView;

@end
