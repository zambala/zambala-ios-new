//
//  IndexWatch.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 14/12/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "IndexWatch.h"

@implementation IndexWatch
- (id)init {
    if (self = [super init]) {
        
        self.usAT=0;
        self.btDataType=0; // 0:UnCompress , 1:lzo_compress.zLib
        self.usSize=0;
        self.usMsgCode=0;
        self.inSeqID=0;
        self.stExchange=@""; //48
        self.btNull1=0;
        
        self.stSymbol1=@""; //48
        self.btNull2=0;
        
        self.inBroadcastTime=0;
        
        self.stSymbol2=@""; // 32
        self.btNull3=0;
        
        self.dbIndexPrice=0;
        self.dbPrevIndexClose=0;
        self.dbOpen=0;
        self.dbHigh=0;
        self.dbLow=0;
        self.dbLifeTimeHigh=0;
        self.dbLifeTimeLow=0;
    }
    return self;
}


+ (id)Mt6 {
    static IndexWatch *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

@end
