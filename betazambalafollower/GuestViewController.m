//
//  GuestViewController.m
//  testing
//
//  Created by zenwise technologies on 22/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "GuestViewController.h"
#import "OTPVerificationViewViewController.h"
#import "AppDelegate.h"

@interface GuestViewController ()
{
    AppDelegate * delegate1;
    
}

@end

@implementation GuestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
//    [self outFocusTextField];
    
    self.numberTxtField.keyboardType=UIKeyboardTypePhonePad;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)dismissKeyboard
{
    [self.numberTxtField resignFirstResponder];
}

//-(void)inFocusTextField
//{
//    self.numberTxtField.leftViewMode = UITextFieldViewModeAlways;
//    self.numberTxtField.font=[UIFont fontWithName:@"OpenSans" size:13];
//    self.numberTxtField.textColor = [UIColor blackColor];
//    self.numberTxtField.backgroundColor = [UIColor clearColor];
//    
//    CALayer *bottomBorder = [CALayer layer];
//    bottomBorder.frame = CGRectMake(0.0f, self.numberTxtField.frame.size.height - 1, self.numberTxtField.frame.size.width, 1.0f);
//    bottomBorder.backgroundColor = [UIColor redColor].CGColor;
//    [self.numberTxtField.layer addSublayer:bottomBorder];
//}
//
//-(void)outFocusTextField
//{
//    self.numberTxtField.leftViewMode = UITextFieldViewModeAlways;
//    self.numberTxtField.font=[UIFont fontWithName:@"OpenSans" size:13];
//    self.numberTxtField.textColor = [UIColor blackColor];
//    self.numberTxtField.backgroundColor = [UIColor clearColor];
//    
//    CALayer *bottomBorder = [CALayer layer];
//    bottomBorder.frame = CGRectMake(0.0f, self.numberTxtField.frame.size.height - 1, self.numberTxtField.frame.size.width, 1.0f);
//    bottomBorder.backgroundColor = [UIColor greenColor].CGColor;
//    [self.numberTxtField.layer addSublayer:bottomBorder];
//}


//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//
//{
//    [self inFocusTextField];
//    return YES;
//}
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [self outFocusTextField];
//    return YES;
//}



- (IBAction)sendAction:(id)sender {
    
    if(_numberTxtField.text.length==10)
    {
        delegate1.numberStr=self.numberTxtField.text;
        OTPVerificationViewViewController * otp=[self.storyboard instantiateViewControllerWithIdentifier:@"OTPVerificationViewViewController"];
        [self.navigationController pushViewController:otp animated:YES];
    }
    
    else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter the mobile number" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        

    }
}
@end
