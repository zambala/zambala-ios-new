//
//  ViewController.m
//  ProfileFollower
//
//  Created by guna on 16/03/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "NewProfile.h"
#import "TableViewCell.h"
#import "AppDelegate.h"
#import "MKSHorizontalLineProgressView.h"
#import "HCSStarRatingView.h"
#import "NewProfileTableViewCell.h"
#import "KNCirclePercentView.h"
#import "KNPercentLabel.h"
#import "StockView1.h"
#import "StockOrderView.h"
#import "NewOffersByMonksViewController.h"
#import "FeedsView.h"
#import "NoAdvicesProfileTableViewCell.h"
#import <Mixpanel/Mixpanel.h>
#import "KATCircularProgress.h"

@interface NewProfile ()
{
    AppDelegate * delegate1;
    int subscribed;
    NSString * EPString;
    NSString * TPString;
    NSString * SLString;
    NSString * buySell1;
    NSString * messageStr;
    int buySell;
    NSString * str;
    NSArray * segmentsArray;
}

@end

@implementation NewProfile

- (void)viewDidLoad {
    
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Leader Recent Advices page"];
 str=@"%";
    [self profileServer];
    [self recentAdvicesServer];
    self.profileTableView.delegate=self;
    self.profileTableView.dataSource=self;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.premiumSerivceBtn addGestureRecognizer:singleFingerTap];
    
    
    
    UITapGestureRecognizer *singleFingerTap1 =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap1:)];
    [self.feedsBtn addGestureRecognizer:singleFingerTap1];
    

    
    
    [self.followBtn addTarget:self action:@selector(followAction) forControlEvents:UIControlEventTouchUpInside];
  
    
    

}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    delegate1.cartLeaderName=[NSString stringWithFormat:@"%@",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"firstname"]];
    
    delegate1.leaderid=[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"userid"];
    NewOffersByMonksViewController * offers = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOffersByMonksViewController"];
    [self.navigationController pushViewController:offers animated:YES];
    
    //Do stuff here...
}

- (void)handleSingleTap1:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    delegate1.profileTOFeeds=@"YES";
    
    FeedsView * feed = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedsView"];
    [self.navigationController pushViewController:feed animated:YES];
    
    //Do stuff here...
}

-(void)followAction
{
    NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                               @"cache-control": @"no-cache",
                               };
    
    NSString * userid=[NSString stringWithFormat:@"leaderid=%@",delegate1.leaderIDWhoToFollow];
    
    NSString * clientId=[NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
    
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[userid dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[clientId dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSArray * subscribeArray=@[@1];
    
    NSString * subscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",subscribeArray];
    NSString * sub = [subscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
    NSString * sub1 = [sub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
    
    NSArray * unSubscribeArray=@[];
    
    NSString * unSubscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",unSubscribeArray];
    NSString * unSub = [unSubscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
    NSString * unSub1 = [unSub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
    
    
    NSString * btnTitle=[NSString stringWithFormat:@"%@",self.followBtn.currentTitle];
    
    
    if([btnTitle isEqualToString:@"Following"])
        
    {
        [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[unSub1 dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
    }
    else if ([btnTitle isEqualToString:@"Follow +"])
    {
        [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[sub1 dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
    }
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"%@",followDict);
                                                        
                                                        messageStr=[followDict objectForKey:@"message"];
                                                        
                                                        
                                                        
                                                    }
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        if([messageStr isEqualToString:@"Subscriber created"])
                                                        {
                                                            if([self.followBtn.currentTitle isEqualToString:@"Follow +"])
                                                            {
                                                                 [self.followBtn setTitle:@"Following" forState:UIControlStateNormal];
                                                            }
                                                            else
                                                            {
                                                                  [self.followBtn setTitle:@"Follow +" forState:UIControlStateNormal];
                                                                
                                                            }
                                                           
                                                            
                                                            
                                                        }
                                                    });
                                                    
                                                    
                                                    
                                                }];
    [dataTask resume];
    
    
    
    
    
    
    
    
    
}





-(void)recentAdvicesServer
{
     @try {
    segmentsArray=[[NSArray alloc]init];
    
    segmentsArray=[delegate1.brokerInfoDict objectForKey:@"segment"];
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                                };
    NSDictionary *parameters;
         
     
   
        parameters = @{ @"leaderid": delegate1.leaderIDWhoToFollow,
                        @"messagetypeid":@1,
                        @"active":@(true),
                        @"sortby":@"DESC",
                        @"orderby":@"createdon",
                        @"clientid":delegate1.userID,
                        @"limit":@100,
                        @"segment":segmentsArray,
                        @"isleaderadvice":@(true)
                        };

  
//
    
  
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/wisdomgarden",delegate1.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        if(data!=nil)
                                                        {
                                                            self.recentAdvicesDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            NSLog(@"Recent Advices:%@",self.recentAdvicesDictionary);
                                                            
                                                            
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            [self.profileTableView reloadData];
                                                            
                                                            
                                                        });

                                                        
                                                    }
                                                }];
    [dataTask resume];
         
     } @catch (NSException *exception) {
         
         
         
     } @finally {
         
     }
}

-(void)profileServer
{
    NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                               @"cache-control": @"no-cache",
                               };
    
    NSString * leaderID = [NSString stringWithFormat:@"leaderid=%@",delegate1.leaderIDWhoToFollow];
     NSString * clientid = [NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[leaderID dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postData appendData:[clientid dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/leaders",delegate1.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        self.profileDetailReponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"Response Dict leader:%@",self.profileDetailReponseDictionary);
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        @try {
                                                            
                                                            NSString * subscribeString=[NSString stringWithFormat:@"%@",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"subscribed"]];
                                                            
                                                            int subscribeInt=[subscribeString intValue];
                                                            
                                                            if(subscribeInt==0)
                                                            {
                                                                [self.followBtn setTitle:@"Follow +" forState:UIControlStateNormal];
                                                                
                                                            }
                                                            
                                                            else if(subscribeInt==1)
                                                            {
                                                               [self.followBtn setTitle:@"Following" forState:UIControlStateNormal];
                                                            }
                                                            
                                                            @try {
                                                                NSString* rating=[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"rating"];
                                                                if([rating isEqual:[NSNull null]])
                                                                {
                                                                    self.starRatingView.value=0;
                                                                }else
                                                                {
                                                                    self.starRatingView.value = [rating floatValue];
                                                                }
                                                                
                                                                
                                                                self.starRatingView.minimumValue=0;
                                                                self.starRatingView.maximumValue=5;
                                                                self.starRatingView.allowsHalfStars=YES;
                                                                self.starRatingView.allowsHalfStars=YES;
                                                            } @catch (NSException *exception) {
                                                                
                                                            } @finally {
                                                                
                                                            }
                                                            
                                                            
                                                           
                                int dayTrade = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"daytradecount"] intValue];

                             int shortTerm = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"shorttermcount"]intValue];

                                 int longTerm = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"longtermcount"]intValue];

                                        int totalAdvices=dayTrade+shortTerm+longTerm;

                                                                float dayPer=0;
                                                                float shortPer=0;
                                                                float longPer=0;


                                                                if(dayTrade!=0)
                                                                {
                                                                      dayPer=(float)dayTrade/(float)totalAdvices*100;

                                                                }

                                                                if(shortTerm!=0)
                                                                {
                                                         shortPer=(float)shortTerm/(float)totalAdvices*100;

                                                                }

                                                                if(longTerm!=0)
                                                                {
                                                                     longPer=(float)longTerm/(float)totalAdvices*100;

                                                                }
                              
                                                            NSString * str1=@"%";
                                                            
    NSString * dayTrade1 = [NSString stringWithFormat:@"%.2f%@",[[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"dthitrate"]floatValue],str1];

     NSString * shortTerm1 = [NSString stringWithFormat:@"%.2f%@",[[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"sthitrate"]floatValue],str1];

     NSString * longTerm1 = [NSString stringWithFormat:@"%.2f%@",[[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"lthitrate"]floatValue],str1];
                                                            
                                                            self.dtLbl.text=dayTrade1;
                                                            self.stLbl.text=shortTerm1;
                                                            self.ltLbl.text=longTerm1;
                                                            
                                                            self.dtLbl.textColor=[UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:102.0f/255.0f alpha:1.0f];
                                                            
                                                            self.stLbl.textColor=[UIColor colorWithRed:165.0f/255.0f green:225.0f/255.0f blue:241.0f/255.0f alpha:1.0f];
                                                            
                                                            self.ltLbl.textColor=[UIColor colorWithRed:227.0f/255.0f green:168.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
                                                            
                                                            
          
                                                                  self.dayTradeView.progressValue=100.0f;
                                                            
                                                                    self.shortTermView.progressValue=100.0f;
                                                            
                                                                    self.longTermView.progressValue=100.0f;
                                                            
                                                            
                                                            NSString * dt = [NSString stringWithFormat:@"(%@/%@)",[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"dthitcount"],[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"daytradecount"]];
                                                            NSString * st = [NSString stringWithFormat:@"(%@/%@)",[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"sthitcount"],[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"shorttermcount"]];
                                                            NSString * lt = [NSString stringWithFormat:@"(%@/%@)",[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"lthitcount"],[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"longtermcount"]];
                                                            self.DT.text=dt;
                                                            self.ST.text=st;
                                                            self.LT.text=lt;
                                                                
                                                                
                                                                self.dayTradeView.barColor = [UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:102.0f/255.0f alpha:1.0f];
                                                                self.dayTradeView.trackColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1];
                                                                self.dayTradeView.barThickness = self.dayTradeView.frame.size.height;
                                                                self.dayTradeView.showPercentageText = NO;
                                                                
                                                                
                                                                
                                                                self.shortTermView.barColor = [UIColor colorWithRed:165.0f/255.0f green:225.0f/255.0f blue:241.0f/255.0f alpha:1.0f];
                                                                self.shortTermView.trackColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1];
                                                                self.shortTermView.barThickness = self.shortTermView.frame.size.height;
                                                                self.shortTermView.showPercentageText = NO;
                                                                
                                                                
                                                                
                                                                self.longTermView.barColor = [UIColor colorWithRed:227.0f/255.0f green:168.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
                                                                self.longTermView.trackColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1];
                                                                self.longTermView.barThickness = self.shortTermView.frame.size.height;
                                                                self.longTermView.showPercentageText = NO;
                                                            
                                                         
                                                            
                                                            @try {
                                                                
                                                                int hitRate = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"hitrate"] intValue];
                                                                NSString * str=@"%";
                                                                NSString * hitrateStr=[NSString stringWithFormat:@"%i%@",hitRate,str];
                                                                
                                                                self.overallAccLbl.text=hitrateStr;
                                                              
//                                                                int hitrate1=5;
                                                                
                                                                //Clear the slice items array.
                                                                [self.progressView.sliceItems removeAllObjects];

                                                                //set bar thickness
                                                                [self.progressView setLineWidth:15.0];
                                                                //set animation duration
                                                                [self.progressView setAnimationDuration:2];

                                                                //Create Slice Item objects.
                                                                SliceItem *item1 = [[SliceItem alloc] init];
                                                                item1.itemValue = dayPer;
                                                                item1.itemColor = [UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:102.0f/255.0f alpha:1.0f];;

                                                                SliceItem *item2 = [[SliceItem alloc] init];
                                                                item2.itemValue = shortPer;
                                                                item2.itemColor =[UIColor colorWithRed:165.0f/255.0f green:225.0f/255.0f blue:241.0f/255.0f alpha:1.0f];

                                                                SliceItem *item3 = [[SliceItem alloc] init];
                                                                item3.itemValue = longPer;
                                                                item3.itemColor = [UIColor colorWithRed:227.0f/255.0f green:168.0f/255.0f blue:246.0f/255.0f alpha:1.0f];

                                                                
                                                                //add objects to the sliceItems array.
                                                                [self.progressView.sliceItems addObject:item1];
                                                                [self.progressView.sliceItems addObject:item2];
                                                                [self.progressView.sliceItems addObject:item3];
                                                               
                                                                //reload the chart
                                                                [self.progressView reloadData];
                                                                

                                                            } @catch (NSException *exception) {
                                                                
                                                            } @finally {
                                                                
                                                            }
                                                            
                                                            
                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                            
                                                        
                                                        self.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
                                                        self.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
                                                        self.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
                                                        [self.starRatingView setUserInteractionEnabled:NO];
                                                        
                                                        @try {
                                                              self.leaderNameLbl.text=[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"firstname"];
                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        
                                                      
                                                        
                                                        @try {
                                                            NSString *about = [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"about"] objectForKey:@"info"];
                                                            if([about isEqual:[NSNull null]]||[about isEqualToString:@""])
                                                            {
                                                                self.aboutLbl.text = @"No Info";
                                                            }else
                                                            {
                                                                self.aboutLbl.text = about;
                                                            }
                                                        } @catch (NSException *exception) {
                                                            
                                                             self.aboutLbl.text = @"No Info";
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        @try {
                                                            NSString * gainPercentage = [NSString stringWithFormat:@"%@",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"gainpercent"]];
                                                            if([gainPercentage isEqual:[NSNull null]])
                                                            {
                                                                self.gainPercentLabel.text=@"0 %";
                                                            }else
                                                            {
                                                                NSString * percent = @"%";
                                                               
                                                                float gainFloat = [gainPercentage floatValue];
                                                                NSString * gainString = [NSString stringWithFormat:@"%.2f ",gainFloat];
                                                                NSString * finalGainPercent = [gainString stringByAppendingString:percent];
                                                                self.gainPercentLabel.text=[NSString stringWithFormat:@"%@gain",finalGainPercent];
                                                            }
                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        @try {
                                                            
                                                            NSString * followersCount =[NSString stringWithFormat:@"%@ followers",[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"followercount"]];
                                                            
                                                            if([followersCount isEqual:[NSNull null]]||[followersCount isEqualToString:@"<null>"])
                                                            {
                                                                self.followerCount.text=@"0 followers";
                                                            }else
                                                            {
                                                                self.followerCount.text=followersCount;
                                                            }

                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        
                                                        @try {
                                                            
                                NSString * avgcalls =[NSString stringWithFormat:@"%i", [[[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"avgcallperday"]intValue]];
                                                            
                                                            if([avgcalls isEqual:[NSNull null]])
                                                            {
                                                                self.averageCalls.text= @"0";
                                                            }else
                                                            {
                                                                self.averageCalls.text =avgcalls;
                                                            }
                                                            

                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        @try {
                                                            NSString * maxgain = [[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"maxgainpercent"];
                                                            
                                                            
                                                            if([maxgain isEqual:[NSNull null]])
                                                            {
                                                                self.maximumGain.text= @"0";
                                                            }else
                                                            {
                                                                NSString * maxGainPercent = [NSString stringWithFormat:@"%@",maxgain];
                                                                float maxGainFloat = [maxGainPercent floatValue];
                                                                
                                                                self.maximumGain.text = [NSString stringWithFormat:@"%.2f%@",maxGainFloat,str];
                                                            }
                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        
                                                        @try {
                                                            NSString * maxLoss = [[[self.profileDetailReponseDictionary objectForKey:@"results"] objectAtIndex:0] objectForKey:@"maxlosspercent"];
                                                            
                                                            if([maxLoss isEqual:[NSNull null]])
                                                            {
                                                                self.maximumLoss.text= @"0";
                                                            }else
                                                            {
                                                                NSString * maxLossPercent = [NSString stringWithFormat:@"%@",maxLoss];
                                                                float maxLoss = [maxLossPercent floatValue];
                                                                
                                                                self.maximumLoss.text =[NSString stringWithFormat:@"%.2f%@",maxLoss,str];
                                                            }

                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                            
                                                        
                                                        @try {
                                                            NSMutableArray * segment = [[NSMutableArray alloc]init];
                                                            
                                                            segment=[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"segment"];
                                                            
                                                            NSMutableString * segmentMutString = [[NSMutableString alloc]init];
                                                            
                                                            for(int i=1;i<=4;i++)
                                                            {
                                                                
                                                                //for (int j=0; j<segment.count; j++) {
                                                                if([segment containsObject:[NSNumber numberWithInt:i]])
                                                                {
                                                                    if(i==1)
                                                                    {
                                                                        [segmentMutString appendString:@",Equities"];
                                                                    }
                                                                    if(i==2)
                                                                    {
                                                                        [segmentMutString appendString:@",Derivatives"];
                                                                    }
                                                                    if(i==3)
                                                                    {
                                                                        [segmentMutString appendString:@",Currency"];
                                                                    }
                                                                    if(i==4)
                                                                    {
                                                                        [segmentMutString appendString:@",Commodities"];
                                                                    }
                                                                    
                                                                    //  }
                                                                    
                                                                }
                                                            }
                                                            NSLog(@"Segment Test:%@",segmentMutString);
                                                            
                                                            NSString * aboutString=[segmentMutString mutableCopy];
                                                            
                                                            
                                                            aboutString = [aboutString substringFromIndex:1];
                                                            
                                                          
                                                            
                                                            
                                                            self.segmentsLabel.text = aboutString;
                                                            
                                                            
                                                            NSMutableArray * specialization = [[NSMutableArray alloc]init];
                                                            
                                                            specialization=[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"specialization"];
                                                            
                                                            NSMutableString * specializationMutString = [[NSMutableString alloc]init];
                                                            
                                                            for(int i=1;i<=3;i++)
                                                            {
                                                                
                                                                //for (int j=0; j<segment.count; j++) {
                                                                if([specialization containsObject:[NSNumber numberWithInt:i]])
                                                                {
                                                                    if(i==1)
                                                                    {
                                                                        [specializationMutString appendString:@",DayTrade"];
                                                                    }
                                                                    if(i==2)
                                                                    {
                                                                        [specializationMutString appendString:@",ShortTerm"];
                                                                    }
                                                                    if(i==3)
                                                                    {
                                                                        [specializationMutString appendString:@",LongTerm"];
                                                                    }
                                                                   
                                                                    
                                                                    //  }
                                                                    
                                                                }
                                                            }
                                                            NSLog(@"Specialization Test:%@",specializationMutString);
                                                            
                                                            NSString * aboutString1=[specializationMutString mutableCopy];
                                                            
                                                            
                                                            aboutString1 = [aboutString1 substringFromIndex:1];
                                                            self.specializationLabel.text = aboutString1;
                                                            
                                                            NSString * logo=[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"logourl"];
                                                            
                                                            self.profileImage.layer.cornerRadius=self.profileImage.frame.size.width / 2;
                                                            self.profileImage.clipsToBounds = YES;
                                                            
                                                            if([logo isEqual:[NSNull null]])
                                                            {
                                                                self.profileImage.image = nil;
                                                            }
                                                            
                                                            else
                                                            {
                                                                
                                                                NSURL *url = [NSURL URLWithString:logo];
                                                                
                                                                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                                    if (data) {
                                                                        UIImage *image = [UIImage imageWithData:data];
                                                                        if (image) {
                                                                            
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                     self.profileImage.image=image;
                                                           
                                                                            });
                                                                        }
                                                                    }
                                                                }];
                                                                [task resume];
                                                            }
                                                            

                                                            NSString *dateStr =[[[self.profileDetailReponseDictionary objectForKey:@"results"]objectAtIndex:0]objectForKey:@"created"];
                                                            
                                                            NSLog(@"%@",dateStr);
                                                            
                                                            // Convert string to date object
                                                            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];  //2017-05-31T13:45:29.623Z
                                                            [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                                                            NSDate *date = [dateFormat dateFromString:dateStr];
                                                            
                                                            
                                                            NSString *differnce = [self remaningTime:date endDate:[NSDate date]];
                                                            
                                                            

                                                            self.activeStatusLbl.text=[NSString stringWithFormat:@"Last Active %@ ago",differnce];

                                                        } @catch (NSException *exception) {
                                                            
                                                        } @finally {
                                                            
                                                        }
                                                        
                                                        
                                                    });
                                                    

                                                }];
    [dataTask resume];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([[self.recentAdvicesDictionary objectForKey:@"data"] count]>0)
    {
    
    return [[self.recentAdvicesDictionary objectForKey:@"data"] count];
    }
    else
    {
        return 1;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if([[self.recentAdvicesDictionary objectForKey:@"data"] count]>0)
    {
    
   
       NewProfileTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"profile" forIndexPath:indexPath];
        
        //        cell.menuLabel.text=[self.nameArray objectAtIndex:indexPath.row ];
        //        cell.menuListImage.image= [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.ImageArray objectAtIndex:indexPath.row]]];
        //        cell.backgroundColor = [UIColor clearColor];
        
        
//        NSString * dateStr=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"];
//
//
//
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
//        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
//        NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
//
//        // change to a readable time format and change to local time zone
//        [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
//        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
//        NSString *finalDate = [dateFormatter stringFromDate:date];
//
//
//
//
//
//        cell.dateAndTimeLabel.text=finalDate;
//
        
        
        
        NSString * dateStr=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"];
        
        
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
        
        // change to a readable time format and change to local time zone
        [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *finalDate = [dateFormatter stringFromDate:date];
        
        
        
        
        
        cell.dateAndTimeLabel.text=finalDate;
        
        
    
    
    @try {
          cell.ltpLabel.text = [[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
   
        
        NSString * changePercentlabel = [NSString stringWithFormat:@"%@",[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
        
        NSString * percent =@"  ";
        NSString * finalChangePercentlabel = [changePercentlabel stringByAppendingString:percent];
        if([changePercentlabel isEqual:[NSNull null]]||changePercentlabel==nil||[changePercentlabel isEqualToString:@"<null>"])
        {
            
            
            
            NSString * percentage = @"";
            NSString * zero = @"  0";
            NSString* final = [zero stringByAppendingString:percentage];
            cell.changePercentLabel.text=final;
            cell.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
        }
        else
        {
            
            if([finalChangePercentlabel containsString:@"-"])
            {
                cell.changePercentLabel.text=finalChangePercentlabel;
                cell.changePercentLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                
                cell.oLabel.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
            }
            else{
                cell.changePercentLabel.text=finalChangePercentlabel;
                cell.changePercentLabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                cell.oLabel.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
            }
            
            
        }
        
            @try {
        
        NSString * duration=[NSString stringWithFormat:@"%@",[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"durationtype"]];
        
        int durationInt=[duration intValue];
        
        if(durationInt==1)
        {
            cell.stLabel.text=@"DT";
            
            cell.stLabel.backgroundColor=[UIColor colorWithRed:(255/255.0) green:(192/255.0) blue:(102/255.0) alpha:1];
        }
        
        else if(durationInt==2)
        {
            cell.stLabel.text=@"ST";
            
            cell.stLabel.backgroundColor=[UIColor colorWithRed:(165/255.0) green:(225/255.0) blue:(241/255.0) alpha:1];
        }
        else if(durationInt==3)
        {
            
            
            
            cell.stLabel.text=@"LT";
            
            cell.stLabel.backgroundColor=[UIColor colorWithRed:(227/255.0) green:(168/255.0) blue:(246/255.0) alpha:1];
            
        }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
        
        
        
    
  
    @try {
        
        NSString * actedBy = [[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"actedby"];
        
        
        if([actedBy isEqual:[NSNull null]])
        {
            cell.actedByLabel.text=@"0";
        }else
        {
            int actedByInt=[actedBy intValue];
            cell.actedByLabel.text = [NSString stringWithFormat:@"%i",actedByInt];
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
   
    @try {
        
        NSString * sharesSold = [[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"sharesold"];
        
        
        
        if([sharesSold isEqual:[NSNull null]])
        {
            cell.sharesSoldLabel.text = @"0";
        }else
        {
            int sharesSoldInt=[sharesSold intValue];
            cell.sharesSoldLabel.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    @try {
        
         NSString * buy = [[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"buysell"];
        
        buySell=[buy intValue];
        
        if(buySell==2)
        {
            [cell.buySellButton setTitle:@"SELL" forState:UIControlStateNormal];
            [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(242.0)/(255.0) green:(30.0)/(255.0) blue:(51.0)/(255.0) alpha:1]];
        }else if (buySell==1)
        {
            [cell.buySellButton setTitle:@"BUY" forState:UIControlStateNormal];
            [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(24.0)/(255.0) green:(131.0)/(255.0) blue:(213.0)/(255.0) alpha:1]];
            
        }

    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
  
    
    
    NSString * mainString=[NSString stringWithFormat:@"%@",[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"]];
        if([mainString containsString:@"@"])
        {
            cell.messageLabel.text=[mainString uppercaseString];
            cell.oLabel.text=@"C";
            [cell.buySellButton addTarget:self action:@selector(buySellAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        else
        {
            
    
    TPString=[NSString stringWithFormat:@"%@",[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"targetprice"]];
    
    if ([mainString isEqual:[NSNull null]]||mainString==nil||[mainString isEqualToString:@"<null>"] ||  [TPString isEqual:[NSNull null]]||TPString==nil||[TPString isEqualToString:@"<null>"]   ) {
        
        cell.messageLabel.text =@"<Null>";
        
    }
    
    else{
    
    @try {
        
        
        NSLog(@"main String %@",mainString);
        
        
        
        NSArray * mainArray=[mainString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        
        
        NSLog(@"array  %@",[mainArray objectAtIndex:0]);
        
        NSLog(@"array  %@",[mainArray objectAtIndex:1]);
        
        //                NSString * buySell=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:0]];
        
        @try {
            if(buySell==1)
            {
                buySell1=@"BUY";
                
            }else if (buySell==2)
            {
                buySell1=@"SELL";
                
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        @try {
            NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
            
            
            
            NSArray * companyArray=[string componentsSeparatedByString:@";"];
            
            
            
            NSLog(@" array1 %@",companyArray);
            
            
            
            //               for (int i=1; i<[companyArray count];i++) {
            
            
            
            //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
            //
            //
            //
            //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
            //
            //                   NSLog(@"%@",array2);
            
            NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
            NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
            NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
            NSString * SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
            NSArray * EPArray=[EP1 componentsSeparatedByString:@"="];
            NSArray * TPArray=[TP componentsSeparatedByString:@"="];
            NSArray * SLArray=[SL componentsSeparatedByString:@"="];
            
            EPString=[NSString stringWithFormat:@"%@",[EPArray objectAtIndex:1]];
            TPString=[NSString stringWithFormat:@"%@",[TPArray objectAtIndex:1]];
            SLString=[NSString stringWithFormat:@"%@",[SLArray objectAtIndex:1]];
            
            
            
            NSString * dummyfinalStr=[NSString stringWithFormat:@"%@ %@ @%@%@%@",buySell1,companyName,EP1,TP,SL];
            
            NSString * finalStr = [dummyfinalStr stringByReplacingOccurrencesOfString:@"=" withString:@" "];
            
            NSLog(@"%@",finalStr);
            
            NSLog(@"COMPANY %@",companyName);
            
            
            //                              if (i==1) {
            //                                  EP=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
            //
            //                                  NSLog(@"EP %@",[array2 objectAtIndex:1]);
            //
            //                              }
            
            //                   if (i==2) {
            //                       EPString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
            //
            //                       NSLog(@"TP %@",[array2 objectAtIndex:1]);
            //
            //                   }
            //
            //                   if (i==3) {
            //                       SLString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
            //
            //                       NSLog(@"SL %@",[array2 objectAtIndex:1]);
            //
            //                   }
            //               }
            //
            
            
            NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",buySell1];
            NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[companyArray objectAtIndex:0]];
            NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
            NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPString];
            NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
            NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPString];
            NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
            NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLString] ;
            
            NSString * string1 = [sell stringByAppendingString:muthootfin];
            NSString *string2 = [string1 stringByAppendingString:ep];
            NSString * string3 = [string2 stringByAppendingString:value1];
            NSString * string4 = [string3 stringByAppendingString:tp];
            NSString * string5 = [string4 stringByAppendingString:value2];
            NSString * string6 = [string5 stringByAppendingString:sl];
            NSString * finalString = [string6 stringByAppendingString:value3];
            
            
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            //UILabel * myLabel = [[UILabel alloc] init];
            // myLabel.attributedText = attrStr;
            cell.messageLabel.attributedText=attrStr;
        } @catch (NSException *exception) {
            
        } @finally {
            
        }

     
        
        [cell.buySellButton addTarget:self action:@selector(buySellAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    }
            
        }

        return cell;
    }
    else
    {
        NoAdvicesProfileTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"noprofile" forIndexPath:indexPath];
        return cell;
    }
    
}

-(void)buySellAction:(UIButton *)sender
{
    
    delegate1.navigationCheck=@"wisdom";
    
    delegate1.wisdomCheck=true;
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.profileTableView];
    
    
    
    NSIndexPath *indexPath = [self.profileTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    NewProfileTableViewCell *cell = [self.profileTableView cellForRowAtIndexPath:indexPath];
    
    delegate1.wisdomGardemTickIDString = [[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageid"];
    
    NSString * messagetypeid = [NSString stringWithFormat:@"%@",[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"messagetypeid"]];
    
    int messageInt = [messagetypeid intValue];
    
    if(messageInt==1)
    {
        
        
        @try {
            delegate1.symbolDepthStr=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"companyname"];
             delegate1.tradingSecurityDes=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"symbolname"];
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
        @try {
            delegate1.orderSegment=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"segmenttype"];
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
        
        @try {
            delegate1.orderBuySell=cell.buySellButton.currentTitle;
           
            
            if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
            {
                delegate1.orderinstrument=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"instrumentid"];
            }
            
            else
            {
                delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"exchangetoken"]];
            
                
                
            }
            
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            
        }
       
        
    }
    
    if(messageInt==1)
    {
        if(delegate1.leaderAdviceDetails.count>0)
        {
            [delegate1.leaderAdviceDetails removeAllObjects];
        }
        //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
        //    {
        //
        //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
        //        {
        //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        //
        //            [self.navigationController pushViewController:allocation animated:YES];
        //
        //        }
        
        //        else{
        delegate1.depth=@"WISDOM";
        
        NSLog(@"%@",delegate1.depth);
        
        
        
        //        delegate1.symbolDepthStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
        //
        //        delegate1.exchaneLblStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
        
        
        
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
        
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
        
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"]];
        
        NSString * logo=[NSString stringWithFormat:@"%@",[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
        
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        
        
        NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        
    }
    
    
    StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
    
    [self.navigationController pushViewController:orderView animated:YES];
        
        
   
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    delegate1.navigationCheck=@"wisdom";
    if([[self.recentAdvicesDictionary objectForKey:@"data"] count])
    {
        
        NSString * messagetypeid = [NSString stringWithFormat:@"%@",[[[self.recentAdvicesDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"messagetypeid"]];
        
        int messageInt = [messagetypeid intValue];
    if(messageInt==1)
    {
        if(delegate1.leaderAdviceDetails.count>0)
        {
            [delegate1.leaderAdviceDetails removeAllObjects];
        }
        //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
        //    {
        //
        //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
        //        {
        //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        //
        //            [self.navigationController pushViewController:allocation animated:YES];
        //
        //        }
        
        //        else{
        delegate1.depth=@"WISDOM";
        
        NSLog(@"%@",delegate1.depth);
        
        StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
        
        
        
        delegate1.symbolDepthStr=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
        
        delegate1.tradingSecurityDes=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"symbolname"];
        
        delegate1.exchaneLblStr=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
        
        delegate1.instrumentDepthStr=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"instrumentid"];
        
//        delegate1.expirySeriesValue=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"expirydate"];
        
        delegate1.expirySeriesValue=@"";
        
        
        
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
        
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
        
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"]];
        
        
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageid"]];
        
        [delegate1.leaderAdviceDetails addObject:[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"buysell"]];
        NSString * logo=[[[self.recentAdvicesDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"logourl"];
        
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        

        
        NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        
        [self.navigationController pushViewController:stockDetails animated:YES];
        
    }
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([[self.recentAdvicesDictionary objectForKey:@"data"] count]>0)
    {
    return 168;
    }
    else
    {
        return 500;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// New Date and Time

-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    NSDateComponents *components;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSString *durationString;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: startDate toDate: endDate options: 0];
    
    days = [components day];
    hour = [components hour];
    minutes = [components minute];
    
    if(days>0)
    {
        if(days>1)
            durationString=[NSString stringWithFormat:@"%ld days",(long)days];
        else
            durationString=[NSString stringWithFormat:@"%ld day",(long)days];
        return durationString;
    }
    if(hour>0)
    {
        if(hour>1)
            durationString=[NSString stringWithFormat:@"%ld hrs",(long)hour];
        else
            durationString=[NSString stringWithFormat:@"%ld hrs",(long)hour];
        return durationString;
    }
    if(minutes>0)
    {
        if(minutes>1)
            durationString = [NSString stringWithFormat:@"%ld min",(long)minutes];
        else
            durationString = [NSString stringWithFormat:@"%ld min",(long)minutes];
        
        return durationString;
    }
    return @""; 
}




@end
