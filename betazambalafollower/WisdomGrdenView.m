//
//  WisdomGrdenView.m
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "WisdomGrdenView.h"
#import "WisdomSubView.h"
#import "UIViewController+MJPopupViewController.h"
#import "MJSecondDetailViewController.h"
#import "PopUpView.h"
#import "WisdomFilterPopup.h"
#import "Equities.h"
#import "HMSegmentedControl.h"
#import "EquitiesCell1.h"
#import "EquitiesCell2.h"
#import "DerivativesCell.h"
#import "WisdomGardenOrders.h"
#import "protofolioAdviceOrders.h"
#import "StockAllocationView.h"
#import "WisdomStockView1.h"
#import "WisdomViewController.h"
#import "StockView1.h"
#import "AppDelegate.h"
#import "StockOrderView.h"
#import "NewProfile.h"
#import "StockAllocationView.h"
#import "NewPortOrderView.h"
#import <Mixpanel/Mixpanel.h>
#import "MQTTKit.h"
#include <AudioToolbox/AudioToolbox.h>
#import "WGFilterViewController.h"






@interface WisdomGrdenView () <MJSecondPopupDelegate>
{
    HMSegmentedControl * segmentedControl;
    NSString * EPString;/*!<it store entry price*/
    NSString * SLString;/*!<it store stop loss*/
    NSString * TPString;/*!<it store target price*/
    AppDelegate * delegate1;/*!<appdelegate reference*/
    NSString * EP;/*!<it store entry price*/
    NSString * buySell;/*!<it store transaction type*/
    NSMutableArray * derivativeResponse;/*!<it store derivative advices response*/
    NSDictionary *headers ;
    NSMutableURLRequest *request;
    NSArray * filterArray;/*!<it store filtered data*/
    NSMutableArray * adviceList;/*!<it store firstname of leader*/
    NSMutableArray * filterAdvice;/*!<it store filtered data*/
    UIImageView * myImageView;/*!<no advices image*/
    NSDictionary *parameters ;
    NSData *postData ;
    NSString * wisdomBuyString11;/*!<transaction type filter*/
    NSString * wisdomDurationTypeString11;/*!<duration type filter*/
    NSString * nameAddingCheck;/*!<unused*/
    NSMutableArray * detailsArray;/*!<it store firstname of leader*/
    NSString * filterString1;
    NSString * finalLederID;
    UIRefreshControl * refreshControl;
    UIRefreshControl * refreshControl1;
    NSNumber * offset;/*!<pagination*/
    NSMutableArray * finalResponseArray;
    UIWindow * window;
    NSArray * segmentsArray;
    NSMutableArray * followingUserId;
    BOOL wisdomRefreshCheck;
    UIButton *refreshButton;
    UILabel *badgeLbl;
    int badgeValueInt;
    NSString * wisdomSymbol;
}
@end
@implementation WisdomGrdenView
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"WISDOM GARDEN";
    self.navigationItem.backBarButtonItem=nil;
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    filterArray=[[NSArray alloc]init];
    adviceList=[[NSMutableArray alloc]init];
    filterAdvice=[[NSMutableArray alloc]init];
    self.equitiesTableView.hidden=YES;
    if(delegate1.wisdomHint==YES)
    {   delegate1.wisdomHint=NO;
        UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
        myImageView =[[UIImageView alloc] initWithFrame:CGRectMake(0.0,0.0,currentWindow.frame.size.width,currentWindow.frame.size.height)];
        myImageView.image=[UIImage imageNamed:@"wisdomgarden"];
        [currentWindow addSubview:myImageView ];
        [currentWindow bringSubviewToFront:myImageView];}
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [myImageView setUserInteractionEnabled:YES];
    [myImageView addGestureRecognizer:singleTap];
    NSMutableArray * localSegmentsArray=[[NSMutableArray alloc]init];
    self.controller.delegate=self;
    self.controller.searchResultsUpdater=self;
    segmentsArray=[[NSArray alloc]init];
    segmentsArray=[delegate1.brokerInfoDict objectForKey:@"segment"];
    if(segmentsArray.count>0)
    {
    if([segmentsArray containsObject:@1])
    {
    [localSegmentsArray addObject:@"EQUITIES"];
    }
    if([segmentsArray containsObject:@2])
    {
    [localSegmentsArray addObject:@"DERIVATIVES"];
    }
    if([segmentsArray containsObject:@3])
    {
    [localSegmentsArray addObject:@"CURRENCY"];
    }
    if([segmentsArray containsObject:@4])
    {
    [localSegmentsArray addObject:@"COMMODITIES"];
    }
    }
    else
    {
    [localSegmentsArray addObject:@"EQUITIES"];
    [localSegmentsArray addObject:@"DERIVATIVES"];
    [localSegmentsArray addObject:@"CURRENCY"];
    [localSegmentsArray addObject:@"COMMODITIES"];
    }
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    [segmentedControl setSelectedSegmentIndex:0];
    self.equitiesTableView.delegate=self;
    self.equitiesTableView.dataSource=self;
    self.derivativesTableView.hidden=YES;
    
    
    badgeLbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+130,self.view.frame.size.height/2+130,30,30)];
    badgeLbl.clipsToBounds = YES;
    badgeLbl.backgroundColor = [UIColor redColor];
    badgeLbl.textColor = [UIColor whiteColor];
    badgeLbl.textAlignment = NSTextAlignmentCenter;
    badgeLbl.hidden=YES;
//    [self.view addSubview:badgeLbl];
    
    refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshButton addTarget:self
               action:@selector(refreshButtonAction)
     forControlEvents:UIControlEventTouchUpInside];
//    UIImage *btnImage = [UIImage imageNamed:@"add"];
//    [refreshButton setImage:btnImage forState:UIControlStateNormal];
    
    [refreshButton setBackgroundColor:[UIColor colorWithRed:2/255.0 green:30/255.0 blue:41/255.0 alpha:1.0f]];
    refreshButton.frame = CGRectMake(self.view.frame.size.width/2+100,self.view.frame.size.height/2+100,60,60);
    refreshButton.layer.cornerRadius = refreshButton.frame.size.width/2; // this value vary as per your desire
    refreshButton.clipsToBounds = YES;
    [self.view addSubview:refreshButton];
    refreshButton.hidden=YES;
    
    refreshButton.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.12f] CGColor];
   refreshButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    refreshButton.layer.shadowOpacity = 1.0f;
   refreshButton.layer.shadowRadius = refreshButton.frame.size.width/2;
   refreshButton.layer.masksToBounds = NO;

   
   

    
    
}

-(void)refreshButtonAction
{
    refreshButton.hidden=YES;
    badgeLbl.hidden=YES;
    badgeValueInt=0;
    finalResponseArray=[[NSMutableArray alloc]init];
    
    offset=[NSNumber numberWithInt:0];
    
    [self.equitiesTableView setContentOffset:CGPointZero animated:YES];
    
    [self serverHit];
    
}
/*!
 @discussion userd to refresh advice list.
 */
-(void)refreshMethod
{
    [self serverHit];
}
-(void)viewDidAppear:(BOOL)animated
{
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    self.equitiesTableView.hidden=YES;
   
    badgeValueInt=0;
  
    [self followerListMethod];
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    finalResponseArray=[[NSMutableArray alloc]init];
    offset=[NSNumber numberWithInt:0];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
   
    
  NSString * verify=[prefs stringForKey:@"profileDurationBool"];
    
    
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    
  
  NSString *  verifyStr=[prefs stringForKey:@"profileDuration"];
    
    if([verifyStr containsString:@"1"])
    {
       [dict setObject:[NSNumber numberWithInt:1] forKey:@"day"];
    }
    
    if([verifyStr containsString:@"2"])
    {
       [dict setObject:[NSNumber numberWithInt:2] forKey:@"short"];
    }
    
    
    if([verifyStr containsString:@"3"])
    {
       [dict setObject:[NSNumber numberWithInt:3] forKey:@"long"];
    }
    
    
    if([verify containsString:@"0"])
    {
        delegate1.durationUpdateBool=false;
    }
   else if([verify containsString:@"1"])
    {
        delegate1.profileDuration=[NSString stringWithFormat:@"%@",[dict allValues]];
        delegate1.durationUpdateBool=true;
    }
    
 
    [self serverHit];
}




-(void)followerListMethod
{
    
    @try {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   };
        
        NSDictionary * params;
        
        
        
        @try {
            
            params=@{@"clientid":delegate1.userID,
                     @"limit":@20000,
                    };
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
       
        
        NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
        
        
        NSString * localUrl=[NSString stringWithFormat:@"%@follower/leaders/%@",delegate1.baseUrl,delegate1.userID];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                    followingUserId=[[NSMutableArray alloc]init];
                                                            
                                                        NSMutableArray *  followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                            for(int i=0;i<followDict.count;i++)
                                                            {
                                                                [followingUserId addObject:[[followDict objectAtIndex:i] objectForKey:@"userid"]];
                                                            }
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                          wisdomRefreshCheck=true;
//                                                            [self refreshSocket];
                                                           
                                                        });
                                                    }];
        [dataTask resume];
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    


}

-(void)serverHit
{
    
   
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        @try {
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/wisdomgarden",delegate1.baseUrl]]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:10.0];
            
            
            wisdomDurationTypeString11=@"";
            wisdomBuyString11=@"";
            finalLederID=@"";
            NSString * wisdomFromDate;
            NSString  * wisdomToDate;
         
            
            /*!
             @discussion used to check filter is applied or not.
             */
            
            if(delegate1.wisdomBool==true)
            {
                wisdomToDate=delegate1.wisdomaToStr;
                wisdomFromDate=delegate1.wisdomFromStr;
                wisdomSymbol=delegate1.wisdomSearchSymbol;
                NSArray* words = [delegate1.wisdomBuySell componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString* wisdomBuyString = [words componentsJoinedByString:@""];
                NSString* wisdomBuyString1 = [wisdomBuyString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                wisdomBuyString11 = [wisdomBuyString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                
                if(wisdomBuyString11.length==0)
                {
                    wisdomBuyString11=@"";
                }
                
                if(delegate1.durationUpdateBool==true)
                {
                    NSArray* words1 = [delegate1.profileDuration componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSString* wisdomDurationString = [words1 componentsJoinedByString:@""];
                    NSString* wisdomDurationString1 = [wisdomDurationString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    wisdomDurationTypeString11 = [wisdomDurationString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                }
                
                if(delegate1.wisdomDuration.length>0)
                {
                    NSArray* words1 = [delegate1.wisdomDuration componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSString* wisdomDurationString = [words1 componentsJoinedByString:@""];
                    NSString* wisdomDurationString1 = [wisdomDurationString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    wisdomDurationTypeString11 = [wisdomDurationString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                }
                else
                {
                    wisdomDurationTypeString11=@"";
                }
                
                
                
                if(delegate1.selectLeadersID.count>0)
                {
                    NSString *leaderID = [NSString stringWithFormat:@"%@",delegate1.selectLeadersID];
                    NSString * leaderID1 = [leaderID stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    NSString * leaderID2 = [leaderID1 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    finalLederID = [leaderID2 stringByReplacingOccurrencesOfString:@")" withString:@""];
                    
                }else
                {
                    finalLederID=@"";
                }
                
                delegate1.selectLeadersID=[[NSMutableArray alloc]init];
                delegate1.selectLeadersName=[[NSMutableArray alloc]init];
                
            }else
            {
                if(delegate1.durationUpdateBool==true)
                {
                    NSArray* words1 = [delegate1.profileDuration componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSString* wisdomDurationString = [words1 componentsJoinedByString:@""];
                    NSString* wisdomDurationString1 = [wisdomDurationString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    wisdomDurationTypeString11 = [wisdomDurationString1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                    wisdomBuyString11=@"";
                    finalLederID=@"";
                    wisdomFromDate=@"";
                    wisdomToDate=@"";
                    wisdomSymbol=@"";
                }
                else
                {
                    wisdomDurationTypeString11=@"";
                    wisdomBuyString11=@"";
                    finalLederID=@"";
                    wisdomFromDate=@"";
                    wisdomToDate=@"";
                    wisdomSymbol=@"";
                }
                
               
            }
            
            
            
            headers = @{ @"cache-control": @"no-cache",
                         @"content-type": @"application/json"
                         };
            NSNumber *segment;
            
            if(segmentedControl.selectedSegmentIndex==0)
            {
                
                segment=@1;
            }
            
            else if(segmentedControl.selectedSegmentIndex==1)
            {
                segment=@2;
            }
            else if(segmentedControl.selectedSegmentIndex==2)
            {
                segment=@3;
            }
            else if(segmentedControl.selectedSegmentIndex==3)
            {
                segment=@4;
            }
            if(delegate1.wisdomTopAdvices==true)
            {
                parameters = @{
                               @"clientid":delegate1.userID,
                               @"segment":segment,
                               @"active":@(true),
                               @"issubscribed":@(true),
                               @"limit":@10,
                               @"offset":offset,
                               @"orderby":@"actedby",
                             
                               };
                delegate1.wisdomTopAdvices =false;
                
            }else
            {
                if(wisdomDurationTypeString11.length==0)
                {
                    wisdomDurationTypeString11=@"";
                }
                if(wisdomFromDate.length==0)
                {
                    wisdomFromDate=@"";
                }
                
                if(wisdomToDate.length==0)
                {
                    wisdomToDate=@"";
                }
                if(wisdomSymbol.length==0)
                {
                    wisdomSymbol=@"";
                }
                parameters = @{
                               @"clientid":delegate1.userID,
                               @"segment":segment,
                               @"active":@(true),
                               @"issubscribed":@(true),
                               @"durationtype":wisdomDurationTypeString11,
                               @"buysell":wisdomBuyString11,
                               @"limit":@10,
                               @"offset":offset,
                               @"sortby":@"DESC",
                               @"orderby":@"createdon",
                               @"leaderid":finalLederID,
                               @"fromdate":wisdomFromDate,
                               @"todate":wisdomToDate,
                               @"symbol":wisdomSymbol
                               };
            }
            
            postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
            [request setHTTPMethod:@"POST"];
            [request setAllHTTPHeaderFields:headers];
            [request setHTTPBody:postData];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            
                                                            if(data!=nil)
                                                            {
                                                                if (error) {
                                                                    NSLog(@"%@", error);
                                                                } else {
                                                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                    NSLog(@"%@", httpResponse);
                                                                    
                                                                    NSMutableDictionary * sampleDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    
                                                                    
                                                                    
                                                                    for(int i=0;i<[[sampleDict objectForKey:@"data"] count];i++)
                                                                    {
                                                                        
                                                                        [finalResponseArray addObject:[[sampleDict objectForKey:@"data"]objectAtIndex:i]];
                                                                        
                                                                    }
                                                                    
                                                                    self.responseArray=[[NSMutableDictionary alloc]init];
                                                                    
                                                                    
                                                                    NSArray * userArray=[finalResponseArray mutableCopy];
                                                                    NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:userArray];
                                                                    NSArray *uniqueArray = [set array];
                                                                    
                                                                    
                                                                    
                                                                    [self.responseArray setObject:uniqueArray forKey:@"data"];
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    self.equitiesTableView.delegate=self;
                                                                    self.equitiesTableView.dataSource=self;
                                                                    [self.equitiesTableView reloadData];
                                                                    
                                                                    self.activityIndicator.hidden=YES;
                                                                    [self.activityIndicator stopAnimating];      self.equitiesTableView.hidden=NO;
                                                                    
                                                                    
                                                                    
                                                                });
                                                            }
                                                            
                                                        }];
            
            [dataTask resume];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
  
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.controller.isActive==YES&&filterAdvice.count>0)
    {
        return filterAdvice.count;
    }
   else  if([[self.responseArray objectForKey:@"data"] count]&&self.controller.isActive==NO)
    {
         return [[self.responseArray objectForKey:@"data"] count];
    }
    else
    {
    return 1;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
    {
    
    }
    
    else
    {
        
        [self arrayMethod];
    }
    
    if(filterAdvice.count>0)
        
    {
        
        int messageType=[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
        
        if(messageType!=4)
        {
            //        self.brokerMesssagesTableView.hidden=YES;
            //        self.notoficationstableView.hidden=NO;
            
            EquitiesCell1 * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
            cell.equitiesImg2.layer.cornerRadius=cell.equitiesImg2.frame.size.width / 2;
            cell.equitiesImg2.clipsToBounds = YES;
            
            cell.eqitiesImg1.layer.cornerRadius=cell.eqitiesImg1.frame.size.width / 2;
            cell.eqitiesImg1.clipsToBounds = YES;
            
            cell.profileImg.layer.cornerRadius=cell.profileImg.frame.size.width / 2;
            cell.profileImg.clipsToBounds = YES;
            //NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString:@"SELL MUTHOOTFIN @ EP 3350.00 TP 3390.00 SL 3300.00"];
            //NSString * test = @"SELL MUTHOOTFIN @ EP 3350.00 TP 3390.00 SL 3300.00";
            // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(0,3)];
            // [strText addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 3)];
            
            int tickCheck = [[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"isacted"] intValue];
            if(tickCheck==0)
            {
                cell.tickImageView.hidden=YES;
            }else if (tickCheck==1)
            {
                cell.tickImageView.hidden=NO;
                
            }
            
            
            NSNumber * buttonNumber = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"buysell"];
            int buttonTitle = [buttonNumber intValue];
            if(buttonTitle==1)
            {
                [cell.buySellButton setTitle:@"BUY" forState:UIControlStateNormal];
                
                
                
                [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                
                
                
            }else if (buttonTitle==2)
            {
                [cell.buySellButton setTitle:@"SELL" forState:UIControlStateNormal];
                
                [cell.buySellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                
            }
            
            
            NSString * mainString=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"message"]];
            if([mainString containsString:@"@"])
            {
                cell.valueChangeLabel.text=[mainString uppercaseString];
                
                cell.oLbl.text=@"C";
            }
            
            else
            {
                
                cell.oLbl.text=@"O";
                TPString=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"targetprice"]];
                
                if ([mainString isEqual:[NSNull null]]||mainString==nil||[mainString isEqualToString:@"<null>"] ||  [TPString isEqual:[NSNull null]]||TPString==nil||[TPString isEqualToString:@"<null>"]   ) {
                    cell.valueChangeLabel.text =@"<Null>";
                }
                
                else{
                    
                    @try {
                        
                        
                        NSLog(@"main String %@",mainString);
                        
                        
                        
                        NSArray * mainArray=[mainString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                        
                        
                        
                        NSLog(@"array  %@",[mainArray objectAtIndex:0]);
                        
                        NSLog(@"array  %@",[mainArray objectAtIndex:1]);
                        
                        //                NSString * buySell=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:0]];
                        
                        if(buttonTitle==1)
                        {
                            buySell=@"BUY";
                            
                        }else if (buttonTitle==2)
                        {
                            buySell=@"SELL";
                            
                        }
                        
                        NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
                        
                        
                        
                        NSArray * companyArray=[string componentsSeparatedByString:@";"];
                        
                        
                        
                        NSLog(@" array1 %@",companyArray);
                        
                        
                        
                        //               for (int i=1; i<[companyArray count];i++) {
                        
                        
                        
                        //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                        //
                        //
                        //
                        //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
                        //
                        //                   NSLog(@"%@",array2);
                        
                        NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
                        NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
                        NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
                        NSString * SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
                        NSArray * EPArray=[EP1 componentsSeparatedByString:@"="];
                        NSArray * TPArray=[TP componentsSeparatedByString:@"="];
                        NSArray * SLArray=[SL componentsSeparatedByString:@"="];
                        
                        EPString=[NSString stringWithFormat:@"%@",[EPArray objectAtIndex:1]];
                        TPString=[NSString stringWithFormat:@"%@",[TPArray objectAtIndex:1]];
                        SLString=[NSString stringWithFormat:@"%@",[SLArray objectAtIndex:1]];
                        
                        
                        
                        NSString * dummyfinalStr=[NSString stringWithFormat:@"%@ %@ @%@%@%@",buySell,companyName,EP1,TP,SL];
                        
                        NSString * finalStr = [dummyfinalStr stringByReplacingOccurrencesOfString:@"=" withString:@" "];
                        
                        NSLog(@"%@",finalStr);
                        
                        NSLog(@"COMPANY %@",companyName);
                        
                        
                        //                              if (i==1) {
                        //                                  EP=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                        //
                        //                                  NSLog(@"EP %@",[array2 objectAtIndex:1]);
                        //
                        //                              }
                        
                        //                   if (i==2) {
                        //                       EPString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                        //
                        //                       NSLog(@"TP %@",[array2 objectAtIndex:1]);
                        //
                        //                   }
                        //
                        //                   if (i==3) {
                        //                       SLString=[NSString stringWithFormat:@"%@",[array2 objectAtIndex:1]];
                        //
                        //                       NSLog(@"SL %@",[array2 objectAtIndex:1]);
                        //
                        //                   }
                        //               }
                        //
                        
                        
                        NSString * sell = [NSString stringWithFormat:@"<html><body><div style='text-align:left;display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>  %@</div> </body></html>",buySell];
                        NSString * muthootfin =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",[companyArray objectAtIndex:0]];
                        NSString * ep = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> @ EP</div></html></body>";
                        NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPString];
                        NSString * tp = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> TP</div></html></body>";
                        NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPString];
                        NSString * sl = @"<html><body><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'> SL</div></html></body>";
                        NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:left;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLString] ;
                        
                        NSString * string1 = [sell stringByAppendingString:muthootfin];
                        NSString *string2 = [string1 stringByAppendingString:ep];
                        NSString * string3 = [string2 stringByAppendingString:value1];
                        NSString * string4 = [string3 stringByAppendingString:tp];
                        NSString * string5 = [string4 stringByAppendingString:value2];
                        NSString * string6 = [string5 stringByAppendingString:sl];
                        NSString * finalString = [string6 stringByAppendingString:value3];
                        
                        
                        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[finalString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                        
                        //UILabel * myLabel = [[UILabel alloc] init];
                        // myLabel.attributedText = attrStr;
                        cell.valueChangeLabel.attributedText=attrStr;
                    }
                    @catch (NSException * e) {
                        NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        NSLog(@"finally");
                    }
                }
                
            }
            
            cell.ltpLabel.text =[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
            
            NSString * changePercentlabel = [NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
            
            NSString * percent =@"  ";
            NSString * finalChangePercentlabel = [changePercentlabel stringByAppendingString:percent];
            if([changePercentlabel isEqual:[NSNull null]]||changePercentlabel==nil||[changePercentlabel isEqualToString:@"<null>"])
            {
                
                
                
                NSString * percentage = @"";
                NSString * zero = @"  0";
                NSString* final = [zero stringByAppendingString:percentage];
                cell.changePercentlabel.text=final;
                cell.changePercentlabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
            }
            else
            {
                
                if([finalChangePercentlabel containsString:@"-"])
                {
                    cell.changePercentlabel.text=finalChangePercentlabel;
                    cell.changePercentlabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                    
                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(238/255.0) green:(43/255.0) blue:(53/255.0) alpha:1];
                }
                else{
                    cell.changePercentlabel.text=finalChangePercentlabel;
                    cell.changePercentlabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                    cell.oLbl.backgroundColor=[UIColor colorWithRed:(86/255.0) green:(168/255.0) blue:(66/255.0) alpha:1];
                }
                
                
            }
            
            NSString * actedBy = [NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"actedby"]];
            
            if([actedBy isEqual:[NSNull null]]||actedBy==nil||[actedBy isEqualToString:@"<null>"])
            {
                cell.actedByLabel.text=@"0";
            }else
            {
                cell.actedByLabel.text=actedBy;
            }
            NSString * sharesSold = [NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"sharesold"]];
            if([sharesSold isEqual:[NSNull null]]||sharesSold==nil||[sharesSold isEqualToString:@"<null>"])
            {
                cell.sharesSold.text=@"0";
            }else
            {
                cell.sharesSold.text=sharesSold;
            }
            
            
            
            
            NSString * dateStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"createdon"];
            
            
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
            
            // change to a readable time format and change to local time zone
            [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString *finalDate = [dateFormatter stringFromDate:date];
            
            
            
            
            
            cell.dateAndTimeLabel.text=finalDate;
            
            
            cell.profileName.text=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"firstname"];
            
            // cell.valueChangeLabel.attributedText=strText;
            // [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Ubuntu-Medium" size:12.5] range:NSMakeRange(6, )];
            
            
            //       [cell.ordersBtn addTarget:self action:@selector(showingOrders) forControlEvents:UIControlEventTouchUpInside];
            
            
            [cell.buySellButton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.leaderProfileAction addTarget:self action:@selector(leaderProfileMethod:) forControlEvents:UIControlEventTouchUpInside];
            //       dispatch_async(dispatch_get_main_queue(), ^{
            //           cell.profileImg.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]]]];
            //
            //
            //       });
            
            
           
            
            
            @try {
                
                NSString * actedBy = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"actedby"];
                
                
                if([actedBy isEqual:[NSNull null]])
                {
                    cell.actedByLabel.text=@"0";
                }else
                {
                    int actedByInt=[actedBy intValue];
                    cell.actedByLabel.text = [NSString stringWithFormat:@"%i",actedByInt];
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            @try {
                
                NSString * sharesSold = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"sharesold"];
                
                
                
                if([sharesSold isEqual:[NSNull null]])
                {
                    cell.sharesSold.text = @"0";
                }else
                {
                    int sharesSoldInt=[sharesSold intValue];
                    cell.sharesSold.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            @try {
                
                NSString * averageProfit = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"averageprofit"];
                
                
                
                if([averageProfit isEqual:[NSNull null]])
                {
                    cell.averageProfitLbl.text = @"0";
                }else
                {
                    int sharesSoldInt=[averageProfit intValue];
                    cell.averageProfitLbl.text = [NSString stringWithFormat:@"%i",sharesSoldInt];
                }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            NSString * duration=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"durationtype"]];
            
            int durationInt=[duration intValue];
            
            if(durationInt==1)
            {
                cell.stLbl.text=@"DT";
                
                cell.stLbl.backgroundColor=[UIColor colorWithRed:(255/255.0) green:(192/255.0) blue:(102/255.0) alpha:1];
            }
            
            else if(durationInt==2)
            {
                cell.stLbl.text=@"ST";
                
                cell.stLbl.backgroundColor=[UIColor colorWithRed:(165/255.0) green:(225/255.0) blue:(241/255.0) alpha:1];
            }
            else if(durationInt==3)
            {
                
                
                
                cell.stLbl.text=@"LT";
                
                cell.stLbl.backgroundColor=[UIColor colorWithRed:(227/255.0) green:(168/255.0) blue:(246/255.0) alpha:1];
                
            }
            
            NSString * sub=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"subscriptiontypeid"]];
            
            int subInt=[sub intValue];
            
            if(subInt==1)
            {
                cell.premiumImg.hidden=YES;
                cell.premiumLbl.hidden=YES;
            }
            
            else if(subInt==2)
            {
                cell.premiumImg.hidden=NO;
                cell.premiumLbl.hidden=NO;
                cell.premiumImg.image=[UIImage imageNamed:@"premiumflagFlipped"];
                cell.premiumLbl.text=@"Premium";
            }
            
            NSString * public=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"ispublic"]];
            int publicInt=[public intValue];
            
            if(publicInt==1)
            {
                cell.premiumImg.hidden=NO;
                cell.premiumLbl.hidden=NO;
                cell.premiumImg.image=[UIImage imageNamed:@"publicflag"];
                cell.premiumLbl.text=@"Public Advice";
                [cell.premiumLbl setTextColor:[UIColor whiteColor]];
                
                NSString * source = [NSString stringWithFormat:@"%@",[[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"otherinfo"]objectForKey:@"source"]];
                cell.sourceLabel.hidden=NO;
                cell.sourceDataLabel.hidden=NO;
                cell.sourceDataLabel.text = source;
                cell.analystName.hidden=NO;
                cell.analystNameHeightConstant.constant=21;
                NSString * analystName = [NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"leaderorgname"]];
                NSString * firstName = [NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"firstname"]];
                
                if([analystName containsString:firstName])
                {
                    cell.analystName.hidden=YES;
                    cell.analystNameHeightConstant.constant=0;
                }else
                {
                    cell.analystName.hidden=NO;
                    cell.analystNameHeightConstant.constant=21;
                    cell.profileName.text = analystName;
                }
                
                NSString * lastname = [NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"lastname"]];
                NSString * name = [firstName stringByAppendingString:lastname];
                if([name isEqual:[NSNull null]])
                {
                    lastname=@"";
                    NSString * name = [firstName stringByAppendingString:lastname];
                    cell.analystName.text=name;
                }else
                {
                cell.analystName.text = name;
                }
            }

            else if(publicInt==0)
            {
                cell.premiumImg.hidden=YES;
                cell.premiumLbl.hidden=YES;
                cell.sourceLabel.hidden=YES;
                cell.sourceDataLabel.hidden=YES;
                cell.profileName.text=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"firstname"];
                cell.analystName.hidden=YES;
                cell.analystNameHeightConstant.constant=0;
                
            }
            
            
            cell.profileImg.image=nil;
            
            if(publicInt==1)
            {
                NSURL *url;
                @try {
                    url =  [NSURL URLWithString:@"https://s3-ap-southeast-1.amazonaws.com/elasticbeanstalk-ap-southeast-1-507357262371/Leaders/public_advice_user_logo.png"];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
                
                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (data) {
                        UIImage *image = [UIImage imageWithData:data];
                        if (image) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (cell)
                                    cell.profileImg.image = image;
                            });
                        }
                    }
                }];
                [task resume];
            }else
            {
            
            NSURL *url;
            @try {
                url =  [NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (cell)
                                cell.profileImg.image = image;
                        });
                    }
                }
            }];
            [task resume];
            }
            
            
            
            return cell;
        }
        
        
        else{
            
            EquitiesCell2 * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            cell.oLabel.layer.cornerRadius = 12.5f;
            cell.oLabel.layer.masksToBounds = YES;
            cell.ltLabel.layer.cornerRadius = 12.5f;
            cell.ltLabel.layer.masksToBounds=YES;
            
            //       [cell.portfolioAdviceOrdersBtn addTarget:self action:@selector(showingPortfolioOrders) forControlEvents:UIControlEventTouchUpInside];
            
            cell.profileImage.layer.cornerRadius=cell.profileImage.frame.size.width / 2;
            cell.profileImage.clipsToBounds = YES;
            
            
            NSString * firstName = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"firstname"];
            
            NSString * messageName = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagename"];
            
            cell.adivceName.text=messageName;
            
            
            
            
            cell.profileName.text=firstName;
            
            
            NSString * dateStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"createdon"];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
            
            // change to a readable time format and change to local time zone
            [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString *finalDate = [dateFormatter stringFromDate:date];
            
            
            
            
            
            cell.dateAndTime.text=finalDate;
            
            
            
            
            NSString * assumedInvestment = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"amount"];
            NSString * rupee = @"₹";
            
            NSString * finalString = [rupee stringByAppendingString:assumedInvestment];
            
            cell.assumedInvestment.text = finalString;
            
            NSString * actedBy = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"actedby"];
            
            if([actedBy isEqual:[NSNull null]])
            {
                cell.actedByLabel.text=@"0";
            }else
            {
                cell.actedByLabel.text=actedBy;
            }
            
            NSString * sharesBought = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"sharesold"];
            
            if([sharesBought isEqual:[NSNull null]])
            {
                cell.sharesBroughtLabel.text=@"0";
            }else
            {
                cell.sharesBroughtLabel.text=sharesBought;
            }
            
            
            cell.portfolioArray=[[NSMutableArray alloc]init];
            
            
            cell.portfolioArray= [[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"portfoliojson"] objectForKey:@"stocks"];
            
            
            NSLog(@"cell portfolio array:%@",cell.portfolioArray);
            [cell.portfolioTblView reloadData];
            
            //       dispatch_async(dispatch_get_main_queue(), ^{
            //           cell.profileImage.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]]]];
            //
            //
            //       });
            
            cell.profileImage.image=nil;
           
            NSURL *url;
            @try {
                url =  [NSURL URLWithString:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            EquitiesCell2 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                updateCell.profileImage.image = image;
                        });
                    }
                }
            }];
            [task resume];
            
            [cell.buySellButton addTarget:self action:@selector(portBuySellAction:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        
    }
    
    else
    {
        DerivativesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
        
        return cell;
    }

    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Wisdom stock detail page"];
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
    {
       
          @try
        {
        
        delegate1.navigationCheck=@"wisdom";
            int messageType=[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
            
            if(messageType!=4)
       
        {
            if(delegate1.leaderAdviceDetails.count>0)
            {
                [delegate1.leaderAdviceDetails removeAllObjects];
            }
            //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
            //    {
            //
            //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
            //        {
            //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
            //
            //            [self.navigationController pushViewController:allocation animated:YES];
            //
            //        }
            
            //        else{
            
            NSString * public=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"ispublic"]];
            int publicInt=[public intValue];
            
            if(publicInt==0)
            {
            delegate1.depth=@"WISDOM";
            
            NSLog(@"%@",delegate1.depth);
            
            StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
            
            
            
            delegate1.symbolDepthStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"companyname"];
            
            delegate1.exchaneLblStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
            
            delegate1.instrumentDepthStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"instrumentid"];
            
//            delegate1.expirySeriesValue=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"expirydate"];
             delegate1.expirySeriesValue=@"";
            
            
            [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
            [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
            [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
            
            [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
            
            [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"message"]];
            
            delegate1.instrumentDepthStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"instrumentid"];
            
            
            [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
            [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messageid"]];
            [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"buysell"]];
            NSString * logo=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"];
            
            if([logo isEqual:[NSNull null]])
            {
                [delegate1.leaderAdviceDetails addObject:@"no"];
            }
            
            else
            {
                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
            }

            
            
            
            NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
            
            [self.navigationController pushViewController:stockDetails animated:YES];
            }else if (publicInt ==1)
            {
                        dispatch_async(dispatch_get_main_queue(), ^{
                
                            
                
                
                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"This is a publicly available information from the above source. All users are advised, in their own interests, to seek and obtain advice that is specific or tailored to their own circumstances and that any reliance by any user on any such publicly available advice is entirely at the risk and consequence of such user, for which Zenwise Technologies Private Limited under the brand name Zambala is not responsible or liable." preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                            [self presentViewController:alert animated:YES completion:^{
                
                
                
                            }];
                
                
                
                            UIAlertAction * proceedAction=[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                                delegate1.depth=@"WISDOM";
                                
                                NSLog(@"%@",delegate1.depth);
                                
                                StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
                                
                                
                                
                                delegate1.symbolDepthStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"companyname"];
                                
                                delegate1.exchaneLblStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
                                
                                delegate1.instrumentDepthStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"instrumentid"];
                                
                                //            delegate1.expirySeriesValue=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"expirydate"];
                                delegate1.expirySeriesValue=@"";
                                
                                
                                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
                                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
                                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
                                
                                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
                                
                                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"message"]];
                                
                                delegate1.instrumentDepthStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"instrumentid"];
                                
                                
                                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
                                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messageid"]];
                                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"buysell"]];
                                NSString * logo=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"];
                                
                                if([logo isEqual:[NSNull null]])
                                {
                                    [delegate1.leaderAdviceDetails addObject:@"no"];
                                }
                                
                                else
                                {
                                    [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
                                }
                                
                                
                                
                                
                                NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                                
                                [self.navigationController pushViewController:stockDetails animated:YES];
                
                            }];
                            
                            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                
                                
                                
                            }];
                            
                
                
                
                            [alert addAction:proceedAction];
                            [alert addAction:cancelAction];
                
                        });
                
            }
            
        }
        
        else
        {
            
            delegate1.protFolioUserID = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messageid"];
            
            StockAllocationView * protfolioDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
            [self.navigationController pushViewController:protfolioDetail animated:YES];
//
            
            
            
            
            
            //        dispatch_async(dispatch_get_main_queue(), ^{
            //
            //
            //
            //
            //            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
            //
            //
            //
            //            [self presentViewController:alert animated:YES completion:^{
            //
            //
            //
            //            }];
            //
            //
            //            
            //            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //                
            //                
            //                
            //            }];
            //            
            //            
            //            
            //            [alert addAction:okAction];
            //            
            //        });
            //        
            
            
            
            
        }
        
        //            WisdomViewController * stockView=[self.storyboard instantiateViewControllerWithIdentifier:@"rohit"];
        //            
        //            [self.navigationController pushViewController:stockView animated:YES];
        //        }
        //    }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }

    }

    //////
    
    else
    {
    @try
        {
    
    
    
    delegate1.navigationCheck=@"wisdom";
            
            int messageType=[[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
            
            if(messageType!=4)
  
    {
        
    
    if(delegate1.leaderAdviceDetails.count>0)
    {
        [delegate1.leaderAdviceDetails removeAllObjects];
    }
//    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
//    {
//        
//        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
//        {
//            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
//            
//            [self.navigationController pushViewController:allocation animated:YES];
//        
//        }
    
//        else{
        
        NSString * public=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"ispublic"]];
        int publicInt=[public intValue];
        
        if(publicInt==0)
        {
    delegate1.depth=@"WISDOM";
    
    NSLog(@"%@",delegate1.depth);
            
            StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
        
    
    
    delegate1.symbolDepthStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
    
    delegate1.exchaneLblStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
        
        delegate1.instrumentDepthStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"instrumentid"];
        
//        delegate1.expirySeriesValue=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"expirydate"];
        
        delegate1.expirySeriesValue=@"";
    
    
    [delegate1.leaderAdviceDetails addObject:[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
     [delegate1.leaderAdviceDetails addObject:[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
    [delegate1.leaderAdviceDetails addObject:[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
    
    [delegate1.leaderAdviceDetails addObject:[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
    
    [delegate1.leaderAdviceDetails addObject:[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"message"]];
        
        
    [delegate1.leaderAdviceDetails addObject:[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
        [delegate1.leaderAdviceDetails addObject:[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageid"]];
        
         [delegate1.leaderAdviceDetails addObject:[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"buysell"]];
        NSString * logo=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"logourl"];
        
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        
        
    
    NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
    
    [self.navigationController pushViewController:stockDetails animated:YES];
        } else if (publicInt ==1)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"This is a publicly available information from the above source. All users are advised, in their own interests, to seek and obtain advice that is specific or tailored to their own circumstances and that any reliance by any user on any such publicly available advice is entirely at the risk and consequence of such user, for which Zenwise Technologies Private Limited under the brand name Zambala is not responsible or liable." preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                [self presentViewController:alert animated:YES completion:^{
                    
                    
                    
                }];
                
                
                
                UIAlertAction * proceedAction=[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    delegate1.depth=@"WISDOM";
                    
                    NSLog(@"%@",delegate1.depth);
                    
                    StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
                    
                    
                    
                    delegate1.symbolDepthStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"companyname"];
                    
                    delegate1.exchaneLblStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
                    
                    delegate1.instrumentDepthStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"instrumentid"];
                    
                    //            delegate1.expirySeriesValue=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"expirydate"];
                    delegate1.expirySeriesValue=@"";
                    
                    
                    [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
                    [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
                    [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
                    
                    [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
                    
                    [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"message"]];
                    
                    delegate1.instrumentDepthStr=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"instrumentid"];
                    
                    
                    [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"leaderid"]];
                    [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messageid"]];
                    [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"buysell"]];
                    NSString * logo=[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"];
                    
                    if([logo isEqual:[NSNull null]])
                    {
                        [delegate1.leaderAdviceDetails addObject:@"no"];
                    }
                    
                    else
                    {
                        [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
                    }
                    
                    
                    
                    
                    NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                    
                    [self.navigationController pushViewController:stockDetails animated:YES];
                    
                }];
                
                UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    
                    
                }];
                
                
                
                
                [alert addAction:proceedAction];
                [alert addAction:cancelAction];
                
            });
            
        }
        
    }
    
    else
    {

        delegate1.protFolioUserID = [[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageid"];
        
        StockAllocationView * protfolioDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        [self.navigationController pushViewController:protfolioDetail animated:YES];
        
        
        
        
        
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            
//            
//            
//            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
//            
//            
//            
//            [self presentViewController:alert animated:YES completion:^{
//                
//                
//                
//            }];
//            
//            
//            
//            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                
//                
//                
//            }];
//            
//            
//            
//            [alert addAction:okAction];
//            
//        });
//        

        
        

    }
    
//            WisdomViewController * stockView=[self.storyboard instantiateViewControllerWithIdentifier:@"rohit"];
//            
//            [self.navigationController pushViewController:stockView animated:YES];
//        }
//    }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
    {
        
    }
    
    else
    {
        [self arrayMethod];
    }
        if(tableView==self.equitiesTableView)
        {
            
            if(filterAdvice.count>0)
            {
                
                int messageType=[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
                
                if(messageType!=4)
                
                {
                    return 187;
                }
                
                else
                {
                    
                    if(  [[[[filterAdvice objectAtIndex:indexPath.row ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==1)
                        
                    {
                        
                        
                        return 255;
                        
                    }
                    
                    
                    
                    else if(   [[[[filterAdvice objectAtIndex:indexPath.row ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==2)
                        
                    {
                        
                        
                        
                        return 285;
                        
                        //return 304;
                        
                    }
                    
                    
                    
                    else if(  [[[filterAdvice objectAtIndex:indexPath.row ] objectForKey:@"portfoliojson"] count]==3)
                        
                    {
                        
                        return 315;
                        
                    }
                    
                
                    
                    else if(   [[[[filterAdvice objectAtIndex:indexPath.row ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==4)
                        
                    {
                        
                        return 335;
                        
                    }
                    
                    
                    
                    else if(   [[[[filterAdvice objectAtIndex:indexPath.row ] objectForKey:@"portfoliojson"] objectForKey:@"stocks"] count]==4)
                        
                    {
                        
                        return 365;
                        
                    }
                    
                    
                    
                    
                    
                    return 255;
                    
                    
                    
                    
                }
                
            }
            
            else
            {
                return 393;
            }
            
            
        }
        
    

    
       return 0;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (UISearchController *)controller {
//    
//    if (!_controller) {
//        
//        // instantiate search results table view
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        Equities *equities= [storyboard instantiateViewControllerWithIdentifier:@"Equities"];
//        
//        // create search controller
//        _controller = [[UISearchController alloc]initWithSearchResultsController:equities];
//        _controller.searchResultsUpdater = self;
//        
//        // optional: set the search controller delegate
//        _controller.delegate = self;
//        
//       
//        
//    }
//    return _controller;
//}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.controller setActive:NO];
}


- (IBAction)searchAction:(id)sender {
    
    // present the search controller
    
//    self.controller.view.backgroundColor = [UIColor whiteColor];
//    
//    [self presentViewController:self.controller animated:YES completion:nil];
    
//    UISearchController *searchController = [[UISearchController alloc] initWithSearchResultsController:self];
//    // Use the current view controller to update the search results.
//    searchController.searchResultsUpdater = self;
//    // Install the search bar as the table header.
//    self.navigationItem.titleView = searchController.searchBar;
//    // It is usually good to set the presentation context.
//    self.definesPresentationContext = YES;
//    
    self.controller = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.controller.searchResultsUpdater = self;
    self.controller.hidesNavigationBarDuringPresentation = NO;
    self.controller.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = YES;
//    self.controller.searchBar.scopeButtonTitles = @[@"Posts", @"Users", @"Subreddits"];
    [self presentViewController:self.controller animated:YES completion:nil];
    
}

/*!
 @discussion used to update search results.
 */

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    
    
        @try {
            if(filterAdvice.count>0)
            {
                filterAdvice=[[NSMutableArray alloc]init];
                
            }
            if(adviceList.count>0)
            {
                adviceList=[[NSMutableArray alloc]init];
            }
            
            if([[self.responseArray objectForKey:@"data"] count]>0)
            {
           
            for(int i=0;i<[[self.responseArray objectForKey:@"data"] count];i++)
            {
                
                [adviceList addObject:[[[self.responseArray objectForKey:@"data"] objectAtIndex:i]objectForKey:@"firstname"]];
            }
                
            
            NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self beginswith[c] %@",searchController.searchBar.text];
            NSLog(@"%@",predicate);
            if(self.controller.searchBar.text.length>0)
            {
                filterArray =[[NSArray alloc]init];
                
                
                filterArray=[adviceList  filteredArrayUsingPredicate:predicate];
                
                for(int i=0;i<filterArray.count;i++)
                {
                     detailsArray=[[NSMutableArray alloc]init];
                   
                    if(filterAdvice.count>0&&filterArray.count>0)
                    {
                        
                        filterString1=[filterArray objectAtIndex:i];
                        for(int k=0;k<filterAdvice.count;k++)
                        {
                            [detailsArray addObject:[[filterAdvice objectAtIndex:k]objectForKey:@"firstname"]];
                            
                        }
                        
                    }

                    for(int j=0;j<[[self.responseArray objectForKey:@"data"]count];j++)
                    {
                        NSString * filterString=[filterArray objectAtIndex:i];
                        NSString * responseString=[[[self.responseArray objectForKey:@"data"]  objectAtIndex:j]objectForKey:@"firstname"];
                        
                        
                       
                    
                      
                        if(detailsArray.count>0)
                        {
                        
                        if([detailsArray containsObject:filterString1])
                        {
                        
                          
                        }
                            
                            
                        else
                        {
                            if([filterString isEqualToString:responseString])
                            {
                                [filterAdvice addObject:[[self.responseArray objectForKey:@"data"] objectAtIndex:j]];
                            }
                        }

                            
                        }
                        
                        else
                        {
                            if([filterString isEqualToString:responseString])
                            {
                                [filterAdvice addObject:[[self.responseArray objectForKey:@"data"] objectAtIndex:j]];
                                
                            }
                        }
                        
                        
                        
                    }
                }
            }
            
                if(self.controller.searchBar.text.length>=1)
                {
                
                    [self.equitiesTableView reloadData];
                }
                
                if(self.controller.isActive==NO)
                {
                    [self.equitiesTableView reloadData];
                }
                
            
           
            
                
           
                
            }
            
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
        
       
    
    
}

/*!
 @discussion used to show information.
 */
- (IBAction)popUpBtn:(id)sender
{
    
    PopUpView *detailViewController = [[PopUpView alloc] initWithNibName:@"PopUpView" bundle:nil];
    [self presentPopupViewController:detailViewController animationType: MJPopupViewAnimationSlideTopBottom];
    
    
}
/*!
 @discussion used fro filter.
 */

-(IBAction)filterAction:(id)sender
{
//    WisdomFilterPopup *detailViewController = [[WisdomFilterPopup alloc] initWithNibName:@"WisdomFilterPopup" bundle:nil];
//    [self presentPopupViewController:detailViewController animationType: MJPopupViewAnimationSlideTopBottom];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        
//        
//        
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
//        
//        
//        
//        [self presentViewController:alert animated:YES completion:^{
//            
//            
//            
//        }];
//        
//        
//        
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            
//            
//            
//        }];
//        
//        
//        
//        [alert addAction:okAction];
//        
//    });
//    

}

/*!
 @discussion method will call when there is change in segment selection.
 */

-(void)segmentedControlChangedValue
{
    wisdomSymbol=@"";
//    if(segmentedControl.selectedSegmentIndex==0)
//    {
//        finalResponseArray=[[NSMutableArray alloc]init];
//        self.equitiesTableView.hidden=YES;
//        self.activityIndicator.hidden=NO;
//        [self.activityIndicator startAnimating];
//        offset=[NSNumber numberWithInt:0];
//        [self serverHit];
//
//
//
//
//    }
//    else if(segmentedControl.selectedSegmentIndex==1)
//    {
//        finalResponseArray=[[NSMutableArray alloc]init];
//        self.equitiesTableView.hidden=YES;
//        self.activityIndicator.hidden=NO;
//        [self.activityIndicator startAnimating];
//        offset=[NSNumber numberWithInt:0];
//        [self serverHit];
//
//
//    }
//    else if(segmentedControl.selectedSegmentIndex==2)
//    {
    refreshButton.hidden=YES;
    
        finalResponseArray=[[NSMutableArray alloc]init];
        self.equitiesTableView.hidden=YES;
        self.activityIndicator.hidden=NO;
        [self.activityIndicator startAnimating];
        offset=[NSNumber numberWithInt:0];
        [self serverHit];
        
        
//    }
//    else if(segmentedControl.selectedSegmentIndex==3)
//    {
//        self.equitiesTableView.hidden=YES;
//        [self serverHit];
//
//
//    }
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
}

-(void)showingOrders
{
   
    
    
    
    WisdomGrdenView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"wisdomorders"];
    
    [self.navigationController pushViewController:orderView animated:YES];
}

-(void)showingPortfolioOrders

{
    protofolioAdviceOrders * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"portfolioorders"];
    
    [self.navigationController pushViewController:orderView animated:YES];
}

/*!
 @discussion method called when we click on buy or sell.
 */

-(void)yourButtonClicked:(UIButton*)sender
{
    //    if (sender.tag == 0)
    //    {
    //
    //    }
    
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Wisdom Garden Screen"];
    
    
    
    
    //////
    
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
    {
        
    }
    
    else
    {
        
        [self arrayMethod];
    }

    delegate1.navigationCheck=@"wisdom";
    
     delegate1.wisdomCheck=true;
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.equitiesTableView];
    
    
    
    NSIndexPath *indexPath = [self.equitiesTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    EquitiesCell1 *cell = [self.equitiesTableView cellForRowAtIndexPath:indexPath];
    
    delegate1.wisdomGardemTickIDString = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messageid"];
    
        int messageType=[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"]intValue];
        
        if(messageType!=4)
        
   
    {

        
        NSString * public=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"ispublic"]];
        int publicInt=[public intValue];
        
        if(publicInt==0)
        {
    
     @try {
          delegate1.symbolDepthStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"companyname"]];
         
         delegate1.tradingSecurityDes=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"symbolname"]];
         
         
     }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    @try {
        delegate1.orderSegment=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"segmenttype"];
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    
    delegate1.orderBuySell=cell.buySellButton.currentTitle;
        
        if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
        {
             delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"instrumentid"]];
        }
        else
        {
            
             delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"exchangetoken"]];
            
        }
   
        
    

    
        if(delegate1.leaderAdviceDetails.count>0)
        {
            [delegate1.leaderAdviceDetails removeAllObjects];
        }
        //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
        //    {
        //
        //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
        //        {
        //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
        //
        //            [self.navigationController pushViewController:allocation animated:YES];
        //
        //        }
        
        //        else{
        delegate1.depth=@"WISDOM";
        
        NSLog(@"%@",delegate1.depth);
        
       
        
//        delegate1.symbolDepthStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
//        
//        delegate1.exchaneLblStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
        
        
        
        [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
        [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
        [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
        
        [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
        
        [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"message"]];
        
        NSString * logo=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
        
        if([logo isEqual:[NSNull null]])
        {
            [delegate1.leaderAdviceDetails addObject:@"no"];
        }
        
        else
        {
            [delegate1.leaderAdviceDetails addObject:logo];
        }
        
        NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
        StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
        
        [self.navigationController pushViewController:orderView animated:YES];
        
        
    }else if (publicInt==1)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"This is a publicly available information from the above source. All users are advised, in their own interests, to seek and obtain advice that is specific or tailored to their own circumstances and that any reliance by any user on any such publicly available advice is entirely at the risk and consequence of such user, for which Zenwise Technologies Private Limited under the brand name Zambala is not responsible or liable." preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            [self presentViewController:alert animated:YES completion:^{
                
                
                
            }];
            
            
            
            UIAlertAction * proceedAction=[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                @try {
                    delegate1.symbolDepthStr=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"companyname"]];
                    
                    delegate1.tradingSecurityDes=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"symbolname"]];
                    
                    
                }
                @catch (NSException * e) {
                    NSLog(@"Exception: %@", e);
                }
                @finally {
                    NSLog(@"finally");
                }
                
                @try {
                    delegate1.orderSegment=[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"segmenttype"];
                    
                    
                }
                @catch (NSException * e) {
                    NSLog(@"Exception: %@", e);
                }
                @finally {
                    NSLog(@"finally");
                }
                
                
                delegate1.orderBuySell=cell.buySellButton.currentTitle;
                
                if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
                {
                    delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"instrumentid"]];
                }
                else
                {
                    
                    delegate1.orderinstrument=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row]objectForKey:@"exchangetoken"]];
                    
                }
                
                
                
                
                
                if(delegate1.leaderAdviceDetails.count>0)
                {
                    [delegate1.leaderAdviceDetails removeAllObjects];
                }
                //    if(tableView==self.equitiesTableView&&segmentedControl.selectedSegmentIndex==0)
                //    {
                //
                //        if(indexPath.row==2||indexPath.row==7||indexPath.row==15||indexPath.row==12||indexPath.row==18)
                //        {
                //            StockAllocationView * allocation=[self.storyboard instantiateViewControllerWithIdentifier:@"stockallocation"];
                //
                //            [self.navigationController pushViewController:allocation animated:YES];
                //
                //        }
                
                //        else{
                delegate1.depth=@"WISDOM";
                
                NSLog(@"%@",delegate1.depth);
                
                
                
                //        delegate1.symbolDepthStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
                //
                //        delegate1.exchaneLblStr=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
                
                
                
                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"createdon"]];
                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"]];
                
                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"changepercent"]];
                
                [delegate1.leaderAdviceDetails addObject:[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"message"]];
                
                NSString * logo=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
                
                if([logo isEqual:[NSNull null]])
                {
                    [delegate1.leaderAdviceDetails addObject:@"no"];
                }
                
                else
                {
                    [delegate1.leaderAdviceDetails addObject:logo];
                }
                
                NSLog(@"Leader Advice details:%@",delegate1.leaderAdviceDetails);
                StockOrderView * orderView=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
                
                [self.navigationController pushViewController:orderView animated:YES];
                
            }];
            
            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                
            }];
            
            
            
            
            [alert addAction:proceedAction];
            [alert addAction:cancelAction];
            
        });
        
    }
    

   

   
        
        
    
    }
}

/*!
 @discussion naviagtes to leader details page according to selection.
 */
-(void)leaderProfileMethod:(UIButton *)sender
{
    
   
    
    
        
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1&&self.controller.searchBar.text.length>=1)
    {
    }
    
    else
    {
        [self arrayMethod];
        
    }
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.equitiesTableView];
    
    
    
    NSIndexPath *indexPath = [self.equitiesTableView indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"Index path:%ld",(long)indexPath.row);
    EquitiesCell1 *cell = [self.equitiesTableView cellForRowAtIndexPath:indexPath];
    
    NSString * public=[NSString stringWithFormat:@"%@",[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"ispublic"]];
    int publicInt=[public intValue];
    
        int messageType=[[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
        
        if(messageType!=4)
    
    
    
    {
        
        delegate1.leaderIDWhoToFollow=[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"leaderid"];
        
        
        
        
    }
    
    if(publicInt ==0)
    {
        NewProfile * new=[self.storyboard instantiateViewControllerWithIdentifier:@"NewProfile"];
        
        [self.navigationController pushViewController:new animated:YES];
    }else
    {
       
    }
    
    
        
    

    
}

//rechability//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
     delegate1.wisdomBool=false;
    delegate1.wisdomDuration=@"";
    if(self.controller.isActive==YES)
    {
        [self dismissViewControllerAnimated:self.controller completion:nil];

    }
    
}

//port//

/*!
 @discussion method called when we click on transaction button for portfolio advice.
 */

-(void)portBuySellAction:(UIButton *)sender
{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.equitiesTableView];
    
    
    
    NSIndexPath *indexPath = [self.equitiesTableView indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"Index path:%ld",(long)indexPath.row);
    EquitiesCell2 *cell = [self.equitiesTableView cellForRowAtIndexPath:indexPath];
    
    if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
    {
        int messageType=[[[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
        
        if(messageType!=4)
        
      
        {
            
        }
        
        else
        {
            
             delegate1.protFolioUserID = [[filterAdvice objectAtIndex:indexPath.row] objectForKey:@"messageid"];
            delegate1.transaction=@"BUY";
            
            NewPortOrderView * protfolioDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPortOrderView"];
            [self.navigationController pushViewController:protfolioDetail animated:YES];
            

            
        }

        
        
    }
    
    else
    {
        int messageType=[[[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messagetypeid"] intValue];
        
        if(messageType!=4)
            
      
        {
            
        }
        
        else
        {
            
            delegate1.protFolioUserID = [[[self.responseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"messageid"];
            delegate1.transaction=@"BUY";
            
            NewPortOrderView * protfolioDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPortOrderView"];
            [self.navigationController pushViewController:protfolioDetail animated:YES];
            

        }

        
    }
    
    
    
    
}

//pagination//
-(void)refreshTableVeiwList
{
    int limitInt=[offset intValue];
    
    int finalInt=limitInt+10;
    
    offset=[NSNumber numberWithInt:finalInt];
    
    [self serverHit];
    [refreshControl1 endRefreshing];
}
-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    
    
    
    if (scrollView.contentOffset.y + scrollView.frame.size.height == scrollView.contentSize.height)
    {
        [self refreshTableVeiwList];
    }
    
    else if (self.equitiesTableView.contentOffset.y==0)
    {
        [self refreshTableVeiwList1];
        
    }
    
    NSLog(@"%f",scrollView.contentOffset.y);
}

-(void)refreshTableVeiwList1
{
    
    offset=[NSNumber numberWithInt:0];
    
    [self serverHit];
    [refreshControl1 endRefreshing];
}
-(void)tapDetected{
    NSLog(@"single Tap on imageview");
    [myImageView removeFromSuperview];
}

-(void)refreshSocket
{
   
    NSString * str=[NSString stringWithFormat:@"follower.%@",delegate1.userID];
    MQTTClient *client = [[MQTTClient alloc] initWithClientId:str];
//    srujansoft.com
    [client connectToHost:@"52.77.57.127"
             completionHandler:^(MQTTConnectionReturnCode code) {
                 if (code == ConnectionAccepted) {
                     // when the client is connected, subscribe to the topic to receive message.
                     
//                     if(wisdomRefreshCheck==true)
//                     {
//                         wisdomRefreshCheck=false;
                     for(int i=0;i<followingUserId.count;i++)
                     {
                         NSString * userID=[NSString stringWithFormat:@"%@.null",[followingUserId objectAtIndex:i]];
                       [client subscribe:userID
                      
                      withCompletionHandler:nil];
                     
                     }
                         
//                     }
                 }
             }];
    
    [client setMessageHandler:^(MQTTMessage *message) {
        NSString *text =message.payloadString;
        
        int i=[text intValue];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(segmentedControl.selectedSegmentIndex+1==i)
            {
            AudioServicesPlaySystemSound(1007);
            badgeValueInt=badgeValueInt+1;
//            badgeLbl.text=[NSString stringWithFormat:@"%i",badgeValueInt];
                
                [refreshButton setTitle:[NSString stringWithFormat:@"%i ↑",badgeValueInt] forState:UIControlStateNormal];
                refreshButton.hidden=NO;
            
                badgeLbl.hidden=NO;
                
                 
                
            }
            
            
        });
       
        
        
        NSLog(@"received message %@", text);
    }];
    
    [client disconnectWithCompletionHandler:^(NSUInteger code) {
        // The client is disconnected when this completion handler is called
        NSLog(@"MQTT client is disconnected");
    }];
}

-(void)arrayMethod
{
    filterAdvice=[[NSMutableArray alloc]init];
    
    filterAdvice=[self.responseArray objectForKey:@"data"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"Filter"])
    {
        // Get reference to the destination view controller
        WGFilterViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
      if(segmentedControl.selectedSegmentIndex==0)
      {
          vc.title=@"Equity Filter";
          vc.exchangeType=@"EQ";
      }
        else  if(segmentedControl.selectedSegmentIndex==1)
        {
            vc.title=@"Derivatives Filter";
            vc.exchangeType=@"DR";
        }
        else  if(segmentedControl.selectedSegmentIndex==2)
        {
            vc.title=@"Currency Filter";
            vc.exchangeType=@"CR";
        }
        else  if(segmentedControl.selectedSegmentIndex==3)
        {
            vc.title=@"Commodities Filter";
            vc.exchangeType=@"CO";
        }
    }
}

@end
