//
//  NewMessages.m
//  zambala leader
//
//  Created by zenwise mac 2 on 3/15/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewMessages.h"
#import "HMSegmentedControl.h"
#import "NewNotificationTableViewCell.h"
#import "TabBar.h"
#import "NoMessagesCell.h"

@interface NewMessages ()
{
    HMSegmentedControl *segmentedControl;
    
}


@end

@implementation NewMessages

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.messagesTbl.delegate=self;
    self.messagesTbl.dataSource=self;
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"NOTIFICATIONS",@"OFFERS"]];
    segmentedControl.frame = CGRectMake(0,88, self.view.frame.size.width, 46);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //    segmentedControl.verticalDividerEnabled = YES;
    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    // segmentedControl.backgroundColor = [UIColor lightGrayColor];
    [segmentedControl setTintColor:[UIColor whiteColor]];
    
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    

}

-(void)segmentedControlChangedValue
{
    [self.messagesTbl reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
//          NewNotificationTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        
         NoMessagesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"nomessages" forIndexPath:indexPath];
        return cell;
    }
    
    else if (segmentedControl.selectedSegmentIndex==1)
    {
//        NewNotificationTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
         NoMessagesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"nomessages" forIndexPath:indexPath];
        return cell;
    }
    
    return 0;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    
//    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
//    
//    [self presentViewController:tabPage animated:YES completion:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)filterAction:(id)sender {
    
    if(segmentedControl.selectedSegmentIndex==0)
        
    {
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Filer by:" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        
        
        [self presentViewController:alert animated:YES completion:^{
            
            
            
        }];
        
        
        
        UIAlertAction * All=[UIAlertAction actionWithTitle:@"All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
            
            
            
            
        }];
        
        
        
        UIAlertAction * adviceClosed=[UIAlertAction actionWithTitle:@"Advice closed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
            
            
            
            
            
            
        }];
        
        
        
        UIAlertAction * followedOrSubscribed=[UIAlertAction actionWithTitle:@"Followed or subscribed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
            
            
        }];
        
        UIAlertAction * alerts = [UIAlertAction actionWithTitle:@"Alerts" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        
        
        
        
        
        
        
        
        [alert addAction:All];
        
        [alert addAction:adviceClosed];
        
        [alert addAction:followedOrSubscribed];
        
        [alert addAction:alerts];
        
        [alert addAction:cancel];
        
        
        
    }else if (segmentedControl.selectedSegmentIndex==1)
        
    {
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Filter by:" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        
        
        [self presentViewController:alert animated:YES completion:^{
            
            
            
        }];
        
        
        
        UIAlertAction * All=[UIAlertAction actionWithTitle:@"All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
            
            
            
            
        }];
        
        
        
        //        UIAlertAction * adviceClosed=[UIAlertAction actionWithTitle:@"Advice closed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //
        
        //
        
        //
        
        //
        
        //        }];
        
        //
        
        //        UIAlertAction * followedOrSubscribed=[UIAlertAction actionWithTitle:@"Followed or subscribed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //
        
        //
        
        //        }];
        
        //        UIAlertAction * alerts = [UIAlertAction actionWithTitle:@"Alerts" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //
        
        //        }];
        
        
        
        UIAlertAction * referrals = [UIAlertAction actionWithTitle:@"Referrals" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        UIAlertAction * rewards = [UIAlertAction actionWithTitle:@"Rewards" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        [alert addAction:All];
        
        // [alert addAction:adviceClosed];
        
        //  [alert addAction:followedOrSubscribed];
        
        //  [alert addAction:alerts];
        
        [alert addAction:referrals];
        
        [alert addAction:rewards];
        
        [alert addAction:cancel];
        
        
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    
    
    return 335;
    
}
@end
