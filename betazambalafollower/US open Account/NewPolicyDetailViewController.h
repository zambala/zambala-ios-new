//
//  NewPolicyDetailViewController.h
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 16/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewPolicyDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *topTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *readButton;

@property NSString * buttonType;
@property (weak, nonatomic) IBOutlet UIButton *buttonOne;
@property (weak, nonatomic) IBOutlet UIButton *buttonTwo;

@end
