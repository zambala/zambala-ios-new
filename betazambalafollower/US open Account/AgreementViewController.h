//
//  AgreementViewController.h
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 14/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgreementViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *userButton;
@property (weak, nonatomic) IBOutlet UIButton *customerButton;
@property (weak, nonatomic) IBOutlet UIButton *riskButton;
@property (weak, nonatomic) IBOutlet UIButton *privacyButton;
@property (weak, nonatomic) IBOutlet UIButton *marginButton;
@property (weak, nonatomic) IBOutlet UIButton *orderRoutingButton;

@property (weak, nonatomic) IBOutlet UIButton *businessButton;
@end
