//
//  PolicyViewController.h
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 15/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PolicyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *customerCheck;
@property (weak, nonatomic) IBOutlet UIButton *accountCheck;
@property (weak, nonatomic) IBOutlet UIButton *optionsCheck;
@property (weak, nonatomic) IBOutlet UIButton *taxPayerCheck;
@property (weak, nonatomic) IBOutlet UIButton *finderCheck;
@property (weak, nonatomic) IBOutlet UIButton *customerRead;
@property (weak, nonatomic) IBOutlet UIButton *accountRead;
@property (weak, nonatomic) IBOutlet UIButton *optionsRead;
@property (weak, nonatomic) IBOutlet UIButton *taxpayerRead;
@property (weak, nonatomic) IBOutlet UIButton *finderRead;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIButton *customerTitle;
@property (weak, nonatomic) IBOutlet UIButton *accountTitle;
@property (weak, nonatomic) IBOutlet UIButton *optionsTitle;
@property (weak, nonatomic) IBOutlet UIButton *taxpayerTitle;
@property (weak, nonatomic) IBOutlet UIButton *agreementTitle;

@end
