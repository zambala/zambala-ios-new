//
//  PolicyDetailViewController.h
//  ChoiceTradeAccountOpening
//
//  Created by Zenwise Technologies on 15/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PolicyDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property NSString * buttonCheck;
@property NSString * titleString;

@end
