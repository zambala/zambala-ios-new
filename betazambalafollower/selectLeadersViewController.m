//
//  selectLeadersViewController.m
//  
//
//  Created by zenwise technologies on 10/07/17.
//
//

#import "selectLeadersViewController.h"
#import "selectLeadersTableViewCell.h"
#import "AppDelegate.h"

@interface selectLeadersViewController ()
{
    AppDelegate * delegate;
}

@end

@implementation selectLeadersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self selectLeadersServer];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)selectLeadersServer
{
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                                };
    NSDictionary *parameters = @{ @"clientid": delegate.userID,
                                  @"limit": @1000 };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString * url = [NSString stringWithFormat:@"%@follower/leaders/%@",delegate.baseUrl,delegate.userID];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data!=nil)
                                                    {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        self.responseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            self.selectLeadersTableView.delegate=self;
                                                            self.selectLeadersTableView.dataSource=self;
                                                            [self.selectLeadersTableView reloadData];
                                                            
                                                            
                                                        });

                                                    }
                                                    }
                                                }];
    [dataTask resume];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectLeadersTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"selectLeadersTableViewCell" forIndexPath:indexPath];
    
    [cell.leaderSelectButton addTarget:self action:@selector(onLeaderButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString * leaderName=[NSString stringWithFormat:@"  %@",[[self.responseArray objectAtIndex:indexPath.row]objectForKey:@"firstname"]];
    
    [cell.leaderSelectButton setTitle:leaderName forState:UIControlStateNormal];
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.responseArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)onLeaderButtonTap:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.selectLeadersTableView];
    
    
    
    NSIndexPath *indexPath = [self.selectLeadersTableView indexPathForRowAtPoint:buttonPosition];
    selectLeadersTableViewCell *cell = [self.selectLeadersTableView cellForRowAtIndexPath:indexPath];
    if(cell.leaderSelectButton.selected==YES)
    {
    if(delegate.selectLeadersID.count<3)
    {
       // delegate.selectLeadersID = [[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"userid"] intValue];
        
        [delegate.selectLeadersID addObject:[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"userid"]];
        [delegate.selectLeadersName addObject:[[self.responseArray objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
        
        cell.leaderSelectButton.selected=YES;
        
        cell.leaderSelectButton.tag=indexPath.row;
    }
        
    
    else
    {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.selectLeadersTableView];
        
        
        
        NSIndexPath *indexPath = [self.selectLeadersTableView indexPathForRowAtPoint:buttonPosition];
        selectLeadersTableViewCell *cell = [self.selectLeadersTableView cellForRowAtIndexPath:indexPath];
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Only 3 leaders can be selected" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            cell.leaderSelectButton.selected=NO;
            
        }];
        
        [alert addAction:okAction];
        
    }
    }else if (cell.leaderSelectButton.selected==NO)
    {
//        if(delegate.selectLeadersID.count<3)
//        {
//        [delegate.selectLeadersID removeObjectAtIndex:cell.leaderSelectButton.tag];
//            [delegate.selectLeadersName removeObjectAtIndex:cell.leaderSelectButton.tag];
//        }else if (delegate.selectLeadersID.count==3)
//        {
//            [delegate.selectLeadersID removeObjectAtIndex:cell.leaderSelectButton.tag-1];
//            [delegate.selectLeadersName removeObjectAtIndex:cell.leaderSelectButton.tag-1];
//        }
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.selectLeadersTableView];
        NSIndexPath *indexPath = [self.selectLeadersTableView indexPathForRowAtPoint:buttonPosition];
        [delegate.selectLeadersID removeObjectAtIndex:indexPath.row];
        [delegate.selectLeadersName removeObjectAtIndex:indexPath.row];
        
//        NSArray *selectedRows = [self.selectLeadersTableView indexPathsForSelectedRows];
//        BOOL deleteSpecificRows = selectedRows.count > 0;
//        if (deleteSpecificRows)
//        {
//            // Build an NSIndexSet of all the objects to delete, so they can all be removed at once.
//            NSMutableIndexSet *indicesOfItemsToDelete = [NSMutableIndexSet new];
//            for (NSIndexPath *selectionIndex in selectedRows)
//            {
//                [indicesOfItemsToDelete addIndex:selectionIndex.row];
//
//            }
//
//            [delegate.selectLeadersID removeObjectsAtIndexes:indicesOfItemsToDelete];
//            [delegate.selectLeadersName removeObjectsAtIndexes:indicesOfItemsToDelete];
        
//            NSMutableArray * removeArray = [[NSMutableArray alloc]init];
//
//            [self.removeSymbolsArray addObjectsFromArray:[self.symbolsListArray objectsAtIndexes:indicesOfItemsToDelete]];
//
//            for (int i=0; i<self.removeSymbolsArray.count; i++) {
//                [removeArray addObject:[[self.removeSymbolsArray objectAtIndex:i]objectForKey:@"symbol"]];
//            }
        
        
    //    }
    }

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
