//
//  TableViewCell1.h
//  testing
//
//  Created by zenwise mac 2 on 11/24/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell1 : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *openLabel;


@property (weak, nonatomic) IBOutlet UILabel *highLabel;


@property (weak, nonatomic) IBOutlet UILabel *lowLabel;
@property (weak, nonatomic) IBOutlet UILabel *volumeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
