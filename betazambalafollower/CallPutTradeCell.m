//
//  OrderTableViewCell.m
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "CallPutTradeCell.h"

@implementation CallPutTradeCell

- (void)awakeFromNib {
    [super awakeFromNib];
        // Initialization code
    self.quantityTxt.layer.borderWidth=1.0f;
    self.quantityTxt.layer.borderColor=[[UIColor colorWithRed:(69/255.0) green:(133/255.0) blue:(157/255.0) alpha:1]CGColor];
    
    self.limitPriceTxtFld.layer.borderWidth=1.0f;
    self.limitPriceTxtFld.layer.borderColor=[[UIColor colorWithRed:(69/255.0) green:(133/255.0) blue:(157/255.0) alpha:1]CGColor];
}
//- (void)viewDidUnload {
//    [self setPaStepper:nil];
//    
//    [super viewDidUnload];
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
//    [self.view endEditing:YES];
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
