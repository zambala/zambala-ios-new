//
//  EquitiesCell1.h
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquitiesCell1 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *eqitiesImg1;


@property (weak, nonatomic) IBOutlet UIImageView *equitiesImg2;

@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (strong, nonatomic) IBOutlet UIImageView *tickImageView;

@property (strong, nonatomic) IBOutlet UIButton *buySellButton;

@property (weak, nonatomic) IBOutlet UIButton *ordersBtn;
@property (strong, nonatomic) IBOutlet UILabel *oLbl;
@property (strong, nonatomic) IBOutlet UILabel *stLbl;
@property (strong, nonatomic) IBOutlet UILabel *valueChangeLabel;
@property (strong, nonatomic) IBOutlet UILabel *ltpLabel;
@property (strong, nonatomic) IBOutlet UILabel *changePercentlabel;
@property (strong, nonatomic) IBOutlet UILabel *actedByLabel;
@property (strong, nonatomic) IBOutlet UILabel *sharesSold;
@property (strong, nonatomic) IBOutlet UILabel *dateAndTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *profileName;

- (IBAction)leaderProfileButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *leaderProfileAction;

@property (weak, nonatomic) IBOutlet UILabel *averageProfitLbl;
@property (weak, nonatomic) IBOutlet UILabel *premiumLbl;

@property (weak, nonatomic) IBOutlet UIImageView *premiumImg;
@property (weak, nonatomic) IBOutlet UILabel *sourceLabel;
@property (weak, nonatomic) IBOutlet UILabel *sourceDataLabel;
@property (weak, nonatomic) IBOutlet UILabel *analystName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *analystNameHeightConstant;

@end
