//
//  EquitiesCell1.m
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "EquitiesCell1.h"

@implementation EquitiesCell1

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.oLbl.layer.cornerRadius=12.5;
    self.oLbl.clipsToBounds = YES;
    
    self.stLbl.layer.cornerRadius=12.5;
    self.stLbl.clipsToBounds = YES;
    self.tickImageView.layer.cornerRadius=self.tickImageView.frame.size.width/2;
    self.tickImageView.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)leaderProfileButton:(id)sender {
}
@end
