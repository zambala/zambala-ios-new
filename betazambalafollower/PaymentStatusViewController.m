//
//  PaymentStatusViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 02/01/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import "PaymentStatusViewController.h"
#import "AppDelegate.h"
#import "NewOffersByMonksViewController.h"
#import "FundsViewController.h"

@interface PaymentStatusViewController ()

@end

@implementation PaymentStatusViewController
{
    NSString * status;
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.activityIndicator.hidden=YES;
    [self paymentDetails];
    // Do any additional setup after loading the view.
}

-(void)paymentDetails
{
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    NSDictionary *headers = @{ @"Content-Type": @"application/x-www-form-urlencoded",
                               @"Cache-Control": @"no-cache",
                               };
    
    NSString * transactionNumber=[NSString stringWithFormat:@"&merchantTxnNo=%@",self.txnNo];
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[transactionNumber dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString * url = [NSString stringWithFormat:@"%@clienttrade/phicommerce/paymentdetails",delegate.baseUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.paymentResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"Payment Details:%@",self.paymentResponseDictionary);
                                                        
                                                        status = [NSString stringWithFormat:@"%@",[self.paymentResponseDictionary objectForKey:@"respdesc"]];
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        if([status isEqualToString:@"Transaction successful"])
                                                        {
                                                            self.imageView.image= [UIImage imageNamed:@"success"];
                                                            self.paymentLabel.text=@"Payment Successful";
                                                            UIAlertController * alert = [UIAlertController
                                                                                         alertControllerWithTitle:@"Payment Successful"
                                                                                         message:@"Press ok to continue enjoying our premium services."
                                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            //Add Buttons
                                                            
                                                            UIAlertAction* okButton = [UIAlertAction
                                                                                       actionWithTitle:@"Ok"
                                                                                       style:UIAlertActionStyleDefault
                                                                                       handler:^(UIAlertAction * action) {
                                                                                           //Handle your yes please button action here
                                                                                           if([self.payinCheck isEqualToString:@"payin"])
                                                                                           {
                                                                                               FundsViewController * funds = [self.storyboard instantiateViewControllerWithIdentifier:@"FundsViewController"];
                                                                                               [self.navigationController pushViewController:funds animated:YES];
                                                                                           }else
                                                                                           {
                                                                                           NewOffersByMonksViewController * offersPage = [self.storyboard instantiateViewControllerWithIdentifier:@""];
                                                                                           [delegate.cartDict removeAllObjects];
                                                                                           [self.navigationController pushViewController:offersPage animated:YES];
                                                                                           }
                                                                                           
                                                                                       }];
                                                            //Add your buttons to alert controller
                                                            
                                                            [alert addAction:okButton];
                                                            
                                                            
                                                            [self presentViewController:alert animated:YES completion:nil];
                                                            
                                                            
                                                        }else if ([status isEqualToString:@"Transaction rejected"])
                                                        {
                                                            self.imageView.image = [UIImage imageNamed:@"fail"];
                                                            self.paymentLabel.text=@"Payment Rejected";
                                                        }else
                                                        {
                                                            self.imageView.image = [UIImage imageNamed:@"fail"];
                                                            self.paymentLabel.text=@"Payment Pending";
                                                        }
                                                        self.activityIndicator.hidden=YES;
                                                        [self.activityIndicator stopAnimating];
                                                    });
                                                    
                                                }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
