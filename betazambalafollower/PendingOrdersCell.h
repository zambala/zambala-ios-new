//
//  PendingOrdersCell.h
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PendingOrdersCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *orderIdLbl;

@property (strong, nonatomic) IBOutlet UILabel *timeLbl;

@property (strong, nonatomic) IBOutlet UILabel *tradingSymLbl;
@property (strong, nonatomic) IBOutlet UILabel *pendingLabel;

@property (strong, nonatomic) IBOutlet UILabel *transactionTypeLbl;

@property (strong, nonatomic) IBOutlet UILabel *qtyLbl;
@property (strong, nonatomic) IBOutlet UILabel *exchangeLbl;

@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UIButton *editBtn;

@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
@property (strong, nonatomic) IBOutlet UILabel *filledLabel;


@end
