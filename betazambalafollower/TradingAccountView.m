//
//  TradingAccountView.m
//  testing
//
//  Created by zenwise technologies on 23/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "TradingAccountView.h"

@interface TradingAccountView ()

@end

@implementation TradingAccountView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
