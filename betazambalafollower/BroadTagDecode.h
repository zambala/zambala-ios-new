//
//  BroadTagDecode.h
//  
//
//  Created by zenwise technologies on 05/09/17.
//
//

#import <UIKit/UIKit.h>

@interface BroadTagDecode : UIViewController

-(void)TagDecode:(NSMutableArray *)btBuffer;
-(BOOL)CheckCapacity:(int)inLength;
-(short)NextTag;
-(short)TagShort;
-(int)GetIndex;
-(BOOL)TagBool;
-(int)TagInt;
-(NSString *)TagString;
-(Byte)TagByte;
-(double)TagDouble;
-(long)TagLong;
-(NSMutableArray*)TagBinary;
-(NSString *)TagStringBuffer:(short)shLength;

@end
