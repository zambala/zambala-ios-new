//
//  MonksFeedCell3.h
//  zambala leader
//
//  Created by zenwise mac 2 on 2/20/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonksFeedCell3 : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLbl3, *timeLbl3, *desLbl3;
@property (strong, nonatomic) IBOutlet UIImageView *imgView3, *arrImage;
@property (strong, nonatomic) IBOutlet UIWebView *imageWebView;


@end
