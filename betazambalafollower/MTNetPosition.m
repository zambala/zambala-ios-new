//
//  MTNetPosition.m
//  
//
//  Created by zenwise mac 2 on 9/4/17.
//
//

#import "MTNetPosition.h"

@implementation MTNetPosition

- (id)init {
    if (self = [super init]) {
           self.       inRecordNo=0;
           self.stExchange=@"";
           self.stSecurityID=@"";
           self.stClientID=@"";
           self.    stSymbol=@"";
           self.    stExpiryDate=@"";
           self.       inBuyQty=0;
           self.    dbBuyAvg=0;
           self.    dbBuyAmount=0;
           self.       inSellQty=0;
           self.    dbSellAvg=0;
           self.    dbSellAmount=0;
           self.       inNetQty=0;
           self.    dbNetAvg=0;
           self.    dbNetAmount=0;
           self.    dbLTP=0;
           self.    dbMTM=0;
           self.    dbMultiplier=0; //LotSize
           self.    stDateTime=@""; //time_t
           self.    stUserID=@"";
           self.    dbLastMTM=0; //for Servailance
           self.    dbSettleMTM=0;
           self.    stBaseCurrency=@"";
           self.    stSettleCurrency=@"";
           self.    dbBuyAmountWithExp=0;
           self.    dbSellAmountWithExp=0;

        
        
    }
    return self;
}

+ (id)Mt7 {
    static MTNetPosition *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

@end
