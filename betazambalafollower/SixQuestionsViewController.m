//
//  SixQuestionsViewController.m
//  2FAScreens
//
//  Created by Zenwise Technologies on 27/09/17.
//  Copyright © 2017 Zenwise Technologies. All rights reserved.
//

#import "SixQuestionsViewController.h"
#import "TagEncode.h"
#import "MT.h"
#import "AppDelegate.h"

@interface SixQuestionsViewController ()
{
    
    AppDelegate * delegate1;
    MT * sharedManager;
}

@end

@implementation SixQuestionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sharedManager=[MT Mt1];
    
    self.question1TF.delegate=self;
    self.question2TF.delegate=self;
    self.question3TF.delegate=self;
    self.question4TF.delegate=self;
    self.question5TF.delegate=self;
    self.question6TF.delegate=self;
    
    [self.submitButton addTarget:self action:@selector(SecurityQuestions) forControlEvents:UIControlEventTouchUpInside];
    self.activityInd.hidden=YES;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)SecurityQuestions
{
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    NSMutableArray * answersArray = [[NSMutableArray alloc]initWithObjects:self.question1TF.text,self.question2TF.text,self.question3TF.text,self.question4TF.text,self.question5TF.text,self.question6TF.text, nil];
    NSString * ansString = [answersArray componentsJoinedByString:@","];
    NSLog(@"ansString:%@",ansString);
    //PreLogin
     delegate1.mtCheck=true;
    TagEncode * tag1=[[TagEncode alloc]init];
   
    [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.SecurityQueAndAns];
    [tag1 TagData:sharedManager.stUserID stringMethod:delegate1.mtClientId];
    [tag1 TagData:sharedManager.stPassword stringMethod:delegate1.mtPassword];
    [tag1 TagData:sharedManager.stNewPassword stringMethod:@""];
    [tag1 TagData:sharedManager.inVersion intMethod:2];
    
    
    
    for (int i=0; i<6; i++) {
        NSString * ansString1 = [NSString stringWithFormat:@"%@",[answersArray objectAtIndex:i]];
        [tag1 TagData:sharedManager.stSingleSecurityAnswer stringMethod:ansString1];
        [tag1 TagData:sharedManager.nlTagFooter];
        
    }
    
    [tag1 GetBuffer];
    [self newMessage];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)newMessage
{
    //    delegate1.outputStream = (__bridge NSOutputStream *)writeStream;
    //
    //    [delegate1.outputStream setDelegate:self];
    //
    //    [delegate1.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    //
    //
    //    [delegate1.outputStream open];
    //
    
    Byte UUID[delegate1.btOutBuffer.count];
    
    for (int i = 0; i < delegate1.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate1.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp;
        [scanner scanHexInt:&temp];
        NSLog(@"%d",temp);
        
        UUID[i]=temp;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate1.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate1.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    [delegate1.outputStream write:bytes maxLength:delegate1.btOutBuffer.count];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.question1TF resignFirstResponder];
    [self.question2TF resignFirstResponder];
    [self.question3TF resignFirstResponder];
    [self.question4TF resignFirstResponder];
    [self.question5TF resignFirstResponder];
    [self.question6TF resignFirstResponder];
    return true;
}

@end
