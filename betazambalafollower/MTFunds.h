//
//  MTFunds.h
//  betazambalafollower
//
//  Created by Zenwise Technologies on 03/01/18.
//  Copyright © 2018 zenwise mac 2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTFunds : NSObject

@property short SurveillanceReportRequest;
@property short SurveillanceReport;
@property short stClientID;
@property short stSegmentName;
@property short dbBalance;
@property short dbAdhocBalance;
@property short dbHaircutBalance;
@property short dbCarryBalance;
@property short dbIntraDayBalance;
@property short dbCCEBalance;
@property short dbMMEBalance;
@property short dbCCEUtilize;
@property short dbMMEUtilize;
@property short dbSpanMargin;
@property short dbExposureMargin;
@property short dbPremium;
@property short SurvellianceReportResponse;
+ (id)Mt9;
@end
