//
//  BrokerTableViewCell.h
//  testing
//
//  Created by zenwise technologies on 19/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrokerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *brokerMessages;
@end
