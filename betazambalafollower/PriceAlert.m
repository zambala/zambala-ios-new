//
//  PriceAlert.m
//  PriceAlert
//
//  Created by zenwise mac 2 on 12/28/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "PriceAlert.h"
#import "PriceAlertCell.h"
#import "UIViewController+MJPopupViewController.h"
#import "PriceAlertPopUp.h"
#import "TabBar.h"


@interface PriceAlert ()

@end

@implementation PriceAlert

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
    
}

- (PriceAlertCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellIdentifier = @"Cell";
    
    PriceAlertCell *cell = [self.priceAltTbl dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
       
    return cell;
    
}

-(IBAction)AddBtn:(id)sender
{
    PriceAlertPopUp *detailViewController = [[PriceAlertPopUp alloc] initWithNibName:@"PriceAlertPopUp" bundle:nil];
    [self presentPopupViewController:detailViewController animationType: MJPopupViewAnimationSlideTopBottom];
}

-(IBAction)editBtn:(id)sender
{
//    PriceAlertEditView *detailViewController = [[PriceAlertEditView alloc] initWithNibName:@"PriceAlertEditView" bundle:nil];
//    [self presentPopupViewController:detailViewController animationType: MJPopupViewAnimationSlideTopBottom];
}


- (IBAction)backToMainView:(id)sender {
    
    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
    
    [self presentViewController:tabPage animated:YES completion:nil];
}
@end
