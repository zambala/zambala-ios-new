//
//  NewOffersByMonksViewController.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewOffersByMonksViewController.h"
#import "NewCartViewController.h"
#import "NoitemsInCartViewController.h"

#import "AppDelegate.h"
#import "NewServicesTblCell.h"
#import "NewOffersTableViewCell.h"
#import "UIBarButtonItem+Badge.h"
@import Tune;

@interface NewOffersByMonksViewController ()
{
    BOOL premiumSubscribeCheck;
    AppDelegate * delegate1;
    NSMutableArray * serviceResponseArray;
    NSMutableArray * durationListArray;
    NSMutableArray * monthsArray;
    NSString * monthCheckString;
    BOOL selectionBool;
    NSUInteger objectIndex;
    NSMutableArray *countArray;
    NSArray * allKeys;
    
    float discountPercentage;
}

@end

@implementation NewOffersByMonksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.itemCount = 0;
    
    
    monthsArray=[[NSMutableArray alloc]initWithObjects:@"  1 month",@"  3 months",@"  6 months",@"  12 months", nil];
    self.navigationItem.title =delegate1.cartLeaderName;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    self.popUpView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.popUpView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.popUpView.layer.shadowOpacity = 1.0f;
    self.popUpView.layer.shadowRadius = 1.0f;
    self.popUpView.layer.cornerRadius=2.1f;
    self.popUpView.layer.masksToBounds = NO;
    
       
    monthCheckString=@"check";
    
    premiumSubscribeCheck=false;
    self.popUpView.hidden=YES;
    self.blurView.hidden=YES;
    self.premiumAdvicesView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.premiumAdvicesView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.premiumAdvicesView.layer.shadowOpacity = 1.0f;
    self.premiumAdvicesView.layer.shadowRadius = 4.2f;
    self.premiumAdvicesView.layer.cornerRadius=4.2f;
    self.premiumAdvicesView.layer.masksToBounds = NO;
    
    
    
    self.signalsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.signalsView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.signalsView.layer.shadowOpacity = 1.0f;
    self.signalsView.layer.shadowRadius = 4.2f;
    self.signalsView.layer.cornerRadius=1.0f;
    self.signalsView.layer.masksToBounds = NO;
    
    
    self.dealerModeView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.dealerModeView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.dealerModeView.layer.shadowOpacity = 1.0f;
    self.dealerModeView.layer.shadowRadius = 4.2f;
    self.dealerModeView.layer.cornerRadius=1.0f;
    self.dealerModeView.layer.masksToBounds = NO;

    
    self.subscribeButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.subscribeButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.subscribeButton.layer.shadowOpacity = 1.0f;
    self.subscribeButton.layer.shadowRadius = 2.1f;
    self.subscribeButton.layer.cornerRadius=1.0f;
    self.subscribeButton.layer.masksToBounds = NO;
    
    
   
    
    [self.subscribeButton addTarget:self action:@selector(subscribeAction) forControlEvents:UIControlEventTouchUpInside];
    
    

    // Do any additional vsetup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    monthCheckString=@"check";
if(delegate1.cartDict.count>0)
{
    allKeys=[[NSArray alloc]init];
    countArray=[[NSMutableArray alloc]init];
    allKeys=[delegate1.cartDict allKeys];
    
    if([[delegate1.cartDict allKeys]count]>0)
    {
        for(int i=0;i<delegate1.cartDict.count;i++)
        {
            NSString * string=[allKeys objectAtIndex:i];
            int one=(int)[[delegate1.cartDict objectForKey:string] count];
            
            [countArray addObject:[NSNumber numberWithInt:one]];
            
            
            
        }
        
    }
    
    
    
    self.itemCount = 0;
    
    if(countArray.count>0)
    {
        for (NSNumber *num in countArray)
        {
            self.itemCount += [num intValue];
        }
        
    }
    

    
}
    else
    {
        self.itemCount=0;
    }
    [self barButtonBadge];
    [self serverHit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onCartButtonTap:(id)sender {
    
    NewCartViewController * cart = [self.storyboard instantiateViewControllerWithIdentifier:@"NewCartViewController"];
    [self.navigationController pushViewController:cart animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView==self.serviceTblView)
    {
        return  serviceResponseArray.count;
    }
    
    else
    {
        _offerTblViewHgt.constant=90*4;
        
        return durationListArray.count;
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try
    {
    if(tableView==self.serviceTblView)
    {
       
        if(serviceResponseArray.count>0)
        {
        NewServicesTblCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        
        cell.subscriberCount.text=[NSString stringWithFormat:@"%@",[[serviceResponseArray objectAtIndex:indexPath.row] objectForKey:@"subscribercount"]];
        
        cell.serviceName.text=[NSString stringWithFormat:@"%@",[[serviceResponseArray objectAtIndex:indexPath.row] objectForKey:@"offername"]];
        
        
        
        NSString * subscribeStr=[NSString stringWithFormat:@"%@",[[serviceResponseArray objectAtIndex:indexPath.row] objectForKey:@"subscribed"]];
        
        if([subscribeStr isEqualToString:@"0"])
            
        {
            
            NSArray * array=[delegate1.cartDict objectForKey:delegate1.cartLeaderName];
            
            if(array.count>0)
            {
            for(int i=0;i<array.count;i++)
            {
                NSString * offerName=[NSString stringWithFormat:@"%@",[[array objectAtIndex:i]objectForKey:@"offername"]];
                
            if([offerName isEqualToString:cell.serviceName.text])
            {
                [cell.subscribeBtn setTitle:@"View Cart" forState:UIControlStateNormal];
                break;
                
            }
            
            else
            {
            [cell.subscribeBtn setTitle:@"Subscribe" forState:UIControlStateNormal];
            }
            }
                
            }
            
            else
            {
                
                [cell.subscribeBtn setTitle:@"Subscribe" forState:UIControlStateNormal];
            }
        }
        else if([subscribeStr isEqualToString:@"1"])
        {
             [cell.subscribeBtn setTitle:@"Subscribed" forState:UIControlStateNormal];
            
        }
        
        
        [cell.subscribeBtn addTarget:self action:@selector(subscribeAction1:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        return cell;
        }
            
            
    }
    
    else
    {
    NewOffersTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
        
        
       
        
        float amountFloat=[[[durationListArray objectAtIndex:indexPath.row] objectForKey:@"amount"]floatValue];
        
       
        NSString * discountString=[NSString stringWithFormat:@"%@",[[durationListArray objectAtIndex:indexPath.row] objectForKey:@"discvalue"]];
        
        if([discountString isEqualToString:@"<null>"])
        {
             discountPercentage=0;
        }
        
        else
        {
              discountPercentage=[[[durationListArray objectAtIndex:indexPath.row] objectForKey:@"discvalue"]floatValue];
        }
        
        NSString * str=@"%";
        
         cell.disountLbl.text=[NSString stringWithFormat:@"%.2f%@",discountPercentage,str];
        
       
        
        cell.pricelbl.text=[NSString stringWithFormat:@"%.2f",amountFloat];
        
        float discountPrice=amountFloat*discountPercentage/100;
        
         cell.finalPriceLbl.text=[NSString stringWithFormat:@"%.2f",amountFloat-discountPrice];
        
//         cell.finalPriceLbl.text=[NSString stringWithFormat:@"%@",[[durationListArray objectAtIndex:indexPath.row] objectForKey:@"amount"]];
        
        [cell.monthSelectionBtn setTitle:[monthsArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        
//        [cell.monthSelectionBtn addTarget:self action:@selector(monthBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//
        
//        
//        if([monthCheckString isEqualToString:@"check"])
//        {
//            monthCheckString=@"";
//            cell.monthSelectionBtn.selected=YES;
//            selectionBool=true;
//            
//        }
//        
//        else
//        {
//            cell.monthSelectionBtn.selected=NO;
//            
//            selectionBool=false;
//            
//        }
//        
//        
//        [self monthBtnAction:cell.monthSelectionBtn];
    
    return cell;
        
    }
    
    
    return nil;
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}        
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.serviceTblView)
    {
        return 86;
    }
    
    else
    {
    return 90;
        
    }
    
    return 0;
}


-(void)monthBtnAction:(UIButton *)sender
{
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.offersTbleView];
    
    
    
    NSIndexPath *indexPath = [self.offersTbleView indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"Index path:%ld",(long)indexPath.row);
    NewOffersTableViewCell *cell = [self.offersTbleView cellForRowAtIndexPath:indexPath];
    
    if(selectionBool==true)
    {
        selectionBool=false;
        cell.monthSelectionBtn.selected=YES;
        [cell.monthSelectionBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
        
        
        
    }
    
    else if(selectionBool==false)
    {
        
        
        selectionBool=true;
        cell.monthSelectionBtn.selected=NO;
        [cell.monthSelectionBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
        
        
    }
    
    
    
   
    

    
    
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.offersTbleView)
    {
        
    
    
    
 NewOffersTableViewCell *cell = [self.offersTbleView cellForRowAtIndexPath:indexPath];
    
    objectIndex=indexPath.row;
        
         [cell.monthSelectionBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    if(cell.monthSelectionBtn.selected==YES)
    {
        cell.monthSelectionBtn.selected=NO;
    }
    
    else if(cell.monthSelectionBtn.selected==NO)
    {
        cell.monthSelectionBtn.selected=YES;
    }
    
   
        
//        [self.offersTbleView reloadData];
    
    }
   
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewOffersTableViewCell *cell = [self.offersTbleView cellForRowAtIndexPath:indexPath];
    
    cell.monthSelectionBtn.selected=NO;
    
     [cell.monthSelectionBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self.offersTbleView reloadData];
    
}


-(void)subscribeAction1:(UIButton *)sender
{
    
    
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.serviceTblView];
    
    
    
    NSIndexPath *indexPath = [self.serviceTblView indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"Index path:%ld",(long)indexPath.row);
    NewServicesTblCell *cell = [self.serviceTblView cellForRowAtIndexPath:indexPath];
    
    
    if([cell.subscribeBtn.currentTitle isEqualToString:@"Subscribe"])
    {
    
    NSString * subscriptionid=[NSString stringWithFormat:@"%@",[[serviceResponseArray objectAtIndex:indexPath.row] objectForKey:@"subscriptionid"]];
        
         NSString * offerName=[NSString stringWithFormat:@"%@",[[serviceResponseArray objectAtIndex:indexPath.row] objectForKey:@"offername"]];
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                              };
        
        static CFStringRef charset = CFSTR("!@#$%&*()+'\";:=,/?[] ");
        CFStringRef str = (__bridge CFStringRef)offerName;
        CFStringEncoding encoding = kCFStringEncodingUTF8;
        offerName= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, str, NULL, charset, encoding));
    
    NSString * urlStr=[NSString stringWithFormat:@"%@subscriptionoffer?offername=%@&leaderid=%@&subscriptionid=%@",delegate1.baseUrl,offerName,delegate1.leaderid,subscriptionid];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        durationListArray=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                         durationListArray=[[[durationListArray reverseObjectEnumerator] allObjects] mutableCopy];
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                       
                                                        self.offersTbleView.delegate=self;
                                                        self.offersTbleView.dataSource=self;
                                                        [self.offersTbleView reloadData];
                                                        
                                                        self.popUpView.hidden=NO;
                                                        self.blurView.hidden=NO;
                                                        self.blurView.backgroundColor=[UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:0.1];

                                                    });
                                                    

                                                }];
    [dataTask resume];
    
    
    
//    if([self.subscribeButton.currentTitle isEqualToString:@"VIEW CART"])
//    {
//        NewCartViewController * cart = [self.storyboard instantiateViewControllerWithIdentifier:@"NewCartViewController"];
//        [self.navigationController pushViewController:cart animated:YES];
//
//    }
//    
//    else
//    {
//    
//    
//      
//    }
        
    }
    
    else if([cell.subscribeBtn.currentTitle isEqualToString:@"View Cart"])
    {
        
        [self onCartButtonTap:self];
    }
//
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message" message:@"You already subscribed for the service" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            // Ok action example
        }];
        
        [alert addAction:okAction];
       
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}

- (IBAction)addToCartBtn:(id)sender {
    
    self.popUpView.hidden=YES;
    self.blurView.hidden=YES;
    
    
    
    delegate1.cartArray=[[NSMutableArray alloc]init];
    countArray=[[NSMutableArray alloc]init];
    allKeys=[[NSArray alloc]init];
    
    if(durationListArray.count>0)
    {
    
    [delegate1.cartArray addObject:[durationListArray objectAtIndex:objectIndex]];
    
    NSString * name=delegate1.cartLeaderName;
   if([[delegate1.cartDict allKeys]count]>0)
   {
    if([[delegate1.cartDict allKeys] containsObject:name])
    {
        NSMutableArray *  localArray=[[NSMutableArray alloc]init];
        localArray=[delegate1.cartDict objectForKey:name];
        
        NSArray *newArray=[localArray arrayByAddingObjectsFromArray:delegate1.cartArray ];
        
        [delegate1.cartDict setObject:newArray forKey:name];
    }
    
    
    else
    {
        [delegate1.cartDict setObject:delegate1.cartArray forKey:name];
    }
   }
    
    else
    {
        
        [delegate1.cartDict setObject:delegate1.cartArray forKey:name];

    }
    
    NSLog(@"%@",delegate1.cartDict);
    
    allKeys=[delegate1.cartDict allKeys];
    
    if([[delegate1.cartDict allKeys]count]>0)
    {
    for(int i=0;i<delegate1.cartDict.count;i++)
    {
        NSString * string=[allKeys objectAtIndex:i];
        int one=(int)[[delegate1.cartDict objectForKey:string] count];
        
        [countArray addObject:[NSNumber numberWithInt:one]];
        
        
        
    }
        
    }
    
    
    
    self.itemCount = 0;
    
    if(countArray.count>0)
    {
    for (NSNumber *num in countArray)
    {
        self.itemCount += [num intValue];
    }
        
    }
    
    
    
    
    
    
    [self barButtonBadge];
    
       [self.serviceTblView reloadData];
    
    
    
    
//    if(premiumSubscribeCheck==false)
//    {
//        
//        firstname
//        premiumSubscribeCheck=true;
//        
//        [self.subscribeButton setTitle:@"VIEW CART" forState:UIControlStateNormal];
//        
//        
//    }
//    
//    else if(premiumSubscribeCheck==true)
//    {
//        premiumSubscribeCheck=false;
//        
//        [self.subscribeButton setTitle:@"SUBSCRIBE" forState:UIControlStateNormal];
//        
//    }
        
    }

}

- (IBAction)cancelBtn:(id)sender {
    
    self.popUpView.hidden=YES;
    self.blurView.hidden=YES;
   }

-(void)serverHit
{
    @try {
        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                   @"postman-token": @"1799923b-23dc-ae53-b7e6-ffbc1fda224f" };
        
        NSString * urlStr=[NSString stringWithFormat:@"%@subscriptionoffer/%@/%@",delegate1.baseUrl,delegate1.leaderid,delegate1.userID];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        
                                                    
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            serviceResponseArray=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            self.serviceTblView.delegate=self;
                                                            self.serviceTblView.dataSource=self;
                                                            
                                                            [self.serviceTblView reloadData];
                                                        });

                                                    }];
        [dataTask resume];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
   
}
-(void)barButtonBadge
{
    
    NSNumber * number=[NSNumber numberWithInteger:self.itemCount];
    NSString * string=[number stringValue];
    self.navigationItem.rightBarButtonItem.badgeValue =string;
}

@end
