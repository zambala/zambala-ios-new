//
//  NewProfileTableViewCell.h
//  betazambalafollower
//
//  Created by zenwise technologies on 23/03/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewProfileTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UILabel *ltpLabel;

@property (strong, nonatomic) IBOutlet UILabel *changePercentLabel;
@property (strong, nonatomic) IBOutlet UILabel *actedByLabel;
@property (strong, nonatomic) IBOutlet UILabel *sharesSoldLabel;
@property (strong, nonatomic) IBOutlet UIButton *buySellButton;
@property (strong, nonatomic) IBOutlet UILabel *dateAndTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *oLabel;
@property (strong, nonatomic) IBOutlet UILabel *stLabel;

@end
