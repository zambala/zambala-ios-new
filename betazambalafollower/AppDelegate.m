 //
//  AppDelegate.m
//  testing
//
//  Created by zenwise mac 2 on 11/14/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <CoreLocation/CoreLocation.h>
#import "HelpshiftCore.h"
#import <Mixpanel/Mixpanel.h>
#import "HelpshiftAll.h"
#import <MapKit/MapKit.h>
@import Tune;
#import <HockeySDK/HockeySDK.h>
#import <Pushwoosh/PushNotificationManager.h>
#import <UserNotifications/UserNotifications.h>
//#import <TwitterKit/TwitterKit.h>
@import TwitterKit;






#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()<CLLocationManagerDelegate,PushNotificationDelegate,TuneDelegate,UNUserNotificationCenterDelegate>
{
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder * geocoder;
    Mixpanel *  mixpanel;
    UINavigationController * navigation;
    

}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //tune
    NSLog(@"This is another change");
    NSLog(@"This is another change");
    
//    self.baseUrl=@"http://zambaladev.ap-southeast-1.elasticbeanstalk.com/api/";
    self.baseUrl=@"https://stockserver.zenwise.net/api/";
 
    

    
  //  self.baseUrl=@"http://52.66.159.178:8013/api/";
    
//     self.baseUrl=@"http://192.168.15.222:8013/api/";
    

    
    //////////////////////////////// US Open Account ////////////////
    
    self.addressDict = [[NSMutableDictionary alloc]init];
    self.finalChoiceTradeDict =[[NSMutableDictionary alloc]init];
    self.apiKey=@"A3F671B418E9F573E04F";
   // self.apiKey=@"PvyKTESA2t-NzGR1IjF74";
      self.producationURL=@"https://newacctapi.choicetrade.com/";
  //  self.producationURL=@"http://newacctapi-dev.letsgotrade.com/";
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        
    {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{

//            [[UIApplication sharedApplication] registerForRemoteNotifications];


        });

         dispatch_async(dispatch_get_main_queue(), ^{
                    PushNotificationManager * pushManager = [PushNotificationManager pushManager];
                    pushManager.delegate = self;
        
                    // set default Pushwoosh delegate for iOS10 foreground push handling
                    [UNUserNotificationCenter currentNotificationCenter].delegate = [PushNotificationManager pushManager].notificationCenterDelegate;
        
                    // track application open statistics
                    [[PushNotificationManager pushManager] sendAppOpen];
        
        
        
                        // register for push notifications!
                        [[PushNotificationManager pushManager] registerForPushNotifications];
        
        
         });
        
        
    }
    
    else
        
    {
        
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        
        
    }
    
    
    NSLog(@"TEST2");
    
    self.upStockCheck=true;

    self.searchToWatch=true;

    self.marketWatchBool=true;

    [Tune initializeWithTuneAdvertiserId:@"197332"
                       tuneConversionKey:@"054dbead09a04e5853576bbf985d7d03"];


    
    //hockey//--
    
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"e558ecbde3ae4dbd8ac29377a01e7646"];
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];

    _profileImg=@"YES";

    _firstTimeCheck=YES;
//
//    helpshift//--
    
    [HelpshiftCore initializeWithProvider:[HelpshiftAll sharedInstance]];


    [HelpshiftCore installForApiKey:@"cc1ded606faee71ef68b4128b72d2e1c" domainName:@"zenwise-technologies.helpshift.com" appID:@"zenwise-technologies_platform_20170726065628934-964b6d1e092c000"];

//
//
//
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    if(prefs !=nil)
    {



        self.homeInstrumentToken=[prefs objectForKey:@"instrumentToken"];

    }

    UIDevice *device = [UIDevice currentDevice];

    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];

    NSLog(@"%@",currentDeviceId);



    _cartDict=[[NSMutableDictionary alloc]init];
    _cartArray=[[NSMutableArray alloc]init];
//
//
    [Mixpanel sharedInstanceWithToken:@"38124e20212adfc92d9b44a25f3b8981"];

    [[Mixpanel sharedInstance] track:@"Launched"];

     [mixpanel identify:mixpanel.distinctId];

//
    _launchCheck=true;
    _firstTime=true;
    self.test=true;
    self.test1=true;
    self.urlArray=[[NSMutableArray alloc]init];
    self.optionLimitPriceArray=[[NSMutableArray alloc]init];
    self.leaderAdviceDetails=[[NSMutableArray alloc]init];
    self.sampleDepthLot=[[NSMutableArray alloc]init];
    self.depthLotSize=[[NSMutableArray alloc]init];
    self.imageArray=[[NSMutableArray alloc]init];
     self.optionOrderArray=[[NSMutableArray alloc]init];
    self.optionSymbolArray=[[NSMutableArray alloc]init];
     self.homeFilterCompanyName=[[NSMutableArray alloc]init];
    self.btnCountArray=[[NSMutableArray alloc]init];
    self.selectLeadersID = [[NSMutableArray alloc]initWithCapacity:3];
    self.detailNewsDict=[[NSMutableDictionary alloc]init];
     self.homeinstrumentName=[[NSMutableArray alloc]init];
     self.homeInstrumentToken=[[NSMutableArray alloc]init];
     self.homeLotDepth=[[NSMutableArray alloc]init];

    self.optionOrderTransactionArray=[[NSMutableArray alloc]init];
    self.leaderDetailArray=[[NSMutableArray alloc]init];
    
    self.lastPriceArray=[[NSMutableArray alloc]init];
    self.filteredCompanyName=[[NSMutableArray alloc]init];
   self.percentageChangeArray=[[NSMutableArray alloc]init];
    self.timeArray=[[NSMutableArray alloc]init];

    self.dict=[[NSMutableDictionary alloc]init];
    self.dict123=[[NSMutableDictionary alloc]init];
    self.cashDic=[[NSMutableDictionary alloc]init];
    self.filterResponseDictionary=[[NSMutableDictionary alloc]init];
    self.scripsMt=[[NSArray alloc]init];

     self.searchSymbolDict=[[NSMutableDictionary alloc]init];

    self.dict1=[[NSDictionary alloc]init];
    self.requestToken=[[NSString alloc]init];
    self.NIFTYdict=[[NSMutableDictionary alloc]init];
    self.SENSEXdict=[[NSMutableDictionary alloc]init];
    self.instrumentNameArr=[[NSMutableArray alloc]init];
    self.instrumentToken=[[NSMutableArray alloc]init];
    self.filterMonksArray=[[NSMutableArray alloc]init];
    self.selectLeadersName = [[NSMutableArray alloc]init];
    self.allTradeOrderArray = [[NSMutableArray alloc]init];
    self.holdingRequestCheck=true;
    self.depthMt=false;
    self.homeMt=false;
    self.orderMt=false;
    self.holdingsMt=false;


   
    
    
    

//    multitrade//--
    
    self.btOutBuffer=[[NSMutableArray alloc]init];
    self.allOrderHistory=[[NSMutableArray alloc]init];
    self.allTradeArray=[[NSMutableArray alloc]init];
    self.mtCancelArray=[[NSMutableArray alloc]init];
    self.mtPendingArray=[[NSMutableArray alloc]init];
    self.mtCompletedArray=[[NSMutableArray alloc]init];
    self.exchangeToken=[[NSMutableArray alloc]init];
    self.homeExchangeToken=[[NSMutableArray alloc]init];
    self.marketWatch1=[[NSMutableArray alloc]init];
    self.mtpositionsArray=[[NSMutableArray alloc]init];
    self.homeNewCompanyArray=[[NSMutableArray alloc]init];
    self.expiryDateArray=[[NSMutableArray alloc]init];
    self.strikeArray=[[NSMutableArray alloc]init];
    self.homeStrkeArray=[[NSMutableArray alloc]init];
    self.optionArray=[[NSMutableArray alloc]init];
    self.homeOptionArray=[[NSMutableArray alloc]init];
    self.homeExpiryDate=[[NSMutableArray alloc]init];
    self.lotsizeArray=[[NSMutableArray alloc]init];
    self.homeLotSizeArray=[[NSMutableArray alloc]init];
    self.userMarketWatch=[[NSMutableArray alloc]init];
    self.localExchageToken=[[NSMutableArray alloc]init];
    self.tradeCompletedArray=[[NSMutableArray alloc]init];
    self.stdealerId=[[NSMutableArray alloc]init];
    self.mtHoldingArray=[[NSMutableArray alloc]init];
    self.mtIndexWatchArray = [[NSMutableArray alloc]init];
    self.mtFundsArray = [[NSMutableArray alloc]init];

//
//
//
//
//
//
//
    
//    if([self.usCheck isEqualToString:@"USA"])
//    {
//    UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topUS-1"];
//    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
//
//    [[UINavigationBar appearance] setTranslucent:NO];
//    }else
//    {
////     UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topBg1"];
////    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
//    }

        //crashlytics//---
    
    [Fabric with:@[[Crashlytics class]]];

//    //Initialize InMobi SDK with your account ID
//    [IMSdk initWithAccountID:@"90cbba9db43a4612803c42d8ae538dcd"];

//    //navigation//
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1]];

    [[UINavigationBar appearance] setTranslucent:NO];
    //tab bar//

    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1]];
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];



    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1]}];

   [navigation.navigationItem.leftBarButtonItem setTitle:@" "];


    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1]];

//
//    //localytics//
//
//    [Localytics autoIntegrate:@"2c4b5ac0a2a834b6cb4f909-0aa34e2a-dc8e-11e6-5de4-008c9f3dc0ef" launchOptions:launchOptions];

    //location//
    CLLocationManager *mgr = [[CLLocationManager alloc] init];
    CLLocation *loc = mgr.location;
//    [IMSdk setLocation:loc];
//
//    [IMSdk setGender:kIMSDKGenderMale];
//    [IMSdk setAgeGroup:kIMSDKAgeGroupBetween25And34];




    [self CurrentLocationIdentifier];

    [Tune measureSession];

//
    

    
        
        
        
        
        UIDevice *device1 = [UIDevice currentDevice];
        
        
        
        self.currentDeviceId = [[device1 identifierForVendor]UUIDString];
        
        
    

  
    //twitter//
    
    NSMutableDictionary *twitterKeys;
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Twitter" ofType:@"plist"];
    
    twitterKeys = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    
    [[Twitter sharedInstance] startWithConsumerKey:[twitterKeys objectForKey:@"consumer_key"] consumerSecret:[twitterKeys objectForKey:@"consumer_secret"]];
    
    
    
    [[Twitter sharedInstance] startWithConsumerKey:@"E5b4foHqkKv9AT8I4eDTKCqt2" consumerSecret:@"205ANedDQktKbaNUGFK5Kj2q8TB1ow0PJZeS7PTa8nojrAOJPG"];
    
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//    [self registerForRemoteNotifications];
//
//    });/
    
    
    
    
    
    ////////////////////////US Markets////////////////////
    
//    UIImage *NavigationPortraitBackgroundUSA= [UIImage imageNamed:@"topUS-1"];
//
//    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
//
//
//    [[UINavigationBar appearance] setTranslucent:NO];
    
    
    self.equityTickerNameArray = [[NSMutableArray alloc]init];
    self.equityCompanyNameArray = [[NSMutableArray alloc]init];
    self.marketWatchLocalStoreDict= [[NSMutableDictionary alloc]init];
    self.expertIDArray = [[NSMutableArray alloc]init];
    self.mainOrdersArray = [[NSMutableArray alloc]init];
    self.dummyAccountURL = @"http://api-test.choicetrade.com/";
    self.realAccountURL=@"http://api-trade.choicetrade.com/";
     self.zambalaEndPoint =@"https://stockserver.zenwise.net/api/";
    //self.zambalaEndPoint=@"http://52.66.159.178:8013/api/";
       return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
     self.searchToWatch=true;
    
   
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
    self.searchToWatch=true;
     [[NSNotificationCenter defaultCenter] postNotificationName:@"back" object:nil];
    
   
//    if([self.brokerNameStr isEqualToString:@"Zerodha"]||[self.brokerNameStr isEqualToString:@"Upstox"])
//    {
//        
//    }
//    else
//    {
//         [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
//    }
   }


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    [userDefaults setObject:self.instrumentToken forKey:@"token"];
//    [userDefaults synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"upnifty" object:nil];
    
    [self.inputStream close];
    [self.broadCastInputStream close];

}



-(BOOL)resignFirstResponder

{
    
    [self resignFirstResponder];
    
    return YES;
    
    
}




- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    
    
    
    // after we have current coordinates, we use this method to fetch the information data of fetched coordinate
    [geocoder reverseGeocodeLocation:[locations lastObject] completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks lastObject];
        
        NSString *street = placemark.thoroughfare;
        NSString *city = placemark.locality;
        NSString *posCode = placemark.postalCode;
        NSString *country = placemark.country;
        
        NSLog(@"we live in %@", country);
        NSLog(@"we live in %@", street);
        NSLog(@"we live in %@", city);
        NSLog(@"we live in %@", posCode);
        
        // stopping locationManager from fetching again
        [locationManager stopUpdatingLocation];
    }];
}


-(void)CurrentLocationIdentifier
    {
        //---- For getting current gps location
        CLGeocoder *ceo;
        
        locationManager = [[CLLocationManager alloc]init];
        locationManager.delegate = self;
        [locationManager startUpdatingLocation];
        
        //------
        
        ceo= [[CLGeocoder alloc]init];
        [locationManager requestWhenInUseAuthorization];
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
        CLLocationCoordinate2D coordinate;
        
        coordinate.latitude=locationManager.location.coordinate.latitude;
        coordinate.longitude=locationManager.location.coordinate.longitude;
        //CLLocationCoordinate2D  ctrpoint;
        //  ctrpoint.latitude = ;
        //ctrpoint.longitude =f1;
        //coordinate.latitude=23.6999;
        //coordinate.longitude=75.000;
        MKPointAnnotation *marker = [MKPointAnnotation new];
        marker.coordinate = coordinate;
        NSLog(@"%f",coordinate.latitude);
        //[self.mapView addAnnotation:marker];
        
        
        CLLocation *loc = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude
                           ];
        [ceo reverseGeocodeLocation:loc
                  completionHandler:^(NSArray *placemarks, NSError *error) {
                      CLPlacemark *placemark = [placemarks objectAtIndex:0];
                      NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      NSLog(@"addressDictionary %@", placemark.addressDictionary);
                      
                      NSLog(@"placemark %@",placemark.region);
                      NSLog(@"placemark %@",placemark.country);  // Give Country Name
                      NSLog(@"placemark %@",placemark.locality); // Extract the city name
                      NSLog(@"location %@",placemark.name);
                      NSLog(@"location %@",placemark.ocean);
                      NSLog(@"location %@",placemark.postalCode);
                      NSLog(@"location %@",placemark.subLocality);
                      
                      NSLog(@"location %@",placemark.location);
                      //Print the location to console
                      NSLog(@"I am currently at %@",locatedAt);
                      
                      self.region=[NSString stringWithFormat:@"%@",placemark.region];
                      
                      self.country=[NSString stringWithFormat:@"%@",placemark.country];

                      
                      self.locality=[NSString stringWithFormat:@"%@",placemark.locality];

                      
                      self.name=[NSString stringWithFormat:@"%@",placemark.name];

                      
                      self.ocean=[NSString stringWithFormat:@"%@",placemark.ocean];

                      
                      self.postalCode=[NSString stringWithFormat:@"%@",placemark.postalCode];

                      
                      self.subLocality=[NSString stringWithFormat:@"%@",placemark.subLocality];

                      
                      self.location=[NSString stringWithFormat:@"%@",placemark.location];

                      
                      self.locatedAt=[NSString stringWithFormat:@"%@",locatedAt];

                      
                      
                      
                     
                      [locationManager stopUpdatingLocation];
                  }
         
         ];
}

//tune//

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary *)options {
    // Pass the deep link URL to Tune
    [Tune handleOpenURL:url options:options];
    // Take care of the redirect yourself here
   
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    // Pass the deep link URL to Tune
    [Tune handleOpenURL:url sourceApplication:sourceApplication];
    // Take care of the redirect yourself here
        return YES;
}
- (BOOL)application:(UIApplication *)application continueUserActivity:(nonnull NSUserActivity *)userActivity restorationHandler:(nonnull void (^)(NSArray * _Nullable))restorationHandler {
    BOOL handledByTune = [Tune handleContinueUserActivity:userActivity restorationHandler:restorationHandler];
    // If handledByTune is false, take care of the redirect yourself
    if (!handledByTune) {
      
    }
    return YES;
}

- (void)tuneDidReceiveDeeplink:(NSString *)deeplink {
    NSLog(@"TUNE.deeplink: %@", deeplink);
    // Handle deep link redirect here...
}

- (void)tuneDidFailDeeplinkWithError:(NSError *)error {
    NSLog(@"TUNE.deeplink failed: %@", [error localizedDescription]);
}

//location//

+ (void)setShouldAutoCollectDeviceLocation:(BOOL)autoCollect
{
    autoCollect=YES;
}



// system push notifications callback, delegate to pushManager
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
    [[PushNotificationManager pushManager] handlePushReceived:userInfo];
    completionHandler(UIBackgroundFetchResultNoData);
}






- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mixpanelDeviceToken=deviceToken;
        NSLog(@"deviceToken: %@", deviceToken);
        self.token = [NSString stringWithFormat:@"%@", deviceToken];
        //Format token as you need:
        self.token = [self.token stringByReplacingOccurrencesOfString:@" " withString:@""];
        self.token = [self.token stringByReplacingOccurrencesOfString:@">" withString:@""];
        self.token = [self.token stringByReplacingOccurrencesOfString:@"<" withString:@""];
        
        NSLog(@"Main device token:%@",self.token);
        
        
        NSData* data = [self.token dataUsingEncoding:NSUTF8StringEncoding];
        
        [HelpshiftCore registerDeviceToken:data];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        if(prefs !=nil)
        {
            
            // getting an NSString
            
            [prefs setObject:self.token forKey:@"devicetoken"];
            [prefs setObject:deviceToken forKey:@"devicetokenData"];
            [prefs synchronize];
            
        }
        
        
        
    });
    
   
    [[PushNotificationManager pushManager] handlePushRegistration:deviceToken];
    
    
    
    // This sends the deviceToken to Mixpanel
    
    
    
}



@end
