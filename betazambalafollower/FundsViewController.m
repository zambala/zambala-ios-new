//
//  FundsViewController.m
//  Funds
//
//  Created by Zenwise Technologies on 27/11/17.
//  Copyright © 2017 Zenwise Technologies. All rights reserved.
//

#import "FundsViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "FundTransferView.h"
#import "TagEncode.h"
#import "MTFunds.h"
#import "MT.h"
#import "BrokerViewNavigation.h"
#import <Mixpanel/Mixpanel.h>

@interface FundsViewController ()<NSStreamDelegate>
{
    
    AppDelegate * delegate1;
    MTFunds * sharedManagerFunds;
    MT * sharedManager;
    
}

@end

@implementation FundsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sharedManagerFunds = [MTFunds Mt9];
    sharedManager = [MT Mt1];
      delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.payButton.hidden=YES;
    self.derivativesPayoutButton.hidden=YES;
    
    self.fundsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.fundsView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.fundsView.layer.shadowOpacity = 1.0f;
    self.fundsView.layer.shadowRadius = 1.0f;
    self.fundsView.layer.cornerRadius=1.0f;
    self.fundsView.layer.masksToBounds = NO;
    
    
    self.payButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.payButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.payButton.layer.shadowOpacity = 1.0f;
    self.payButton.layer.shadowRadius = 1.0f;
    self.payButton.layer.cornerRadius=1.0f;
    self.payButton.layer.masksToBounds = NO;
    
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"]||[delegate1.brokerNameStr isEqualToString:@"Upstox"])
    {
        self.eqLabel1.text=@"NET CASH AVAILABLE";
        self.eqLabel2.text=@"AMOUNT TRANSFERED TODAY";
        self.eqLabel3.text=@"TOTAL ACCOUNT CASH";
        self.eqLabel4.text=@"TOTAL CASH USED TODAY";
        self.drLabel1.text=@"NET CASH AVAILABLE";
        self.drLabel2.text=@"AMOUNT TRANSFERED TODAY";
        self.drLabel3.text=@"TOTAL ACCOUNT CASH";
        self.drLabel4.text=@"TOTAL CASH USED TODAY";
    }else
    {
        self.eqLabel1.text=@"LEDGER BALANCE";
        self.eqLabel2.text=@"SECURITY VALUE";
        self.eqLabel3.text=@"TOTAL BUYING POWER";
        self.eqLabel4.text=@"TOTAL CASH USED TODAY";
        self.drLabel1.text=@"EXPOSURE MARGIN";
        self.drLabel2.text=@"SPAN MARGIN";
        self.drLabel3.text=@"TOTAL F&O MARGIN";
        self.drLabel4.text=@"TOTAL CASH USED TODAY";
        
    }
    
   
    
    [self.payButton addTarget:self action:@selector(onPayButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.payInButton addTarget:self action:@selector(onPayinButtontap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}
-(void)loginCheck
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Please choose an option." message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
    UIAlertAction * Login=[UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Navigate to login page
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        
        delegate1.loginActivityStr=@"";
        [prefs removeObjectForKey:@"logintype"];
        [prefs removeObjectForKey:@"ID"];
        [prefs removeObjectForKey:@"userid"];
        [prefs removeObjectForKey:@"brokername"];
        
        delegate1.outputStream.delegate=self;
        delegate1.broadCastOutputstream.delegate=self;
        delegate1.broadCastInputStream.delegate=self;
        
        [delegate1.outputStream close];
        [delegate1.broadCastOutputstream close];
        [delegate1.broadCastInputStream close];
        
        
        BrokerViewNavigation * view=[self.storyboard instantiateViewControllerWithIdentifier:@"brokernavigation"];
        
        [self presentViewController:view animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:Login];
    [alert addAction:cancel];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    if([delegate1.loginActivityStr isEqualToString:@"OTP1"])
    {
        [self loginCheck];
        
    }
    
    else
    {
        
    }
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
    {
        [self zerodhaFundsServer];
    }else if([delegate1.brokerNameStr isEqualToString:@"Upstox"])
    {
        [self upStoxSever];
    }
    
    else
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(multitradeFunds:)
                                                     name:@"funds" object:nil];
        TagEncode * tag = [[TagEncode alloc]init];
        delegate1.mtCheck=true;
        [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManagerFunds.SurveillanceReportRequest];
        [tag GetBuffer];
        
        [self newMessage];
    }
}
-(void)newMessage
{
    //    [delegate2.outputStream setDelegate:self];
    Byte UUID[delegate1.btOutBuffer.count];
    
    for (int i = 0; i < delegate1.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate1.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp1;
        [scanner scanHexInt:&temp1];
        NSLog(@"%d",temp1);
        
        UUID[i]=temp1;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate1.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate1.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    [delegate1.outputStream write:bytes maxLength:delegate1.btOutBuffer.count];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onPayinButtontap
{
    
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Oops!"
                                         message:@"Payin is not available with Zerodha now. It will be available soon."
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                           
                                           
                                       }];
            //Add your buttons to alert controller
            
            [alert addAction:okButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }else
    {
        FundTransferView * fundsTransfer = [self.storyboard instantiateViewControllerWithIdentifier:@"FundTransferView"];
        [self presentViewController:fundsTransfer animated:YES completion:nil];
    }
    
//ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
//    view.payInCheck=@"payin";
//[self.navigationController pushViewController:view animated:YES];
}

-(void)multitradeFunds:(NSNotification *)note
{
    NSLog(@"Funds:%@",delegate1.mtFundsArray);
    
    NSString * netCash = [NSString stringWithFormat:@"%.2f",[[[delegate1.mtFundsArray objectAtIndex:0]objectForKey:@"dbBalance"]floatValue]];
    self.netCashAvailable.text = netCash;
    
    NSString * amountToday = [NSString stringWithFormat:@"%.2f",[[[delegate1.mtFundsArray objectAtIndex:0]objectForKey:@"dbHaircutbalance"]floatValue]];
    self.amountTodayLabel.text = amountToday;
    
    float netcashint = [netCash floatValue];
    float amounttodayint = [amountToday floatValue];
    float totalInt = netcashint + amounttodayint;
    
    NSString * totalAvailableCash  = [NSString stringWithFormat:@"%.2f",totalInt];
    self.accontCashLabel.text = totalAvailableCash;
    
    NSString * cashUsedToday = [NSString stringWithFormat:@"%.2f",[[[delegate1.mtFundsArray objectAtIndex:0]objectForKey:@"dbCCEUtilize"]floatValue]];
    self.cashTodayLabel.text = cashUsedToday;
   
    
    NSString * exposureMargin = [NSString stringWithFormat:@"%.2f",[[[delegate1.mtFundsArray objectAtIndex:0]objectForKey:@"dbExposureMargin"]floatValue]];
    self.drCashAvailableLabel.text = exposureMargin;
    
    NSString * spanMargin = [NSString stringWithFormat:@"%.2f",[[[delegate1.mtFundsArray objectAtIndex:0]objectForKey:@"dbSpanMargin"]floatValue]];
    self.drAmountTodayLabel.text = spanMargin;
    
    float exposureFloat = [exposureMargin floatValue];
    float spanMarginFloat = [spanMargin floatValue];
    float totalFandOMargin = exposureFloat + spanMarginFloat;
    NSString * fandoMargin = [NSString stringWithFormat:@"%.2f",totalFandOMargin];
    self.drAccountCashLabel.text = fandoMargin;
    
    NSString * usedToday = [NSString stringWithFormat:@"%.2f",[[[delegate1.mtFundsArray objectAtIndex:0]objectForKey:@"dbExposureMargin"]floatValue]];
    self.drUsedTodayLabel.text = @"--";
    
    Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
    
    [mixpanel1 identify:delegate1.userID];
    
    
    
    
    [mixpanel1.people set:@{@"TOTAL_ACCOUNT_CASH":totalAvailableCash}];
    [mixpanel1.people set:@{@"MARGIN_USED":cashUsedToday}];
    [mixpanel1.people set:@{@"AMOUNT_ADDED_TODAY":amountToday}];
    [mixpanel1.people set:@{@"NET_CASH_AVAILABLE":netCash}];
}

-(void)upStoxSever
{
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate1.accessToken];
    NSDictionary *headers = @{ @"x-api-key": delegate1.APIKey,
                               @"authorization": access,
                               };
    NSString * urlStr=[NSString stringWithFormat:@"https://api.upstox.com/live/profile/balance"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.upstoxResponseDicitionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"upstox balance%@",self.upstoxResponseDicitionary);
                                                    }      
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        self.netCashAvailable.text=[NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"available_margin"]];
                                                        self.amountTodayLabel.text= [NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"payin_amount"]];
                                                        self.accontCashLabel.text=[NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"available_margin"]];
                                                        self.cashTodayLabel.text=[NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"used_margin"]];
                                                        
                                                        Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
                                                        
                                                        [mixpanel1 identify:delegate1.userID];
                                                        
                                                        
                                                        
                                                        
                                                        [mixpanel1.people set:@{@"TOTAL_ACCOUNT_CASH":self.accontCashLabel.text}];
                                                        [mixpanel1.people set:@{@"MARGIN_USED":self.cashTodayLabel.text}];
                                                        [mixpanel1.people set:@{@"AMOUNT_ADDED_TODAY":self.amountTodayLabel.text}];
                                                        [mixpanel1.people set:@{@"NET_CASH_AVAILABLE":self.netCashAvailable.text}];
                                                    });
                                                }];
    [dataTask resume];
}

-(void)zerodhaFundsServer
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               };
    NSString * urlStr=[NSString stringWithFormat:@"https://api.kite.trade/user/margins/equity?api_key=%@&access_token=%@",delegate1.APIKey,delegate1.accessToken];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.zerodhaResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        self.netCashAvailable.text=[NSString stringWithFormat:@"%@",[[self.zerodhaResponseDictionary objectForKey:@"data"] objectForKey:@"net"]];
                                                        self.amountTodayLabel.text= [NSString stringWithFormat:@"%@",[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"available"]objectForKey:@"intraday_payin"]];
                                                        self.accontCashLabel.text=[NSString stringWithFormat:@"%@",[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"available"]objectForKey:@"cash"]];
                                                        self.cashTodayLabel.text=[NSString stringWithFormat:@"%@",[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"utilised"]objectForKey:@"debits"]];
                                                        
                                                        Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
                                                        
                                                        [mixpanel1 identify:delegate1.userID];
                                                        
                                                      
                                        
                                                        
                                                        [mixpanel1.people set:@{@"TOTAL_ACCOUNT_CASH":self.accontCashLabel.text}];
                                                        [mixpanel1.people set:@{@"MARGIN_USED":self.cashTodayLabel.text}];
                                                        [mixpanel1.people set:@{@"AMOUNT_ADDED_TODAY":self.amountTodayLabel.text}];
                                                        [mixpanel1.people set:@{@"NET_CASH_AVAILABLE":self.netCashAvailable.text}];
                                                        
                                                        
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
}
-(void)onPayButtonTap
{
//    FundTransferView * payin = [self.storyboard instantiateViewControllerWithIdentifier:@"FundTransferView"];
//    //[self.navigationController pushViewController:payin animated:YES];
//    [self.navigationController presentViewController:payin animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
