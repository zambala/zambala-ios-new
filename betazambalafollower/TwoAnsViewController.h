//
//  TwoAnsViewController.h
//  2FAScreens
//
//  Created by Zenwise Technologies on 27/09/17.
//  Copyright © 2017 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwoAnsViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *brokerImageView;
@property (weak, nonatomic) IBOutlet UILabel *question1Label;
@property (weak, nonatomic) IBOutlet UITextField *ans1TF;
@property (weak, nonatomic) IBOutlet UILabel *question2Label;
@property (weak, nonatomic) IBOutlet UITextField *ans2TF;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;

@end
