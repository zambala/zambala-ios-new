//
//  KiteWebView.h
//  testing
//
//  Created by zenwise technologies on 24/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KiteWebView : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *kiteWebView;

@end
