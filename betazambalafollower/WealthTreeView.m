//
//  WealthTreeView.m
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "WealthTreeView.h"
#import "AppDelegate.h"
#import "HMSegmentedControl.h"



@interface WealthTreeView ()
{
    
    
    AppDelegate * delegate1;
    NSString * referralCode;
    HMSegmentedControl * segmentedControl;
}
@end

@implementation WealthTreeView

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.downView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.downView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.downView.layer.shadowOpacity = 1.0f;
    self.downView.layer.shadowRadius = 1.0f;
    self.downView.layer.cornerRadius=1.0f;
    self.downView.layer.masksToBounds = NO;
    
    self.inviteFriendsButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.inviteFriendsButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.inviteFriendsButton.layer.shadowOpacity = 1.0f;
    self.inviteFriendsButton.layer.shadowRadius = 1.0f;
    self.inviteFriendsButton.layer.cornerRadius=1.0f;
    self.inviteFriendsButton.layer.masksToBounds = NO;
    
    
    
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"REFERRAL",@"ACHIEVEMENTS",@"REWARDS"]];
    segmentedControl.frame = CGRectMake(0, 88, self.view.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    [segmentedControl setSelectedSegmentIndex:0];
    [self getRefeeralCode];
    // Do any additional setup after loading the view.
   
}

-(void)segmentedControlChangedValue
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        self.rewardView.hidden=YES;
        self.referralView.hidden=NO;
    }
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        self.rewardView.hidden=NO;
        self.referralView.hidden=YES;
    }
        
}

-(void)getRefeeralCode
{

    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               };
    
    NSString * urlString=[NSString stringWithFormat:@"%@clientreferral/code?clientid=%@",delegate1.baseUrl,delegate1.userID];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                           NSMutableDictionary *referralDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        referralCode=[NSString stringWithFormat:@"%@",[referralDict objectForKey:@"referralcode"]];
                                                               
                                                        
                                                        
                                                    }
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.referralCodeLbl.text=referralCode;
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
    
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)shareBtnAction:(id)sender {
    
    if(referralCode.length!=0)
    {
    
    NSString *textToShare =[NSString stringWithFormat:@"I would recommend you to download ZAMBALA.Use my referral code %@ and get zambala credits",referralCode];
        
        NSURL *myWebsite = [NSURL URLWithString:@"https://itunes.apple.com/us/app/zambala-stocks-by-zenwise/id1281356740?ls=1&mt=8"];
   // NSURL *myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"https://i06-6.tlnk.io/serve?action=click&publisher_id=358381&site_id=137664&my_publisher=%@&my_keyword=%@",delegate1.brokerNameStr,referralCode]];
        UIImage * image=[UIImage imageNamed:@"success"];
    NSArray *objectsToShare = @[textToShare,myWebsite,image];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
        
    }
}

- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
