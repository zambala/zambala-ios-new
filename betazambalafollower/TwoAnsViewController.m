//
//  TwoAnsViewController.m
//  2FAScreens
//
//  Created by Zenwise Technologies on 27/09/17.
//  Copyright © 2017 Zenwise Technologies. All rights reserved.
//

#import "TwoAnsViewController.h"
#import "AppDelegate.h"
#import "TagEncode.h"
#import "MT.h"
#import "TabBar.h"

@interface TwoAnsViewController ()
{
    AppDelegate * delegate1;
    MT * sharedManager;
    NSDictionary * loginReplyDict;
    
}

@end

@implementation TwoAnsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ans1TF.delegate=self;
    self.ans2TF.delegate=self;
    sharedManager=[MT Mt1];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [self.loginButton addTarget:self action:@selector(SecurityQuestions) forControlEvents:UIControlEventTouchUpInside];
    
    self.activityInd.hidden=YES;
    
    for(int i=0;i<delegate1.questionArray.count;i++)
    {
        if(i==0)
        {
        if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:1]])
        {
            self.question1Label.text=@"What was your favourite place to visit as a child?";
            
        }
        
        else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:2]])
        {
            self.question1Label.text=@"What is the name of your favourite pet?";
            
        }
            
        else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:3]])
        {
            self.question1Label.text=@"Which is your favourite color?";
            
        }
            
        else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:4]])
        {
            self.question1Label.text=@"Which is your favourite web browser?";
            
        }
            
        else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:5]])
        {
            self.question1Label.text=@"In which city were you born?";
            
        }
            
        else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:6]])
        {
            self.question1Label.text=@"Which is your favourite movie?";
            
        }
            
            
        }
            
            if(i==1)
            {
                if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:1]])
                {
                    self.question2Label.text=@"What was your favourite place to visit as a child?";
                    
                }
                
                else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:2]])
                {
                    self.question2Label.text=@"What is the name of your favourite pet?";
                    
                }
                
                else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:3]])
                {
                    self.question2Label.text=@"Which is your favourite color?";
                    
                }
                
                else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:4]])
                {
                    self.question2Label.text=@"Which is your favourite web browser?";
                    
                }
                
                else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:5]])
                {
                    self.question2Label.text=@"In which city were you born?";
                    
                }
                
                else if([[delegate1.questionArray objectAtIndex:i] isEqual:[NSNumber numberWithInteger:6]])
                {
                    self.question2Label.text=@"Which is your favourite movie?";
                    
                }
                
            
        }
        
        
    }
    
    // Do any additional setup after loading the view.
}
    


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.ans1TF resignFirstResponder];
    [self.ans2TF resignFirstResponder];
   
    return true;
}
-(void)SecurityQuestions
{
    if(self.ans1TF.text.length!=0&&self.ans2TF.text.length!=0)
    {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    NSMutableArray * answersArray = [[NSMutableArray alloc]initWithObjects:self.ans1TF.text,self.ans2TF.text,nil];
    NSString * ansString = [answersArray componentsJoinedByString:@","];
    NSLog(@"ansString:%@",ansString);
    //PreLogin
    delegate1.mtCheck=true;
    TagEncode * tag1=[[TagEncode alloc]init];

    [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.SecurityQueAndAns];
    [tag1 TagData:sharedManager.stUserID stringMethod:delegate1.mtClientId];
    [tag1 TagData:sharedManager.stPassword stringMethod:delegate1.mtPassword];
    [tag1 TagData:sharedManager.stNewPassword stringMethod:@""];
    [tag1 TagData:sharedManager.inVersion intMethod:2];



    for (int i=0; i<1; i++) {
        NSString * ansString1 = [NSString stringWithFormat:@"%@",[answersArray objectAtIndex:i]];
        [tag1 TagData:sharedManager.stSingleSecurityAnswer stringMethod:ansString1];
        [tag1 TagData:sharedManager.nlTagFooter];

    }

    [tag1 GetBuffer];
    [self newMessage];
    }
        
        //new//
        
//        TagEncode * enocde=[[TagEncode alloc]init];
//        loginReplyDict=[[NSDictionary alloc]init];
//        enocde.inputRequestString=@"prelogin";
//        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
//
//        inputArray=answersArray;
//        [inputArray addObject:[NSString stringWithFormat:@"%@multitradeapi/prelogin",delegate1.baseUrl]];
//
//        [enocde inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
//
//            loginReplyDict=dict;
//
//            NSString * msg=[[[loginReplyDict objectForKey:@"data"] objectForKey:@"questions"]objectForKey:@"msg"];
//
//             if([msg containsString:@"Login Successful"])
//             {
//                 TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
//
//                 [self presentViewController:tabPage animated:YES completion:nil];
//             }
//
//        }];
//
//    }
//    else
//    {
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter missing fields" preferredStyle:UIAlertControllerStyleAlert];
//
//        [self presentViewController:alert animated:YES completion:^{
//
//        }];
//
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
//
//        [alert addAction:okAction];
//    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)newMessage
{
    //    delegate1.outputStream = (__bridge NSOutputStream *)writeStream;
    //
    //    [delegate1.outputStream setDelegate:self];
    //
    //    [delegate1.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    //
    //
    //    [delegate1.outputStream open];
    //
    
    Byte UUID[delegate1.btOutBuffer.count];
    
    for (int i = 0; i < delegate1.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate1.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp;
        [scanner scanHexInt:&temp];
        NSLog(@"%d",temp);
        
        UUID[i]=temp;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate1.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate1.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    [delegate1.outputStream write:bytes maxLength:delegate1.btOutBuffer.count];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
